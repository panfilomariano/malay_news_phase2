
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ECSlidingViewController
#define COCOAPODS_POD_AVAILABLE_ECSlidingViewController
#define COCOAPODS_VERSION_MAJOR_ECSlidingViewController 2
#define COCOAPODS_VERSION_MINOR_ECSlidingViewController 0
#define COCOAPODS_VERSION_PATCH_ECSlidingViewController 3

// Google-Mobile-Ads-SDK
#define COCOAPODS_POD_AVAILABLE_Google_Mobile_Ads_SDK
#define COCOAPODS_VERSION_MAJOR_Google_Mobile_Ads_SDK 7
#define COCOAPODS_VERSION_MINOR_Google_Mobile_Ads_SDK 3
#define COCOAPODS_VERSION_PATCH_Google_Mobile_Ads_SDK 1

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 1

// Nimbus/AttributedLabel
#define COCOAPODS_POD_AVAILABLE_Nimbus_AttributedLabel
#define COCOAPODS_VERSION_MAJOR_Nimbus_AttributedLabel 1
#define COCOAPODS_VERSION_MINOR_Nimbus_AttributedLabel 2
#define COCOAPODS_VERSION_PATCH_Nimbus_AttributedLabel 0

// Nimbus/Core
#define COCOAPODS_POD_AVAILABLE_Nimbus_Core
#define COCOAPODS_VERSION_MAJOR_Nimbus_Core 1
#define COCOAPODS_VERSION_MINOR_Nimbus_Core 2
#define COCOAPODS_VERSION_PATCH_Nimbus_Core 0

// QPSplitViewController
#define COCOAPODS_POD_AVAILABLE_QPSplitViewController
#define COCOAPODS_VERSION_MAJOR_QPSplitViewController 0
#define COCOAPODS_VERSION_MINOR_QPSplitViewController 0
#define COCOAPODS_VERSION_PATCH_QPSplitViewController 1

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 7
#define COCOAPODS_VERSION_PATCH_SDWebImage 2

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 7
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 2

// STTwitter
#define COCOAPODS_POD_AVAILABLE_STTwitter
#define COCOAPODS_VERSION_MAJOR_STTwitter 0
#define COCOAPODS_VERSION_MINOR_STTwitter 2
#define COCOAPODS_VERSION_PATCH_STTwitter 1

