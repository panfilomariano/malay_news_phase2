//
//  TMWebViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/13/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Article.h"

@interface TMWebViewController : UIViewController

@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) NSString *htmlString;
@property (nonatomic, strong) NSString *sectionUrl;
@property (nonatomic, assign) BOOL forPushNotif;
@property (nonatomic) BOOL isFromSpecialReport;

@end
