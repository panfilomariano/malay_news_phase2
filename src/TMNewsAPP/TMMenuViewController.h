//
//  TMMenuViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/8/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMMenuViewController : UIViewController
@property (nonatomic) BOOL isSubmenu;
- (IBAction)unwindToMenuViewController:(UIStoryboardSegue *)segue;
@end
