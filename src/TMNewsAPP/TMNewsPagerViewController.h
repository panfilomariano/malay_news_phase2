//
//  TMNewsPagerViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/5/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPagerController.h"
#import "Article.h"

typedef enum {
    newsType,
    caType,
} ViewType;

typedef enum
{
    OthersNewsPagerScreenType,
    LatestNewsNewsPagerScreenType,
    CANewsPagerScreenType,
    SpecialReportNewsPagerScreenType,
    NewsNewsPagerScreenType,
    MenuNewsPagerScreenType
} NewsPagerScreenType;

@interface TMNewsPagerViewController : ViewPagerController

@property (nonatomic, strong) NSString *pagerTittle;
@property (nonatomic, strong) NSArray *newsPages;
@property (nonatomic, strong) NSString *feedLink;
@property (nonatomic, strong) NSString *sectionTitleForCategory;
@property (nonatomic, strong) NSMutableArray *sectionContent;
@property (nonatomic, strong) NSString *enTitle;
@property (nonatomic, strong) Article *eksklusifArticle;
@property (nonatomic, strong) NSString *eklusifEntitle;
@property (nonatomic, assign) NewsPagerScreenType screenType;
@property (nonatomic, assign) NSUInteger activeIndex;

@end
