//
//  TMSettingsiPadViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 2/18/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSettingsiPadViewController.h"
#import "TMWebViewController.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "QPSplitViewController.h"
#import "TMFontSizeTableViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "MBProgressHUD.h"
#import "Data.h"
#import "TMSubmitNewsViewController.h"
#import "TMDownloadViewController.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import "Util.h"

@interface TMSettingsiPadViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation TMSettingsiPadViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {
        self.title = @"Settings";
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Settings";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.barTintColor = [Util headerColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [[AnalyticManager sharedInstance] trackSettings];
    [self.qp_splitViewController setTitle:@"Settings"];
    
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor grayColor].CGColor;
        // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:19/255.0f green:125/255.0f blue:205/255.0f alpha:1];
    [cell setSelectedBackgroundView:bgColorView];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    UILabel *title = (UILabel*)[cell viewWithTag:1];
    title.highlightedTextColor = [UIColor whiteColor];
    UILabel *version = (UILabel*)[cell viewWithTag:3];
    version.highlightedTextColor = [UIColor whiteColor];
    UIImageView *image = (UIImageView *) [cell viewWithTag:2];
    if (indexPath.section == 0){
        switch (indexPath.row) {
            case 0: {
                title.text = [Constants SETTING_ABOUT_US];
                [image setImage:[UIImage imageNamed:@"icon_settings_aboutus"]];
                break;
            }case 1: {
                title.text = [Constants SETTING_CONTACT_US];
                [image setImage:[UIImage imageNamed:@"icon_settings_contactus"]];
                 break;
            }case 2: {
                title.text = [Constants SETTING_SUBMIT_NEWS];
                [image setImage:[UIImage imageNamed:@"icon_settings_submitnews"]];
                break;
            }case 3: {
                title.text = [Constants SETTINGS_DOWNLOAD_ALL];
                [image setImage:[UIImage imageNamed:@"icon_settings_downloadall"]];
                break;
            }case 4: {
                title.text = [Constants SETTING_REGULATIONS];
                [image setImage:[UIImage imageNamed:@"icon_settings_terms"]];
                break;
            }case 5: {
                title.text = [Constants SETTING_PRIVACY_POLICY];
                [image setImage:[UIImage imageNamed:@"icon_settings_privacy"]];
                break;
            }case 6: {
                //title.text = @"Glossary";
                //[image setImage:[UIImage imageNamed:@"icon_settings_glossary"]];
                break;

    }
        }
    }else if (indexPath.section == 1) {
            switch (indexPath.row){
                case 0: {
                    title.text = [Constants SETTING_FONT_SIZE];
                    [image setImage:[UIImage imageNamed:@"icon_settings_fontsize"]];
                    break;
                }
            }
    }else if (indexPath.section == 2){
        UITableViewCell *appVersionCell = [tableView dequeueReusableCellWithIdentifier:@"cell2"];
        version.text = [Constants SETTING_VERSION];
        UILabel *appVersion = (UILabel*)[appVersionCell.contentView viewWithTag:111];
        appVersion.text = [Util appVersion];
        return appVersionCell;
    }
    
    return cell;
}

#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        return 66.0f;
    }else {
        return 66.0f;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 6;
    }else if (section == 1) {
        return 1;
    }else if (section == 2){
        return 1;
    }else {
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0: {
                TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
                webViewController.urlString = Constants.URL_ABOUT;
                [self.qp_splitViewController setRightController:webViewController];
                break;
            }case 1: {
                if ([MFMailComposeViewController canSendMail]) {
                    [UINavigationBar appearance].barTintColor = [Util headerColor];
                    NSString *emailTitle = [Constants CONTACT_US_EMAIL_SUBJECT];
                    NSString *messageBody = [Constants CONTACT_US_EMAIL_MESSAGE];
                    NSArray *toRecipents = [NSArray arrayWithObject:[Constants CONTACT_US_EMAIL_TO]];
                    
                    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                    mc.mailComposeDelegate = self;
                    [mc setSubject:emailTitle];
                    [mc setMessageBody:messageBody isHTML:NO];
                    [mc setToRecipients:toRecipents];
                    mc.navigationBar.translucent = NO;
                    [mc.navigationBar setTintColor:[UIColor whiteColor]];
                    
                    [self presentViewController:mc animated:YES completion:^{
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                    }];
                    
                }else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                        message:@"Your device doesn't support the composer sheet"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles: nil];
                    [alertView show];
                }
                
                break;
            }case 2: {
                TMSubmitNewsViewController *submit = [self.storyboard instantiateViewControllerWithIdentifier:@"submitNewsViewController"];
                [self.qp_splitViewController setRightController:submit];
                break;
            }case 3: {
                TMDownloadViewController *download = [self.storyboard instantiateViewControllerWithIdentifier:@"downloadViewController"];
                [self.qp_splitViewController setRightController:download];
                break;
            }case 4: {
                TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
                webViewController.urlString = Constants.URL_REGULATION;
                [self.qp_splitViewController setRightController:webViewController];
                break;
            }case 5: {
                TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
                webViewController.urlString = Constants.URL_PRIVACY;
                [self.qp_splitViewController setRightController:webViewController];
                break;
            }case 6: {
                //TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
                //webViewController.urlString = Constants.URL_GLOSSARY;
                //webViewController.title = @"Glossary";
                //[self.qp_splitViewController setRightController:webViewController];
                break;
            }
        }
    }else if (indexPath.section == 1){
            switch (indexPath.row) {
                case 0: {
                    TMFontSizeTableViewController *fontSizeController = [self.storyboard instantiateViewControllerWithIdentifier:@"fontsize"];
                    [self.qp_splitViewController setRightController:fontSizeController];
                    break;
                    
                }default:
                    break;

            }
    }

    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 1){
        if (buttonIndex == 1){
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            
            NSString *emailTitle = [Constants SUBMIT_NEWS_EMAIL_SUBJECT];
            
            NSString *messageBody = [Constants SUBMIT_NEWS_EMAIL_MESSAGE];
            
            NSArray *toRecipents = [NSArray arrayWithObject:[Constants SUBMIT_NEWS_EMAIL_TO]];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTranslucent:NO];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            
            [self presentViewController:mc animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
            
        }else if (buttonIndex == 2){
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
            
            UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
            imagepicker.delegate = self;
            imagepicker.allowsEditing = NO;
            [imagepicker.navigationBar setTintColor:[UIColor whiteColor]];
            imagepicker.navigationBar.translucent = NO;
            imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagepicker.mediaTypes = [NSArray arrayWithObjects:
                                      (NSString *) kUTTypeMovie, (NSString *) kUTTypeImage,
                                      nil];
            //[[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
            
            [self presentViewController:imagepicker animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Alert"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
    } else if (actionSheet.tag == 2) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //hud.mode = MBProgressHUDModeAnnularDeterminate;
        hud.labelText = @"Downloading..";
        if (buttonIndex == 1){
            [[Data sharedInstance] downloadAllArticlesWithImages:NO andCompletionBlock:^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Download finished" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }];
        } else if (buttonIndex == 2){
            [[Data sharedInstance] downloadAllArticlesWithImages:YES andCompletionBlock:^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Download finished" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];

                [hud hide:YES];
            }];
        }
    }
    
}

#pragma mark Private Functions
- (void)submitNews {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Submit News"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Sumnit news", @"Submit news with photos/videos", nil];

    actionSheet.tag = 1;

    
    [actionSheet showInView:self.view];
}

- (void)download {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Download" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil];
    [actionSheet buttonTitleAtIndex:[actionSheet addButtonWithTitle:@"Download without images"]];
    [actionSheet buttonTitleAtIndex:[actionSheet addButtonWithTitle:@"Download with images"]];
    actionSheet.tag = 2;
    
    [actionSheet showInView:self.view];
}
#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
