//
//  TMCurrentAffairPagerViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/31/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMCurrentAffairPagerViewController.h"
#import "TMTableBeritaListViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Constants.h"
#import "TMWebViewController.h"
#import "TMMenuViewController.h"
#import "Util.h"
#import "TMPhotosViewController.h"
#import "TMVideosViewController.h"
#import "TMNavigationController.h"
#import "AnalyticManager.h"

@interface TMCurrentAffairPagerViewController ()<ViewPagerDataSource,ViewPagerDelegate, TMTableBeritaListViewControllerDelegate>
@end

@implementation TMCurrentAffairPagerViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *entiTle = self.enTitle;
    //if (!self.isFromMenu){
        [[AnalyticManager sharedInstance] trackCAProgrammeContentListingWithProgram:entiTle];
    //}
        
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataSource = self;
    self.delegate = self;
    self.activeTabIndex = self.selectedIndex;

    if(self.isFromMenu){
        float width = [UIScreen mainScreen].bounds.size.width;
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        //[self.navigationController.navigationBar setFrame:CGRectMake(0, 8, width, 36)];
    }
    
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark ViewPager Datasource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    
    if(self.isTopNews){
        return 1;
    }else{
        return [self.newsCategories count];
    }
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    NSDictionary *dictionary = (NSDictionary *)[self.newsCategories objectAtIndex:index];
    NSString *type = [dictionary valueForKey:@"type"];
    if ([type isEqualToString:@"news"] ||[type isEqualToString:@"special report"]) {
        self.title = [Constants NEWS_LISTING_TITTLE];
    }
    
    UILabel *label = [UILabel new];
    NSString *uperCaseLabel = [NSString new];
    label.textAlignment = NSTextAlignmentCenter;
    if([dictionary valueForKey:@"title"]){
        uperCaseLabel = [[dictionary valueForKey:@"title"] uppercaseString];
        label.text = uperCaseLabel;
    }else{
        uperCaseLabel = [[dictionary valueForKey:@"category"] uppercaseString];
        label.text = uperCaseLabel;
    }
    
    [label sizeToFit];
    [label setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [label setTextColor:[UIColor colorWithRed:180.0f/255.0f green:200.0f/255.0f blue:241.0f/255.0f alpha:1]];
    return  label;
    
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
    
    NSDictionary *dictionary = (NSDictionary *)[self.newsCategories objectAtIndex:index];
    NSString *tittle = @"";
    
    if([dictionary valueForKey:@"title"]){
        tittle = [dictionary valueForKey:@"enTitle"];
    }else{
        tittle = [dictionary valueForKey:@"enCategory"];
    }
    
    //[[AnalyticManager sharedInstance] trackCAProgrammeContentListingWithProgram:self.enTitle];
    NSString *sectionType = [dictionary valueForKey:@"sectionType"];
    NSString *type = [dictionary valueForKey:@"type"];
    
    if ([type isEqualToString:[Constants HTML_TYPE]]|| [sectionType isEqualToString:[Constants HTML_TYPE]]) {
        [[AnalyticManager sharedInstance] trackCAAboutScreenWithCategory:tittle andProgram:self.enTitle];
    }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
        [[AnalyticManager sharedInstance] trackCAPhotoListingWithProgram:self.enTitle];
    }
    
    
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index{
    NSMutableDictionary *dictionary = (NSMutableDictionary *)[self.newsCategories objectAtIndex:index];

    NSString *tittle = @"";
    if([dictionary valueForKey:@"entTitle"]){
        tittle = [dictionary valueForKey:@"entTitle"];
        
    }else{
        tittle = [dictionary valueForKey:@"enCategory"];
    }
    
    NSString *sectionType = [dictionary valueForKey:@"sectionType"];
    NSString *type = [dictionary valueForKey:@"type"];
    
    if ([type isEqualToString:[Constants HTML_TYPE]]|| [sectionType isEqualToString:[Constants HTML_TYPE]]) {
        TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
        webViewController.sectionUrl = [dictionary valueForKey:@"sectionURL"];
        return webViewController;
        
    }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
        TMPhotosViewController *others = (TMPhotosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoViewController"];
        others.sectionURL = [dictionary valueForKey:@"sectionURL"];
        others.isFromMain = NO;
        return others;
    }else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]){
        TMVideosViewController *videos = (TMVideosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videosViewController"];
        videos.sectionURL = [dictionary valueForKey:@"sectionURL"];
        videos.isFromMain = NO;
        return videos;
        
    }else{
        TMTableBeritaListViewController *tableVC = (TMTableBeritaListViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"tableBeritaID"];
        tableVC.dictionary = dictionary;
        tableVC.tabIndex = index;
        tableVC.feedLink = self.feedLink;
        tableVC.delegate = self;
        tableVC.screenType = CABeritaType;
        tableVC.hasLiveStream = YES;
        return tableVC;
    }
    
    
}
- (NSNumber *)tabHeight {
    return [NSNumber numberWithFloat:29.0f];
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value{
    
    switch (option) {
            
        case ViewPagerOptionTabOffset: {
           
                return 20.0f;
           
            
        }
    }
    
    return value;
}

#pragma mark ViewPager Delegate
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerTabsView:
            return [UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}

#pragma mark TMTableBeritaListViewControllerDelegate
-(void)loadViewedItems:(NSString *)UrlString
{
    if (self.currentAffairViewControllerdelegate && [self.currentAffairViewControllerdelegate respondsToSelector: @selector(loadViewedItems:atIndex:)]) {
        [self.currentAffairViewControllerdelegate performSelector:@selector(loadViewedItems:atIndex:) withObject:UrlString withObject:[NSNumber numberWithInteger:self.activeTabIndex ]];
    }
    
    
}
#pragma mark Private function
- (IBAction)didTapMenu:(id)sender {
    
    // UINavigationController *nav = (UINavigationController *) self.slidingViewController.underLeftViewController;
    TMNavigationController *nav = (TMNavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"menuNavigationStoryboardId"];
    // TMMenuViewController *dbvc = (TMMenuViewController *)[nav topViewController];
    // dbvc.isSubmenu = NO;
    [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setBarTintColor: [Util headerColor]];
    [self.navigationController presentViewController:nav animated:NO completion:nil];
    // [self.slidingViewController anchorTopViewToRightAnimated:NO];
}

@end
