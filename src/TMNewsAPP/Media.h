//
//  Media.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/12/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Media : NSObject

@property (nonatomic, strong) NSMutableArray *mediaGroup;
@property (nonatomic, strong) NSString *tittle;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, strong) NSString *sectionUrl;
@property (nonatomic, strong) NSString *feedlink;
@property (nonatomic, strong) NSNumber *downloadTime;
@property (nonatomic, strong) NSString *brief;

@end
