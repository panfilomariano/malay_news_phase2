//
//  KNXAdInterstitialDelegate.h
//  TheNationMobile
//
//  Created by Le Vu on 10/12/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import "MobileBDAdInterstitial.h"

@class MobileBDAdInterstitial;
@class MobileBDError;

// A delegate for MRAIDInterstitial to handle callbacks for the interstitial lifecycle.
@protocol MobileBDAdInterstitialDelegate <NSObject>

@required
- (void)onMobileBDAdInterstitialReady:(MobileBDAdInterstitial *)interstitial;
- (void)onMobileBDAdInterstitialFailed:(MobileBDAdInterstitial *)interstitial
                             withError:(MobileBDError *)error;
- (void)onMobileBDAdInterstitialWillShow:(MobileBDAdInterstitial *)interstitial;
- (void)onMobileBDAdInterstitialDidHide:(MobileBDAdInterstitial *)interstitial;

@end