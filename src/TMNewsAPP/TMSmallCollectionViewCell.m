//
//  TMSmallCollectionViewCell.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/30/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSmallCollectionViewCell.h"

@implementation TMSmallCollectionViewCell

-(IBAction)didTapBookmarkButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}

@end
