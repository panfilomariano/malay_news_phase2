//
//  TMNewsPagesWebTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/19/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMNewsPagesWebTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIWebView *webview;

@end
