//
//  Weather.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/23/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "Weather.h"

@implementation Weather

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super init]))
    {
        self.period = [decoder decodeObjectForKey:@"period"];
        self.min = [decoder decodeObjectForKey:@"min"];
        self.max = [decoder decodeObjectForKey:@"max"];
        self.rh = [decoder decodeObjectForKey:@"rh"];
        self.psi = [decoder decodeObjectForKey:@"psi"];
        self.psi_3h = [decoder decodeObjectForKey:@"psi_3h"];
        self.psi_24h = [decoder decodeObjectForKey:@"psi_24h"];
        self.tide = [decoder decodeObjectForKey:@"tide"];
        self.date = [decoder decodeObjectForKey:@"date"];
        self.condition = [decoder decodeObjectForKey:@"condition"];
        self.thumbnail = [decoder decodeObjectForKey:@"thumbnail"];
        self.time = [decoder decodeObjectForKey:@"psi_time"];
        self.lastBuildDate = [decoder decodeObjectForKey:@"lastBuildDate"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.period forKey:@"period"];
    [encoder encodeObject:self.min forKey:@"min"];
    [encoder encodeObject:self.max forKey:@"max"];
    [encoder encodeObject:self.rh forKey:@"rh"];
    [encoder encodeObject:self.psi forKey:@"psi"];
    [encoder encodeObject:self.psi_3h forKey:@"psi_3h"];
    [encoder encodeObject:self.psi_24h forKey:@"psi_24h"];
    [encoder encodeObject:self.tide forKey:@"tide"];
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.condition forKey:@"condition"];
    [encoder encodeObject:self.thumbnail forKey:@"thumbnail"];
    [encoder encodeObject:self.time forKey:@"psi_time"];
    [encoder encodeObject:self.lastBuildDate forKey:@"lastBuildDate"];
    
}


@end
