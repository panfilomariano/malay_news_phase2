//
//  TMSearchViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSearchViewController : UIViewController
@property (nonatomic, weak) IBOutlet UILabel *noresultsLabel;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *noresultsHeight;
@property (nonatomic, weak) IBOutlet UILabel *searchResultsLabel;


@end
