
//
//  TMCurrentAffairViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMCurrentAffairViewController.h"
#import "SwipeView.h"
#import "CurrentAffair.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "TMEpisodeTableViewCell.h"

#import "UIView+Toast.h"
#import "NSDate+Helpers.h"
#import <MediaPlayer/MediaPlayer.h>
#import "TMNewsPagesViewController.h"
#import "TMNewsDetailsViewController.h"
#import "MBProgressHUD.h"
#import "DataManager.h"
#import "LibraryAPI.h"
#import "TMDetailedBeritaViewController.h"
#import "NSString+Helpers.h"
#import "TMCurrentAffairTableViewCell.h"
#import "MBProgressHUD.h"
#import "UIImageView+Helpers.h"
#import "AsyncImageView.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "TMCurrentAffairPagerViewController.h"
#import "TMMoviePlayerViewController.h"

@interface TMCurrentAffairViewController () <SwipeViewDelegate, SwipeViewDataSource, UITableViewDelegate, UITableViewDataSource, LatestEpisodesCellDelegate, GADBannerViewDelegate, GADAdSizeDelegate>
@property (nonatomic, strong) NSMutableArray *currentAffairArray;
@property (nonatomic, strong) NSMutableArray *episodeArray;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrayEpisodes;
@property (nonatomic, strong) TMMoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) TMCurrentAffairTableViewCell *currentAffairCell;
@property (nonatomic, strong) SwipeView *swipeView;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic) BOOL isWaitingForArticles;


@end

@implementation TMCurrentAffairViewController

static dispatch_group_t group;

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    self.currentAffairArray = [[NSMutableArray alloc] init];
    self.episodeArray = [[NSMutableArray alloc] init];
    self.bookmarks = [[NSMutableArray alloc]init];
    self.isWaitingForArticles = FALSE;

    return self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadBookmarkedItems];
    [[AnalyticManager sharedInstance] trackCACategoryTab];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:self action:@selector(reloadTableData:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshFeed];
    
    group = dispatch_group_create();
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_group_enter(group);
    [self requestContentsWithForce:NO];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{ 
        
        [MBProgressHUD hideHUDForView:self.view animated:NO];
    });
    [self setUpads];
}

#pragma mark swipeview datasource


- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView {

    return [self.currentAffairArray count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    AsyncImageView *asyncImageView = nil;
    
    float width = [UIScreen mainScreen].bounds.size.width-13;
    float aheight = 9;
    float awidth = 16;
    //UIImageView *imageView = nil;
    
    if(view == nil){
        view = [[UIView alloc] initWithFrame:swipeView.bounds];
        asyncImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, width-1, aheight/awidth * width-1)];
        [asyncImageView setBackgroundColor:[UIColor redColor]];
        asyncImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        asyncImageView.tag = 1;
        [asyncImageView sizeToFit];
        [view addSubview:asyncImageView];
        
    } else{
        
        asyncImageView = (AsyncImageView *) [view viewWithTag:1];
    }
    
    Article *article = (Article *)[self.currentAffairArray objectAtIndex:index];
    [asyncImageView.imageView sd_setImageWithURL:[NSURL URLWithString:article.thumbnail] placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:index == 0 ? SDWebImageRefreshCached:0 ];
    
    return view;


}

#pragma  mark swipeView delegate

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index {
    Article *article = [self.currentAffairArray objectAtIndex:index];
    NSString *stringUrl = article.sectionURL;
    NSString *title = article.tittle;
    
    if (self.isWaitingForArticles) return;
    self.isWaitingForArticles = TRUE;
    [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:article.feedlink  isForced:NO withCompletionBlock:^(NSMutableArray *array){
        if ([array count]) {    
            Article *newarticle = [array firstObject];
            NSArray *sectionsArray = newarticle.sections;
            TMCurrentAffairPagerViewController *dbvc = (TMCurrentAffairPagerViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"currentAffairController"];
            dbvc.newsCategories = sectionsArray;
            dbvc.enTitle = newarticle.entittle;
            dbvc.selectedIndex = 0;
            dbvc.title = title;
            [self.navigationController pushViewController:dbvc animated:YES];
        }
        self.isWaitingForArticles = FALSE;
    }];
}



#pragma mark swipeView methods

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    float width = [UIScreen mainScreen].bounds.size.width;
    float aheight = 9;
    float awidth = 16;
    return CGSizeMake(width-30, (aheight/awidth * (width-30)));
    //return self.currentAffairCell.swipeView.bounds.size;
    
    
}


#pragma mark tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return 1;
    }else if (section == 1){
        return 1;
    }else{
          return [self.episodeArray count];
    }
  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        TMCurrentAffairTableViewCell *cell = (TMCurrentAffairTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"svCell" forIndexPath:indexPath];
        if (!self.swipeView) {
            self.swipeView = cell.swipeView;
            
            self.swipeView.pagingEnabled = YES;
            [self.swipeView setCurrentItemIndex:0];
            //self.swipeView.verticalAdjust = -25.0f;
            self.swipeView.truncateFinalPage = YES;
            self.swipeView.wrapEnabled = NO;
            self.swipeView.itemsPerPage = 1;
            self.swipeView.dataSource = self;
            self.swipeView.delegate = self;
            [self.swipeView reloadData];
        
        }

        return cell;
    } else if (indexPath.section == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"staticCell" forIndexPath:indexPath];
        UILabel *label = (UILabel *)[cell viewWithTag:1];
        label.text = [Constants LATEST_EPISODE];
        return cell;
    } else {
        Article *article = (Article *)[self.episodeArray objectAtIndex:indexPath.row];
        TMEpisodeTableViewCell *cell = (TMEpisodeTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.indexPath = indexPath;
        cell.categoryLabel.text = article.category;
        cell.pubDateLabel.text = [article.pubDate formatDate];
        cell.titleLabel.text = article.tittle;
        
        [cell.thumbnail.layer setBorderWidth:1.0];
        [cell.thumbnail.layer setBorderColor:[[UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1] CGColor]];
        
        NSString *type = @"";
        NSString *thumbnail = @"";
        if (article.mediagroupList) {
            NSDictionary *dictionary = [article.mediagroupList firstObject];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
            type = [dictionary valueForKey:@"type"];
            if([type isEqualToString:@"video"]){
                [cell.playOverlay setHidden:NO];
            }else {
                [cell.playOverlay setHidden:YES];
                
            }
        }
        
        [cell.contentView bringSubviewToFront:cell.playOverlay];
        [cell.thumbnail  sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                           placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:SDWebImageRefreshCached];
        
        [cell.bookmarkButton setSelected:NO];
        if(self.bookmarks.count){
            for (Article *bookmarkArticle in self.bookmarks){
                if ([bookmarkArticle.guid isEqualToString:article.guid]){
                    [cell.bookmarkButton setSelected:YES];
                    break;
                }
            }
        }
        //[cell.imageView setBorder];
        return cell;

    }
    
    
}


#pragma mark tableview delegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

        TMNewsDetailsViewController *detailednewsVC = (TMNewsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetailsID"];
        detailednewsVC.passedArray = self.episodeArray;
        detailednewsVC.index = indexPath.row;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        Article *article = (Article *)[self.episodeArray objectAtIndex:indexPath.row];
        if (_bookmarks.count) {
            for (Article *articleFromBookmark in _bookmarks) {
            if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                detailednewsVC.isBookmark = YES;
                break;
            }
        }
    }
    
    
        [self.navigationController pushViewController:detailednewsVC animated:YES];

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        
        float width = [UIScreen mainScreen].bounds.size.width;
        float aheight = 9;
        float awidth = 16;
        return (aheight/awidth * width)-6;
    }else if (indexPath.section == 1){
        return 34.0f;
    }else{
        return 86.0f;
    }
    
    
}

#pragma mark LatestEpisodeDelegate

- (void) didTapBookMarkButtonAtIndexPath:(NSIndexPath *)indexPath {
    Article *article = (Article *) [self.episodeArray objectAtIndex:indexPath.row];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
        
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            [_bookmarks removeObject:bookmarkArticle];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
            [_bookmarks addObject:article];
        }
        
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [_bookmarks addObject:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
    }
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    if (!bookmarkArticle){
        [[AnalyticManager sharedInstance] trackCABookmarkWithProgram:article.enCategory andEpisodeId:article.guid andEpisodeTitle:article.tittle];
    }
    
    

}

#pragma  mark private function

-(void)requestContentsWithForce: (BOOL)forced
{
    dispatch_group_enter(group);
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        for(NSDictionary *dictionary in array){
            NSString *enTitle = [dictionary valueForKey:@"enTitle"];
            if ([enTitle isEqualToString:@"ca"]){
                [self getCurrentAffairWithForce:NO];
            }else if([enTitle isEqualToString:@"latest episodes"]){
                [self getEpisodeWithURL:[dictionary valueForKey:@"sectionURL"] withForce:NO];
                
            }
        }
        dispatch_group_leave(group);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)getEpisodeWithURL: (NSString *)urlString withForce: (BOOL) forced {
    dispatch_group_enter(group);
    [[LibraryAPI sharedInstance] getArticlesWithURL:urlString isForced:forced withCompletionBlock:^(NSMutableArray *array) {
        self.episodeArray = [array mutableCopy];
        [self loadBookmarkedItems];
        [self.tableView reloadData];
        
        dispatch_group_leave(group);
    }];
    
}

- (void)getCurrentAffairWithForce: (BOOL)forced {
    dispatch_group_enter(group);
    [[LibraryAPI sharedInstance] getCAWithForce:forced andCompletionBlock:^(NSMutableArray *array) {
        self.currentAffairArray = [array mutableCopy];
        if (self.swipeView)
            [self.swipeView scrollToItemAtIndex:0 duration:0.5];
        [self.swipeView reloadData];
        
        dispatch_group_leave(group);
    }];
}

-(void)loadBookmarkedItems
{
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.tableView reloadData];
        }
    }];
    
}

-(void)reloadTableData: (UIRefreshControl *)refreshFeed {
    
    if (refreshFeed) {
        [self requestContentsWithForce:YES];
        [self getCurrentAffairWithForce:YES];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        
        [refreshFeed endRefreshing];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    self.bannerView.adSize = kGADAdSizeBanner;
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       ]];*/
   // self.bannerView.adSizeDelegate = self;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:request];
    
    
}
#pragma mark GADbannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view{
    self.bannerHeight.constant = 50;
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
    
}
- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)size{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
