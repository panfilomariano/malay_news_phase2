//
//  TMVideoDescTableViewCell.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/14/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMVideoDescTableViewCell.h"

@implementation TMVideoDescTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark Public Function
+ (float) heightFromText:(NSString *)text
{/*
    float height = 17.0f;
    
    CGSize maximumLabelSize = CGSizeMake(250, FLT_MAX);
    CGRect expectedLabelSize = [text boundingRectWithSize:maximumLabelSize
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]}
                                                 context:nil];

    if (expectedLabelSize.size.height > 15.0f) {
        height += expectedLabelSize.size.height - 13.0f;
    }
    
    return height;
    */
    float width = [UIScreen mainScreen].bounds.size.width;
    float height;
    CGSize maximumLabelSize = CGSizeMake(width-36, FLT_MAX);
    UIFont *font =[UIFont systemFontOfSize:12];
    CGRect labelRect = [text  boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    height = labelRect.size.height;
    return height;
}
@end
