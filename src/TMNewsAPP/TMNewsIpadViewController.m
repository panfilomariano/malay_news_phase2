//
//  TMNewsIpadViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/2/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMNewsIpadViewController.h"
#import "TMNewsCollectionViewLayout.h"
#import "TMSquareCollectionViewCell.h"
#import "TMSmallCollectionViewCell.h"
#import "UIImageView+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "NSString+Helpers.h"
#import "TMExclusiveCollectionViewCell.h"
#import "Util.h"
#import "TMNewsPagerViewController.h"
#import "TMNewsArticleViewController.h"
#import "TMNewsDetailsPagerViewController.h"
#import "Constants.h"
#import "AnalyticManager.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"

#define SPECIAL_REPORT @"special report"

@interface TMNewsIpadViewController () <UICollectionViewDataSource, UICollectionViewDelegate, TMNewsCollectionViewLayoutDelegate, TMSquareCollectionViewCellDelagate, TMSmallCollectionViewCellDelagate,TMNewsDetailsPagerViewControllerDelagate, GADBannerViewDelegate, GADAdSizeDelegate>


@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) UINib *squareCell;
@property (nonatomic, strong) UINib *smallCell;
@property (nonatomic, strong) NSMutableArray *sections;
@property (nonatomic, strong) NSMutableArray *latestSectionFirst3Articles;
@property (nonatomic, strong) NSMutableArray *exclusiveArticles;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *latestArticlesInSections;
@property (nonatomic) BOOL isPortrait;
@property (nonatomic) BOOL hasPreviousContents;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@property (nonatomic) dispatch_group_t group;
@property (nonatomic, strong) DFPBannerView *tBannerView;
@property (nonatomic) BOOL isFromMenu;
@end

@implementation TMNewsIpadViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _sections = [[NSMutableArray alloc] init];
    _latestSectionFirst3Articles = [[NSMutableArray alloc] init];
    _exclusiveArticles = [[NSMutableArray alloc] init];
    _bookmarks = [[NSMutableArray alloc] init];
    
    self.squareCell = [UINib nibWithNibName:@"SquareCollectionViewCell" bundle:nil];
    self.smallCell = [UINib nibWithNibName:@"LongCollectionViewCell" bundle:nil];

    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AnalyticManager sharedInstance] trackNewsCategoryTab];
    
    TMNewsCollectionViewLayout* layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    [self loadLayout:layout forOrientation:self.interfaceOrientation];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation) == self.isPortrait) {
        [self loadData];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TMNewsCollectionViewLayout* layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    [self loadLayout:layout forOrientation:self.interfaceOrientation];
    
    self.bannerHeight.constant = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadData)
                                                 name:@"reloadDataNotif"
                                               object:nil];
    
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:self action:@selector(reloadTableData:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshFeed];
    
    [self.collectionView registerNib:self.squareCell  forCellWithReuseIdentifier:@"squareCell"];
    [self.collectionView registerNib:self.smallCell  forCellWithReuseIdentifier:@"smallCell"];
    
    [self setUpads];
    //[self loadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UICollectionView Datasource
- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f blue:237/255.0f alpha:0.5];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    int itemsPerSection = (self.isPortrait) ? 4 : 5;
    BOOL hasSpecial = [_sections.lastObject[@"type"] isEqualToString:SPECIAL_REPORT];
    if (hasSpecial) {
        return (_sections.count - 1) * itemsPerSection + (self.exclusiveArticles.count ? self.exclusiveArticles.count + 1 : 0) ;
    } else {
        return _sections.count * itemsPerSection;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //0 = square
    //1 = small
    //2 = header
    //3 = exclusive
    
    UICollectionViewCell *cell = nil;
    
    int itemsPerSection = (self.isPortrait) ? 4 : 5;
    int sectionIndex = (int)indexPath.row / itemsPerSection;
    int remainder = indexPath.row % itemsPerSection;
    BOOL isHeader = (sectionIndex < _sections.count && remainder == 0);
    BOOL isSpecialArticle = (!isHeader && [_sections.lastObject[@"type"] isEqualToString:SPECIAL_REPORT] && sectionIndex >= _sections.count - 1);
    
    int cellId = -1;
    if (isHeader) cellId = 2;
    else if (isSpecialArticle) cellId = 3;
    else if (remainder == 1 || (!self.isPortrait && remainder == 2)) cellId = 0;
    else cellId = 1;
    
    switch (cellId) {
        case 0: {
            TMSquareCollectionViewCell *squareCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"squareCell" forIndexPath:indexPath];
             [squareCell setHidden:YES];
            if (_latestSectionFirst3Articles.count) {
                int articleIndex = (sectionIndex * (itemsPerSection - 1)) + remainder;
                NSDictionary *sectionDictionary = (NSDictionary *) [_sections objectAtIndex:sectionIndex];
                NSString *enTittle = [sectionDictionary valueForKey:@"enTitle"];
                Article *article = (Article *) [_latestSectionFirst3Articles objectAtIndex:articleIndex - 1];
                if ([article.sectionName isEqualToString:enTittle]) {
                    //NSLog(@"SHOWED %@ === %@", enTittle, article.sectionName);
                    [squareCell setHidden:NO];
                    squareCell.delegate = self;
                    squareCell.indexPath = indexPath;
                    squareCell.tittle.text = article.tittle;
                    [squareCell.category setHidden:YES];
                    squareCell.pubdateSpace.constant = -5;
                    squareCell.pubDate.text = [article.pubDate formatDate];
                    
                    NSString *photoThumbnail = @"";
                    if (article.mediagroupList) {
                        if (article.mediagroupList.count) {
                            NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                            photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                            NSString *type = [mediaDictionary valueForKey:@"type"];
                            if ([type isEqualToString:@"video"]) {
                                [squareCell.playImage setHidden:NO];
                            } else {
                                [squareCell.playImage setHidden:YES];
                            }
                        }
                    }
                    
                    [squareCell.bookmarkButton setSelected:NO];
                    if (_bookmarks.count) {
                        for (Article *articleFromBookmark in _bookmarks) {
                            if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                                [squareCell.bookmarkButton setSelected:YES];
                                break;
                            }
                        }
                    }
                    
                    [squareCell.thumbnail setBorder];
                    [squareCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                            placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];

                } else {


                    //[squareCell setHidden:YES];
                    squareCell.backgroundColor = [UIColor clearColor];
                    squareCell.thumbnail.backgroundColor = [UIColor clearColor];

                    squareCell.delegate = nil;
                    squareCell.tittle.text = @"";
                    squareCell.category.text = @"";
                    squareCell.pubDate.text = @"";
                    [squareCell.playImage setHidden:YES];
                    [squareCell.bookmarkButton setHidden:YES];
                    [squareCell.thumbnail hideBorder];
                    [squareCell.thumbnail sd_setImageWithURL:nil];
                }
            }
            
            cell = squareCell;
            break;
        } case 1: {
            
            TMSmallCollectionViewCell *smallCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"smallCell" forIndexPath:indexPath];
            [smallCell setHidden:YES];
            [smallCell.category setFrame:CGRectZero];
            if (_latestSectionFirst3Articles.count) {
                int articleIndex = (sectionIndex * (itemsPerSection - 1)) + remainder;
                NSDictionary *sectionDictionary = (NSDictionary *) [_sections objectAtIndex:sectionIndex];
                NSString *enTittle = [sectionDictionary valueForKey:@"enTitle"];
                Article *article = (Article *) [_latestSectionFirst3Articles objectAtIndex:articleIndex - 1];
                if ([article.sectionName isEqualToString:enTittle]) {
                     [smallCell setHidden:NO];
                    NSLog(@"SHOWED %@ === %@", enTittle, article.sectionName);
                    smallCell.delegate = self;
                    smallCell.indexPath = indexPath;
                    smallCell.tittle.text = article.tittle;
                    smallCell.category.text = @"";
                    smallCell.pubDate.text = [article.pubDate formatDate];
                    
                    NSString *photoThumbnail = @"";
                    if (article.mediagroupList) {
                        if (article.mediagroupList.count) {
                            NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                            photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                            NSString *type = [mediaDictionary valueForKey:@"type"];
                            if ([type isEqualToString:@"video"]) {
                                [smallCell.playImage setHidden:NO];
                            } else {
                                [smallCell.playImage setHidden:YES];
                            }
                        }
                    }
                    
                    [smallCell.bookmarkButton setSelected:NO];
                    if (_bookmarks.count) {
                        for (Article *articleFromBookmark in _bookmarks) {
                            if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                                [smallCell.bookmarkButton setSelected:YES];
                                break;
                            }
                        }
                    }
                    
                    [smallCell.thumbnail setBorder];
                    [smallCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                           placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                } else {

                    //[smallCell setHidden:YES];
                    smallCell.backgroundColor = [UIColor clearColor];
                    smallCell.thumbnail.backgroundColor = [UIColor clearColor];
                    smallCell.delegate = nil;

                    smallCell.tittle.text = @"";
                    smallCell.category.text = @"";
                    smallCell.pubDate.text = @"";
                    [smallCell.playImage setHidden:YES];
                    [smallCell.bookmarkButton setHidden:YES];
                    [smallCell.thumbnail hideBorder];
                    [smallCell.thumbnail sd_setImageWithURL:nil];
                    
                }
            }
            
            cell = smallCell;

            break;
        } case 2: {
            UICollectionViewCell *headerCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"headerCell" forIndexPath:indexPath];
            if (_sections.count) {
                NSDictionary *sectionDictionary =  [_sections objectAtIndex:sectionIndex];
                NSString *title = [sectionDictionary valueForKey:@"title"];
                UILabel *label = (UILabel *) [headerCell viewWithTag:111];
                label.text = title;
            }
            cell = headerCell;
            break;
        } case 3: {
            TMExclusiveCollectionViewCell *exclusiveCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"exclusiveCell" forIndexPath:indexPath];
            if (_latestSectionFirst3Articles.count) {
                int articleIndex = ((sectionIndex - _sections.count + 1) * itemsPerSection) + remainder - 1;
                Article *article = (Article *) [_exclusiveArticles objectAtIndex:articleIndex];
                exclusiveCell.tittleLabel.text = article.tittle;
                exclusiveCell.descriptionLabel.text = article.brief;
                [exclusiveCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:article.thumbnail] placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            }
            cell = exclusiveCell;
            break;
        }
    }
    
    return  cell;
}

#pragma mark - TMNewsCollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    /*NSUInteger selectedSection = indexPath.row / 4;
    NSUInteger rowsPerSection = 4;
    if (!UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        selectedSection = indexPath.row / 5;
        rowsPerSection = 5;
    }*/
    
    int itemsPerSection = (self.isPortrait) ? 4 : 5;
    int sectionIndex = (int)indexPath.row / itemsPerSection;
    int remainder = indexPath.row % itemsPerSection;
    BOOL isHeader = (sectionIndex < _sections.count && remainder == 0);
    BOOL isSpecialArticle = (!isHeader && [_sections.lastObject[@"type"] isEqualToString:SPECIAL_REPORT] && sectionIndex >= _sections.count - 1);
    
    if (indexPath.row % itemsPerSection == 0 || indexPath.row == 0) {
        NSDictionary *dictionary = (NSDictionary *)[self.sections objectAtIndex:sectionIndex];
        TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
        newsPagerVC.pagerTittle = [Constants NEWS_LISTING_TITTLE];
        newsPagerVC.newsPages = self.sections;
        newsPagerVC.eklusifEntitle = dictionary[@"enTitle"];
        newsPagerVC.sectionContent = self.latestArticlesInSections;
        newsPagerVC.activeIndex = sectionIndex;
        newsPagerVC.screenType = NewsNewsPagerScreenType;
        
        if (sectionIndex == _sections.count - 1)
            newsPagerVC.screenType = SpecialReportNewsPagerScreenType;
        
        [self.navigationController pushViewController:newsPagerVC animated:YES];
        
    } else {
        
        int remainder = indexPath.row % 4;
        int sectionIndex = indexPath.row / 4;
        int articleIndex = (sectionIndex * 3) + remainder;
        
        if (!self.isPortrait) {
            remainder = indexPath.row % 5;
            sectionIndex = indexPath.row / 5;
            articleIndex = (sectionIndex * 4) + remainder;
        }
        
        Article *article = nil;
        BOOL isExclusive = NO;
        if (sectionIndex == _sections.count - 1) {//exclusive
            if (!self.isPortrait) {
                remainder = indexPath.row % 5;
            }
            article = (Article *) [_exclusiveArticles objectAtIndex:remainder - 1];
            isExclusive = YES;
        } else {
            article = (Article *) _latestSectionFirst3Articles[articleIndex - 1];
        }
        
        BOOL isEmptyArticle = NO;
        if (([article.tittle isEqualToString:@""] && [article.pubDate isEqualToString:@""])) {
            isEmptyArticle = YES;
        }
        
        if (!isEmptyArticle) {
            if (isExclusive) {
                
                TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                newsPagerVC.eksklusifArticle = article;
                newsPagerVC.pagerTittle = article.tittle;
                newsPagerVC.screenType = SpecialReportNewsPagerScreenType;
                [self.navigationController pushViewController:newsPagerVC animated:YES];
            } else {
                NSDictionary *dictionary = (NSDictionary *)[self.sections objectAtIndex:sectionIndex];
                NSString *sectionType = [dictionary valueForKey:@"sectionType"];
                NSString *type = [dictionary valueForKey:@"type"];
                NSString *urlString = [dictionary valueForKey:@"sectionURL"];
                if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]) {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [[LibraryAPI sharedInstance]getSectionArticlesFromURL:urlString withFeedUrl:article.feedlink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                        if (array.count) {
                            TMNewsDetailsPagerViewController *pager = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsPagerViewControllerId"];
                            //pager.customDelegate = self;
                            pager.newsArticles = array;
                            pager.bookmarks = [_bookmarks mutableCopy];
                            pager.activeTabIndex = remainder - 1;
                            pager.pagerTittle = @"";
                            
                            [self.navigationController pushViewController:pager animated:YES];
                        }
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    }];
                    
                } else {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    
                    [[LibraryAPI sharedInstance]getArticlesWithURL:urlString isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                        if (array.count) {
                            
                            int a_index = 0;
                            for (int i = 0; i < array.count; i++) {
                                Article *a = (Article *) array[i];
                                if ([a.guid isEqualToString:article.guid]) {
                                    a_index = i;
                                }
                            }
                            
                            TMNewsDetailsPagerViewController *pager = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsPagerViewControllerId"];
                            pager.customDelegate = self;
                            pager.newsArticles = array;
                            pager.bookmarks = [_bookmarks mutableCopy];
                            pager.activeTabIndex = a_index;
                            pager.pagerTittle = @"";
                            
                            [self.navigationController pushViewController:pager animated:YES];
                        }else{
                            int index = 0;
                            if(indexPath.row>0){
                                if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
                                    index = (int)indexPath.row/4;
                                }else{
                                    index = (int)indexPath.row/5;
                                }
                            }
                            
                            NSDictionary *sectionDictionary = _sections[index];
                            NSString *enTitle = [sectionDictionary valueForKey:@"enTitle"];
                            NSMutableArray *arrayToBePassed = [[NSMutableArray alloc]init];
                            for (Article *article in self.latestArticlesInSections) {
                                if ([enTitle isEqualToString:article.sectionName]) {
                                    [arrayToBePassed addObject:article];
                                }
                                
                            }
                            
                            TMNewsDetailsPagerViewController *pager = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsPagerViewControllerId"];
                            pager.customDelegate = self;
                            pager.newsArticles = arrayToBePassed;
                            pager.bookmarks = [_bookmarks mutableCopy];
                            pager.activeTabIndex = remainder - 1;
                            pager.pagerTittle = @"";
                            
                            [self.navigationController pushViewController:pager animated:YES];
                            
                        }
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    }];
                }
            }
        }
    }
}

#pragma mark - TMNewsCollectionViewLayoutDelegate

- (UIEdgeInsets) insetsForItemAtIndexPath:(NSIndexPath *)indexPath {
    return UIEdgeInsetsZero;
}

- (NSUInteger) layoutIndexAtIndexPath: (NSIndexPath *)indexPath
{
    //0 = square
    //1 = exclisive
    //2 = header
    
    if (self.isPortrait) {
        if (indexPath.row % 4 == 0 || indexPath.row == 0) {
            return 2;
        }
        
        int sectionIndex = indexPath.row / 4;
        if (sectionIndex == _sections.count - 1)
            return 1;
    } else {
        if (indexPath.row % 5 == 0 || indexPath.row == 0) {
            return 2;
        }
        
        int sectionIndex = indexPath.row / 5;
        if (sectionIndex == _sections.count - 1)
            return 1;
    }
    
    return 0;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark TMSquareCollectionViewCellDelagate | TMSmallCollectionViewCellDelagate

- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    //0 = square
    //1 = small
    //2 = header
    //3 = exclusive
    
    int sectionIndex = (int)indexPath.row / 4;
    int remainder = indexPath.row % 4;
    int articleIndex = (sectionIndex * 3) + remainder;
    if (!self.isPortrait) {
        sectionIndex = (int)indexPath.row / 5;
        remainder = indexPath.row % 5;
        articleIndex = (sectionIndex * 4) + remainder;
    }
    
    

    Article *article = (Article *) [_latestSectionFirst3Articles objectAtIndex:articleIndex - 1];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {

        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            [_bookmarks removeObject:bookmarkArticle];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
            [_bookmarks addObject:article];
        }
        
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
    }
    
    if (!bookmarkArticle){
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    }
    
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

#pragma mark Notif Observers

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    TMNewsCollectionViewLayout *layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    [self loadLayout:layout forOrientation:toInterfaceOrientation];

    [self loadData];
    [self setUpads];
}

#pragma mark Private Functions

- (BOOL)checkBookmarkWithArticle: (Article*)article
{
    BOOL isBookmark = NO;
    for (Article *articleFromBookmark in _bookmarks) {
        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
            isBookmark = YES;
            break;
        }
    }
    
    return isBookmark;
}

- (void) loadLayout:(TMNewsCollectionViewLayout *)layout forOrientation:(UIInterfaceOrientation) toInterfaceOrientation
{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        
        [layout addLayoutWithRowWidth:768.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 0.0f, 300.f, 250.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 0.0f, 441, 121)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 130, 441, 121)]]];//====news contents
        
        [layout addLayoutWithRowWidth:768.f rowHeight:178.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 0.0f, 750.0f, 169.f)]]];//====exclusive contents
        
        [layout addLayoutWithRowWidth:768.f rowHeight:48.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(0.f, 0.0f, 768.0f, 48.f)]]];//===header
        
        self.isPortrait = YES;
    } else {
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 0, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(318, 0, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(627, 0, 388, 121)],
                                                                        [NSValue valueWithCGRect:CGRectMake(627.f, 130, 388, 121)]
                                                                        ]];
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 0.0f, 1005.0f, 250.0f)]]];//====exclusive contents
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:48.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(0.f, 0.0f, 1024.f, 48.f)]]];//===header
        self.isPortrait = NO;
    }
}

- (void) loadData
{
    if (!self.group)
        self.group = dispatch_group_create();
    
    if (!self.hasPreviousContents)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self getSectionHeaders];
    [self getSpecialReportArticles];
    [self getBookmarkItems];
   
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{ // dispatch_group_notify just to make sure all request are all done
        // Do whatever you need to do when all requests are finished
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        self.hasPreviousContents = YES;
    });
}

- (void)getSectionHeaders
{
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getNewsWithCompletionBlock:^(NSMutableArray *array) {
        if(array && array.count){
            [array sortUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                if ([obj1[@"type"] isEqualToString:SPECIAL_REPORT] && ![obj2[@"type"] isEqualToString:SPECIAL_REPORT]) {
                    return NSOrderedDescending;
                }
                return NSOrderedSame;
            }];
            self.sections = array;
            [self getLatestSectionsNewsArticles];
        }
        dispatch_group_leave(self.group);
    }];
}

- (void)getLatestSectionsNewsArticles
{
    int itemsPerSection = 3;
    if (!self.isPortrait) {
        itemsPerSection = 4;
    }
    
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        NSMutableArray *emptySections = [NSMutableArray new];
        [self.latestSectionFirst3Articles removeAllObjects];
        NSMutableArray *tempArticleHolder = [NSMutableArray new];
        for (NSDictionary *feedItemDict in array) {
            if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:@"latest section news"]) {
                
                NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                dispatch_group_enter(self.group);
                [[LibraryAPI sharedInstance]getSectionArticlesFromURL:sectionURL isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                    if ([array count]) {
                        
                        self.latestArticlesInSections = [array mutableCopy];
                        int sectionIndexCounter = 0;
                        int itemCounter = 0;
                        while (sectionIndexCounter < _sections.count - 1) {
                            NSDictionary *sectionDictionary = _sections[sectionIndexCounter];
                            NSString *enTitle = [sectionDictionary valueForKey:@"enTitle"];
                            
                            for (Article *article in array) {
                                if (itemCounter < itemsPerSection) {
                                    if ([enTitle isEqualToString:article.sectionName]) {
                                        //NSLog(@"Added %@ === %@", enTitle, article.sectionName);
                                        [tempArticleHolder addObject:article];
                                        itemCounter++;
                                    }
                                } else {
                                    itemCounter = 0;
                                    sectionIndexCounter++;
                                    break;
                                }
                                
                                
                                if ([article isEqual:[array lastObject]] && itemCounter < itemsPerSection) {//add empty instance of article if category have < 3 articles
                                    
                                    if (itemCounter == 0) {
                                        [emptySections addObject:sectionDictionary];
                                        
                                    } else {
                                        for (int i = itemCounter; i < itemsPerSection; i++) {
                                            Article *empArticle = [Article new];
                                            empArticle.tittle = @"";
                                            empArticle.pubDate = @"";
                                            [tempArticleHolder addObject:empArticle];
                                        }
                                    }
                                    
                                    itemCounter = 0;
                                    sectionIndexCounter++;
                                }
                            }
                        }
                        
                        if (emptySections.count) {
                            for (NSDictionary *dictionary in emptySections) {
                                [_sections removeObject:dictionary];
                            }
                        }
                        self.latestSectionFirst3Articles = [tempArticleHolder mutableCopy];
                    }
                    
                    dispatch_group_leave(self.group);
                }];
            }
        }

        dispatch_group_leave(self.group);
    }];

}

- (void)getSpecialReportArticles
{
    int itemsPerSection = 3;
    
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        
        [self.exclusiveArticles removeAllObjects];
        for (NSDictionary *feedItemDict in array) {
            if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:@"specialreports"]) {
                
                dispatch_group_enter(self.group);
                NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                [[LibraryAPI sharedInstance]getArticlesWithURL:sectionURL isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                    if(array){
                        if (array.count > itemsPerSection) {
                            for (int i = 0; i < itemsPerSection; i++) {
                                Article *article = (Article *) [array objectAtIndex:i];
                                [self.exclusiveArticles addObject:article];
                            }
                        } else {
                            self.exclusiveArticles = [array mutableCopy];
                        }
                    }
                    dispatch_group_leave(self.group);
                }];
            }
        }
        
        dispatch_group_leave(self.group);
    }];
}

- (void)getBookmarkItems
{
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.collectionView reloadData];
        }
        dispatch_group_leave(self.group);
    }];
    
}

-(void)reloadTableData: (UIRefreshControl *)refreshFeed {
    
    [self loadData];
    
  if (refreshFeed) {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    refreshFeed.attributedTitle = attributedTitle;
    
    [refreshFeed endRefreshing];
  }
}

#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    
    //self.bannerView =  [[DFPBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];;
    self.bannerView.adSize = kGADAdSizeBanner;
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       ]];*/
    
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;
    self.bannerView.delegate = self;
    //self.bannerView.adSizeDelegate =self;
    [self.bannerView setAutoloadEnabled:YES];
    [self.bannerView loadRequest:request];
    [self.bannerView setHidden:YES];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog( @"Error: %@", error);
    self.bannerHeight.constant = 0;
    [self.bannerView setHidden:YES];
}

- (void)adViewDidReceiveAd:(GADBannerView *)view {
    if (self.bannerHeight.constant == 0)
        self.bannerHeight.constant = 50;
    [self.bannerView setHidden:NO];
}

@end
