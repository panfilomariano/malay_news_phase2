//
//  TMBookmarkiPadViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 2/25/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMBookmarkiPadViewController.h"
#import "LibraryAPI.h"
#import "Util.h"
#import "TMSquareCollectionViewCell.h"
#import "NSString+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TMNewsDetailsPagerViewController.h"
#import "TMNewsCollectionViewLayout.h"
#import "TMNewsArticleViewController.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import "Util.h"
#import "UIView+Toast.h"

@interface TMBookmarkiPadViewController () <UICollectionViewDataSource, UICollectionViewDelegate, TMSquareCollectionViewCellDelagate, TMNewsDetailsPagerViewControllerDelagate>
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) UINib *squareCell;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *removedBookmarks;






@end

@implementation TMBookmarkiPadViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    _removedBookmarks = [[NSMutableArray alloc] init];
    _bookmarks = [[NSMutableArray alloc] init];
    self.squareCell = [UINib nibWithNibName:@"TMBookmarksCollectionViewCell" bundle:nil];
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.barTintColor = [Util headerColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.title = [Constants MENU_BOOKMARKS];

    [self.collectionView registerNib:self.squareCell forCellWithReuseIdentifier:@"bookmarkCell"];
    [[AnalyticManager sharedInstance] trackBookmark];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.noItemsLabel setHidden:YES];
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            self.bookmarks = [array mutableCopy];
            [self.collectionView reloadData];
        }
        if(array.count == 0){
            [self.noItemsLabel setHidden:NO];
            [self.noItemsLabel setText:[Constants NO_BOOKMARK_ARTICLES]];
        }
    }];
}

#pragma mark UICollectionViewDataSource
- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f blue:237/255.0f alpha:0.5];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    float rW = 224.0f;
    float rH = 137.0f;
    
    float width = (768/ 3) - 11;
    if (!UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        width = (1024 / 4) - 11;
    }
    
    float height = (rH / rW * width) + 105;
    return CGSizeMake(width, height+10);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(9, 9, 9, 9);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 4.5f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.0f;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  self.bookmarks.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TMSquareCollectionViewCell *cell = (TMSquareCollectionViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"bookmarkCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    if (_bookmarks.count) {
        Article *article = (Article *) [self.bookmarks objectAtIndex:indexPath.row];
        cell.tittle.text = article.tittle;
        cell.category.text = article.category;
        cell.delegate = self;
        cell.indexPath = indexPath;
        if (article.mediagroupList.count) {
            NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
            NSString *thumbnail = [mediaDictionary valueForKey:@"thumbnail"];
            NSString *type = [mediaDictionary valueForKey:@"type"];
            
            cell.pubDate.text = [article.pubDate formatDate];
            if([type isEqualToString:@"video"]){
                [cell.playImage setHidden:NO];
            }else{
                [cell.playImage setHidden:YES];
            }
            
            
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                              placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            
            [cell.bookmarkButton setSelected:YES];
            [cell.bookmarkButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:article]];
            [cell.thumbnail.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
            [cell.thumbnail.layer setBorderWidth: 1.0];
            
        }
    }
    return cell;
}


#pragma mark UICollectionViewDelegate

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
    newsArticleVC.article = [self.bookmarks objectAtIndex:indexPath.row];
    newsArticleVC.isBookmark = YES;;
    newsArticleVC.hasBarItems = YES;
    //[[AnalyticManager sharedInstance] trackClickNewsArticleInSearchWithCategory:newsArticleVC.article.category andGuid:newsArticleVC.article.guid andArticleTitle:newsArticleVC.article.tittle];
    
    [self.navigationController pushViewController:newsArticleVC animated:YES];
}


- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    Article *article = (Article *) [self.bookmarks objectAtIndex:indexPath.row];
    Article *remBookmarkArticle = nil;
    if(_removedBookmarks.count){
        
        for(Article *removedArticles in _removedBookmarks){
            if ([removedArticles.guid isEqualToString:article.guid]) {
                remBookmarkArticle = removedArticles;
                break;
            }
        }
        if (remBookmarkArticle) {
            [_removedBookmarks removeObject:remBookmarkArticle];
        }
    }
    
    if (_bookmarks.count) {
        Article *bookmarkArticle = nil;
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle && !remBookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [_removedBookmarks addObject:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            
        } else if (remBookmarkArticle) {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
        }
        
    }
    
    
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

#pragma mark OrientationChange

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
