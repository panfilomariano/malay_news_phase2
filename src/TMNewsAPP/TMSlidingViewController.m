//
//  TMSlidingViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 3/25/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSlidingViewController.h"
#import "Util.h"

@interface TMSlidingViewController ()

@end

@implementation TMSlidingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([Util is_iPad]){
        return UIInterfaceOrientationMaskAll;
    }else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([Util is_iPad]){
        return YES;
    } else {
        return UIInterfaceOrientationPortrait == toInterfaceOrientation;
    }
}

@end
