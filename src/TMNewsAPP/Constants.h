//
//  Constants.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//
/*
#define URL_REGULATION @"http://www.mediacorp.sg/en/termsofuse"
#ifdef TAMIL
#define URL_BASE @"http://seithi.mediacorp.sg"
#define APPVERSION_URL @"http://54.254.105.235/tmnews/notification/APNS/ios_update.json"
#else
#define URL_BASE @"http://berita.mediacorp.sg";
#define APPVERSION_URL @"http://54.254.105.235/mlnews/notification/APNS/ios_update.json"
#endif
*/

#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#pragma mark URL
+ (NSString *) INDEX_API;
+ (NSString *) URL_BASE;
+ (NSString *) URL_REGULATION;
+ (NSString *) URL_PRIVACY;
+ (NSString *) URL_ABOUT;
+ (NSString *) URL_GLOSSARY;
+ (NSString *) APPVERSION_URL;
+ (NSString *) ITUNES_APP_URL;
+ (NSString *) PUSHNOTIF_URL;
+ (NSString *) WEATHER_URL;
+ (NSString *) MALAY_RADIO_WARNA_URL;
+ (NSString *) MALAY_RADIO_RIA_URL;
+ (NSString *) TAMIL_RADIO_OLI_URL;

#pragma mark COMSCORE
+ (NSString *) COMSCORE_APPNAME;

#pragma mark CONSTANT TEXT
+ (NSArray *) MENU_TAGGING;
+ (NSArray *) TAB_TITTLES;
+ (NSString *) LATEST_NEWS_PHOTOS;
+ (NSString *) LATEST_PHOTO_GALLERIES;
+ (NSString *) TOP5_NEWS;
+ (NSString *) LATEST_EPISODE;
+ (NSString *) YOU_MIGHT_ALSO_BE_INTERESTED_IN;
+ (NSString *) LANDING_BREAKING_NEWS;
+ (NSString *) SEARCH_PLACEHOLDER;
+ (NSString *) SEARCH_RESULTS;
+ (NSString *) WEATHER_24HR_PSI;
+ (NSString *) WEATHER_3HR_PSI;

+ (NSString *) SETTING_CONTACT_US;
+ (NSString *) SETTING_SUBMIT_NEWS;
+ (NSString *) SETTING_ABOUT_US;
+ (NSString *) SETTING_PRIVACY_POLICY;
+ (NSString *) SETTING_REGULATIONS;
+ (NSString *) SETTING_FONT_SIZE;
+ (NSString *) SETTING_VERSION;
+ (NSString *) SETTING_GLOSSARY;

+ (NSString *) SETTINGS_DOWNLOAD_ALL;
+ (NSString *) SETTINGS_DOWNLOADING;
+ (NSString *) SETTINGS_DOWNLOAD_COMPLETED;
+ (NSString *) SETTINGS_DOWNLOAD_OPTION1;
+ (NSString *) SETTINGS_DOWNLOAD_OPTION2;

+ (NSString *) SETTINGS_SUBMIT_NEWS;
+ (NSString *) SETTINGS_SUBMIT_NEWS_PHOTOS_OR_VIDEOS;

+ (NSString *) SETTINGS_FONTSIZE_XSMALL;
+ (NSString *) SETTINGS_FONTSIZE_SMALL;
+ (NSString *) SETTINGS_FONTSIZE_MEDIUM;
+ (NSString *) SETTINGS_FONTSIZE_LARGE;
+ (NSString *) SETTINGS_FONTSIZE_XLARGE;

+ (NSString *) SUBMIT_NEWS_EMAIL_TO;
+ (NSString *) SUBMIT_NEWS_EMAIL_SUBJECT;
+ (NSString *) SUBMIT_NEWS_EMAIL_MESSAGE;

+ (NSString *) CONTACT_US_EMAIL_TO;
+ (NSString *) CONTACT_US_EMAIL_SUBJECT;
+ (NSString *) CONTACT_US_EMAIL_MESSAGE;

+ (NSString *) DIALOG_NETWORK_MESSAGE;

+ (NSString *) SHARE_TITTLE;
+ (NSString *) SHARE_PRE_TITTLE;
+ (NSString *) NEWS_LISTING_TITTLE;

+ (NSString *) MENU_SEARCH;
+ (NSString *) MENU_BOOKMARKS;


+ (NSString *) PLACEHOLDER;
+ (NSString *) PLACEHOLDER_BIG;

+ (NSString *) MORE_FROM_NEWS;

#pragma mark Article English Tittle
+ (NSString *) LATEST_NEWS;
+ (NSString *) LATEST_SECTION_NEWS;
+ (NSString *) MOST_POPULAR;
+ (NSString *) TOP_NEWS;
+ (NSString *) SINGAPORE;
+ (NSString *) WORLD;
+ (NSString *) BUSINESS;
+ (NSString *) SPECIAL_REPORT;
+ (NSString *) SPORT;
+ (NSString *) LIFE_STYLE;
+ (NSString *) ENTERTAINMENT;
+ (NSString *) BREAKING_NEWS;
+ (NSString *) ADVERTORIAL;
+ (NSString *) LATEST_EPISODES;
+ (NSString *) VIDEOS;
+ (NSString *) CA;
+ (NSString *) PHOTOS;



#pragma mark Article Types
+ (NSString *) NEWS_TYPE;
+ (NSString *) OTHERS_TYPE;
+ (NSString *) SPECIAL_REPORT_TYPE;
+ (NSString *) PHOTOS_TYPE;
+ (NSString *) VIDEOS_TYPE;
+ (NSString *) CA_TYPE;
+ (NSString *) EPISODE_TYPE;
+ (NSString *) HTML_TYPE;

#pragma mark Key
+ (NSString *) NEWS_KEY;
+ (NSString *) TOP_NEWS_KEY;
+ (NSString *) LATEST_NEWS_KEY;
+ (NSString *) ARTICLES_KEY;
+ (NSString *) INDEX_ITEM_LIST_KEY;
+ (NSString *) CA_KEY;
+ (NSString *) PHOTOS_KEY;
+ (NSString *) VIDEOS_KEY;
+ (NSString *) MEDIA_KEY;
+ (NSString *) BOOKMARKS_KEY;
+ (NSString *) WEATHER_KEY;
+ (NSString *) TOP_FIVE_NEWS_KEY;
+ (NSString *) HAS_BREAKING_NEWS_KEY;
+ (NSString *) RADIO_CURRENT_STATION_KEY;
+ (NSString *) ADVERTORIAL_KEY;

#pragma EXP LENGTH
+ (double) BOOKMARKS_EXP_LENGHT;
+ (double) DOWNLOAD_EXP_LENGHT;
+ (double) WEATHER_EXP_LENGHT;
+ (double) BREAKING_NEWS_REQUEST_INTERVAL;
+ (double) TOP_5_NEWS_DOWNLOAD_EXP_LENGHT;
+ (double) INDEX_LIST_REQUEST_INTERVAL;

#pragma mark ID's
+ (NSString *) FB_APP_ID;
+ (NSString *) OUTBRAIN_WIDGET_ID;
+ (NSString *) ADMOB_UNITID_SPLASHAD;
+ (NSString *) ADMOB_UNITID_BANNERAD;
+ (NSString *) BREAKING_NEWS_GUID;

#pragma toastMessages
+ (NSString *) PLAY_RADIO;
+ (NSString *) PUSH_HEADER;
+ (NSString *) NO_SEARCH_RESULT;
+ (NSString *) NO_BOOKMARK_ARTICLES;
+ (NSString *) ADD_TO_FAV;
+ (NSString *) REMOVE_FROM_FAV;
+ (NSString *) DOWNLOAD_PROMPT;

+ (NSString *) HEADER_HTMLDATA;
+ (NSString *) XPHOTOS;
+ (NSString *) XPHOTO;

#pragma mark dialogMessage
+ (NSString *) OK_DIALOG_MESSAGE;
+ (NSString *) CLOSE_DIALOG_MESSAGE;
+ (NSString *) OPEN_DIALOG_MESSAGE;

@end
