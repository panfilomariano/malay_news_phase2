//
//  TMBookmarksTableViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/23/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMBookmarksTableViewController.h"
#import "TMNewsTableViewCell.h"
#import "Article.h"
#import "NSString+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LibraryAPI.h"
#import "TMNewsDetailsViewController.h"
#import "Util.h"
#import "TMNewsArticleViewController.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "TMNewsPagesViewController.h"

@interface TMBookmarksTableViewController ()<TMNewsTableViewCellDelagate, TMNewsDetailsViewControllerDelagate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *removedBookmarks;



@end

@implementation TMBookmarksTableViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _bookmarks = [[NSMutableArray alloc] init];
    _removedBookmarks = [[NSMutableArray alloc] init];
    
    
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    self.title = [Constants MENU_BOOKMARKS];
    [self.noItemsLabel setHidden:YES];
    [self.noItemsLabel setText:[Constants NO_BOOKMARK_ARTICLES]];
    [self.noItemsLabel sizeToFit];
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.tableView reloadData];
        }
        
        if(array.count){
            [self.noItemsLabel setHidden:YES];
            self.tableView.contentInset = UIEdgeInsetsMake(-20, 8, 8, 8);
        }else{
            [self.noItemsLabel setHidden:NO];
            self.tableView.contentInset = UIEdgeInsetsMake(8, 8, 8, 8);
        }
       

    }];
    
    //[[AnalyticManager sharedInstance] trackBookmark];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _bookmarks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TMNewsTableViewCell *cell = (TMNewsTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"bookmarkCellId" forIndexPath:indexPath];
    
    if (_bookmarks.count) {
        Article *article = (Article *) _bookmarks[indexPath.row];
        cell.titleLabelLATEST.text = article.tittle;
        cell.categoryLabelLATEST.text = article.category;
        cell.delegate = self;
        cell.indexPath = indexPath;
        if (article.mediagroupList.count) {
            NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
            NSString *photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
            NSString *type = [mediaDictionary valueForKey:@"type"];
            
            cell.dateLabelLATEST.text = [article.pubDate formatDate];
            if([type isEqualToString:@"video"]){
                [cell.playOverlay setHidden:NO];
            }else{
                [cell.playOverlay setHidden:YES];
            }
           
            [cell.starButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:article]];
            
            [cell.thumbnail.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
            [cell.thumbnail.layer setBorderWidth: 1.0];
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                              placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
        }
        
    }
    return cell;
}


#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*TMNewsDetailsViewController *detailednewsVC = (TMNewsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetailsID"];
    detailednewsVC.passedArray = _bookmarks;
    detailednewsVC.index = indexPath.row;
    detailednewsVC.didTapBookmarkButtonDelegate = self;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:detailednewsVC animated:YES];*/
    
    TMNewsPagesViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"newsPagesId"];
    newsArticleVC.article = [self.bookmarks objectAtIndex:indexPath.row];
    newsArticleVC.isBookmark = YES;
    //newsArticleVC.hasBarItems = YES;
    [self.navigationController pushViewController:newsArticleVC animated:YES];
}
#pragma mark TMNewsTableViewCellDelagate
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    
    Article *article = (Article *) [self.bookmarks objectAtIndex:indexPath.row];
     Article *remBookmarkArticle = nil;
    if(_removedBookmarks.count){
      
        for(Article *removedArticles in _removedBookmarks){
            if ([removedArticles.guid isEqualToString:article.guid]) {
                remBookmarkArticle = removedArticles;
                break;
            }
        }
        if (remBookmarkArticle) {
            [_removedBookmarks removeObject:remBookmarkArticle];
        }
    }

        if (_bookmarks.count) {
            Article *bookmarkArticle = nil;
            for (Article *bookmark in _bookmarks) {
                if ([bookmark.guid isEqualToString:article.guid]) {
                    bookmarkArticle = bookmark;
                    break;
                }
            }
            
            if (bookmarkArticle && !remBookmarkArticle) {
                [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
                [_removedBookmarks addObject:bookmarkArticle];
                [self.view makeToast:[Constants REMOVE_FROM_FAV]];
                
            } else if (remBookmarkArticle) {
                [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
                [self.view makeToast:[Constants ADD_TO_FAV]];
            }
            
        }
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
}
#pragma mark TMNewsDetailsViewControllerDelagate
-(void)didTapBookmarkButton
{
  
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
