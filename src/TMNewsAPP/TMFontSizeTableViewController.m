//
//  TMFontSizeTableViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/23/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMFontSizeTableViewController.h"
#import "Data.h"
#import "Constants.h"

@interface TMFontSizeTableViewController ()
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSIndexPath *currentSelectedIndexPath;
@end

@implementation TMFontSizeTableViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _items = [[NSArray alloc] initWithObjects:[Constants SETTINGS_FONTSIZE_XSMALL], [Constants SETTINGS_FONTSIZE_SMALL], [Constants SETTINGS_FONTSIZE_MEDIUM], [Constants SETTINGS_FONTSIZE_LARGE], [Constants SETTINGS_FONTSIZE_XLARGE], nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_items count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *) [cell.contentView viewWithTag:1];
    label.text = _items[indexPath.row];
    
    UIImageView *checkmarkImageView = (UIImageView *) [cell.contentView viewWithTag:2];
    checkmarkImageView.hidden = YES;
    
    if ([Data sharedInstance].fontSizeType == FontSizeTypeDefault) {
        if (indexPath.row == 2)
            checkmarkImageView.hidden = NO;
    } else {
        int index = [Data sharedInstance].fontSizeType;
        if ([Data sharedInstance].fontSizeType  <= 2)
            index = [Data sharedInstance].fontSizeType - 1;
        
        if (indexPath.row == index) {
            checkmarkImageView.hidden = NO;
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentSelectedIndexPath == indexPath) return;
    self.currentSelectedIndexPath = indexPath;
    
    switch (indexPath.row) {
        case 0: {
            [Data sharedInstance].fontSizeType = FontSizeTypeXSmall;
            break;
        } case 1: {
            [Data sharedInstance].fontSizeType = FontSizeTypeSmall;
            break;
        } case 2: {
            [Data sharedInstance].fontSizeType = FontSizeTypeDefault;
            break;
        } case 3: {
            [Data sharedInstance].fontSizeType = FontSizeTypeLarge;
            break;
        } case 4: {
            [Data sharedInstance].fontSizeType = FontSizeTypeXLarge;
            break;
        }
            
    }
    
    [tableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
