//
//  TMNewsListViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/5/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"

typedef NS_OPTIONS(NSUInteger, Type)
{
    NewsType = 0,
    EpisodesType = 1,
    SpecialReportType = 2,
    PhotosType = 3,
    VideosType = 4,
    CAType = 5,
};

typedef enum
{
    OthersNewsListScreenType,
    LatestNewsNewsListScreenType,
    CANewsListScreenType,
    SpecialReportNewsListScreenType,
    NewsNewsListScreenType
    
} NewsListScreenType;

@interface TMNewsListViewController : UIViewController


@property (nonatomic, strong) NSString *feedLink;
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) NSString *enTitle;
@property (nonatomic, strong) NSString *titleForCategory;
@property (nonatomic) BOOL isFromCA;
@property (nonatomic) BOOL isFromMenu;
@property (nonatomic, strong) NSString *CAEntitle;
@property (nonatomic, strong) NSMutableArray *passedLatestNews;
@property (nonatomic, strong) Article *specialReportArticle;
@property (nonatomic, strong) NSString *eklusifEntitle;
@property (nonatomic, assign) Type type;
@property (nonatomic, assign) NewsListScreenType screenType;


@end
