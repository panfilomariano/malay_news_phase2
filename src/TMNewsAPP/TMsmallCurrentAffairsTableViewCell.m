//
//  TMsmallCurrentAffairsTableViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMsmallCurrentAffairsTableViewCell.h"

@implementation TMsmallCurrentAffairsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)didTapBookmark:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookMarkButtonAtIndexPath:)]){
        [self.delegate didTapBookMarkButtonAtIndexPath:self.indexPath];
    }
}


@end
