//
//  TMDownloadViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/3/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMDownloadViewController.h"
#import "MBProgressHUD.h"
#import "Data.h"
#import "Constants.h"
#import "AnalyticManager.h"

@interface TMDownloadViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) NSArray *downloadList;
@property (nonatomic, strong) NSIndexPath *index;
@end

@implementation TMDownloadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _downloadList = [[NSArray alloc]initWithObjects:[Constants SETTINGS_DOWNLOAD_OPTION1],[Constants SETTINGS_DOWNLOAD_OPTION2], nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _downloadList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *) [cell.contentView viewWithTag:1];
    label.text = _downloadList [indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  66.0f;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download"
                                                    message:[Constants DOWNLOAD_PROMPT]
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK",nil];
    
    self.index = indexPath;
    alert.tag = 1;
    [alert show];
 
}



#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag ==1){
        if(buttonIndex != alertView.cancelButtonIndex){
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = [Constants SETTINGS_DOWNLOADING];
            if (self.index.row == 0) {
                [[Data sharedInstance] downloadAllArticlesWithImages:NO andCompletionBlock:^{
               
                    [hud hide:YES];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Constants SETTINGS_DOWNLOAD_COMPLETED]
                                                                    message:@""
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    alert.tag =2;
                    [alert show];
                }];
            }else {
                [[Data sharedInstance] downloadAllArticlesWithImages:YES andCompletionBlock:^{
                  
                    [hud hide:YES];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Constants SETTINGS_DOWNLOAD_COMPLETED]
                                                                    message:@""
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    alert.tag =2;
                    [alert show];
                }];
            }
        }[[AnalyticManager sharedInstance] trackOfficeDownload];
    }

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
