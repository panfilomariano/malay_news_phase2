//
//  TMSearchTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/2/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSearchTableViewCellDelegate <NSObject>

- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMSearchTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIButton *starButton;
@property (nonatomic, weak) IBOutlet UIImageView *playOverlay;
@property (nonatomic, strong) id <TMSearchTableViewCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end
