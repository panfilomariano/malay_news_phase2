//
//  AsyncImageView.m
//  EightDays
//
//  Created by Wong Johnson on 4/12/13.
//  Copyright (c) 2013 PI Entertainment Limited. All rights reserved.
//

#import "AsyncImageView.h"
#import "NSString+PercentEscapes.h"

@implementation AsyncImageView

@synthesize imageView = _imageView;

- (void)dealloc
{
	[_connection cancel];
}

- (void)setFrame:(CGRect)frame
{
	[super setFrame:frame];
	if (_imageView)
		_imageView.frame = CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height);
	if (_indicator)
		_indicator.center = CGPointMake(self.frame.size.width / 2.0f, self.frame.size.height / 2.0f);
}

- (UIImageView *)imageView
{
    if (!_imageView) {
        self.clipsToBounds = YES;
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        _imageView.opaque = NO;
        _imageView.backgroundColor = [UIColor clearColor];
        
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
        
        [self addSubview:_imageView];
        _imageView.frame = self.bounds;
        [_imageView setNeedsLayout];
        [self setNeedsLayout];
    }
    return _imageView;
}

- (void)setSpinnerImage:(UIImage*)image
{
    _spinnerImage = image;
    self.imageView.image = image;
}

- (void)loadImageFromURL:(NSURL*)url fromCache:(BOOL)fromCache
{
	[self clearImage];
    _fromCache = fromCache;
	
    if (_fromCache) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *fileName = url.absoluteString;
        if (_prefix) {
            fileName = [_prefix stringByAppendingString:fileName];
        }
        fileName = [fileName stringByAddingPercentEscapes];
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            _data = [[NSMutableData alloc] initWithContentsOfMappedFile:filePath];
            if (_data) {
                [self createImage:YES];
                return;
            }
        }
    }
	_url = url;
    if (_spinnerImage == nil) {
        if (_indicator == nil) {
            _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            _indicator.center = CGPointMake(self.frame.size.width / 2.0f, self.frame.size.height / 2.0f);
            [self addSubview:_indicator];
        }
        [_indicator startAnimating];
    } else {
        self.imageView.image = _spinnerImage;
    }
    
	NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	[_connection start];
}

#pragma mark NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if (_data == nil) {
		_data = [[NSMutableData alloc] initWithCapacity:2048];
	}
	[_data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection*)connection
{
	_connection = nil;
	
    NSData *data = _data;
    UIImage *image = [self createImage:NO];
    if (image && _fromCache) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *fileName = _url.absoluteString;
        if (_prefix) {
            fileName = [_prefix stringByAppendingString:fileName];
        }
        fileName = [fileName stringByAddingPercentEscapes];
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
        [data writeToFile:filePath atomically:YES];
    }
	
	_url = nil;
	
	if (_indicator) {
		[_indicator stopAnimating];
		[_indicator removeFromSuperview];
		_indicator = nil;
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (_delegate && [_delegate respondsToSelector:@selector(clickedAsyncImageView:)]) {
		UITouch *touch = [touches anyObject];
		if ([self pointInside:[touch locationInView:self] withEvent:event]) {
            [_delegate performSelector:@selector(clickedAsyncImageView:) withObject:self];
        }
	}
}

- (UIImage *)createImage:(BOOL)cached
{
	UIImage *image = nil;
	if (_delegate && [_delegate respondsToSelector:@selector(asyncImageView:didLoadData:fromCache:)]) {
        image = [_delegate asyncImageView:self didLoadData:_data fromCache:cached];
    }
	if (image == nil) {
		image = [UIImage imageWithData:_data];
    }
    if (image) {
        self.imageView.image = image;
    }
	_data = nil;
	
	if (_delegate && [_delegate respondsToSelector:@selector(didLoadAsyncImageView:fromCache:)]) {
        [_delegate didLoadAsyncImageView:self fromCache:cached];
    }
    return image;
}

- (void)clearImage
{
	if (_connection != nil) {
		[_connection cancel];
		_connection = nil;
	}
	if (_data != nil) {
		_data = nil;
	}
	if (_imageView) {
		[_imageView removeFromSuperview];
		_imageView = nil;
	}
	if (_indicator) {
		[_indicator removeFromSuperview];
		_indicator = nil;
	}
	if (_url) {
		_url = nil;
	}
}

@end
