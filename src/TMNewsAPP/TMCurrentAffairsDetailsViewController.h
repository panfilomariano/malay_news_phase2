//
//  TMCurrentAffairsDetailsViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "ViewPagerController.h"

@interface TMCurrentAffairsDetailsViewController : ViewPagerController
@property (nonatomic, strong) NSString *stringApiUrl;
@property (nonatomic, strong) NSArray *newsCategories;
@property (nonatomic) NSUInteger *selectedIndex;
@property (nonatomic, strong) NSString *feedLink;
@end
