//
//  TMNewsPagerViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/5/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMNewsPagerViewController.h"
#import "TMLandingPageViewController.h"
#import "TMNewsListViewController.h"
#import "Constants.h"
#import "TMWebViewController.h"
#import "LibraryAPI.h"
#import "Util.h"
#import "TMIPADMenuViewController.h"
#import "TMCAandSpecialReportListViewController.h"
#import "TMVideosViewController.h"
#import "TMPhotosViewController.h"
#import "TMCAandSRCollectionViewCell.h"
#import "AnalyticManager.h"
#import "TMNewsIpadViewController.h"
#import "TMIpadfCurrentAffairsViewController.h"
#import "MBProgressHUD.h"

@interface TMNewsPagerViewController () <ViewPagerDataSource, ViewPagerDelegate, UIPopoverControllerDelegate, TMIPADMenuViewControllerDelegate>
@property (nonatomic, strong) UIButton *menubutton;
@property (nonatomic, strong) UIBarButtonItem *menuBarButton;
@property (nonatomic, strong) UIPopoverController *popOverMenu;
@property (nonatomic, strong) UIView *dimView;
@property (nonatomic) bool isTherePopOver;
@property (nonatomic, strong) NSMutableArray *menuArrayList;

@end

@implementation TMNewsPagerViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setNeedsReloadOptions];

    [self trackTagging];

}

- (void)trackTagging
{
    if (self.screenType == CANewsPagerScreenType) {
        [[AnalyticManager sharedInstance] trackCAProgrammeContentListingWithProgram:self.enTitle];
    }else if (self.screenType == SpecialReportNewsPagerScreenType) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[LibraryAPI sharedInstance]getArticlesWithURL:self.eksklusifArticle.sectionURL andFeedLink:self.eksklusifArticle.feedlink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
            if (array.count) {
                Article *spArticle = (Article *)[array firstObject];
                self.newsPages = spArticle.sections;
                self.activeIndex = 0;
                self.sectionTitleForCategory = spArticle.tittle;
                self.activeTabIndex = 0;
                [self reloadData];
                self.eklusifEntitle = spArticle.entittle;
            }
            [[AnalyticManager sharedInstance] trackSPNewsListingScreen:self.eklusifEntitle];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
        

    }else if (self.screenType == NewsNewsPagerScreenType){

        NSDictionary *dictionary = (NSDictionary *)[self.newsPages objectAtIndex:self.activeTabIndex];
        NSString *tittle = @"";
        
        if([dictionary valueForKey:@"title"]){
            tittle = [dictionary valueForKey:@"enTitle"];

        }else{
            tittle = [dictionary valueForKey:@"enCategory"];
        }

        [[AnalyticManager sharedInstance] trackNewsListingWithCategoryName:tittle];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    self.reuseViewControllers = YES;
    self.title = self.pagerTittle;
    
    self.activeTabIndex = self.activeIndex;
    self.menubutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.menubutton setFrame:CGRectMake(0, 0, 28, 29)];
    [self.menubutton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [self.menubutton setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateSelected];
    [self.menubutton addTarget:self action:@selector(didTapMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    self.menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:self.menubutton];
    self.navigationItem.rightBarButtonItem = self.menuBarButton;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark ViewPager Datasource/Delegate

- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    
    return [self.newsPages count];
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    NSDictionary *dictionary = (NSDictionary *)[self.newsPages objectAtIndex:index];
    NSString *tittle = @"";
    
    if([dictionary valueForKey:@"title"]){
        tittle = [[dictionary valueForKey:@"title"] uppercaseString];
    }else{
        tittle = [[dictionary valueForKey:@"category"] uppercaseString];
    }
    
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = tittle;
    [label sizeToFit];
    [label setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [label setTextColor:[UIColor colorWithRed:180.0f/255.0f green:200.0f/255.0f blue:241.0f/255.0f alpha:1]];
    
    return  label;
    
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
    NSDictionary *dictionary = (NSDictionary *)[self.newsPages objectAtIndex:index];
    NSString *tittle = @"";
    NSString *type = [dictionary valueForKey:@"type"];
    NSString *sectionType = [dictionary valueForKey:@"sectionType"];
    NSString *enTitle = self.enTitle;
    
    if([dictionary valueForKey:@"title"]){
        tittle = [dictionary valueForKey:@"enTitle"];
        
    }else{
        tittle = [dictionary valueForKey:@"enCategory"];
    }

    if (self.screenType == SpecialReportNewsPagerScreenType) {
        if([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
            [[AnalyticManager sharedInstance] trackPhotosWithSpecialReport:self.eklusifEntitle];
        }else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]){
            [[AnalyticManager sharedInstance] trackSPVideoListing:self.eklusifEntitle];
        }else if ([type isEqualToString:[Constants HTML_TYPE]]|| [sectionType isEqualToString:[Constants HTML_TYPE]]){
            [[AnalyticManager sharedInstance] trackSPHtmlPageWithEnCategory:tittle withEnCategory:self.eklusifEntitle];
        }else {
            [[AnalyticManager sharedInstance] trackSPNewsListingScreen:self.eklusifEntitle];
        }
    } else if (self.screenType == NewsNewsPagerScreenType){
        [[AnalyticManager sharedInstance] trackNewsListingWithCategoryName:tittle];
    } else if (self.screenType == CANewsPagerScreenType){
        if ([type isEqualToString:[Constants HTML_TYPE]]|| [sectionType isEqualToString:[Constants HTML_TYPE]]) {
            [[AnalyticManager sharedInstance] trackCAAboutScreenWithCategory:tittle andProgram:enTitle];
        }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
            [[AnalyticManager sharedInstance] trackCAPhotoListingWithProgram:self.enTitle];
        }
    }
    
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index{
    
    NSDictionary *dictionary = (NSDictionary *)[self.newsPages objectAtIndex:index];
    NSString *tittle = @"";
    if([dictionary valueForKey:@"entTitle"]){
        tittle = [dictionary valueForKey:@"entTitle"];
        
    }else{
        tittle = [dictionary valueForKey:@"enCategory"];
    }
    
    NSString *sectionType = [dictionary valueForKey:@"sectionType"];
    NSString *type = [dictionary valueForKey:@"type"];
    NSString *sectionURL = [dictionary valueForKey:@"sectionURL"];
    NSString *enTitle = self.enTitle;
    
    if ([type isEqualToString:[Constants HTML_TYPE]]|| [sectionType isEqualToString:[Constants HTML_TYPE]]) {
        
        TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
        webViewController.sectionUrl = [dictionary valueForKey:@"sectionURL"];
        return webViewController;
        
    } else {
        
        if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]) {
            TMPhotosViewController *photoListVC = (TMPhotosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoViewController"];
            photoListVC.isFromMain = NO;
            if (self.screenType == SpecialReportNewsPagerScreenType){
                photoListVC.screenType = SpecialReportPhotosScreenType;
                photoListVC.photoEklusifTitle = self.eklusifEntitle;
            }else if (self.screenType == CANewsPagerScreenType){
                photoListVC.screenType = CAPhotosScreenType;
                photoListVC.photoCATitle = self.enTitle;
            }
            photoListVC.sectionURL = sectionURL;
            return photoListVC;
        } else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]) {
            
            TMVideosViewController *videosListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"videosViewController"];
            videosListVC.isFromMain = NO;
            if (self.screenType == SpecialReportNewsPagerScreenType){
                videosListVC.isFromSpecialReport = TRUE;
                videosListVC.videosEksklusifTitle = self.eklusifEntitle;
            }
            videosListVC.sectionURL = sectionURL;
            return videosListVC;
        } else if ([type isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants NEWS_TYPE]]) {
            
            TMNewsListViewController *newsListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsListViewControllerId"];
            newsListVC.urlString = [dictionary valueForKey:@"sectionURL"];
            newsListVC.feedLink = self.feedLink;
            newsListVC.enTitle = enTitle;
            newsListVC.specialReportArticle = self.eksklusifArticle;
            newsListVC.passedLatestNews = self.sectionContent;
            newsListVC.titleForCategory = self.sectionTitleForCategory;
            [newsListVC setType:NewsType];
            switch (self.screenType) {
                case OthersNewsPagerScreenType:
                    
                    break;
                case LatestNewsNewsPagerScreenType:
                    
                    break;
                case CANewsPagerScreenType:
                    newsListVC.screenType = CANewsListScreenType;
                    break;
                case SpecialReportNewsPagerScreenType:
                    newsListVC.screenType = SpecialReportNewsListScreenType;
                    break;
                case NewsNewsPagerScreenType:
                    
                    break;
                case MenuNewsPagerScreenType:
                    
                    break;
            }
            
            return newsListVC;
            
        } else if ([type isEqualToString:[Constants EPISODE_TYPE]] || [sectionType isEqualToString:[Constants EPISODE_TYPE]]) {
            
            TMNewsListViewController *newsListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsListViewControllerId"];
            newsListVC.urlString = [dictionary valueForKey:@"sectionURL"];
            newsListVC.feedLink = self.feedLink;
            newsListVC.enTitle = enTitle;
            newsListVC.passedLatestNews = self.sectionContent;
            newsListVC.specialReportArticle = self.eksklusifArticle;
            newsListVC.titleForCategory = self.sectionTitleForCategory;
            [newsListVC setType:EpisodesType];
            
            switch (self.screenType) {
                case OthersNewsPagerScreenType:
                    
                    break;
                case LatestNewsNewsPagerScreenType:
                    
                    break;
                case CANewsPagerScreenType:
                    newsListVC.screenType = CANewsListScreenType;
                    break;
                case SpecialReportNewsPagerScreenType:
                    newsListVC.screenType = SpecialReportNewsListScreenType;
                    break;
                case NewsNewsPagerScreenType:
                    
                    break;
                case MenuNewsPagerScreenType:
                    
                    break;
            }
            
            return newsListVC;
            
        } else if ([type isEqualToString:[Constants CA_TYPE]] || [sectionType isEqualToString:[Constants CA_TYPE]]) {
            
            TMNewsListViewController *newsListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsListViewControllerId"];
            newsListVC.urlString = [dictionary valueForKey:@"sectionURL"];
            newsListVC.isFromCA = TRUE;
            newsListVC.feedLink = self.feedLink;
            newsListVC.enTitle = enTitle;
            newsListVC.passedLatestNews = self.sectionContent;

            newsListVC.specialReportArticle = self.eksklusifArticle;
            newsListVC.titleForCategory = self.sectionTitleForCategory;
            newsListVC.CAEntitle = tittle;
            [newsListVC setType:CAType];
            
            switch (self.screenType) {
                case OthersNewsPagerScreenType:
                    
                    break;
                case LatestNewsNewsPagerScreenType:
                    
                    break;
                case CANewsPagerScreenType:
                    newsListVC.screenType = CANewsListScreenType;
                    break;
                case SpecialReportNewsPagerScreenType:
                    newsListVC.screenType = SpecialReportNewsListScreenType;
                    break;
                case NewsNewsPagerScreenType:
                    
                    break;
                case MenuNewsPagerScreenType:
                    
                    break;
            }
            return newsListVC;
            
        } else if ([type isEqualToString:[Constants SPECIAL_REPORT_TYPE]] || [sectionType isEqualToString:[Constants SPECIAL_REPORT_TYPE]]) {
            
            TMNewsListViewController *newsListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsListViewControllerId"];
            newsListVC.urlString = [dictionary valueForKey:@"sectionURL"];
            newsListVC.feedLink = self.feedLink;
            newsListVC.passedLatestNews = self.sectionContent;
            [newsListVC setType:SpecialReportType];
            newsListVC.eklusifEntitle = self.eklusifEntitle;
            newsListVC.titleForCategory = self.sectionTitleForCategory;

            
            switch (self.screenType) {
                case OthersNewsPagerScreenType:
                    
                    break;
                case LatestNewsNewsPagerScreenType:
                    
                    break;
                case CANewsPagerScreenType:
                    
                    break;
                case SpecialReportNewsPagerScreenType:
                    newsListVC.screenType = SpecialReportNewsListScreenType;
                    break;
                case NewsNewsPagerScreenType:
                    
                    break;
                case MenuNewsPagerScreenType:
                    
                    break;
            }
            

            return newsListVC;
            
        }
    }
    
    return nil;
}

- (NSNumber *)tabHeight {
    return [NSNumber numberWithFloat:29.0f];
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerTabsView:
            return [UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value{
    
    switch (option) {
        case ViewPagerOptionTabOffset:
            return 30.0f;
        default:
            return value;
    }

}




- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self setNeedsReloadOptions];
    if (self.isTherePopOver) {
        
        if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
            int ctr = [self.menuArrayList count];
            [self.popOverMenu dismissPopoverAnimated: YES];
            self.popOverMenu = nil;
            
            [self instantiateUIPopoverController];
            [self.popOverMenu setPopoverContentSize:CGSizeMake(320, 560+(40*ctr))];// for ios 7
            self.popOverMenu.contentViewController.preferredContentSize = CGSizeMake(320, 560+(40*ctr));
            [self.popOverMenu presentPopoverFromRect:CGRectMake(453,7, 320, 560+(40*ctr)) inView:self.view permittedArrowDirections:0 animated:YES];
            [self.dimView setFrame:CGRectMake(0, 0, 768, 1024)];
        }else{
            int ctr =  ([self.menuArrayList count]+2-1)/2;
            [self.popOverMenu dismissPopoverAnimated: YES];
            self.popOverMenu = nil;
            
            [self instantiateUIPopoverController];
            [self.popOverMenu setPopoverContentSize:CGSizeMake(400, 440+(ctr*40))];// for ios 7
            self.popOverMenu.contentViewController.preferredContentSize =CGSizeMake(400, 440+(ctr*40));
            [self.popOverMenu presentPopoverFromRect:CGRectMake(702,7, 400, 440+(ctr*40)) inView:self.view permittedArrowDirections:0 animated:YES];
            [self.dimView setFrame:CGRectMake(0, 0, 1024, 768)];
        

        }
    }

}
#pragma mark TMIPADMenuViewControllerDelegate
-(void)didTapItemAtMenu{
    [self.dimView removeFromSuperview];
    [self.popOverMenu dismissPopoverAnimated: YES];
    [self.menubutton setSelected:NO];
    self.isTherePopOver = NO;
}


#pragma mark private Function
- (void)instantiateUIPopoverController{
    TMIPADMenuViewController *dbvc = (TMIPADMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"menuStoryboardId"];
    dbvc.delegate = self;
    dbvc.ecSlidingVC = self.slidingViewController;
    self.popOverMenu = [[UIPopoverController alloc] initWithContentViewController:dbvc];
    self.popOverMenu.delegate = self;
    
    
}

- (void)didTapMenu:(UIButton *)sender {
    
    UIButton *button =(UIButton *)sender;
      [[AnalyticManager sharedInstance] trackMenuTab];
    [[LibraryAPI sharedInstance] getCAWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
        self.menuArrayList = array;
        
        if(self.popOverMenu == nil){
            [self instantiateUIPopoverController];
        }
        self.dimView = [[UIView alloc]initWithFrame:self.parentViewController.view.bounds];
        [self.dimView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
        [self.parentViewController.view addSubview:self.dimView];
        if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
            int ctr = [self.menuArrayList count];
            [self.popOverMenu setPopoverContentSize:CGSizeMake(320, 560+(40*ctr))];// for ios 7
            self.popOverMenu.contentViewController.preferredContentSize =CGSizeMake(320, 560+(40*ctr));
            [self.popOverMenu presentPopoverFromRect:CGRectMake(453,7, 320, 560+(40*ctr)) inView:self.view permittedArrowDirections:0 animated:YES];
            
        }else{
            int ctr =  ([self.menuArrayList count]+2-1)/2;
            [self.popOverMenu setPopoverContentSize:CGSizeMake(400, 440+(ctr*40))];// for ios 7
            self.popOverMenu.contentViewController.preferredContentSize =CGSizeMake(400, 440+(ctr*40));
            [self.popOverMenu presentPopoverFromRect:CGRectMake(702,7, 400, 440+(ctr*40)) inView:self.view permittedArrowDirections:0 animated:YES];
        }
        
        [button setSelected:YES];
        self.isTherePopOver = YES;
    }];
}
#pragma  mark UIPopOverDelegate
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    [self.menubutton setSelected:NO];
    [self.dimView removeFromSuperview];
     self.isTherePopOver = NO;
}







/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
