//
//  UIImageView+Helpers.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/22/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "UIImageView+Helpers.h"

@implementation UIImageView (Helpers)

- (void) setBorder
{
    [self.layer setBorderWidth:1.0];
    [self.layer setBorderColor:[[UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1] CGColor]];
    
}

- (void) hideBorder
{
    [self.layer setBorderWidth:0.0];
    [self.layer setBorderColor:[[UIColor clearColor] CGColor]];
    
}
@end
