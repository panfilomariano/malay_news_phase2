//
//  TMbigCurrentAffairsTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMbigCurrentAffairsTableViewCell : UITableViewCell
@property (nonatomic ,weak) IBOutlet UIImageView *imageview;
@end
