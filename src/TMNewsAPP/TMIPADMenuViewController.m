//
//  TMIPADMenuViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/24/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMIPADMenuViewController.h"
#import "TMMenuMainCollectionViewCell.h"
#import "TMMenuSubCollectionViewCell.h"
#import "Util.h"
#import "LibraryAPI.h"
#import "Constants.h"
#import "TMMainPageViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMNewsPagerViewController.h"
#import "RadioManager.h"
#import "TMBookmarkiPadViewController.h"
#import "TMSettingsiPadViewController.h"
#import "TMWebViewController.h"
#import "QPSplitViewController.h"
#import "TMSearchViewController.h"
#import "AnalyticManager.h"
#import "TMNavigationController.h"
#import "UIView+Toast.h"

@interface TMIPADMenuViewController ()
@property (nonatomic, weak) IBOutlet UICollectionView *collectiionView;
@property (nonatomic, weak) IBOutlet UIButton *searchButton;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, weak) IBOutlet UIButton *settingsButton;
@property (nonatomic, weak) IBOutlet UIButton *stationSwitchButton;
@property (nonatomic, weak) IBOutlet UIButton *radioPlayPauseButton;
@property (nonatomic, weak) IBOutlet UILabel *liveradiotext1;
@property (nonatomic, weak) IBOutlet UILabel *liveradiotext2;
@property (nonatomic, weak) IBOutlet UISwitch *onOffSwitch;
@property (nonatomic, strong) NSArray *latestNewsList;
@property (nonatomic, strong) NSMutableArray *newsList;
@property (nonatomic, strong) NSArray *caList;
@property (nonatomic, strong) NSString *radioURL;
@property (nonatomic) BOOL isfromMenu;

@end

@implementation TMIPADMenuViewController
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _latestNewsList = [[NSArray alloc] init];
    _newsList = [[NSMutableArray alloc] init];
    _caList = [[NSArray alloc] init];
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    
    [[LibraryAPI sharedInstance] getNewsWithCompletionBlock:^(NSMutableArray *array) {
        self.newsList = [array mutableCopy];;
        for(NSDictionary *dict in array){
            NSString *type = [dict valueForKey:@"type"];
            if([type isEqualToString:@"special report"]){
                [self.newsList removeObjectIdenticalTo:dict];
                [self.newsList addObject:dict];
                break;
            }
        }
        
        [self.collectiionView reloadData];
    }];
    
    [[LibraryAPI sharedInstance] getCAWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
        self.caList = array;
        [self.collectiionView reloadData];
    }];
    
    
    [self.settingsButton addTarget:self action:@selector(didTapSettings) forControlEvents:UIControlEventTouchUpInside];
    [self.searchButton addTarget:self action:@selector(didTapSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.bookmarkButton addTarget:self action:@selector(didTapBookmarks) forControlEvents:UIControlEventTouchUpInside];
    
#if TAMIL
    [self.radioPlayPauseButton setHidden:YES];
    [self.stationSwitchButton setHidden:YES];
    [self.liveradiotext1 setHidden:NO];
    [self.liveradiotext2 setHidden:NO];
    [self.onOffSwitch setHidden:NO];
#else
    [self.radioPlayPauseButton setHidden:NO];
    [self.stationSwitchButton setHidden:NO];
    [self.liveradiotext1 setHidden:YES];
    [self.liveradiotext2 setHidden:YES];
    [self.onOffSwitch setHidden:YES];
#endif
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0;
    
#if TAMIL
    if([[RadioManager sharedInstance]isRadioPlaying]){
        [self.onOffSwitch setOn:YES];
    }else{
        [self.onOffSwitch setOn:NO];
    }
#else
    if([[RadioManager sharedInstance]isRadioPlaying]){
        [self.radioPlayPauseButton setSelected:YES];
    }else{
        [self.radioPlayPauseButton setSelected:NO];
    }
    
    if([RadioManager sharedInstance].radioStation == RIA){
        [self.stationSwitchButton setSelected:YES];
    }else if ([RadioManager sharedInstance].radioStation == WARNA){
        [self.stationSwitchButton setSelected:NO];
    }
#endif
    
     [self.collectiionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark <UICollectionViewDataSource>
- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
  //  UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor lightGrayColor];
    //cell.contentView.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f blue:237/255.0f alpha:0.5];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return [Constants TAB_TITTLES].count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(section == 1){
        return self.newsList.count+1;
    }else if (section == 2){
        return self.caList.count+1;
    }else{
        return 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *tittle = @"";
    if (indexPath.row == 0) {
        TMMenuMainCollectionViewCell *cell = (TMMenuMainCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"mainCell" forIndexPath:indexPath];
        
        cell.tittleLabel.text = [Constants TAB_TITTLES][indexPath.section];
        return cell;
    }else{
        TMMenuSubCollectionViewCell *cell = (TMMenuSubCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"subCell" forIndexPath:indexPath];
        
        if(indexPath.section == 1 && indexPath.row !=0){
            NSDictionary *newsDictionary = (NSDictionary *) self.newsList [indexPath.row-1];
            tittle = [newsDictionary valueForKey:@"title"];
            
        }else if (indexPath.section == 2 && indexPath.row !=0){
            Article *article = (Article *) [self.caList objectAtIndex:indexPath.row-1];
            tittle = article.tittle;
            
        }
        
        cell.tittleLabel.text = tittle;
        return cell;
    }
    return nil;
    
}

/*- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor grayColor];
}*/


#pragma mark <UICollectionViewDelegate>
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        if (indexPath.row == 0) {
            return CGSizeMake(self.view.frame.size.width, 45);
        }else{
            if (self.view.frame.size.width > 320){
                return CGSizeMake(self.view.frame.size.width/2,40);
            }else {
                return CGSizeMake(self.view.frame.size.width, 40);
                
            }
            
        }
        return CGSizeZero;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.isfromMenu = TRUE;
    if(indexPath.row == 0){
        TMNavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"mainPageNavigationID"];
        TMMainPageViewController *mainVc = (TMMainPageViewController *) [nav.viewControllers firstObject];
        mainVc.index =indexPath.section;
        self.ecSlidingVC.topViewController = nav;
        [self.ecSlidingVC resetTopViewAnimated:NO];
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapItemAtMenu)]) {
            [self.delegate didTapItemAtMenu];
        }
        if (self.isfromMenu){
            [[AnalyticManager sharedInstance] trackMenuCategoryWithCategoryName:[Constants MENU_TAGGING][indexPath.section]];
        }
        
    }else {
      

        if (indexPath.section == 1) {
            NSString *tittle = @"";
            NSDictionary *newsDictionary = (NSDictionary *) self.newsList [indexPath.row-1];
            tittle = [newsDictionary valueForKey:@"enTitle"];
            TMNavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"beritaListNavigationID"];
            self.ecSlidingVC.topViewController = nav;
            TMNewsPagerViewController *npvc = (TMNewsPagerViewController *)[nav.viewControllers firstObject];
            npvc.pagerTittle = [Constants NEWS_LISTING_TITTLE];
            npvc.screenType = NewsNewsPagerScreenType;
            npvc.newsPages = self.newsList;
            npvc.activeIndex = indexPath.row-1;
            [self.ecSlidingVC resetTopViewAnimated:NO];
            if (self.delegate && [self.delegate respondsToSelector:@selector(didTapItemAtMenu)]) {
                [self.delegate didTapItemAtMenu];
            }
            [[AnalyticManager sharedInstance] trackMenuCategoryWithCategoryName:tittle];
        }else if(indexPath.section == 2){
            
            Article *article = [self.caList objectAtIndex:indexPath.row-1];
            
            NSString *stringUrl = article.sectionURL;
            NSString *title = article.tittle;
            [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:article.feedlink  isForced:NO withCompletionBlock:^(NSMutableArray *array){
                
                if ([array count]) {
                    
                    Article *article = [array firstObject];
                    NSArray *sectionsArray = article.sections;
                    
                    TMNavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"beritaListNavigationID"];
                    self.ecSlidingVC.topViewController = nav;
                    TMNewsPagerViewController *npvc = (TMNewsPagerViewController *)[nav.viewControllers firstObject];
                    npvc.pagerTittle = title;
                    npvc.screenType = CANewsPagerScreenType;
                    npvc.newsPages= sectionsArray;
                    npvc.activeIndex = 0;
                    [self.ecSlidingVC resetTopViewAnimated:NO];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapItemAtMenu)]) {
                        [self.delegate didTapItemAtMenu];
                    }
                    [[AnalyticManager sharedInstance] trackMenuCategoryWithCategoryName:article.entittle];
                    npvc.enTitle = article.entittle;
                }
                
            }];
        }
    }
}

#pragma mark PrivateFunctions

- (void) didTapSettings
{
    TMSettingsiPadViewController *iPadSettings = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
    TMWebViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
    vc.urlString = Constants.URL_ABOUT;
    
    QPSplitViewController *splitVC = [[QPSplitViewController alloc] initWithLeftViewController:iPadSettings rightViewController:vc];
    if ([self.ecSlidingVC.topViewController isKindOfClass:[TMNavigationController class]]) {
        TMNavigationController *nav = (TMNavigationController *) self.ecSlidingVC.topViewController;

        [nav pushViewController:splitVC animated:NO];
        [self.ecSlidingVC resetTopViewAnimated:NO];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapItemAtMenu)]) {
            [self.delegate didTapItemAtMenu];
        }
    }
}

- (void) didTapSearch {
    TMSearchViewController *searchVc = [self.storyboard instantiateViewControllerWithIdentifier:@"searchViewController"];
    if ([self.ecSlidingVC.topViewController isKindOfClass:[TMNavigationController class]]) {
        TMNavigationController *nav = (TMNavigationController *) self.ecSlidingVC.topViewController;
        
        [nav pushViewController:searchVc animated:NO];
        [self.ecSlidingVC resetTopViewAnimated:NO];
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapItemAtMenu)]) {
            [self.delegate didTapItemAtMenu];
        }
    }
    
}

- (void) didTapBookmarks {
    TMBookmarkiPadViewController *bmiPadVc = [self.storyboard instantiateViewControllerWithIdentifier:@"bookmarkViewController"];
    if ([self.ecSlidingVC.topViewController isKindOfClass:[TMNavigationController class]]) {
        TMNavigationController *nav = (TMNavigationController *) self.ecSlidingVC.topViewController;
        
        [nav pushViewController:bmiPadVc animated:NO];
        [self.ecSlidingVC resetTopViewAnimated:NO];
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapItemAtMenu)]) {
            [self.delegate didTapItemAtMenu];
        }
    }
}
#pragma  mark IBActions
-(IBAction)didTapRadioPlayPausebutton:(id)sender{
    
    UIButton *button = (UIButton *) sender;
    [button setSelected:button.isSelected?NO:YES];
    if(button.isSelected){
        [self.view makeToast:[Constants PLAY_RADIO]];
        [[RadioManager sharedInstance] play];
      
    }else{
        [[RadioManager sharedInstance] stop];
    }
    
}

-(IBAction)didTapStaionSelectionButton:(id)sender{
    UIButton *button = (UIButton *) sender;
    [button setSelected:button.isSelected?NO:YES];
    
    if (button.isSelected) {
        [RadioManager sharedInstance].radioStation = RIA;
        
    }else{
        [RadioManager sharedInstance].radioStation = WARNA;
       
    }
    
    if([self.radioPlayPauseButton isSelected]){
        [[RadioManager sharedInstance] play];
    }
    
}
-(IBAction)didSwitchRadio:(id)sender{
    [RadioManager sharedInstance].radioStation = OLI;
    UISwitch *button = (UISwitch *) sender;
    if(button.isOn){
        [self.view makeToast:[Constants PLAY_RADIO]];
        [[RadioManager sharedInstance] play];
    }else{
        [[RadioManager sharedInstance] stop];
    }
}

#pragma mark Notif Observers

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.collectiionView reloadData];
}

@end
