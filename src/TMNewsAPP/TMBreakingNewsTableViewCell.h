//
//  TMBreakingNewsTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "SMPageControl.h"
@protocol TMBreakingNewsTableViewCellDelegate <NSObject>
@optional
-(void)closeBreakingNews;
@end

@interface TMBreakingNewsTableViewCell : UITableViewCell
@property (nonatomic, weak) id <TMBreakingNewsTableViewCellDelegate> delegate;
@property (nonatomic, weak) IBOutlet SwipeView *bnSwipeview;
@property (nonatomic, weak) IBOutlet UIButton *closeBtn;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;

@end
