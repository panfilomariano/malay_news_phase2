//
//  NSString+PercentEscapes.m
//  EightDays
//
//  Created by Payne Chu on 1/2/14.
//  Copyright (c) 2014 PI Entertainment Limited. All rights reserved.
//

#import "NSString+PercentEscapes.h"

#define PRECENT_ENCODING_CHARACTERS @"!*'();:@&=+$,/?%#[]|§"

@implementation NSString(PercentEscapes)

- (NSString *)stringByAddingPercentEscapes
{
    return (__bridge NSString *)(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)(self), NULL, (__bridge CFStringRef)(PRECENT_ENCODING_CHARACTERS), kCFStringEncodingUTF8));
}

- (NSString *)stringByReplacingPercentEscapes
{
    return (__bridge NSString *)(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (__bridge CFStringRef)(self), (__bridge CFStringRef)(PRECENT_ENCODING_CHARACTERS), kCFStringEncodingUTF8));
    
}

@end
