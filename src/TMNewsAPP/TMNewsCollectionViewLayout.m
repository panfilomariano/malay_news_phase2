//
//  TMNewsCollectionViewLayout.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/29/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMNewsCollectionViewLayout.h"

@interface TMNewsCollectionViewLayout ()

@property (nonatomic) NSMutableDictionary *layouts;
@property (nonatomic) CGFloat bottomline;
@property (nonatomic) CGFloat viewHeight;
@property (nonatomic) NSMutableDictionary *placedIndexPath;
@property (nonatomic) NSIndexPath *lastIndexPathPlaced;
@property (nonatomic) BOOL beforePrepareLayout;
@property (nonatomic) UIEdgeInsets itemInsets;

@end

@implementation TMNewsCollectionViewLayout

- (id)init {
    if((self = [super init]))
        [self initialize];
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initialize];
    }
    return self;
}

- (void) initialize {
    
    _placedIndexPath = [NSMutableDictionary dictionary];
    _lastIndexPathPlaced = nil;
    _bottomline = 0.f;
    _viewHeight = 0.f;
    _beforePrepareLayout = NO;
    
}

#pragma mark Overrides
- (void) prepareLayout {
    [super prepareLayout];
    
    NSIndexPath *lastCellIndexPath = nil;
    _beforePrepareLayout = YES;
    NSInteger nSections = [self.collectionView numberOfSections];
    for (int j=0; j<nSections; j++) {
        NSInteger nRows = [self.collectionView numberOfItemsInSection:j];
        for (int i=0; i<nRows; i++) {
            lastCellIndexPath = [NSIndexPath indexPathForRow:i inSection:j];
        }
    }
    
    [self fillBlocksToIndexPath:lastCellIndexPath];
}

- (CGSize) collectionViewContentSize {
    return CGSizeMake([[_layouts valueForKey:@"maxWidth"] floatValue], _viewHeight);
}

- (NSArray *) layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray* attributes = [NSMutableArray array];
    for(NSInteger i=0 ; i < self.collectionView.numberOfSections; i++) {
        for (NSInteger j=0 ; j < [self.collectionView numberOfItemsInSection:i]; j++) {
            NSIndexPath* indexPath = [NSIndexPath indexPathForItem:j inSection:i];
            [attributes addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
        }
    }
    
    return attributes;
    /*
    NSIndexPath *lastCellIndexPath = nil;
    for (UICollectionViewCell *cell in [self.collectionView visibleCells]) {
        NSIndexPath* indexpath = [self.collectionView indexPathForCell:cell];
        if (!lastCellIndexPath) {
            lastCellIndexPath = indexpath;
        }
        if (indexpath.section > lastCellIndexPath.section || indexpath.row > lastCellIndexPath.row) {
            lastCellIndexPath = indexpath;
        }
    }
    if (![[_placedIndexPath allKeys] containsObject:lastCellIndexPath]) {
        [self fillBlocksToIndexPath:lastCellIndexPath];
    }
    
    NSMutableSet* attributes = [NSMutableSet set];
    for (NSIndexPath* path in [_placedIndexPath allKeys]) {
        [attributes addObject:[self layoutAttributesForItemAtIndexPath:path]];
    }
    
    return [attributes allObjects];*/
}

- (UICollectionViewLayoutAttributes *) layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UIEdgeInsets insets = UIEdgeInsetsZero;
    if([self.delegate respondsToSelector:@selector(insetsForItemAtIndexPath:)])
        insets = [self.delegate insetsForItemAtIndexPath:indexPath];
    
    if (![[_placedIndexPath allKeys] containsObject:indexPath]) {
        [self fillBlocksToIndexPath:indexPath];
        
    }
    
    CGRect frame = [[_placedIndexPath objectForKey:indexPath] CGRectValue];
    UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    attributes.frame = UIEdgeInsetsInsetRect(frame, insets);
    return attributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return !(CGSizeEqualToSize(newBounds.size, self.collectionView.frame.size));
}

- (void)prepareForCollectionViewUpdates:(NSArray *)updateItems {
    [super prepareForCollectionViewUpdates:updateItems];
    
    for(UICollectionViewUpdateItem* item in updateItems) {
        if(item.updateAction == UICollectionUpdateActionInsert || item.updateAction == UICollectionUpdateActionMove) {
            [self fillBlocksToIndexPath:item.indexPathAfterUpdate];
        }
    }
}

- (void) invalidateLayout {
    [super invalidateLayout];
    
    [_layouts setObject:[NSNumber numberWithInt:0] forKey:@"selected"];
    for (NSMutableDictionary* layout in [_layouts valueForKey:@"layouts"]) {
        if ([[layout valueForKey:@"filled"] count] == 0) {
            
        } else {
            [[layout valueForKey:@"filled"] addObjectsFromArray:[layout valueForKey:@"available"]];
            [layout setObject:[layout valueForKey:@"filled"] forKey:@"available"];
            [layout setObject:[NSMutableArray array] forKey:@"filled"];
        }
    }
    [self resetPosition];
}

#pragma mark - API

//Only be called before prepareLayout is called
- (void) addLayoutWithRowWidth:(CGFloat)rowWidth rowHeight:(CGFloat)rowHeight layouts:(NSArray *)layouts {
    
    //if (_beforePrepareLayout) {
       // return;
    //}
    
    CGFloat maxWidth = [[_layouts valueForKey:@"maxWidth"] floatValue];
    if (maxWidth < rowWidth) {
        [_layouts setObject:[NSNumber numberWithFloat:rowWidth] forKey:@"maxWidth"];
    }
    NSMutableDictionary* newLayout = [NSMutableDictionary dictionary];
    [newLayout setObject:[NSNumber numberWithFloat:rowWidth] forKey:@"rowWidth"];
    [newLayout setObject:[NSNumber numberWithFloat:rowHeight] forKey:@"rowHeight"];
    [newLayout setObject:[NSMutableArray array] forKey:@"filled"];
    [newLayout setObject:[NSMutableArray arrayWithArray:layouts] forKey:@"available"];
    
    [[_layouts valueForKey:@"layouts"] addObject:newLayout];
    
}

//Only be called before prepareLayout is called
- (void) clearLayout {
    //if (_beforePrepareLayout) {
       // return;
   // }
    _layouts = [NSMutableDictionary dictionary];
    [_layouts setObject:[NSNumber numberWithInt:0 ] forKey:@"selected"];
    [_layouts setObject:[NSNumber numberWithFloat:0.f] forKey:@"maxWidth"];
    [_layouts setObject:[NSMutableArray array] forKey:@"layouts"];
}

#pragma mark - helper method


- (BOOL) placeBlockAtIndexPath: (NSIndexPath *)indexPath {
    
    
    //already placed
    if ([[_placedIndexPath allKeys] containsObject:indexPath]) {
        return YES;
    }
    
    NSUInteger layoutIndex = [[_layouts objectForKey:@"selected"] intValue];
    if (self.delegate && [self.delegate respondsToSelector:@selector(layoutIndexAtIndexPath:)]) {
        layoutIndex = [self.delegate layoutIndexAtIndexPath:indexPath];
    }
    
    NSMutableDictionary* layout = [_layouts valueForKey:@"layouts"][layoutIndex];

    NSMutableArray *avaiable = [layout valueForKey:@"available"];
    NSMutableArray *filled = [layout valueForKey:@"filled"];
    
    if ([filled count] == 0) {
        _viewHeight += [[layout valueForKey:@"rowHeight"] floatValue];
    }
    
    CGRect selectedLayout = [[avaiable objectAtIndex:0] CGRectValue];
    [avaiable removeObjectAtIndex:0];
    //NSLog(@"%@", NSStringFromCGRect(selectedLayout));
    
    [filled addObject:[NSValue valueWithCGRect:selectedLayout]];
    
    //adjust top value of frame and place block for indexPath
    CGRect frame = CGRectMake(selectedLayout.origin.x, selectedLayout.origin.y+_bottomline, selectedLayout.size.width, selectedLayout.size.height);
    [_placedIndexPath setObject:[NSValue valueWithCGRect:frame]  forKey:indexPath];
    _lastIndexPathPlaced = indexPath;
    
    
    //current row is fullfilled
    if ([avaiable count] == 0) {
        _bottomline = _bottomline + [[layout objectForKey:@"rowHeight"] floatValue];
        
        [layout setValue:filled forKey:@"available"];
        [layout setValue:[NSMutableArray array] forKey:@"filled"];
        if (![_layouts valueForKey:@"layouts"])
            NSAssert(nil, @"pls. make sure you have layouts");
        
        NSUInteger next = (layoutIndex + 1) % [[_layouts valueForKey:@"layouts"] count];
        [_layouts setObject:[NSNumber numberWithInteger:next] forKey:@"selected"];
    }
    
    
    return YES;
}

- (void) fillBlocksToIndexPath: (NSIndexPath *) path {
    
    NSInteger numOfSections = [self.collectionView numberOfSections];
    for (NSInteger section=self.lastIndexPathPlaced.section; section < numOfSections; section++) {
        NSInteger numOfRows = [self.collectionView numberOfItemsInSection:section];
        
        for (NSInteger row = (!self.lastIndexPathPlaced? 0 : self.lastIndexPathPlaced.row + 1); row < numOfRows; row++) {
            
            //exit condition : indexpath passed
            if (section >= path.section && row > path.row) {
                //TO DO : Reset Layouts Dictionary
                return;
            }
            
            NSIndexPath * currentIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
            
            if ([self placeBlockAtIndexPath:currentIndexPath]) {
                self.lastIndexPathPlaced = currentIndexPath;
            }
        }
    }
}

- (void) resetPosition {
    _lastIndexPathPlaced = nil;
    _bottomline = 0.f;
    _viewHeight = 0.f;
    _placedIndexPath = [NSMutableDictionary dictionary];
}


@end
