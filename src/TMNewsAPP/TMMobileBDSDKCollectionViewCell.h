//
//  TMMobileBDSDKCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Ace Agtang on 5/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TMMobileBDSDKCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIView *adView;

@end
