//
//  TMNewsCollectionViewLayout.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/29/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMNewsCollectionViewLayoutDelegate <UICollectionViewDelegate>

@optional
- (UIEdgeInsets) insetsForItemAtIndexPath: (NSIndexPath *)indexPath;
- (NSUInteger) layoutIndexAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMNewsCollectionViewLayout : UICollectionViewLayout

@property (nonatomic, weak) IBOutlet NSObject<TMNewsCollectionViewLayoutDelegate>* delegate;

- (void) addLayoutWithRowWidth:(CGFloat) rowWidth rowHeight:(CGFloat) rowHeight layouts:(NSArray*) layouts;
- (void) clearLayout;

@end
