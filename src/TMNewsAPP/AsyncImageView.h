//
//  AsyncImageView.h
//  EightDays
//
//  Created by Wong Johnson on 4/12/13.
//  Copyright (c) 2013 PI Entertainment Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsyncImageView;

@protocol AsyncImageViewDelegate<NSObject>
@optional
- (UIImage *)asyncImageView:(AsyncImageView *)asyncImageView didLoadData:(NSData *)data fromCache:(BOOL)cached;
- (void)didLoadAsyncImageView:(AsyncImageView *)asyncImageView fromCache:(BOOL)cached;
- (void)clickedAsyncImageView:(AsyncImageView *)asyncImageView;

@end

@interface AsyncImageView : UIView
{
    NSURLConnection* _connection;
    NSMutableData* _data;
    NSURL *_url;
    UIImage *_spinnerImage;
    UIActivityIndicatorView *_indicator;
}

@property (nonatomic, strong) NSObject *userData;
@property (nonatomic, readonly, strong) UIImageView *imageView;
@property (nonatomic, weak) IBOutlet id<AsyncImageViewDelegate> delegate;
@property (nonatomic) UIActivityIndicatorViewStyle indicatorStyle;
@property (nonatomic, copy) NSString *prefix;
@property (nonatomic) BOOL fromCache;

- (void)setSpinnerImage:(UIImage*)image;
- (void)loadImageFromURL:(NSURL*)url fromCache:(BOOL)fromCache;
- (void)clearImage;

@end

