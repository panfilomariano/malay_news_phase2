//
//  TMLandingPageViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/29/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMLandingPageViewController.h"
#import "TMNewsCollectionViewLayout.h"
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "TMSquareCollectionViewCell.h"
#import "TMSmallCollectionViewCell.h"
#import "NSString+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+Helpers.h"
#import "TMSwipeCollectionViewCell.h"
#import "Util.h"
#import "SMPageControl.h"
#import "TMNewsArticleViewController.h"
#import "TMNewsDetailsPagerViewController.h"
#import "Constants.h"
#import "TMNewsPagerViewController.h"
#import "Reachability.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AppDelegate.h"
#import "TMPhotoBrowserViewController.h"
#import "TMiPADVideoDetailsViewController.h"
#import "Media.h"
#import "TMWebViewController.h"
#import "TMSquareBannerCollectionViewCell.h"
#import "AnalyticManager.h"
#import "UIView+Toast.h"
#import "MobileBDAdViewDelegate.h"
#import "MobileBDAdView.h"
#import "MobileBDConfigs.h"
#import "MobileBDSDK.h"
#import "TMMobileBDSDKCollectionViewCell.h"

@interface TMLandingPageViewController () <TMNewsCollectionViewLayoutDelegate, UICollectionViewDataSource, UICollectionViewDelegate, TMSquareCollectionViewCellDelagate, TMSmallCollectionViewCellDelagate, SwipeViewDataSource, SwipeViewDelegate, TMNewsDetailsPagerViewControllerDelagate, GADBannerViewDelegate, GADAdSizeDelegate, MobileBDAdViewDelegate>

@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) SMPageControl *pageControl;
@property (nonatomic, strong) SMPageControl *breakingNewsPageControl;
@property (nonatomic, strong) SwipeView *breakingNewsSwipeView;
@property (nonatomic, strong) NSMutableArray *topNewsArray;
@property (nonatomic, strong) NSDictionary *advertorialDict;
@property (nonatomic) BOOL hasSponsoredAd;
@property (nonatomic, strong) NSMutableArray *latestNewsArray;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *breakingNewsList;
@property (nonatomic, strong) UINib *squareCell;
@property (nonatomic, strong) UINib *smallCell;
@property (nonatomic, strong) UINib *bigCell;
@property (nonatomic, strong) UINib *squareBannerCell;
@property (nonatomic, strong) UINib *mobileBDCell;
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, strong) UIView *connectionStatusView;
@property (nonatomic, strong) UIView *breakingNewsView;
@property (nonatomic, strong) GADBannerView * gADBannerView;
@property (nonatomic, strong) GADBannerView * gADBannerView2;
@property (nonatomic, strong) MobileBDAdView *mobileAdView;
@property (nonatomic, strong) TMSquareBannerCollectionViewCell *squareBannerAdCell;
@property (nonatomic, strong) TMSquareCollectionViewCell *squareAdViewCell;
@property (nonatomic, strong) UILabel *nonetworkLabel;



@property (nonatomic) BOOL hasBreakingNews;
@property (nonatomic) BOOL hasAds;
@property (nonatomic) BOOL hasPreviousContents;
@property (nonatomic) BOOL isPortrait;
@property (nonatomic) BOOL hasBDAd;
@property (nonatomic) dispatch_group_t group;

@end

int num = 0;


@implementation TMLandingPageViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _latestNewsArray = [[NSMutableArray alloc] init];
    _topNewsArray = [[NSMutableArray alloc] init];
    _bookmarks = [[NSMutableArray alloc] init];
    _breakingNewsList = [[NSMutableArray alloc] init];
    
    self.squareCell = [UINib nibWithNibName:@"SquareCollectionViewCell" bundle:nil];
    self.smallCell = [UINib nibWithNibName:@"LongCollectionViewCell" bundle:nil];
    self.bigCell = [UINib nibWithNibName:@"BigCollectionViewCell" bundle:nil];
    self.squareBannerCell = [UINib nibWithNibName:@"SquareBannerCollectionViewCell" bundle:nil];
    //self.mobileBDCell = [UINib nibWithNibName:@"TMMobileBDSDKCollectionViewCell" bundle:nil];
    
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.isPortrait = UIInterfaceOrientationIsPortrait(self.interfaceOrientation);
    
    TMNewsCollectionViewLayout* layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    [self loadLayout:layout forOrientation:self.interfaceOrientation];
    
    if (self.topNewsArray.count == 0) {
        [self.collectionView setContentInset:UIEdgeInsetsMake(9.0f, 0.0f, 0.0f, 0.0f)];
    } else {
        [self.collectionView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    }
    
    [self requestContentsWithForce:NO];
    if (self.hasBreakingNews) {
        [self updateBreakingNewsView];
    }
    
    
    
    [[AnalyticManager sharedInstance] trackLandingPage];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateContentsFromNotif)
                                                 name:@"reloadDataNotif"
                                               object:nil];
    [NSTimer scheduledTimerWithTimeInterval:6.0f target:self selector:@selector(slideBreakingNews) userInfo:nil repeats:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    
    [NSTimer scheduledTimerWithTimeInterval:Constants.BREAKING_NEWS_REQUEST_INTERVAL target:self selector:@selector(requestBreakingNews) userInfo:nil repeats:YES];
    
    self.connectionStatusView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30.0f)];
    [self.connectionStatusView setBackgroundColor:[UIColor blackColor]];
    
    [self.collectionView registerNib:self.squareCell  forCellWithReuseIdentifier:@"squareCell"];
    [self.collectionView registerNib:self.smallCell  forCellWithReuseIdentifier:@"smallCell"];
    [self.collectionView registerNib:self.bigCell  forCellWithReuseIdentifier:@"bigCell"];
    [self.collectionView registerNib:self.squareBannerCell  forCellWithReuseIdentifier:@"squareBannerCell"];
    //[self.collectionView registerNib:self.mobileBDCell forCellWithReuseIdentifier:@"MobileBDSDKCell"];
    [self.collectionView setContentInset:UIEdgeInsetsMake(9.0f, 0.0f, 0.0f, 0.0f)];
    self.hasBDAd = YES;
    self.hasAds = YES;
    //self.hasSponsoredAd = YES;
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:Constants.HAS_BREAKING_NEWS_KEY]) {
        [self requestBreakingNews];
    }
    
    [Util setRefreshControlWithTarget:self inView:self.collectionView withSelector:@selector(reloadData:)];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        self.isPortrait = YES;
    } else {
        self.isPortrait = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - TMNewsCollectionViewLayoutDelegate

- (UIEdgeInsets) insetsForItemAtIndexPath:(NSIndexPath *)indexPath {
    return UIEdgeInsetsZero;
}

- (NSUInteger) layoutIndexAtIndexPath: (NSIndexPath *)indexPath
{
    int itemsPerLayout = (self.isPortrait) ? 3 : 5;
    if (_topNewsArray.count) {
        if (indexPath.row < itemsPerLayout) {
            return self.hasBreakingNews ? 2 : 0;
        }
    } else {
        return self.hasBreakingNews ? 3 : 1;
    }
    
    return 1;
}


#pragma mark GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    if (!self.hasAds) {
        self.hasAds = YES;
        [self.collectionView reloadData];
    }
}

- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
    if (self.hasAds) {
        self.hasAds = NO;
        [self.collectionView reloadData];
    }
}

/*#pragma mark MobileBDAdViewDelegate
- (void)onMobileBDAdViewReady:(MobileBDAdView *)adView {
    if (!self.hasAds) {
        self.hasAds = YES;
        [self.collectionView reloadData];
    }
}

- (void)onMobileBDAdViewFailed:(MobileBDAdView *)adView
                     withError:(MobileBDError *)error {
    if (self.hasAds) {
        self.hasAds = NO;
        [self.collectionView reloadData];
    }
}*/


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    int addCount = 0;
    
    if(self.hasAds){

        if(self.isPortrait){
            if (_latestNewsArray.count > 30 ) {// because of add placements need to
                addCount += 3;
            } else if (_latestNewsArray.count > 18 ) {
                addCount += 2;
            } else if (_latestNewsArray.count > 6 ) {
                addCount += 1;
            }
        }else{
            if (_topNewsArray.count) {
                if (_latestNewsArray.count > 41) {// because of add placements need to
                    addCount += 3;
                } else if (_latestNewsArray.count > 25) {
                    addCount += 2;
                } else if (_latestNewsArray.count > 9) {
                    addCount += 1;
                }
            } else {
                if (_latestNewsArray.count > 40) {// because of add placements need to
                    addCount += 3;
                } else if (_latestNewsArray.count > 24) {
                    addCount += 2;
                } else if (_latestNewsArray.count > 8) {
                    addCount += 1;
                }
            }
        }
    }
/*#ifdef TAMIL
#else
    int bdIndex = self.topNewsArray.count ? 3 : 2;
    if (self.hasBDAd && _latestNewsArray.count + _topNewsArray.count > bdIndex) addCount++;
#endif*/
    int count = _latestNewsArray.count + addCount;
    
    if (_topNewsArray.count) {
        count = count + 1;
    }
    if (self.advertorialDict){
        count = count + 1;
    }

    return count;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     Identify what cell to use (e.g)
        swipeView == cellId 0
        300x250 == cellId 1
        longCell == cellId2
     */
    
    int cellId = 0;
    int itemsForLayout1 = 3;
    int itemsForLayout2 = 3;
    int itemsBigForLayout2 = 1;
    if (!self.isPortrait) {
        itemsForLayout1 = 5;
        itemsForLayout2 = 4;
        itemsBigForLayout2 = 2;
    }
    if (!_topNewsArray.count) itemsForLayout1 = 0;
    
    BOOL isArticleInLayout1 = indexPath.row < itemsForLayout1;
    
    if (isArticleInLayout1) {
        if (indexPath.row == 0) {
            cellId  = 0;
        } else {
            cellId  = 1;
        }
    } else {  // mean layout2
        int indexInsideLyaout = (indexPath.row - itemsForLayout1) % itemsForLayout2;
        if (indexInsideLyaout < itemsBigForLayout2) {
            cellId  = 1;
        } else {
            cellId = 2;
        }
    }


    

    UICollectionViewCell *cell = nil;
    switch (cellId) {
        case 0: {
            TMSwipeCollectionViewCell *swipeCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"bigCell" forIndexPath:indexPath];
            swipeCell.pageControl.numberOfPages = _topNewsArray.count;
            swipeCell.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1];
            swipeCell.pageControl.backgroundColor = [UIColor clearColor];
            swipeCell.pageControl.alpha = 1;
            swipeCell.pageControl.hidesForSinglePage = YES;
            swipeCell.pageControl.indicatorDiameter = 5.0f;
            swipeCell.pageControl.alignment = SMPageControlAlignmentRight;
            swipeCell.pageControl.indicatorMargin = 2.0f;
            self.pageControl = swipeCell.pageControl;
            swipeCell.swipeView.tag = 0;
            swipeCell.swipeView.delegate = self;
            swipeCell.swipeView.dataSource = self;
            [swipeCell.swipeView reloadData];
            
            cell = swipeCell;
            break;
        } case 1: {
            
            BOOL banner = NO;
            int articleIndex = indexPath.row;
            
    
            if (self.hasAds) {
                
                if (self.isPortrait) {
                    if ((indexPath.row == 6 ) || (indexPath.row == 18 ) || (indexPath.row == 30)) {
                        banner = YES;
                    }
                    
                    
                    if (indexPath.row > 30 ) {// because of add placements need to
                        articleIndex -= 3;
                    } else if (indexPath.row > 18 ) {
                        articleIndex -=2;
                    } else if (indexPath.row > 6 ) {
                        articleIndex -= 1;
                    }
                    
                } else {
                    if (_topNewsArray.count) {
                        if ((indexPath.row == 9) || (indexPath.row == 25) || (indexPath.row == 41)) {
                            banner = YES;
                        }
                        
                        if (indexPath.row > 41) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 25) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 9) {
                            articleIndex -= 1;
                        }
                    } else {
                        if ((indexPath.row == 8) || (indexPath.row == 24) || (indexPath.row == 40)) {
                            banner = YES;
                        }
                        
                        if (indexPath.row > 40) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 24) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 8) {
                            articleIndex -= 1;
                        }
                    }
                }
            }
            
            if (_topNewsArray.count) {
                articleIndex -= 1;
            }
            
            
            
            if (banner) {
                
                TMSquareBannerCollectionViewCell *bannerCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"squareBannerCell" forIndexPath:indexPath];
                BOOL loadFirstBanner = YES;
                GADBannerView *banner = nil;
                if (_topNewsArray.count) {
                    if (indexPath.row == 18 || indexPath.row == 25) {
                        loadFirstBanner = NO;
                    }
                } else {
                    if ((indexPath.row == 18) || (indexPath.row == 24)) {
                        loadFirstBanner = NO;
                    }
                }
                
                if (loadFirstBanner) {
                    if (!self.gADBannerView.superview) {
                        self.gADBannerView = [[GADBannerView alloc] initWithAdSize:GADAdSizeFromCGSize(CGSizeMake(300, 250))];
                        GADRequest *request = [GADRequest request];
                        request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                                                @"Simulator"
                                                ];
                        
                        self.gADBannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
                        self.gADBannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;
                        self.gADBannerView.autoloadEnabled = YES;
                        self.gADBannerView.delegate = self;
                        [self.gADBannerView loadRequest:request];
                        
                    }
                    banner = self.gADBannerView;
                } else {
                    if (!self.gADBannerView2.superview) {
                        self.gADBannerView2 = [[GADBannerView alloc] initWithAdSize:GADAdSizeFromCGSize(CGSizeMake(300, 250))];
                        GADRequest *request = [GADRequest request];
                        request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                                                @"Simulator"
                                                ];
                        
                        self.gADBannerView2.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
                        self.gADBannerView2.rootViewController = [AppDelegate sharedInstance].window.rootViewController;
                        self.gADBannerView2.autoloadEnabled = YES;
                        self.gADBannerView2.delegate = self;
                        [self.gADBannerView2 loadRequest:request];
                        
                    }
                    
                    banner = self.gADBannerView2;
                }
                
                
                [bannerCell addSubview:banner];
                return bannerCell;
                
            } else {
                TMSquareCollectionViewCell *squareCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"squareCell" forIndexPath:indexPath];
                int adIndex = 3;
                if (!self.isPortrait && !self.topNewsArray.count) {
                    adIndex = 4;
                }
                NSString *photoThumbnail = @"";
                if (self.latestNewsArray.count) {

                        if (self.advertorialDict && indexPath.row == adIndex) {

                            photoThumbnail = self.advertorialDict[@"thumbnail"];
                            squareCell.tittle.text = self.advertorialDict[@"title"];
                            squareCell.category.text = @"Tajaan";
                            squareCell.pubDate.text = self.advertorialDict[[@"pubDate" formatDate]];
                            [squareCell.bookmarkButton setHidden:YES];


                            
                        } else {

                            if (indexPath.row > adIndex && self.advertorialDict) {
                                articleIndex -= 1;
                            }

                            Article *article = (Article *)[self.latestNewsArray objectAtIndex:articleIndex];
                            squareCell.delegate = self;
                            squareCell.indexPath = indexPath;
                            squareCell.tittle.text = article.tittle;
                            squareCell.category.text = article.category;
                            squareCell.pubDate.text = [article.pubDate formatDate];
                            
                            if (article.mediagroupList) {
                                if (article.mediagroupList.count) {
                                    NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                                    photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                                    NSString *type = [mediaDictionary valueForKey:@"type"];
                                    if ([type isEqualToString:@"video"]) {
                                        [squareCell.playImage setHidden:NO];
                                    } else {
                                        [squareCell.playImage setHidden:YES];
                                    }
                                }
                            }
                            [squareCell.bookmarkButton setSelected:NO];
                            if (_bookmarks.count) {
                                for (Article *articleFromBookmark in _bookmarks) {
                                    if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                                        [squareCell.bookmarkButton setSelected:YES];
                                        break;
                                    }
                                }
                            }
        

                    }
                    
                    
                    [squareCell.thumbnail setBorder];
                    [squareCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                            placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                    
                    
                }
                
                cell = squareCell;
               
            }
            
            
            break;
        } case 2: {
            int articleIndex = indexPath.row;
           
            
            if (self.hasAds) {
                
                if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
                    if (indexPath.row > 30) {// because of add placements need to
                        articleIndex -= 3;
                    } else if (indexPath.row > 18) {
                        articleIndex -= 2;
                    } else if (indexPath.row > 6) {
                        articleIndex -= 1;
                    }
                }else{
                    if (_topNewsArray.count) {
                        if (indexPath.row > 41) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 25) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 9) {
                            articleIndex -= 1;
                        }
                    } else {
                        if (indexPath.row > 40) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 24) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 8) {
                            articleIndex -= 1;
                        }
                    }
                }
            }
            
            if (_topNewsArray.count) {
                articleIndex -= 1;
            }
            
            //if (self.advertorialDict){
           //     articleIndex
           // }
/*#ifdef TAMIL
#else
            int bDIndex = self.topNewsArray.count ? 3 : 2;
            if (self.hasBDAd && indexPath.row > bDIndex) {
                articleIndex -= 1;
            }
#endif*/
            
            TMSmallCollectionViewCell *smallCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"smallCell" forIndexPath:indexPath];
            if (self.latestNewsArray.count){
                int adIndex = 3;
                if (!self.isPortrait && !self.topNewsArray.count) {
                    adIndex = 4;
                }
                if (indexPath.row > adIndex && self.advertorialDict) {
                    articleIndex -= 1;
                }
                
                Article *article = (Article *)[self.latestNewsArray objectAtIndex:articleIndex];
                smallCell.delegate = self;
                smallCell.indexPath = indexPath;
                smallCell.tittle.text = article.tittle;
                smallCell.category.text = article.category;
                smallCell.pubDate.text = [article.pubDate formatDate];
                
                NSString *photoThumbnail = @"";
                if (article.mediagroupList) {
                    if (article.mediagroupList.count) {
                        NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                        photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                        NSString *type = [mediaDictionary valueForKey:@"type"];
                        if ([type isEqualToString:@"video"]) {
                            [smallCell.playImage setHidden:NO];
                        } else {
                            [smallCell.playImage setHidden:YES];
                        }
                    }
                }
                
                [smallCell.bookmarkButton setSelected:NO];
                if (_bookmarks.count) {
                    for (Article *articleFromBookmark in _bookmarks) {
                        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                            [smallCell.bookmarkButton setSelected:YES];
                            break;
                        }
                    }
                }
                
                [smallCell.thumbnail setBorder];
                [smallCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                       placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            }
            
            
            cell = smallCell;
            break;
        }
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f blue:237/255.0f alpha:.5];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    int index = indexPath.row;
    
    
    if (_topNewsArray.count) {
        index--;
    }
/*#ifdef TAMIL
#else
    int bDIndex = self.topNewsArray.count ? 3 : 2;
    if (self.hasBDAd && indexPath.row > bDIndex) {
        index--;
    }
#endif*/
    
    
     if(self.hasAds){
         if(self.isPortrait){
             if (indexPath.row > 30) {// because of add placements need to
                 index -= 3;
             } else if (indexPath.row > 18) {
                 index -= 2;
             } else if (indexPath.row > 6) {
                 index -= 1;
             }
         }else{
             if (indexPath.row > 41) {// because of add placements need to
                 index -= 3;
             } else if (indexPath.row > 25) {
                 index -= 2;
             } else if (indexPath.row > 9) {
                 index -= 1;
             }
         }
    }
    
    
    int adIndex = 3;
    if (!self.isPortrait && !self.topNewsArray.count) {
        adIndex = 4;
    }
    if (indexPath.row > adIndex && self.advertorialDict) {
        index -= 1;
    }
    
    if (self.advertorialDict && indexPath.row == adIndex) {
        NSString *link = self.advertorialDict[@"link"];
        TMWebViewController *webView = (TMWebViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
        webView.urlString = link;
        [self.navigationController pushViewController:webView animated:YES];
    } else {
        TMNewsDetailsPagerViewController *pager = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsPagerViewControllerId"];
        pager.customDelegate = self;
        pager.newsArticles = self.latestNewsArray;
        pager.bookmarks = [_bookmarks mutableCopy];
        pager.activeTabIndex = index;
        pager.pagerTittle = @"";
        pager.screenType = LatestNewsScreen;
        [self.navigationController pushViewController:pager animated:YES];
    }
    
    
}

#pragma mark SwipeViewDataSOurce
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    if (swipeView.tag == 0)
        return _topNewsArray.count;
    else
        return self.breakingNewsList.count;
    return 0;
}

#pragma mark SwipeViewDelegate
-(void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index
{
    if (swipeView.tag == 0) {
        typedef enum : NSUInteger {
            PhotosType,
            VideosType,
            NewsType,
            EpisodeType,
            CAType,
            SpecialReportType
        } ArticleType;
        
        Article *article = (Article *)[_topNewsArray objectAtIndex:index];
        NSArray *mediaGroupArray = article.mediagroupList;
        NSString *feedLink = article.feedlink;
        NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:0];
        NSString *content = [mediaDictionary valueForKey:@"content"];
        NSString *type = article.type;
        NSString *sectionType = article.sectionType;
        NSString *stringUrl = article.sectionURL;
        
        
        ArticleType articletype;
        
        if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]) {
            NSLog(@"Selected PHOTOS TYPE");
            articletype = PhotosType;
        } else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]) {
            NSLog(@"Selected VIDEOS TYPE");
            articletype = VideosType;
        } else if ([type isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants NEWS_TYPE]]) {
            articletype = NewsType;
        } else if ([type isEqualToString:[Constants EPISODE_TYPE]] || [sectionType isEqualToString:[Constants EPISODE_TYPE]]) {
            articletype = EpisodeType;
        } else if ([type isEqualToString:[Constants CA_TYPE]] || [sectionType isEqualToString:[Constants CA_TYPE]]) {
            articletype = CAType;
        } else if ([type isEqualToString:[Constants SPECIAL_REPORT_TYPE]] || [sectionType isEqualToString:[Constants SPECIAL_REPORT_TYPE]]) {
            articletype = SpecialReportType;
        }
        
        switch (articletype) {
            case PhotosType:{
                TMPhotoBrowserViewController *photobrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
                photobrowser.article = article;
                photobrowser.isNewsPhoto = NO;
                photobrowser.modalPresentationStyle = UIModalPresentationOverFullScreen;
                [self presentViewController:photobrowser animated:NO completion:nil];
                photobrowser.view.alpha = .96;
                photobrowser.isTopNews = YES;
                break;
                
            }
            case VideosType:{
                Media *media = [Media new];
                media.tittle = article.tittle;
                media.mediaGroup = (NSMutableArray *)article.mediagroupList;
                media.guid = article.guid;
                media.pubDate = article.pubDate;
                media.sectionUrl = article.sectionURL;
                media.feedlink = article.feedlink;
                media.downloadTime = article.downloadTime;
                media.brief = article.brief;
                
                TMiPADVideoDetailsViewController *vdc = (TMiPADVideoDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videoDetailsVC"];
                vdc.index = index;
                vdc.videoBrowserList = [[NSMutableArray alloc]initWithObjects:media, nil];
                vdc.videoList = [[NSMutableArray alloc]initWithObjects:media, nil];
                [self.navigationController pushViewController:vdc animated:YES];
                [[AnalyticManager sharedInstance] trackOpenVideoWithGuid:media.guid andArticleTitle:media.tittle];
                break;
            }
            case NewsType:{
                BOOL isBookmark = [self checkBookmarkWithArticle:article];
                TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
                newsArticleVC.article = article;
                newsArticleVC.isBookmark = isBookmark;
                newsArticleVC.hasBarItems = YES;

                [self.navigationController pushViewController:newsArticleVC animated:YES];
                [[AnalyticManager sharedInstance] trackOpenNewsDetailScreenWithCategoryName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
                break;

            }
            case EpisodeType: {
                BOOL isBookmark = [self checkBookmarkWithArticle:article];
                TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
                newsArticleVC.article = article;
                newsArticleVC.isBookmark = isBookmark;
                newsArticleVC.hasBarItems = YES;
                
                [self.navigationController pushViewController:newsArticleVC animated:YES];
                
                break;
            }
            case CAType:{
                [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:feedLink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                    if ([array count]) {
                        Article *article = [array objectAtIndex:0];
                        TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                        newsPagerVC.pagerTittle = article.tittle;
                        newsPagerVC.newsPages = article.sections;
                        newsPagerVC.activeIndex = 0;
                        newsPagerVC.feedLink = article.feedlink;
                        newsPagerVC.screenType = CANewsPagerScreenType;
                        [self.navigationController pushViewController:newsPagerVC animated:YES];
                        [[AnalyticManager sharedInstance] trackCAOpenDetailScreenWithCategoryName:article.category andGuid:article.guid andArticleTitle:article.entittle];
                    }
                }];
                
                break;
            }
            case SpecialReportType:
                [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:feedLink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                    if ([array count]) {
                        Article *article = [array objectAtIndex:0];
                        TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                        newsPagerVC.pagerTittle = article.tittle;
                        newsPagerVC.newsPages = article.sections;
                        newsPagerVC.activeIndex = 0;
                        newsPagerVC.feedLink = article.feedlink;
                        newsPagerVC.eksklusifArticle = article;
                        newsPagerVC.screenType = SpecialReportNewsPagerScreenType;
                        [self.navigationController pushViewController:newsPagerVC animated:YES];
                    }
                }];
                [[AnalyticManager sharedInstance] trackOpenSpecialReportScreenWithCategoryName:article.tittle];
                break;
        }
    } else {
        Article *article = (Article *)[self.breakingNewsList objectAtIndex:index];
        NSString *link = article.link;
        if (![link isEqualToString:@""]){
            TMWebViewController *mwvc = (TMWebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            mwvc.title = [Constants LANDING_BREAKING_NEWS];
            mwvc.urlString = link;
            [self.navigationController pushViewController:mwvc animated:YES];
        }
    }
    
}


-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView {

    if (swipeView.tag == 0)
        self.pageControl.currentPage = swipeView.currentPage;
    else if(swipeView.tag == 1){
        self.breakingNewsPageControl.currentPage = swipeView.currentPage;
        [swipeView reloadData];
    }
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (swipeView.tag == 0) {
        float width = 441;
        float aheight = 9;
        float awidth = 16;
        
        if (!self.isPortrait) {
            width =  388;
            aheight = 9;
            awidth = 16;
        }
        
        
        UIImageView *imageView = nil;
        UILabel *tittleLabel = nil;
        UILabel *subTittleLabel = nil;
        UILabel *categoryLabel = nil;
        UILabel *pubDateLabel = nil;
        UIImageView *playOverlay = nil;
        
        if (view == nil) {
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 510)];
            [view setBackgroundColor:[UIColor colorWithRed:2.0/225.0 green:19.0/225.0 blue:29.0/225.0 alpha:1]];
            imageView = [[UIImageView alloc] init];
            imageView.tag = 1;
            
            tittleLabel = [[UILabel alloc] init];
            [tittleLabel setFont:[UIFont systemFontOfSize:24]];
            [tittleLabel setTextColor:[UIColor whiteColor]];
            [tittleLabel setNumberOfLines:2];
            [tittleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            [tittleLabel setBackgroundColor:[UIColor clearColor]];
            tittleLabel.tag = 2;
            
            subTittleLabel = [[UILabel alloc] init];
            [subTittleLabel setFont:[UIFont systemFontOfSize:16]];
            [subTittleLabel setTextColor:[UIColor colorWithRed:175/255.0f green:188/255.0f blue:195/255.0f alpha:1]];
            [subTittleLabel setNumberOfLines:4];
            [subTittleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            [subTittleLabel setBackgroundColor:[UIColor clearColor]];
            subTittleLabel.tag = 3;
            
            categoryLabel = [[UILabel alloc] init];
            [categoryLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:13]];
            [categoryLabel setTextColor:[UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1]];
            [categoryLabel setNumberOfLines:1];
            categoryLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [categoryLabel setBackgroundColor:[UIColor clearColor]];
            categoryLabel.tag = 4;
            
            pubDateLabel = [[UILabel alloc] init];
            [pubDateLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
            [pubDateLabel setTextColor:[UIColor colorWithRed:175/255.0f green:188/255.0f blue:195/255.0f alpha:1]];
            [pubDateLabel setNumberOfLines:1];
            pubDateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [pubDateLabel setBackgroundColor:[UIColor clearColor]];
            pubDateLabel.tag = 5;
            
            playOverlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play_overlay"]];
            playOverlay.tag = 6;
            
            [view addSubview:imageView];
            [view addSubview:tittleLabel];
            [view addSubview:subTittleLabel];
            [view addSubview:categoryLabel];
            [view addSubview:pubDateLabel];
            [view addSubview:playOverlay];
            
        } else {
            imageView = (UIImageView *)[view viewWithTag:1];
            tittleLabel = (UILabel *)[view viewWithTag:2];
            subTittleLabel = (UILabel *) [view viewWithTag:3];
            categoryLabel = (UILabel *) [view viewWithTag:4];
            pubDateLabel = (UILabel *) [view viewWithTag:5];
            playOverlay = (UIImageView *) [view viewWithTag:6];
        }
        
        Article *article = (Article *)[_topNewsArray objectAtIndex:index];
        
        NSArray *mediaGroupArray = article.mediagroupList;
        NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:0];
        NSString *photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
        NSString *type = [mediaDictionary valueForKey:@"type"];
        
        [view setFrame:CGRectMake(0, 0, width, 510)];
        
        float imageViewHeight = aheight/awidth * width;
        [imageView setFrame:CGRectMake(0, 0, width, imageViewHeight)];
        //[imageView setBorder];
        [imageView sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                     placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:index == 0 ? SDWebImageRefreshCached : 0];
        
        [playOverlay setFrame:CGRectMake(12, imageViewHeight + imageView.frame.origin.y - (8 + 20), 20, 20)];
        if ([type isEqualToString:@"video"]){
            playOverlay.hidden = NO;
        } else {
            playOverlay.hidden = YES;
        }
        
        CGSize tittleMaxSize = CGSizeMake(width - 48, 65);
        float tittleLabelHeight = [Util expectedHeightFromText:article.tittle withFont:[UIFont systemFontOfSize:27] andMaximumLabelSize:tittleMaxSize];//[Util expectedHeightFromText:article.tittle withFont:[UIFont systemFontOfSize:27] andMaximumLabelWidth:width - 24];
        //tittleLabel.backgroundColor = [UIColor redColor];
        [tittleLabel setFrame:CGRectMake(12, imageView.frame.origin.y + imageViewHeight + 17, width - 24, tittleLabelHeight)];
        tittleLabel.text = article.tittle;
        
        CGSize subtittleMaxSize = CGSizeMake(width - 24, 80);
        float subtittleLabelHeight = [Util expectedHeightFromText:article.brief withFont:[UIFont systemFontOfSize:16] andMaximumLabelSize:subtittleMaxSize];
        [subTittleLabel setFrame:CGRectMake(12, tittleLabel.frame.origin.y + tittleLabelHeight + 17, width - 24, subtittleLabelHeight+5)];
        subTittleLabel.text = article.brief;
        //subTittleLabel.backgroundColor = [UIColor yellowColor];
        
        float categoryLabelWidth = [Util expectedWidthFromText:article.category withFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14] andMaximumLabelHeight:15];
        [categoryLabel setFrame:CGRectMake(12, subTittleLabel.frame.origin.y + subtittleLabelHeight + 17, categoryLabelWidth, 15)];
        categoryLabel.text = article.category;
        
        float pubLabelWidth = [Util expectedWidthFromText:[article.pubDate formatDate] withFont:[UIFont fontWithName:@"HelveticaNeue" size:13] andMaximumLabelHeight:15];
        [pubDateLabel setFrame:CGRectMake(categoryLabelWidth + categoryLabel.frame.origin.x + 3, subTittleLabel.frame.origin.y + subtittleLabelHeight + 18.0f, pubLabelWidth, 15)];
        pubDateLabel.text = [article.pubDate formatDate];
    } else if(swipeView.tag == 1){
        [swipeView setWrapEnabled:YES];
        UILabel *label = nil;
        if (view == nil) {
            float swipeViewWidth = swipeView.frame.size.width;
            view = [[UIView alloc] initWithFrame:swipeView.bounds];
            [view setBackgroundColor:[UIColor clearColor]];
            label = [[UILabel alloc] initWithFrame:CGRectMake(8, view.frame.size.height - 25, swipeViewWidth-8, 30)];
            label.backgroundColor = [UIColor redColor];
            [label setNumberOfLines:22];
            [label setFont:[UIFont systemFontOfSize:19.0f]];
            label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [label setBackgroundColor:[UIColor clearColor]];
            [view addSubview:label];
        }else {
            label = (UILabel *)[view viewWithTag:22];
        }
        
        Article *article = (Article *)[self.breakingNewsList objectAtIndex:index];
        label.text = article.tittle;
    }
    
    return view;
}

#pragma mark TMSquareCollectionViewCellDelagate | TMSmallCollectionViewCellDelagate

- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    int index = 0;
    if (_topNewsArray.count) {
        index = indexPath.row - 1;
    }
    if(self.hasAds){
    
        if(self.isPortrait){
            if (indexPath.row > 30) {// because of add placements need to
                index -= 3;
            } else if (indexPath.row > 18) {
                index -= 2;
            } else if (indexPath.row > 6) {
                index -= 1;
            }
            
        }else{
            if (indexPath.row > 41) {// because of add placements need to
                index -= 3;
            } else if (indexPath.row > 25) {
                index -= 2;
            } else if (indexPath.row > 9) {
                index -= 1;
            }
        }
    }
    
    Article *article = (Article *) [_latestNewsArray objectAtIndex:index];
     Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
       
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            [_bookmarks removeObject:bookmarkArticle];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
            [_bookmarks addObject:article];
        }
        
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
    }
    if (!bookmarkArticle){
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    }
    
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

#pragma mark TMNewsDetailsPagerViewControllerDelagate
- (void) didTapBookmarkAtIndex:(NSNumber *)indexNumber
{
    int index = [indexNumber integerValue];
    int finalIndexPath = index;
    if (_topNewsArray.count) {
        finalIndexPath = index + 1;
    }
    
    if(self.hasAds){
        if(self.isPortrait){
            if (index > 30) {// because of add placements need to
                finalIndexPath -= 3;
            } else if (index> 18) {
                finalIndexPath -= 2;
            } else if (index> 6) {
                finalIndexPath -= 1;
            }
        }else{
            if (index > 41) {// because of add placements need to
                finalIndexPath -= 3;
            } else if (index> 25) {
                finalIndexPath -= 2;
            } else if (index> 9) {
                finalIndexPath -= 1;
            }
            
        }
    }
    
    Article *article = (Article *) [_latestNewsArray objectAtIndex:index];
    if ([self checkBookmarkForIndex:index]) {
        [_bookmarks removeObject:article];
    } else {
        [_bookmarks addObject:article];
    }
    
    if (!_bookmarks){
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:finalIndexPath inSection:0] ;
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

#pragma mark Notif Observers

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
        self.isPortrait = YES;
    else
        self.isPortrait = NO;
    
    TMNewsCollectionViewLayout *layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    [self loadLayout:layout forOrientation:toInterfaceOrientation];
    //[layout invalidateLayout];
    //[layout prepareLayout];
    [self checkNetworkStatus];
    [self.collectionView reloadData];
    
    if (self.hasBreakingNews) {
        [self updateBreakingNewsView];
    }
    
    
    
}


- (void) handleNetworkChanged: (NSNotification *)notification
{
    [self checkNetworkStatus];
}

#pragma mark Private Functions
-(void)slideBreakingNews{
    if([self.breakingNewsList count] > 1){
        [self.breakingNewsSwipeView scrollByNumberOfItems:1 duration:0];
        
    }
}

- (void)checkNetworkStatus {
    
    /*Reachability *networkReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
         [self displayNoNetwork];
    } else {
        [self dismissNoNetworkDisplay];
    }*/
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        [self.connectionStatusView removeFromSuperview];
        [self.nonetworkLabel removeFromSuperview];
        [self displayNoNetwork];
    } else {
        [self dismissNoNetworkDisplay];
    }
}

-(void)reloadData: (UIRefreshControl *)refreshFeed
{
    //self.firstAddOk = YES;
    //self.secondAddOk =YES;
    //self.thirdAddOk = YES;
    self.hasAds = YES;
    //self.gADBannerView1 = nil;
    //self.gADBannerView2 = nil;
    //self.gADBannerView3 = nil;
    
    //   [self requestAds];
    [self requestContentsWithForce:YES];
    if (refreshFeed) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        [refreshFeed endRefreshing];
        
    }
}
- (BOOL)checkBookmarkWithArticle: (Article*)article
{
    BOOL isBookmark = NO;
    for (Article *articleFromBookmark in _bookmarks) {
        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
            isBookmark = YES;
            break;
        }
    }
    
    return isBookmark;
}

- (BOOL)checkBookmarkForIndex: (NSUInteger)index
{
    Article *article = (Article *) [self.latestNewsArray objectAtIndex:index];
    BOOL isBookmark = NO;
    for (Article *articleFromBookmark in _bookmarks) {
        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
            isBookmark = YES;
            break;
        }
    }
    
    return isBookmark;
}

- (BOOL) checkPortrait
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            return YES;
        }
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            return NO;
        }
        case UIInterfaceOrientationUnknown:break;
    }
    
    return YES;
}

- (void) loadLayout:(TMNewsCollectionViewLayout *)layout forOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        [layout addLayoutWithRowWidth:768.f rowHeight:528.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 9, 441, 510.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(459, 9.0f, 300.f, 250.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(459, 270.0f, 300.f, 250.f)]]];
        
        [layout addLayoutWithRowWidth:768.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 0.0f, 300.f, 250.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 0.0f, 441, 121)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 130, 441, 121)]]];

        [layout addLayoutWithRowWidth:768.f rowHeight:587.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 67, 441, 510.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(459, 67.0f, 300.f, 250.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(459, 327.0f, 300.f, 250.f)]]];
        
        [layout addLayoutWithRowWidth:768.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 0.0f, 300.f, 250.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 0.0f, 441, 121)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 130, 441, 121)]]];
    } else {
        [layout addLayoutWithRowWidth:1024.f rowHeight:528.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 9, 388, 510.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(406, 9.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(715, 9.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(406, 270.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(715, 270.0f, 300.f, 250.f)]
                                                                        ]];
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 0, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(318, 0, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(627, 0, 388, 121)],
                                                                        [NSValue valueWithCGRect:CGRectMake(627.f, 130, 388, 121)]
                                                                        ]];
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:587.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 67, 388, 510.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(406, 67.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(715, 67.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(406, 327.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(715, 327.0f, 300.f, 250.f)]
                                                                        ]];
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 0, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(318, 0, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(627, 0, 388, 121)],
                                                                        [NSValue valueWithCGRect:CGRectMake(627.f, 130, 388, 121)]
                                                                        ]];
        
    }
    
    [self.collectionView reloadData];
}

- (void) updateContentsFromNotif
{
    [self requestContentsWithForce:YES];
    
}
- (void) requestContentsWithForce: (BOOL)forced
{
    if (!self.group)
        self.group = dispatch_group_create();
    
    if (!self.hasPreviousContents || forced)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self getBookmarkItems];
    [self getLatestNewsWithForce:forced];
    [self getTopNewsWithForce:forced];
    [self getAdvertorialWithForce:forced];
    
    
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        if (self.topNewsArray.count == 0) {
            [self.collectionView setContentInset:UIEdgeInsetsMake(9.0f, 0.0f, 0.0f, 0.0f)];
        } else {
            [self.collectionView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        }
        
        [self checkNetworkStatus];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        
        if (self.latestNewsArray.count || self.topNewsArray.count) {
            [self.collectionView reloadData];
            self.hasPreviousContents = YES;
        }
        
    });
}

-(void)getLatestNewsWithForce:(BOOL)isForced{
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance] getLatestNewsWithForce:isForced andCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            self.latestNewsArray = [array mutableCopy];
            //[self getAdvertorialWithForce:isForced];
        }
        dispatch_group_leave(self.group);
    }];
}

- (void)getAdvertorialWithForce: (BOOL)isForced {
    [[LibraryAPI sharedInstance] getAdvertorialWithForce:isForced andCompletionBlock:^(NSError *error, NSDictionary *dict) {
        if (!error) {

            self.advertorialDict = dict;
            //self.hasSponsoredAd = YES;
            [self.collectionView reloadData];

            
        }
    }];
}

-(void)getTopNewsWithForce:(BOOL)isForced{
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getTopNewsWithForce:isForced andCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            [_topNewsArray removeAllObjects];
            for (Article *article in array) {
                NSString *category = article.sectionName;
                if ([category isEqualToString:@"home"]){
                    [_topNewsArray addObject:article];
                }
            }
        }
        
        dispatch_group_leave(self.group);
    }];
    
}

-(void)getBookmarkItems
{
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.collectionView reloadData];
        }
        dispatch_group_leave(self.group);
    }];
    
}

-(void)displayNoNetwork {
    
    
    float screenWidth = 0;
    if (self.isPortrait) {
        screenWidth = 768;
    }else{
        screenWidth = 1024;
    }
    self.connectionStatusView.frame = CGRectMake(0.0f, -30.0f, screenWidth, 30.0f);
    
    self.nonetworkLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.connectionStatusView.frame.origin.x / 2, (self.connectionStatusView.frame.origin.y +  self.connectionStatusView.frame.size.height), self.connectionStatusView.frame.size.width, 13.0f)];
    self.nonetworkLabel.center = CGPointMake(self.connectionStatusView.center.x, self.connectionStatusView.center.y + 30.0f);
    [self.nonetworkLabel setFont:[UIFont systemFontOfSize:12.0f]];
    self.nonetworkLabel.text = [Constants DIALOG_NETWORK_MESSAGE];

    
    self.nonetworkLabel.textAlignment = NSTextAlignmentCenter;
    self.nonetworkLabel.textColor = [UIColor whiteColor];
    
    [self.connectionStatusView addSubview:self.nonetworkLabel];
    //[self.connectionStatusView addSubview:buttonClose];
    
    [self.view addSubview:self.connectionStatusView];
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.collectionView setContentInset:UIEdgeInsetsMake(30.0f, 0.0f, 0.0f, 0.0f)];
        [self.collectionView scrollRectToVisible:CGRectMake(0, 0, screenWidth, self.connectionStatusView.frame.size.height) animated:NO];
        self.connectionStatusView.center = CGPointMake(self.connectionStatusView.center.x, self.connectionStatusView.center.y + 30.0f);
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)dismissNoNetworkDisplay {
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.connectionStatusView.center = CGPointMake(self.connectionStatusView.center.x, self.connectionStatusView.center.y - 30.0f);
        if (self.topNewsArray.count == 0) {
            [self.collectionView setContentInset:UIEdgeInsetsMake(9.0f, 0.0f, 0.0f, 0.0f)];
        } else {
            [self.collectionView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        }
    } completion:^(BOOL finished) {
        [self.nonetworkLabel removeFromSuperview];
        [self.connectionStatusView removeFromSuperview];
    }];
}

- (void)didTapCloseConnectionCell
{
    [self dismissNoNetworkDisplay];
}

- (void)setUpBreakingNews
{
    float screenWidth = 0;
    if (self.isPortrait) {
        screenWidth = 768;
    }else{
        screenWidth = 1024;
    }
    
    self.breakingNewsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, -50.0f)];
    self.breakingNewsView.backgroundColor = [UIColor colorWithRed:1.0f green:212/255.0f blue:0/255.0f alpha:1];
    
    SwipeView *swipeView = [[SwipeView alloc] initWithFrame:self.breakingNewsView.bounds];
    swipeView.tag = 1;
    swipeView.backgroundColor = [UIColor clearColor];
    swipeView.delegate = self;
    swipeView.dataSource = self;
    self.breakingNewsSwipeView = swipeView;
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, self.breakingNewsView.frame.origin.y + 8, 20, 20)];
    label.tag = 2;
    label.font = [UIFont boldSystemFontOfSize:13.0f];
    [label setNumberOfLines:1];
    label.text = [Constants LANDING_BREAKING_NEWS];
    [label sizeToFit];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = 3;
    [button setImage:[UIImage imageNamed:@"icon_close_breakingnews" ] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didTapBreakingNewsCloseButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    SMPageControl *pageControl = [[SMPageControl alloc] init];
    pageControl.tag = 4;
    pageControl.numberOfPages = self.breakingNewsList.count;
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1.0];
    pageControl.pageIndicatorTintColor = [UIColor colorWithRed:208.0f/255.0f green:168.0/255.0 blue:38.0f/255.0 alpha:1];
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.alpha = 1;
    pageControl.hidesForSinglePage = YES;
    pageControl.indicatorDiameter = 5.0f;
    pageControl.alignment = SMPageControlAlignmentRight;
    pageControl.indicatorMargin = 2.0f;
    self.breakingNewsPageControl = pageControl;
    
    button.frame = CGRectMake(screenWidth - 38, self.breakingNewsView.frame.origin.y + 8, 30, 30);
    pageControl.frame = CGRectMake(8, self.breakingNewsView.frame.origin.y + self.breakingNewsView.frame.size.height - 8, self.breakingNewsView.frame.size.width - 16, 16);
    
    [self.breakingNewsView addSubview:swipeView];
    [self.breakingNewsView addSubview:label];
    [self.breakingNewsView addSubview:button];
    [self.breakingNewsView addSubview:pageControl];
    
    [self.breakingNewsView bringSubviewToFront:swipeView];
    [self.breakingNewsView bringSubviewToFront:button];
    //[self.view addSubview:self.breakingNewsView];
    [self.collectionView addSubview:self.breakingNewsView];
    
  
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.breakingNewsView.frame = CGRectMake(0, 0, screenWidth, 58.0f);
        [self.collectionView reloadData];
    } completion:^(BOOL finished) {
    }];
}

- (void)showBreakingNews
{
    if (self.hasBreakingNews) {
        [self updateBreakingNewsView];
        [self.breakingNewsSwipeView reloadData];
    } else {
        if (!self.breakingNewsView) {
            [self setUpBreakingNews];
        } else {
            float screenWidth = 0;
            if (self.isPortrait){
                screenWidth = 768;
            }else{
                screenWidth = 1024;
            }
            [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                //[self.collectionView setContentInset:UIEdgeInsetsMake(50.0f, 0.0f, 0.0f, 0.0f)];
                //[self.collectionView scrollRectToVisible:CGRectMake(0, 0, screenWidth, self.breakingNewsView.frame.size.height) animated:NO];
                self.breakingNewsView.frame = CGRectMake(0, 0, screenWidth, 50.0f);
                //self.breakingNewsView.center = CGPointMake(self.breakingNewsView.center.x, 50.0f);
                [self.collectionView reloadData];
            } completion:^(BOOL finished) {
            }];
        }
        
        self.hasBreakingNews = YES;
    }
}

- (void)updateBreakingNewsView
{
    float screenWidth = 0;
    if (self.isPortrait){
        screenWidth =768;
    }else{
        screenWidth =1024;
    }
    self.breakingNewsView.frame = CGRectMake(self.breakingNewsView.frame.origin.x, self.breakingNewsView.frame.origin.y, screenWidth, self.breakingNewsView.frame.size.height);
    UIButton *button = (UIButton *) [self.breakingNewsView viewWithTag:3];
    SMPageControl *pageControl = (SMPageControl *) [self.breakingNewsView viewWithTag:4];
    
    button.frame = CGRectMake(screenWidth - 38, button.frame.origin.y, 30, 30);
    pageControl.frame = CGRectMake(8, pageControl.frame.origin.y, self.breakingNewsView.frame.size.width - 16, 16);
}

- (void)didTapBreakingNewsCloseButton
{
    [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:Constants.HAS_BREAKING_NEWS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.breakingNewsView removeFromSuperview];
    self.hasBreakingNews = NO;
    //close breakingNews
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        //self.breakingNewsView.center = CGPointMake(self.breakingNewsView.center.x, self.connectionStatusView.center.y - 50.0f);
        self.breakingNewsView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, -58.0f);
        if (self.topNewsArray.count == 0) {
            [self.collectionView setContentInset:UIEdgeInsetsMake(9.0f, 0.0f, 0.0f, 0.0f)];
        } else {
            [self.collectionView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        }
        self.breakingNewsView = nil;
        [self.collectionView reloadData];
    } completion:^(BOOL finished) {
        //[self.breakingNewsView removeFromSuperview];
    }];
}

- (void) requestBreakingNews{
    
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        for(NSDictionary *dictionary in array){
            NSString *enTitle = [dictionary valueForKey:@"enTitle"];
            if ([enTitle isEqualToString:@"breaking news"]){
                NSString *breakingNewsUrl = [dictionary valueForKey:@"sectionURL"];
                [[LibraryAPI sharedInstance]getArticlesWithURL:breakingNewsUrl isForced:YES withCompletionBlock:^(NSMutableArray *array) {
                    
                    if([array count]){
                        Article *newArticle = [array firstObject];
                        NSString *oldArticleGUID = [[NSUserDefaults standardUserDefaults]objectForKey:Constants.BREAKING_NEWS_GUID];
                        if (oldArticleGUID) {
                            if(![newArticle.guid isEqualToString:oldArticleGUID]){
                                
                                [[NSUserDefaults standardUserDefaults]removeObjectForKey:Constants.BREAKING_NEWS_GUID];
                                [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:Constants.HAS_BREAKING_NEWS_KEY];
                                
                                [[NSUserDefaults standardUserDefaults]setObject:newArticle.guid forKey:Constants.BREAKING_NEWS_GUID];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                self.breakingNewsList = [array mutableCopy];
                                [self showBreakingNews];
                            }else{
                                if ([[NSUserDefaults standardUserDefaults]boolForKey:Constants.HAS_BREAKING_NEWS_KEY]) {
                                    self.breakingNewsList = [array mutableCopy];
                                    [self showBreakingNews];
                                }
                            }
                        }else{
                            [[NSUserDefaults standardUserDefaults]removeObjectForKey:Constants.BREAKING_NEWS_GUID];
                            [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:Constants.HAS_BREAKING_NEWS_KEY];
                            [[NSUserDefaults standardUserDefaults]setObject:newArticle.guid forKey:Constants.BREAKING_NEWS_GUID];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            self.breakingNewsList = [array mutableCopy];
                            [self showBreakingNews];
                        }
                        
                        
                    }else{
                        [self didTapBreakingNewsCloseButton];
                        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:Constants.HAS_BREAKING_NEWS_KEY];
                    }
                    
                }];
                break;
            }
        }
    }];
    
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
