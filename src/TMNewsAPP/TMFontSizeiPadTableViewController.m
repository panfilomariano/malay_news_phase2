//
//  TMFontSizeiPadTableViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 2/18/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMFontSizeiPadTableViewController.h"
#import "Data.h"
#import "Constants.h"

@interface TMFontSizeiPadTableViewController ()
@property (nonatomic, strong) NSArray *fontArray;
@property (nonatomic, strong) NSIndexPath *currentIndexPath;

@end

@implementation TMFontSizeiPadTableViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    self.fontArray = [[NSArray alloc] initWithObjects:[Constants SETTINGS_FONTSIZE_XSMALL], [Constants SETTINGS_FONTSIZE_SMALL], [Constants SETTINGS_FONTSIZE_MEDIUM], [Constants SETTINGS_FONTSIZE_LARGE], [Constants SETTINGS_FONTSIZE_XLARGE], nil];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fontArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *) [cell.contentView viewWithTag:1];
    label.text = self.fontArray [indexPath.row];
    
    UIImageView *checkmarkImageView = (UIImageView *) [cell.contentView viewWithTag:2];
    checkmarkImageView.hidden = YES;
    
    if ([Data sharedInstance].fontSizeType == FontSizeTypeDefault) {
        if (indexPath.row == 2)
            checkmarkImageView.hidden = NO;
    } else {
        int index = [Data sharedInstance].fontSizeType;
        if ([Data sharedInstance].fontSizeType  <= 2)
            index = [Data sharedInstance].fontSizeType - 1;
        
        if (indexPath.row == index) {
            checkmarkImageView.hidden = NO;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentIndexPath == indexPath) return;
    self.currentIndexPath = indexPath;
    
    switch (indexPath.row) {
        case 0: {
            [Data sharedInstance].fontSizeType = FontSizeTypeXSmall;
            break;
        } case 1: {
            [Data sharedInstance].fontSizeType = FontSizeTypeSmall;
            break;
        } case 2: {
            [Data sharedInstance].fontSizeType = FontSizeTypeDefault;
            break;
        } case 3: {
            [Data sharedInstance].fontSizeType = FontSizeTypeLarge;
            break;
        } case 4: {
            [Data sharedInstance].fontSizeType = FontSizeTypeXLarge;
            break;
        }
            
    }
    
    [tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66.0f;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
