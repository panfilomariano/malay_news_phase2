//
//  TMSwipeCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/30/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "SMPageControl.h"

@interface TMSwipeCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;

@end
