//
//  Util.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Article.h"

@interface Util : NSObject

+ (float) expectedHeightFromText:(NSString *)text withFont:(UIFont *)font andMaximumLabelWidth: (float)labelWidth;
+ (float) expectedWidthFromText:(NSString *)text withFont:(UIFont *)font andMaximumLabelHeight: (float)labelHeight;
+ (float) expectedHeightFromText:(NSString *)text withFont:(UIFont *)font andMaximumLabelSize: (CGSize)maxSize;
+ (NSString *) urlWithTokenFromURL:(NSString *)url;
+ (NSString *) hmac:(NSString *)plaintext withKey:(NSString *)key;
+ (NSString *) urlTokenForFeed: (NSString *)url;
+ (void)reloadWeatherWithCompletionBlock:(void (^)(NSError *error))completionBlock;
+ (BOOL) is_iPad;
+ (BOOL) isPortrait;
//+(BOOL)checkifPortrait;
+(UIRefreshControl *) setRefreshControlWithTarget:(UIViewController *)viewController  inView:(UIView *)view withSelector:(SEL)selector;
+ (UIColor *) headerColor;
+ (BOOL) needsUpgradeVersion: (NSString *)serverVersionStr;
+ (NSString *) urlWithTokenFromURL:(NSString *)url;
+ (NSString *) formatHtmlData: (NSString *)htmldata;
+ (NSString *) appVersion;
+ (BOOL)checkURLForPush: (NSString *)urlStr withNavigationType: (UIWebViewNavigationType)navigationType;
+ (BOOL)isVersion8Above;
+ (void)updateBookmarkWith: (Article *)article addedArticleSuccess: (void (^)(Article *addedBookmarkArticle))addedArticleSuccess removedArticleSuccess: (void (^)(Article *removedBookmarkArticle))removedArticleSuccess;
@end
