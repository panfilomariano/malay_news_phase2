//
//  TMiPadMainVideoCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/17/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMiPadMainVideoCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak)IBOutlet UILabel *tittle;
@property (nonatomic, weak)IBOutlet UILabel *pudDate;
@property (nonatomic, weak)IBOutlet UILabel *caption;
@property (nonatomic, weak)IBOutlet UIImageView *thumbnail;
@property (nonatomic, strong) IBOutlet UIView *view;
@property (nonatomic, strong) IBOutlet UILabel *ymiLabel;

@end
