//
//  TMVideosViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/8/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
@interface TMVideosViewController : UIViewController
@property (nonatomic) BOOL isFromMain;
@property (nonatomic, strong) NSString *sectionURL;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@property (nonatomic) BOOL isFromSpecialReport;
@property (nonatomic, strong) NSString *videosEksklusifTitle;
@end
