//
//  TMCAandSRCollectionViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/26/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMCAandSRCollectionViewCell.h"

@implementation TMCAandSRCollectionViewCell

-(IBAction)didTapBookmarkButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}


@end
