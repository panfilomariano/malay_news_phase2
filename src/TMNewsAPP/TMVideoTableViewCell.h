//
//  TMVideoTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/12/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMVideoTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *tittle;
@property (nonatomic, weak) IBOutlet  UILabel *date;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;

@end
