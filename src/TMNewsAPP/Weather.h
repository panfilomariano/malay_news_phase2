//
//  Weather.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/23/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject

@property (nonatomic, strong) NSString *period;
@property (nonatomic, strong) NSString *min;
@property (nonatomic, strong) NSString *max;
@property (nonatomic, strong) NSString *rh;
@property (nonatomic, strong) NSString *psi;
@property (nonatomic, strong) NSString *psi_3h;
@property (nonatomic, strong) NSString *psi_24h;
@property (nonatomic, strong) NSString *tide;
@property (nonatomic, strong) NSString *condition;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSNumber *lastBuildDate;
@property (nonatomic, strong) NSNumber *lastDownloadDate;


@end
