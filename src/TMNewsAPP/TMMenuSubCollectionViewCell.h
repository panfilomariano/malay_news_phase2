//
//  TMMenuSubCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/24/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMMenuSubCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UILabel *tittleLabel;
@end
