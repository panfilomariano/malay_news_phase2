//
//  MobileBDAdView.h
//  MobileBDSDK
//
//  Created by Le Vu on 22/1/15.
//  Copyright (c) 2015 Knorex. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MobileBDAdViewDelegate.h"

typedef NS_ENUM(NSUInteger, MobileBDAdViewState) {
    kMobileBDAdViewStateUnloaded,
    kMobileBDAdViewStateLoading,
    kMobileBDAdViewStateSuccessful,
    kMobileBDAdViewStateFailed
};

@interface MobileBDAdView : UIView

@property (nonatomic, strong) id<MobileBDAdViewDelegate> delegate;

@property (nonatomic, strong) NSString *adUnitId;
@property (nonatomic, strong) UIViewController *presentViewController;

- (void)loadAdViewFromAdUnitId:(NSString *)adUnitId;
- (MobileBDAdViewState)getState;

@end
