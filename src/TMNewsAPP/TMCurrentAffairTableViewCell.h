//
//  TMCurrentAffairTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"

@interface TMCurrentAffairTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@end
