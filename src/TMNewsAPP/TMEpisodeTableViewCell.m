//
//  TMEpisodeTableViewCell.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMEpisodeTableViewCell.h"

@implementation TMEpisodeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(8,7,127,71);
    float limgW =  self.imageView.image.size.width;
    if(limgW > 0) {
        self.textLabel.frame = CGRectMake(55,self.textLabel.frame.origin.y,self.textLabel.frame.size.width,self.textLabel.frame.size.height);
        self.detailTextLabel.frame = CGRectMake(55,self.detailTextLabel.frame.origin.y,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);
    }
}

- (IBAction)didTapBookmark:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookMarkButtonAtIndexPath:)]){
        [self.delegate didTapBookMarkButtonAtIndexPath:self.indexPath];
    }
}


@end
