//
//  Article.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "Article.h"

@implementation Article

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.category = [decoder decodeObjectForKey:@"category"];
    self.enCategory = [decoder decodeObjectForKey:@"enCategory"];
    self.byLine = [decoder decodeObjectForKey:@"byLine"];
    self.type = [decoder decodeObjectForKey:@"type"];
    self.sectionURL = [decoder decodeObjectForKey:@"sectionURL"];
    self.mediagroupList = [decoder decodeObjectForKey:@"mediagroupList"];
    self.tittle = [decoder decodeObjectForKey:@"tittle"];
    self.entittle = [decoder decodeObjectForKey:@"enTitle"];
    self.brief = [decoder decodeObjectForKey:@"brief"];
    self.link = [decoder decodeObjectForKey:@"link"];
    self.desc = [decoder decodeObjectForKey:@"desc"];
    self.guid = [decoder decodeObjectForKey:@"guid"];
    self.lastUpdateDate = [decoder decodeObjectForKey:@"lastUpdateDate"];
    self.pubDate = [decoder decodeObjectForKey:@"pubDate"];
    self.sectionType = [decoder decodeObjectForKey:@"sectionType"];
    self.feedlink = [decoder decodeObjectForKey:@"feedlink"];
    self.sections = [decoder decodeObjectForKey:@"sections"];
    self.related = [decoder decodeObjectForKey:@"related"];
    self.sectionName = [decoder decodeObjectForKey:@"sectionName"];
    self.totalNum = [decoder decodeObjectForKey:@"totalNum"];
    self.thumbnail = [decoder decodeObjectForKey:@"thumbnail"];
    self.downloadTime = [decoder decodeObjectForKey:@"downloadTime"];
    self.parentUrl = [decoder decodeObjectForKey:@"sectionURL"];
    self.copyright = [decoder decodeObjectForKey:@"copyright"];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.category forKey:@"category"];
    [encoder encodeObject:self.enCategory forKey:@"enCategory"];
    [encoder encodeObject:self.byLine forKey:@"byLine"];
    [encoder encodeObject:self.type forKey:@"type"];
    [encoder encodeObject:self.sectionURL forKey:@"sectionURL"];
    [encoder encodeObject:self.mediagroupList forKey:@"mediagroupList"];
    [encoder encodeObject:self.tittle forKey:@"tittle"];
    [encoder encodeObject:self.entittle forKey:@"enTitle"];
    [encoder encodeObject:self.brief forKey:@"brief"];
    [encoder encodeObject:self.link forKey:@"link"];
    [encoder encodeObject:self.desc forKey:@"desc"];
    [encoder encodeObject:self.guid forKey:@"guid"];
    [encoder encodeObject:self.lastUpdateDate forKey:@"lastUpdateDate"];
    [encoder encodeObject:self.pubDate forKey:@"pubDate"];
    [encoder encodeObject:self.sectionType forKey:@"sectionType"];
    [encoder encodeObject:self.feedlink forKey:@"feedlink"];
    [encoder encodeObject:self.sections forKey:@"sections"];
    [encoder encodeObject:self.related forKey:@"related"];
    [encoder encodeObject:self.sectionName forKey:@"sectionName"];
    [encoder encodeObject:self.totalNum forKey:@"totalNum"];
    [encoder encodeObject:self.thumbnail forKey:@"thumbnail"];
    [encoder encodeObject:self.downloadTime forKey:@"downloadTime"];
    [encoder encodeObject:self.parentUrl forKey:@"parentUrl"];
    [encoder encodeObject:self.copyright forKey:@"copyright"];
}

@end
