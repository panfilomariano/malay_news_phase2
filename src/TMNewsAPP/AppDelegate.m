//
//  AppDelegate.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/4/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworking.h"
#import "LibraryAPI.h"
#import "Article.h"
#import "SDWebImageManager.h"
#import <OutbrainSDK/OutbrainSDK.h>
#import "Constants.h"

#import <GoogleMobileAds/GoogleMobileAds.h>
#import "NSString+PercentEscapes.h"
#import "TMWebViewController.h"
#import "Util.h"
#import "CSComScore.h"
#import "TMNavigationController.h"
#import "TMMainPageViewController.h"
#import "TMSlidingViewController.h"
#define ALERTVIEW_PUSHMESSAGE 2

@interface AppDelegate () <GADInterstitialDelegate>

@property (strong, nonatomic) DFPInterstitial *splashInterstitial;
@property (strong, nonatomic) NSDate *timeCheck;
@property (nonatomic, strong) NSData *deviceToken;
@property (nonatomic, strong) NSDictionary *notifyByOpen;
@property (nonatomic, assign) BOOL shouldFireFullScreenAd;

@end

@implementation AppDelegate

+ (AppDelegate *)sharedInstance
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    //NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    //remove back button tittle
   
    
    [[UIBarButtonItem appearance]
     setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000)
     forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor], NSForegroundColorAttributeName,
                                               [UIFont fontWithName:@"HelveticaNeue-Regular" size:17.0f], NSFontAttributeName,
                                               nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    


   //  NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
     //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];



    [NSTimer scheduledTimerWithTimeInterval:[Constants INDEX_LIST_REQUEST_INTERVAL] target:self selector:@selector(requestIndexListItems) userInfo:nil repeats:YES];


    [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:[Constants HAS_BREAKING_NEWS_KEY]];
#pragma mark Notification
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else{
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationType)(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    //[[LibraryAPI sharedInstance] requestAppVersionWithURLString:[Constants APPVERSION_URL]];
    
    SDWebImageDownloader *manager = [SDWebImageManager sharedManager].imageDownloader;
    manager.username = @"UfinityUser";
    manager.password = @"Acc*Web@2013";
    
    self.timeCheck = [NSDate date];
    sleep(3);
#ifdef TAMIL
    [Outbrain initializeOutbrainWithConfigFile:@"OBConfig-Tamil.plist"];
#else
    [Outbrain initializeOutbrainWithConfigFile:@"OBConfig-Berita.plist"];
#endif

    [CSComScore setAppContext];
    [CSComScore setAppName:[Constants COMSCORE_APPNAME]];
    [CSComScore onUxActive];
    
    [self fireFullScreenAdv];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"applicationDidEnterBackground");
    [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:[Constants HAS_BREAKING_NEWS_KEY]];
    self.timeCheck = [NSDate date];
    [CSComScore onUxInactive];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"applicationWillEnterForeground");
    [CSComScore onUxActive];
    NSDate *date = [NSDate date];
    NSTimeInterval interval = [date timeIntervalSinceDate:self.timeCheck];
    //in secs 3600=1 hour
    if (interval >= 3600){
        TMSlidingViewController *slidingViewController = (TMSlidingViewController *) self.window.rootViewController;
        TMNavigationController *nav = [slidingViewController.storyboard instantiateViewControllerWithIdentifier:@"mainPageNavigationID"];
        TMMainPageViewController *mainVc = (TMMainPageViewController *) [nav.viewControllers firstObject];
        mainVc.index = 0;
        slidingViewController.topViewController = nav;
        [slidingViewController resetTopViewAnimated:NO];
       
    }
    if(interval >= 180){
        self.shouldFireFullScreenAd = YES;
        NSLog(@"shouldFireFullScreenAd YES!");
    }else{
        self.shouldFireFullScreenAd = NO;
        NSLog(@"shouldFireFullScreenAd NO!");
    }

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"applicationDidBecomeActive");
    if(self.shouldFireFullScreenAd){
        NSLog(@"fireFullScreenAdv");
        [self fireFullScreenAdv];
    } else {
        [self presentNotify];
    }
    
    [self callListVersionCheck];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadDataNotif"
                                                        object:nil
                                                      userInfo:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    NSLog(@"didReceiveRemoteNotification");
    [self processPushWithDictionary:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler NS_AVAILABLE_IOS(7_0)
{
    NSLog(@"didReceiveRemoteNotification NS_AVAILABLE_IOS(7_0)");
    [self processPushWithDictionary:userInfo];
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
        
    } else if ([identifier isEqualToString:@"answerAction"]){
        
    }
}
#endif

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    self.deviceToken = deviceToken;
    
    [self registerPushNotification];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"NOTIFICATION REGISTRAION ERROR: %@", error);
}

#pragma mark GADRequest implementation

- (GADRequest *)request {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as well as any devices
    // you want to receive test ads.
    request.testDevices = @[
                            // TODO: Add your device/simulator test identifiers here. Your device identifier is printed to
                            // the console when the app is launched.
                            @"Simulator"
                            ];
    return request;
}

#pragma mark GADInterstitialDelegate <NSObject>

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    NSDate *date = [NSDate date];
    NSTimeInterval interval = [date timeIntervalSinceDate:self.timeCheck];
    
    if(interval >= 3) {
        //self performSelector:@selector(removeSplashImageView:) withObject:self.splashImageView afterDelay:0.5];
        [self performSelector:@selector(presentAds) withObject:nil];
    } else {
        float timeToShow = 3 - interval;
        
        //[self performSelector:@selector(removeSplashImageView:) withObject:self.splashImageView afterDelay:timeToShow + 0.5];
        //self performSelector:@selector(presentAds) withObject:nil afterDelay:timeToShow];
        [self performSelector:@selector(presentAds) withObject:nil afterDelay:timeToShow];
    }
    
    //[self presentAds];
}
- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
     self.splashInterstitial = nil;
    NSLog(@"didFailToReceiveAd ==Will present Notify");
    [self presentNotify];
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didCloseBannerNotif"
                                                        object:nil];
    
    self.splashInterstitial = nil;
    NSLog(@"Did DissmissScreen ==Will present Notify");
    [self presentNotify];
}

#pragma mark - UIAlertViewDelegate <NSObject>

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ((alertView.tag == 1 && buttonIndex == 0) || (alertView.tag == 0 && buttonIndex == 1)) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[Constants ITUNES_APP_URL]]];
    } else if (alertView.tag == ALERTVIEW_PUSHMESSAGE) {
        if (buttonIndex == 1) {
            [self presentNotify];
        }
        self.notifyByOpen = nil;
    }
}

#pragma mark Private Functions

- (void) requestIndexListItems{
    [[LibraryAPI sharedInstance]requestIndexListItems:^{
        NSLog(@"index refreshed");
    }];

}
- (void)fireFullScreenAdv
{
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADBannerView automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[
                            @"Simulator"
                            ];
    
    self.splashInterstitial = [[DFPInterstitial alloc] init];
    self.splashInterstitial.adUnitID = [Constants ADMOB_UNITID_SPLASHAD];
    self.splashInterstitial.delegate = self;
    [self.splashInterstitial loadRequest:request];
}

-(void)presentAds
{
    UIViewController *presented = _window.rootViewController;
    while (presented.presentedViewController) {
        presented = presented.presentedViewController;
    }
    if (presented) {
        [self.splashInterstitial presentFromRootViewController:presented];
    }
}

- (void)processPushWithDictionary:(NSDictionary *)dictionary
{
    //self.notifyByOpen = dictionary;
    
    
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateInactive) {
        NSLog(@"There is notifyByOpen instance");
        self.notifyByOpen = dictionary;
    } else if (UIApplication.sharedApplication.applicationState == UIApplicationStateBackground) {
        NSLog(@"There is notifyByOpen instance");
        self.notifyByOpen = dictionary;
    } else if (UIApplication.sharedApplication.applicationState == UIApplicationStateActive){
        NSLog(@"Present Notify on ACTIVE STATE");
        self.notifyByOpen = dictionary;
        NSString *url = self.notifyByOpen[@"url"];
        NSString *msg = self.notifyByOpen[@"aps"][@"alert"];
        
        UIAlertView *alertView = nil;
        if (url == nil || url.length == 0) {
            alertView = [[UIAlertView alloc] initWithTitle:[Constants PUSH_HEADER] message:msg delegate:self cancelButtonTitle:[Constants OK_DIALOG_MESSAGE] otherButtonTitles:nil];
        } else {
            alertView = [[UIAlertView alloc] initWithTitle:[Constants PUSH_HEADER] message:msg delegate:self cancelButtonTitle:[Constants CLOSE_DIALOG_MESSAGE] otherButtonTitles:[Constants OPEN_DIALOG_MESSAGE], nil];
        }
        alertView.tag = ALERTVIEW_PUSHMESSAGE;
        [alertView show];
    }
}

- (void)registerPushNotification
{
    if (!self.deviceToken) return;
    NSString* deviceToken = [[[self.deviceToken.description
                               stringByReplacingOccurrencesOfString: @"<" withString: @""]
                              stringByReplacingOccurrencesOfString: @">" withString: @""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSDictionary *parameters = @{@"task": @"register",
                                 @"appname": @"tmnews",
                                 @"appversion": @"1.0",
                                 @"deviceuid": deviceID,
                                 @"devicetoken": deviceToken,
                                 @"devicename": UIDevice.currentDevice.name,
                                 @"devicemodel": UIDevice.currentDevice.model,
                                 @"deviceversion": UIDevice.currentDevice.systemVersion,
                                 @"pushbadge": @"enabled",
                                 @"pushalert": @"enabled",
                                 @"pushsound": @"enabled"};
    
    NSMutableString *paramsString = NSMutableString.alloc.init;
    for (NSString *key in parameters) {
        NSString *value = parameters[key];
        [paramsString appendFormat:@"%@=%@&", key.stringByAddingPercentEscapes, value.stringByAddingPercentEscapes];
    }
    [paramsString deleteCharactersInRange:NSMakeRange(paramsString.length - 1, 1)];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", [Constants PUSHNOTIF_URL], paramsString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    AFHTTPRequestOperation *httpOp = [AFHTTPRequestOperation.alloc initWithRequest:request];
    [httpOp setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    [httpOp start];
}

- (void)presentNotify
{
    if (!self.notifyByOpen) return;
    NSString *url = self.notifyByOpen[@"url"];
    UIApplication.sharedApplication.applicationIconBadgeNumber = 0;
    UIViewController *presented = _window.rootViewController;
    while (presented.presentedViewController) {
        presented = presented.presentedViewController;
    }
    NSLog(@"The URL is: %@", url);
    if (url) {
        TMWebViewController *webViewController = nil;
        webViewController = (TMWebViewController *) [[UIStoryboard storyboardWithName:[Util is_iPad] ?@"MainStoryboard-iPad":@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
        
        if (webViewController) {
            webViewController.urlString = url;
            webViewController.forPushNotif = YES;
            NSLog(@"WebViews is now presenting and active!");
            TMNavigationController *navigation = [[TMNavigationController alloc] initWithRootViewController:webViewController];
            [presented presentViewController:navigation animated:YES completion:nil];
            
        }
    }
    
    NSString *messageId = self.notifyByOpen[@"msgid"];
    if (messageId) {
        [self requestReadNotification:messageId];
    }
    self.notifyByOpen = nil;
}

- (void)requestReadNotification:(NSString *)messageId
{
    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#ifdef TAMIL
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://54.254.105.235/tmnews/notification/GCM/onclick_message.php?"]];
#else
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://54.254.105.235/mlnews/notification/GCM/onclick_message.php?"]];
#endif
    
    NSString *postData = [NSString stringWithFormat:@"msgid=%@&deviceuid=%@&platform=ios",messageId,deviceID];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

- (void)callListVersionCheck
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString * encryptedURL = [Util urlWithTokenFromURL:[Constants APPVERSION_URL]];
    
    [manager GET:encryptedURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (responseObject) {
            NSString *message = [responseObject valueForKeyPath:@"message"];
            NSString *server_version = [responseObject valueForKeyPath:@"version"];
            NSLog(@"SERVER VERSION:%@, MESSAGE:%@", server_version, message);

            if ([Util needsUpgradeVersion:server_version]) {
                NSString *cancel = @"OK";
                NSString *other = nil;
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:cancel otherButtonTitles:other, nil];
                alertView.tag = 1;
                [alertView show];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Internet connection lost. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }];
}

@end
