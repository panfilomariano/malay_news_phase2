//
//  AnalyticManager.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/4/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "AnalyticManager.h"
#import "Util.h"
#import "ADBMobile.h"
//#import "ADMS_Measurement.h"


@implementation AnalyticManager

static AnalyticManager *instance;
const NSString *prop1 = @"sg";
#ifdef TAMIL
    const NSString *prop2 = @"seithi";
#else
    const NSString *prop2 = @"berita";
#endif

static NSString *baseProp = @"";
static NSString *baseProphier1 = @"";




+ (AnalyticManager *)sharedInstance;
{
    if (!instance) {
        instance = [[AnalyticManager alloc] init];
    }
    
    return instance;
}

- (instancetype)init
{
    if(self = [super init]){
        if ([Util is_iPad]){
#ifdef TAMIL
            baseProp = @"sg:seithi:mobileipad";
            baseProphier1 = @"sg|seithi|mobileipad";
#else
            baseProp = @"sg:berita:mobileipad";
            baseProphier1 = @"sg|berita|mobileipad";
#endif
        }else {
#ifdef TAMIL
            baseProp = @"sg:seithi:mobileiphone";
            baseProphier1 = @"sg|seithi|mobileiphone";
#else
            baseProp = @"sg:berita:mobileiphone";
            baseProphier1 = @"sg|berita|mobileiphone";
#endif
        }
    }
    
    return self;
}

- (void)trackLandingPage {

    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|main",baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:main",baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:@"" forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"home page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
   
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}


- (void)trackTopNewsWithCategoryName:(NSString *)categoryName andArticleTitle:(NSString *)articleTitle andGUID:(NSString *)guId{

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news",baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|top|%@|%@",baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:top:%@:%@",baseProp, categoryName, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:top", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:top:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:top|%@|%@", baseProp, categoryName, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary  setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackNewsCategoryTab{

    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|home", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:home", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:home",baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary  setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackNewsListingWithCategoryName: (NSString *)categoryName {

    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|%@|home", baseProphier1, categoryName];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:%@:home", baseProp, categoryName];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:%@", baseProp, categoryName];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:%@:home", baseProp, categoryName];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"article landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}
//edit by ace
- (void)trackContentScreenWithCategoryName: (NSString *)categoryName andArticleTitle: (NSString *)articleTitle andGUID: (NSString *)guId{

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:%@:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:%@", baseProp, categoryName];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:%@:%@", baseProp, categoryName, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:articleTitle forKey:@"prop6"];
    [dictionary setObject:guId forKey:@"prop7"];
    [dictionary setObject:@"article detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];
    
    
}


- (void)trackContentScreenInCAWithCategoryName: (NSString *)categoryName andArticleTitle: (NSString *)articleTitle andGUID: (NSString *)guId {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|%@|episodes|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:%@:episodes:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:%@", baseProp, categoryName];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:%@:episodes", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:ca:%@:episodes:%@", baseProp, categoryName, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:articleTitle forKey:@"prop6"];
    [dictionary setObject:guId forKey:@"prop7"];
    [dictionary setObject:@"episode detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackRelatedNewsWithArticleTitle: (NSString *)articleTitle {

    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|related|%@", baseProphier1, articleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:related:%@", baseProp, articleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:related", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:related:%@", baseProp, articleTitle];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackPopularNewsWithArticleTitle: (NSString *)articleTitle andGUID: (NSString *)guId {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|popular|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:popular:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:popular", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:popular:%@", baseProp, guidArticleTitle];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


    
}

/*- (void)trackBookmarkNewsWithGuiID: (NSString *)guId andArticleTitle: (NSString *)articleTitle {
#ifdef TAMIL
    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|bookmark|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:bookmark:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:bookmark", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:bookmark:%@", baseProp, guidArticleTitle];
#else
    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|bookmark|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:bookmark:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:bookmark", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:bookmark:%@", baseProp, guidArticleTitle];
#endif
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}*/

- (void)trackVideoTab {

    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|videos|home", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:videos:home", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:videos", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:videos:home", baseProp];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"videos landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackVideoPlayWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle andVideoTitle: (NSString *)videoTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|videos|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:videos:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:videos", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:videos:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:videoTitle forKey:@"prop6"];
    [dictionary setObject:guId forKey:@"prop7"];
    [dictionary setObject:@"videos detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}



- (void)trackPhotoTab {

    NSString *prop4 = [NSString stringWithFormat:@"%@:news",baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|photos|home",baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:photos:home",baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:photos",baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:photos:home",baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"photos landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

  
}

- (void)trackOpenPhotoAlbumWithAlbumName: (NSString *)albumName  andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle andPhotoTitle: (NSString *)photoTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|photos|%@|%@", baseProphier1, albumName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:photos:%@:%@", baseProp, albumName,guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:photos", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:photos:%@", baseProp, albumName];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:photoTitle forKey:@"prop6"];
    [dictionary  setObject:guId forKey:@"prop7"];
    [dictionary  setObject:@"photos detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

   
}



- (void)trackCACategoryTab {

    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|home", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:home", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:home",baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackCAProgrammeContentListingWithProgram: (NSString *)program {

    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|%@|episodes|home", baseProphier1, program];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:%@:episodes:home", baseProp, program];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:%@",baseProp, program];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:%@:episodes", baseProp, program];
    NSString *prop71 = [NSString stringWithFormat:@"%@:ca:%@:episodes:home", baseProp, program];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"episode landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackCADetailScreenWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle {

    NSString *episodeIdEpisodeTitle = [NSString stringWithFormat:@"%@_%@", episodeId, episodeTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|%@|episodes|%@", baseProphier1, program, episodeIdEpisodeTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:%@:episodes:%@", baseProp, program, episodeIdEpisodeTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:%@",baseProp, program];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:%@:episodes", baseProp, program];
    NSString *prop71 = [NSString stringWithFormat:@"%@:ca:%@:episodes:%@", baseProp, program, episodeIdEpisodeTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:episodeTitle forKey:@"prop6"];
    [dictionary  setObject:episodeId forKey:@"prop7"];
    [dictionary  setObject:@"episode detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackCABookmarkWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle {

    NSString *episodeIdEpisodeTitle = [NSString stringWithFormat:@"%@_%@", episodeId, episodeTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|bookmark|%@|%@", baseProphier1, program, episodeIdEpisodeTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:bookmark:%@:%@", baseProp, program, episodeIdEpisodeTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:bookmark:%@",baseProp, program];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:bookmark:%@:%@", baseProp, program, episodeIdEpisodeTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


    
}

- (void)trackCAPhotoListingWithProgram: (NSString *)program {

    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|%@|photos|home", baseProphier1, program];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:%@:photos:home", baseProp, program];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:%@", baseProp, program];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:%@:photos", baseProp, program];
    NSString *prop71 = [NSString stringWithFormat:@"%@:ca:%@:photos:home", baseProp, program];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"photos landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}


- (void)trackCAAboutScreenWithCategory: (NSString *)categoryName andProgram: (NSString *)program{
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|%@|%@", baseProphier1, program, categoryName];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:%@", baseProp, program];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:%@:%@", baseProp, program, categoryName];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:%@:%@", baseProp, program, categoryName];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"info page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}



- (void)CALiveStreamingWithProgram: (NSString *)program {
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|%@|live streaming", baseProphier1, program];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:@"" forKey:@"pageName"];
    [dictionary setObject:@"" forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
   // [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackFacebookShareNewsWithCategory: (NSString *)categoryName andArticleTitle: (NSString *)articleTitle andGuid: (NSString *)guId {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|facebook|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:facebook:%@:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:facebook", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:facebook:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:facebook:%@:%@", baseProp, categoryName, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


    
    
}

- (void)trackTwitterShareNewsWithCategoryName: (NSString *)categoryName andGuId: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|twitter|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:twitter:%@:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:twitter", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:twitter:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:twitter:%@:%@", baseProp, categoryName, guidArticleTitle];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


    
}

- (void)trackEmailShareNewsWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guid andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guid, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|email|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:email:%@:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:email", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:email:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:email:%@:%@", baseProp, categoryName, guidArticleTitle];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackFacebookShareCAWithEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle andProgram: (NSString *)program{

    NSString *episodeIdEpsiodeTitle = [NSString stringWithFormat:@"%@_%@", episodeId, episodeTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|facebook|%@|%@", baseProphier1, program, episodeIdEpsiodeTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:facebook:%@:%@", baseProp, program, episodeIdEpsiodeTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:facebook", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:facebook:%@", baseProp, program];
    NSString *prop71 = [NSString stringWithFormat:@"%@:ca:facebook:%@:%@", baseProp, program, episodeIdEpsiodeTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


    
}

- (void)trackTwitterShareCAWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle {

    NSString *episodeIdEpisodeTitle = [NSString stringWithFormat:@"%@_%@", episodeId, episodeTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|twitter|%@|%@", baseProphier1, program, episodeIdEpisodeTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:twitter:%@:%@", baseProp, program, episodeIdEpisodeTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:twitter", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:twitter:%@", baseProp, program];
    NSString *prop71 = [NSString stringWithFormat:@"%@:ca:twitter:%@:%@", baseProp, program, episodeIdEpisodeTitle];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackEmailShareCAWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle {

    NSString *episodeIdEpisodeTitle = [NSString stringWithFormat:@"%@_%@", episodeId, episodeTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:ca", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|ca|email|%@|%@", baseProphier1, program, episodeIdEpisodeTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:ca:email:%@:%@", baseProp, program, episodeIdEpisodeTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:ca:email", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:ca:email:%@", baseProp, program];
    NSString *prop71 = [NSString stringWithFormat:@"%@:ca:email:%@:%@", baseProp, program, episodeIdEpisodeTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackFacebookShareVideoWithGuId: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:video", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|video|facebook|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:video:facebook:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:video:facebook", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:video:facebook:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackTwitterShareVideoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:video", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|video|twitter|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:video:twitter:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:video:twitter", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:video:twitter:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackEmailShareVideoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:video", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|video|email|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:video:email:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:video:email", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:video:email:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackFacebookSharePhotoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:photo", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|photo|facebook|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:photo:facebook:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:photo:facebook", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:photo:facebook:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackTwitterSharePhotoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:photo", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|photo|twitter|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:photo:twitter:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:photo:twitter", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:photo:twitter:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackEmailSharePhotoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:photo", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|photo|email|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:photo:email:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:photo:email", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:photo:email:%@", baseProp, guidArticleTitle];
 
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackMenuTab {

    NSString *prop4 = [NSString stringWithFormat:@"%@:menu", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|menu|main", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:menu:home", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:menu:home", baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}
//
- (void)trackMenuCategoryWithCategoryName: (NSString *)categoryName {
    NSString *prop4 = [NSString stringWithFormat:@"%@:menu", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|menu|%@", baseProphier1, categoryName];
    NSString *pageName = [NSString stringWithFormat:@"%@:menu:%@", baseProp, categoryName];
    NSString *prop5 = [NSString stringWithFormat:@"%@:menu:%@", baseProp, categoryName];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackRadioWithName: (NSString *)radioName {

    NSString *prop4 = [NSString stringWithFormat:@"%@:radio", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|radio|%@", baseProphier1, radioName];
    NSString *pageName = [NSString stringWithFormat:@"%@:radio:%@", baseProp, radioName];
    NSString *prop5 = [NSString stringWithFormat:@"%@:radio:%@", baseProp, radioName];
 
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}


//edit by ace
- (void)trackSearch {

    NSString *prop4 = [NSString stringWithFormat:@"%@:search", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|search|home", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:search:home", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:search:home", baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackSearchStringWithCategoryName: (NSString *)categoryName andCount:(NSUInteger)count {

    NSString *prop4 = [NSString stringWithFormat:@"%@:search", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|search|result", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:search:result", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:search:result", baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"search result page" forKey:@"prop8"];
    [dictionary setObject:categoryName forKey:@"prop11"];
    [dictionary setObject:@(count) forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackBookmark {

    NSString *prop4 = [NSString stringWithFormat:@"%@:bookmark", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|boomark|main", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:bookmark:home", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:bookmark:home", baseProp];
 
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


    
}

- (void)trackBookmarkWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:news:", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|bookmark|%@", baseProphier1, guidArticleTitle];
    NSString *pageName =[NSString stringWithFormat:@"%@:news:bookmark:%@", baseProp, guidArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:bookmark", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:bookmark:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
    
    
}

- (void)trackSettings {

    NSString *prop4 = [NSString stringWithFormat:@"%@:settings", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|settings|main", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:settings:home", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:settings:home", baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary  setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary  setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary  setObject:@"" forKey:@"prop6"];
    [dictionary  setObject:@"" forKey:@"prop7"];
    [dictionary  setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackOfficeDownload {

    NSString *prop4 = [NSString stringWithFormat:@"%@:settings", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|settings|offline download", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:settings:offlinedownload", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:settings:offlinedownload", baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:@"" forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackSpecialReportHome{
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|home", baseProp];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreport:home", baseProphier1];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:home", baseProp];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}
/*
- (void)trackReportsWithSpecialreport: (NSString *)specialReport {
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|home", baseProphier1, specialReport];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreport:%@:home", baseProp, specialReport];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specailreports", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialReport];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:home", baseProp, specialReport];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}
*/
- (void)trackVideosWithVideoId: (NSString *)videoId andVideoTitle: (NSString *)videoTitle {

    NSString *videoIdVideotitle = [NSString stringWithFormat:@"%@_%@", videoId, videoTitle];
    
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|videos|%@", baseProphier1, videoIdVideotitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:videos:%@", baseProp, videoIdVideotitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:videos", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:videos:%@", baseProp, videoIdVideotitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:videoTitle forKey:@"prop6"];
    [dictionary setObject:videoId forKey:@"prop7"];
    [dictionary setObject:@"article detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}


- (void)trackPhotosWithGalleryTitle: (NSString *)galleryTitle andPhotoId: (NSString *)photoId andPhotoTitle: (NSString *)photoTitle {

    NSString *photoIdPhotoTitle = [NSString stringWithFormat:@"%@_%@", photoId, photoTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|photos|%@|%@", baseProphier1, galleryTitle, photoIdPhotoTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:photos:%@|%@", baseProp, galleryTitle, photoIdPhotoTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:photos", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:photos:%@", baseProp, galleryTitle];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:photos:%@:%@", baseProp, galleryTitle, photoIdPhotoTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:photoTitle forKey:@"prop6"];
    [dictionary setObject:photoId forKey:@"prop7"];
    [dictionary setObject:@"photo detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackNewsSearch {

    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|search", baseProphier1];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:search", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:search", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:search:home", baseProp];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"search result page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"]; //leave this
    [dictionary setObject:@"" forKey:@"prop12"]; //leave this
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackPhotosWithSpecialReport: (NSString *)specialreport {
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|photos|home", baseProphier1, specialreport];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:photos:home", baseProp, specialreport];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialreport];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:photos", baseProp, specialreport];
    NSString *prop72 = [NSString stringWithFormat:@"%@:news:specialreports:%@:photos:home", baseProp, specialreport];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"photo landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void) trackSPVideoListing: (NSString *)specialReport {

    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|videos|home", baseProphier1, specialReport];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:videos:home", baseProp, specialReport];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialReport];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:videos", baseProp, specialReport];
    NSString *prop72 = [NSString stringWithFormat:@"%@:news:specialreports:%@:videos:home", baseProp, specialReport];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"videos landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackVideosWithSpecialReports: (NSString *)specialReport andGuid: (NSString *)guId andvideoTitle: (NSString *)videoTitle {

    NSString *videoIdVideoTitle = [NSString stringWithFormat:@"%@_%@", guId, videoTitle];
    
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|videos|%@", baseProphier1, specialReport, videoIdVideoTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:videos:%@", baseProp, specialReport, videoIdVideoTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialReport];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:videos", baseProp, specialReport];
    NSString *prop72 = [NSString stringWithFormat:@"%@:news:specialreports:%@:videos:%@", baseProp, specialReport, videoIdVideoTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:videoTitle forKey:@"prop6"];
    [dictionary setObject:guId forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackSPNewsListingScreen: (NSString *)specialreport {

    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|news|home", baseProphier1, specialreport];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:news:home", baseProp, specialreport];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialreport];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:news", baseProp, specialreport];
    NSString *prop72 = [NSString stringWithFormat:@"%@:news:specialreports:%@:news:home", baseProp, specialreport];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"article landing page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];
}

- (void)trackSpecialReportNewsDetailScreenWithGuId: (NSString *)GuId andArticleTitle: (NSString *)articleTitle  andSpecialReport: (NSString *)specialReport andSpecialReportEnTittle: (NSString *)spEnTittle {

    NSString *articleIdArticleTitle = [NSString stringWithFormat:@"%@_%@", GuId, articleTitle];
    
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|news|%@", baseProphier1, spEnTittle, articleIdArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:news:%@", baseProp, spEnTittle, articleIdArticleTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, spEnTittle];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:news", baseProp, spEnTittle];
    NSString *prop72 = [NSString stringWithFormat:@"%@:news:specialreports:%@:news:%@", baseProp, spEnTittle, articleIdArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:articleTitle forKey:@"prop6"];
    [dictionary setObject:GuId forKey:@"prop7"];
    [dictionary setObject:@"article detail page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackPhotosWithGuid: (NSString *)guId andPhotoTitle: (NSString *)photoTitle andAlbumName: (NSString *)albumName andSpecialReport: (NSString *)specialReport {

    NSString *photoIdPhotoTitle = [NSString stringWithFormat:@"%@_%@", guId, photoTitle];
     NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];
    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|photos|%@|%@", baseProphier1, specialReport, albumName, photoIdPhotoTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:photos:%@:%@", baseProp, specialReport, albumName, photoIdPhotoTitle];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialReport];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:photos", baseProp, specialReport];
    NSString *prop72 = [NSString stringWithFormat:@"%@:news:specialreports:%@:photos:%@", baseProp, specialReport, albumName];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:photoIdPhotoTitle forKey:@"prop6"];
    [dictionary setObject:guId forKey:@"prop7"];
    [dictionary setObject:@"photo details page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


}

- (void)trackSPLiveStreaming: (NSString *)specialreport andLiveStreaming: (NSString *)livestream {
    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];

    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|%@", baseProphier1, specialreport, livestream];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:%@", baseProp, specialreport, livestream];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialreport];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, specialreport];
    NSString *prop72 = [NSString stringWithFormat:@"%@:news:specialreports:%@:%@", baseProp, specialreport, livestream];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackSPHtmlPageWithEnCategory:(NSString *)specialReport withEnCategory: (NSString *)enCategory{

    NSString *prop4 = [NSString stringWithFormat:@"%@:news", baseProp];

    NSString *hier1 = [NSString stringWithFormat:@"%@|news|specialreports|%@|%@", baseProphier1, enCategory, specialReport];
    NSString *pageName = [NSString stringWithFormat:@"%@:news:specialreports:%@:%@", baseProp, enCategory, specialReport];
    NSString *prop5 = [NSString stringWithFormat:@"%@:news:specialreports", baseProp];//[NSString stringWithFormat:@"%@:news:specialreports:%@:%@", baseProp, enCategory, specialReport];
    NSString *prop70 = [NSString stringWithFormat:@"%@:news:specialreports:%@", baseProp, enCategory];
    NSString *prop71 = [NSString stringWithFormat:@"%@:news:specialreports:%@:%@", baseProp, enCategory, specialReport];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"html page" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];


    
}

- (void) trackClickNewsArticleInSearchWithCategory: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *hier1 = [NSString stringWithFormat:@"%@|search|news|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:search:news:%@:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:search", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:search:news", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:search:news:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:search:news:%@:%@", baseProp, categoryName, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void) trackClickSpecialReportArticleInSearchWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *hier1 = [NSString stringWithFormat:@"%@|search|news|specialreports|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:search:news:specialreports:%@:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:search", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:search:news", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:search:news:specialreports", baseProp];
    NSString *prop71 = [NSString stringWithFormat:@"%@:search:news:specialreports:%@", baseProp, categoryName];
    NSString *prop72 = [NSString stringWithFormat:@"%@:search:news:specialreports:%@:%@", baseProp, categoryName, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

    
}

- (void)trackCAArticleInSearchWithProgramName: (NSString *)programName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *hier1 = [NSString stringWithFormat:@"%@|search|ca|%@|%@", baseProphier1, programName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:search:ca:%@:%@", baseProp, programName, guidArticleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:search", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:search:ca", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:search:ca:%@", baseProp, programName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:search:ca:%@:%@", baseProp, programName, guidArticleTitle];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];
    
}

//======

- (void)trackOpenCAProgramScreenWithCategoryname: (NSString *)categoryName{

    NSString *hier1 = [NSString stringWithFormat:@"%@|top|ca|%@|home", baseProphier1, categoryName];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:ca:%@:home", baseProp, categoryName];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:ca", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:ca:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:top:ca:%@:home", baseProp, categoryName];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackCAOpenDetailScreenWithCategoryName: (NSString *)categoryName andGuid: (NSString *) guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *hier1 = [NSString stringWithFormat:@"%@|top|ca|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:ca:%@:episodes:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:ca", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:ca:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:top:ca:%@:episodes", baseProp, categoryName];
    NSString *prop72 = [NSString stringWithFormat:@"%@:top:ca:%@:episodes:%@", baseProp, categoryName, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

 }

- (void)trackCAOpenPhotoGalleryWithCategoryName: (NSString *)categoryName andAlbumName: (NSString *)albumName {

    NSString *hier1 = [NSString stringWithFormat:@"%@|top|ca|%@|photos|%@", baseProphier1, categoryName, albumName];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:ca:%@:photos:%@", baseProp, categoryName, albumName];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:ca", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:ca:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:top:ca:%@:photos", baseProp, categoryName];
    NSString *prop72 = [NSString stringWithFormat:@"%@:top:ca:%@:photos:%@", baseProp, categoryName, albumName];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackOpenNewsDetailScreenWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *hier1 = [NSString stringWithFormat:@"%@|top|news|%@|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:news:%@:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:news", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:news:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:top:news:%@:%@", baseProp, categoryName, guidArticleTitle];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];
    
}

- (void)trackOpenSpecialReportScreenWithCategoryName: (NSString *)categoryName {

    NSString *hier1 = [NSString stringWithFormat:@"%@|top|specialreports|%@|home", baseProphier1, categoryName];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:specialreports:%@:home", baseProp, categoryName];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:specialreports", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:specialreports:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:top:specialreports:%@:home", baseProp, categoryName];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];
}

- (void)trackOpenSpecialReportDetailScreenWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {
    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *hier1 = [NSString stringWithFormat:@"%@|top|specialreports|%@|news|%@", baseProphier1, categoryName, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:specialreports:%@:news:%@", baseProp, categoryName, guidArticleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:specialreports", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:specialreports:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:top:specialreports:%@:news", baseProp, categoryName];
    NSString *prop72 = [NSString stringWithFormat:@"%@:top:specialreports:%@:news:%@", baseProp, categoryName, guidArticleTitle];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackOpenSpecialReportPhotoGalleryWithCategoryName: (NSString *)categoryName andAlbumName: (NSString *)albumName {

    NSString *hier1 = [NSString stringWithFormat:@"%@|top|specialreports|%@|photos|%@", baseProphier1, categoryName, albumName];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:specialreports:%@:photos:%@", baseProp, categoryName, albumName];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:specialreports:", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:specialreports:%@", baseProp, categoryName];
    NSString *prop71 = [NSString stringWithFormat:@"%@:top:specialreports:%@:photos", baseProp, categoryName];
    NSString *prop72 = [NSString stringWithFormat:@"%@:top:specialreports:%@:photos:%@", baseProp, categoryName, albumName];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:prop71 forKey:@"prop71"];
    [dictionary setObject:prop72 forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];
    
}

- (void)trackOpenVideoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle {

    NSString *guidArticleTitle = [NSString stringWithFormat:@"%@_%@", guId, articleTitle];
    NSString *hier1 = [NSString stringWithFormat:@"%@|top|videos|%@", baseProphier1, guidArticleTitle];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:videos:%@", baseProp, guidArticleTitle];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:videos", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:videos:%@", baseProp, guidArticleTitle];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];

}

- (void)trackOpenPhotoGalleryWithAlbumName: (NSString *)albumName {

    NSString *hier1 = [NSString stringWithFormat:@"%@|top|photos|%@", baseProphier1, albumName];
    NSString *pageName = [NSString stringWithFormat:@"%@:top:photos:%@", baseProp, albumName];
    NSString *prop4 = [NSString stringWithFormat:@"%@:top", baseProp];
    NSString *prop5 = [NSString stringWithFormat:@"%@:top:photos", baseProp];
    NSString *prop70 = [NSString stringWithFormat:@"%@:top:photos:%@", baseProp, albumName];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:prop1 forKey:@"prop1"];
    [dictionary setObject:prop2 forKey:@"prop2"];
    [dictionary setObject:baseProp forKey:@"prop3"];
    [dictionary setObject:prop4 forKey:@"prop4"];
    [dictionary setObject:hier1 forKey:@"hier1"];
    [dictionary setObject:pageName forKey:@"pageName"];
    [dictionary setObject:prop5 forKey:@"prop5"];
    [dictionary setObject:prop70 forKey:@"prop70"];
    [dictionary setObject:@"" forKey:@"prop71"];
    [dictionary setObject:@"" forKey:@"prop72"];
    [dictionary setObject:@"" forKey:@"prop73"];
    [dictionary setObject:@"" forKey:@"prop74"];
    [dictionary setObject:@"" forKey:@"prop75"];
    [dictionary setObject:@"" forKey:@"prop6"];
    [dictionary setObject:@"" forKey:@"prop7"];
    [dictionary setObject:@"" forKey:@"prop8"];
    [dictionary setObject:@"" forKey:@"prop11"];
    [dictionary setObject:@"" forKey:@"prop12"];
    
    NSLog(@"log %@", dictionary);
    [ADBMobile trackState:pageName data:dictionary];
    [ADBMobile setDebugLogging:YES];
    
}


@end
