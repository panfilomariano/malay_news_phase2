//
//  TMsmallCurrentAffairsTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LatestEpisodesCellDelegate <NSObject>

- (void) didTapBookMarkButtonAtIndexPath: (NSIndexPath *)indexPath;

@end

@interface TMsmallCurrentAffairsTableViewCell : UITableViewCell
@property (nonatomic ,weak) IBOutlet UIImageView *imageview;
@property (nonatomic ,weak) IBOutlet UIImageView *playIcon;
@property (nonatomic ,weak) IBOutlet UILabel *tittleLabel;
@property (nonatomic ,weak) IBOutlet UILabel *dateLabel;
@property (nonatomic ,weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, strong) id <LatestEpisodesCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end
