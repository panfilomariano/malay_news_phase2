//
//  TMCollectionTableViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/5/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMCollectionTableViewCell.h"

@implementation TMCollectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
