//
//  TMNewsPagesCellTableViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/19/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMNewsPagesCellTableViewCell.h"

@implementation TMNewsPagesCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
