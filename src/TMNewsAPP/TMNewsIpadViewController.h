//
//  TMNewsIpadViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/2/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface TMNewsIpadViewController : UIViewController
@property (nonatomic, weak) IBOutlet DFPBannerView *bannerView;

@end
