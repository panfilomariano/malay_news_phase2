//
//  TMiPADVideoDetailsViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/17/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMiPADVideoDetailsViewController.h"
#import "TMNewsCollectionViewLayout.h"
#import "TMSmallCollectionViewCell.h"
#import "TMSquareCollectionViewCell.h"
#import "TMiPadMainVideoCollectionViewCell.h"
#import "TMMoviePlayerViewController.h"
#import "Media.h"
#import "NSString+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "Constants.h"
#import <Social/Social.h>
#import "Util.h"
#import "TMStaticCollectionViewCell.h"
#import "TMsmallCurrentAffairsTableViewCell.h"
#import <MessageUI/MessageUI.h>
#import "AnalyticManager.h"


@interface TMiPADVideoDetailsViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, TMNewsCollectionViewLayoutDelegate, TMSquareCollectionViewCellDelagate, TMSmallCollectionViewCellDelagate, UIActionSheetDelegate, UITableViewDataSource, UITableViewDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *smallCellWidth;
@property (nonatomic, strong) UINib *smallCell;
@property (nonatomic, strong) UINib *squareCell;
@property (nonatomic, strong) UINib *videoDetailsCell;
@property (nonatomic, strong) UINib *staticCell;
@property (nonatomic) BOOL isPortrait;


@property (nonatomic) float titleHeight;
@property (nonatomic) float capHeight;
@end

@implementation TMiPADVideoDetailsViewController
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    self.squareCell = [UINib nibWithNibName:@"SquareCollectionViewCell" bundle:nil];
    self.smallCell = [UINib nibWithNibName:@"LongCollectionViewCell" bundle:nil];
    self.videoDetailsCell = [UINib nibWithNibName:@"TMVideoDetailsCollectionViewCell" bundle:nil];
    self.staticCell = [UINib nibWithNibName:@"TMStaticCollectionViewCell" bundle:nil];
    
    return self;
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        [self.tableView setHidden:YES];
    }else{
        [self.tableView setHidden:NO];
        
        self.smallCellWidth.constant = 244.0f;
    }
    
    TMNewsCollectionViewLayout* layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    
    [self loadLayout:layout forOrientation:self.interfaceOrientation];

}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shareButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didPressShare) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 32, 32)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UIButton *customBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBack setImage:[UIImage imageNamed:@"right-btn-s"] forState:UIControlStateNormal];
    [customBack addTarget:self action:@selector(didTapBakcbutton) forControlEvents:UIControlEventTouchUpInside];
    customBack.frame = CGRectMake(-75, 0, 150, 44);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(-75, 0, 150, 44)];
    //backButtonView.bounds = CGRectOffset(backButtonView.bounds, 8, 5);
    [backButtonView addSubview:customBack];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = item;
    
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:1.0/225.0 green:18.0/225.0 blue:30.0/225.0 alpha:1]];
    
    // Do any additional setup after loading the view.
    Media *media = [self.videoBrowserList firstObject];
    NSDictionary *dictionary = [media.mediaGroup firstObject];
    NSString *movieUrl = [dictionary valueForKey:@"content"];
    [self playMovieWithURL:movieUrl];
    [self.collectionView registerNib:self.squareCell  forCellWithReuseIdentifier:@"squareCell"];
    [self.collectionView registerNib:self.smallCell  forCellWithReuseIdentifier:@"smallCell"];
    [self.collectionView registerNib:self.videoDetailsCell  forCellWithReuseIdentifier:@"videoDetailsCell"];
    [self.collectionView registerNib:self.staticCell forCellWithReuseIdentifier:@"staticCell"];
  
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        [self.tableView setHidden:YES];
    }else{
         [self.tableView setHidden:NO];
        
       self.smallCellWidth.constant = 244.0f;
    }
    TMNewsCollectionViewLayout* layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    
    [self loadLayout:layout forOrientation:self.interfaceOrientation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark collectionView dataSource
- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f blue:237/255.0f alpha:0.5];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        if(section == 0){
            return 1;
        }else if (section == 1){
            return 1;
        }else{
            return [self.videoBrowserList count];
        }

    }else {
        return 1;
    }
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        return 3;
    }else{
        return 1;
    }
   
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Media *media = [self.videoBrowserList objectAtIndex:indexPath.row];
    NSDictionary *dictionary = [media.mediaGroup firstObject];
    NSString *thumbnail = [dictionary valueForKey:@"thumbnail"];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        if(indexPath.section == 0){
            TMiPadMainVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"videoDetailsCell" forIndexPath:indexPath];
            cell.tittle.text = media.tittle;
            cell.caption.text = [dictionary valueForKey:@"caption"];
            cell.pudDate.text = [media.pubDate formatDate];
            
            if (self.videoList.count > 1) {
                cell.ymiLabel.text = [Constants YOU_MIGHT_ALSO_BE_INTERESTED_IN];
            } else {
                cell.ymiLabel.text = @"";
            }
            
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                              placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];
            [cell.view setHidden:NO];
            return cell;
            
        } else if (indexPath.section == 1){
           /* TMStaticCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"staticCell" forIndexPath:indexPath];
            UILabel *label = (UILabel *) [cell viewWithTag:111];
            label.text = [Constants YOU_MIGHT_ALSO_BE_INTERESTED_IN];
            return cell;*/
            
        } else if (indexPath.section == 2){
            
            
            if (indexPath.row == 1 ||indexPath.row == 4) {
                TMSquareCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"squareCell" forIndexPath:indexPath];
                cell.tittle.text = media.tittle;
                cell.pubDate.text = [media.pubDate formatDate];
                cell.pubdateSpace.constant = -4;
                [cell.bookmarkButton setHidden:YES];
                [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                                  placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];
                return cell;
            } else {
                TMSmallCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"smallCell" forIndexPath:indexPath];
                [cell.category setHidden:YES];
                [cell.category setFrame:CGRectZero];
                cell.categoryHeight.constant = 0;
                cell.tittle.text = media.tittle;
                cell.pubDate.text = [media.pubDate formatDate];
                [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                                  placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];
                [cell.bookmarkButton setHidden:YES];
                
                return cell;
            }
            
        }
    }else{
        TMiPadMainVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"videoDetailsCell" forIndexPath:indexPath];
        cell.tittle.text = media.tittle;
        cell.caption.text = [dictionary valueForKey:@"caption"];
       
        
        cell.pudDate.text = [media.pubDate formatDate];
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                          placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];
        [cell.view setHidden:YES];
        return cell;
        

    }
  
    return nil;
}
#pragma mark collectionView delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Media *media = [self.videoBrowserList objectAtIndex:indexPath.row];
     NSMutableArray *videosToPass = [NSMutableArray new];
    
    float itemIdex = 0;
    for(Media *media in self.videoList){
        
        if ([media isEqual:[self.videoBrowserList objectAtIndex:indexPath.row]]) {
            break;
        }
        itemIdex++;
    }
    
  
    int itemCount =[self.videoList count];
    int numberofSuggestions = 6;
    if(itemCount >= itemIdex+numberofSuggestions){
        int ctr = 0;
        while(ctr<=numberofSuggestions){
            [videosToPass addObject:[self.videoList objectAtIndex:itemIdex +ctr]];
            ctr++;
        }
    }else{
        int ctr = (itemIdex +numberofSuggestions)-itemCount+1;
        int index = 0;
        while(ctr<=numberofSuggestions){
            [videosToPass addObject:[self.videoList objectAtIndex:itemIdex +index]];
            ctr++;
            index++;
        }
        
        int extraindex = (itemIdex+numberofSuggestions)-itemCount+1;
        int extraCtr = 0;
        while(extraindex<=(itemIdex +numberofSuggestions)-itemCount+1 && extraindex!=0){
            if (self.videoList.count < extraCtr) {
                [videosToPass addObject:[self.videoList objectAtIndex:0+extraCtr]];
                extraindex--;
                extraCtr++;
            } else {
                break;
            }
            
        }
    }
 
    TMiPADVideoDetailsViewController *vdc = (TMiPADVideoDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videoDetailsVC"];
    vdc.index = itemIdex;
    vdc.isFromMain = TRUE;
    vdc.videoBrowserList = videosToPass;
    vdc.videoList = self.videoList;
    [[AnalyticManager sharedInstance] trackVideoPlayWithGuid:media.guid andArticleTitle:media.tittle andVideoTitle:media.tittle];
    [self.navigationController pushViewController:vdc animated:NO];
}
#pragma mark - TMNewsCollectionViewLayoutDelegate
- (NSUInteger) layoutIndexAtIndexPath: (NSIndexPath *)indexPath
{
    if (self.isPortrait) {
        if (indexPath.section == 0) {
            return 0;
        }else if (indexPath.section == 1){
            return 1;
        }else{
            return 2;
        }

    } else {
        return 0;
    }
    return 0;
}

#pragma mark Private Functions

-(void)didTapBakcbutton{
    if (self.isFromMain){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void) playMovieWithURL: (NSString *)url
{
    TMMoviePlayerViewController *player = [[TMMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:url]];
    
    [self presentMoviePlayerViewControllerAnimated:player];
    [player.moviePlayer play];
    
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    TMNewsCollectionViewLayout* layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    
    [self loadLayout:layout forOrientation:toInterfaceOrientation];
}



- (void) loadLayout:(TMNewsCollectionViewLayout *)layout forOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{

    Media *media = [self.videoBrowserList objectAtIndex:0];
    NSDictionary *dictionary = [media.mediaGroup firstObject];
    NSString *caption =[dictionary valueForKey:@"caption"];
    NSString *title =media.tittle;
    self.capHeight = [Util expectedHeightFromText:caption withFont:[UIFont systemFontOfSize:16.0f] andMaximumLabelWidth:732.0f]+16;
    self.titleHeight = [Util expectedHeightFromText:title withFont:[UIFont systemFontOfSize:20.0f]andMaximumLabelWidth:732.0f]+16;
    
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        
        [layout addLayoutWithRowWidth:768.f rowHeight:510+self.capHeight+self.titleHeight layouts:@[[NSValue valueWithCGRect:CGRectMake(0.f, 0.0f, 768.0f, 510+self.capHeight+self.titleHeight)]]];
         [layout addLayoutWithRowWidth:768.f rowHeight:44.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(0.f, 0.0f, 768.0f, 44.f)]]];
        [layout addLayoutWithRowWidth:768.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 0.0f, 300.f, 250.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 0.0f, 441, 121)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 130, 441, 121)]]];//====videos recomendations
        [self.collectionView reloadData];
        [self.tableView reloadData];
        [self.tableView setHidden:YES];
        self.isPortrait = YES;
    } else {
         [layout addLayoutWithRowWidth:768.f rowHeight:510+self.capHeight+self.titleHeight layouts:@[[NSValue valueWithCGRect:CGRectMake(0.f, 0.0f, 768.0f, 510+self.capHeight+self.titleHeight)]]];
        [self.tableView reloadData];
        [self.collectionView reloadData];
        [self.tableView setHidden:NO];
        self.isPortrait = NO;
    }
}

- (BOOL) checkPortrait
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            return YES;
        }
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            return NO;
        }
        case UIInterfaceOrientationUnknown:break;
    }
    
    return YES;
}

#pragma mark actionsheet 

- (void)didPressShare {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Facebook",
                                  @"Twitter",
                                  @"Email",
                                  nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
    
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
            switch (buttonIndex) {
                case 0:
                    [self FBShare];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self emailShare];
                    break;
                default:
                    break;
            }
}

#pragma mark private function

- (void)FBShare {
    Media *media = [self.videoList objectAtIndex:self.index];
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    NSString *thumbnail= @"";
    NSString *caption = @"";
    NSString *content = @"";
    if(self.videoList.count){
       
        NSArray *mediaGroup = [media.mediaGroup copy];
        if(mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            caption = [dictionary valueForKey:@"caption"];
            content = [dictionary valueForKey:@"content"];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
        }
    }
    
    NSString *title = [NSString stringWithFormat:@"%@ %@\n\n%@", [Constants SHARE_PRE_TITTLE],content, caption];
    
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:thumbnail]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
        if(result == SLComposeViewControllerResultDone){
           [[AnalyticManager sharedInstance] trackFacebookShareVideoWithGuId:media.guid andArticleTitle:media.tittle];
        }
    };
    [self presentViewController:controller animated:YES completion:Nil];
    
}
- (void)TwitterShare {
    Media *media = [self.videoList objectAtIndex:(self.index)];
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    NSString *caption = @"";
    NSString *content = @"";
    if(self.videoList.count){
        
        NSArray *mediaGroup = [media.mediaGroup copy];
        if(mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            caption = [dictionary valueForKey:@"caption"];
            content = [dictionary valueForKey:@"content"];
        }
    }
    NSString *title = [NSString stringWithFormat:@"%@ %@",[Constants SHARE_PRE_TITTLE],caption];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:content]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
       if(result == SLComposeViewControllerResultDone){
            [[AnalyticManager sharedInstance] trackTwitterShareVideoWithGuid:media.guid andArticleTitle:media.tittle];
       }
    };
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)emailShare {
    if (self.videoList.count) {
        
        
        if ([MFMailComposeViewController canSendMail]) {
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [Util headerColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
            
            Media *media = [self.videoList objectAtIndex:self.index];
            NSString *thumbnail = @"";
            NSString *content = @"";
            if (self.videoList.count){
                NSArray *mediaGroup = [media.mediaGroup copy];
                if (mediaGroup.count){
                    NSDictionary *dictionary = [mediaGroup firstObject];
                    thumbnail = [dictionary valueForKey:@"thumbnail"];
                    content = [dictionary valueForKey:@"content"];
                }
            }
            NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE], [Constants SHARE_TITTLE]];
            NSString *messageBody = [NSString stringWithFormat:@"%@\n%@", media.tittle ,content];
            NSArray *toRecipents = [NSArray arrayWithObject:@""];
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:title];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTranslucent:NO];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            
            NSURL *url = [NSURL URLWithString:thumbnail];
            NSData *data = [NSData dataWithContentsOfURL:url];
            [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
            [self presentViewController:mc animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    if(result == MFMailComposeResultSent){
        Media *media = [self.videoList objectAtIndex:self.index];
        [[AnalyticManager sharedInstance] trackEmailSharePhotoWithGuid:media.guid andArticleTitle:media.tittle];
    }

    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark UITableviewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        return 0;
    }else {
         return [self.videoBrowserList count]-1;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Media *media = [self.videoBrowserList objectAtIndex:indexPath.row+1];
   
    NSDictionary *dictionary = [media.mediaGroup firstObject];
    NSString *thumbnail = [dictionary valueForKey:@"thumbnail"];
    TMsmallCurrentAffairsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"smallCAcell" forIndexPath:indexPath];
    [cell.categoryLabel setHidden:YES];
    [cell.bookmarkButton setHidden:YES];
    [cell.categoryLabel setFrame:CGRectZero];
    cell.tittleLabel.text = media.tittle;
    cell.dateLabel.text = [media.pubDate formatDate];
    [cell.imageview sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                      placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];
    [cell.bookmarkButton setHidden:YES];

    return cell;
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    
    
    
    
    if (self.videoList.count > 1) {
        label.text = [Constants YOU_MIGHT_ALSO_BE_INTERESTED_IN];
    } else {
        label.text = @"";
    }
    
    float height =0;
    if (!UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        if (self.videoList.count > 1) {
         height = [Util expectedHeightFromText:[Constants YOU_MIGHT_ALSO_BE_INTERESTED_IN] withFont:[UIFont systemFontOfSize:17] andMaximumLabelWidth:self.smallCellWidth.constant]+18;
        
        }
    }
    
    [label setFrame:CGRectMake(9, 0, tableView.frame.size.width - 18, height)];
    [view addSubview:label];
    return  view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    
    if(!UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        if (self.videoList.count > 1) {
            return [Util expectedHeightFromText:[Constants YOU_MIGHT_ALSO_BE_INTERESTED_IN] withFont:[UIFont systemFontOfSize:17] andMaximumLabelWidth:self.smallCellWidth.constant]+18;
        }
        
    }
    return 0;
 
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
        NSMutableArray *videosToPass = [NSMutableArray new];
        
        float itemIdex = 0;
        for(Media *media in self.videoList){
            
            if ([media isEqual:[self.videoBrowserList objectAtIndex:indexPath.row+1]]) {
                break;
            }
            itemIdex++;
        }
        
        
        int itemCount =[self.videoList count];
        int numberofSuggestions = 6;
        if(itemCount >= itemIdex+numberofSuggestions){
            int ctr = 0;
            while(ctr<=numberofSuggestions){
                [videosToPass addObject:[self.videoList objectAtIndex:itemIdex +ctr]];
                ctr++;
            }
        }else{
            int ctr = (itemIdex +numberofSuggestions)-itemCount+1;
            int index = 0;
            while(ctr<=numberofSuggestions){
                [videosToPass addObject:[self.videoList objectAtIndex:itemIdex +index]];
                ctr++;
                index++;
            }
            
            int extraindex = (itemIdex+numberofSuggestions)-itemCount+1;
            int extraCtr = 0;
            while(extraindex<=(itemIdex +numberofSuggestions)-itemCount+1 && extraindex!=0){
                [videosToPass addObject:[self.videoList objectAtIndex:0+extraCtr]];
                extraindex--;
                extraCtr++;
            }
        }
        
        
        
        TMiPADVideoDetailsViewController *vdc = (TMiPADVideoDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videoDetailsVC"];
        vdc.index = itemIdex;
        vdc.videoBrowserList = videosToPass;
        vdc.videoList = self.videoList;
        [self.navigationController pushViewController:vdc animated:YES];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





@end
