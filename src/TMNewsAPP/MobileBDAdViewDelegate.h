//
//  MobileBDAdViewDelegate.h
//  MobileBDSDK
//
//  Created by Le Vu on 22/1/15.
//  Copyright (c) 2015 Knorex. All rights reserved.
//

@class MobileBDAdView;
@class MobileBDError;

@protocol MobileBDAdViewDelegate<NSObject>

- (void)onMobileBDAdViewReady:(MobileBDAdView *)adView;
- (void)onMobileBDAdViewFailed:(MobileBDAdView *)adView
                     withError:(MobileBDError *)error;

@end