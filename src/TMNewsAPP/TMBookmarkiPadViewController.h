//
//  TMBookmarkiPadViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 2/25/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMBookmarkiPadViewController : UIViewController
@property(nonatomic, weak) IBOutlet UILabel *noItemsLabel;
@end
