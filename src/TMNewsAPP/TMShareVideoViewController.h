//
//  TMShareVideoViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>





@interface TMShareVideoViewController : UIViewController

@property (nonatomic) NSUInteger index;
@property (nonatomic, strong) NSArray *videoList;
@property (nonatomic, strong) NSArray *videoBrowserList;
@property (nonatomic) BOOL isfromMain;

@end

