//
//  TMNewsViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/8/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMNewsViewController.h"
#import "SwipeView.h"
#import "TMNewsTableViewCell.h"
#import "TMNewsPagesViewController.h"
#import "TMNewsDetailsViewController.h"
#import "UIImageView+AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Helpers.h"
#import "NIAttributedLabel.h"
#import "TMPhotoBrowserViewController.h"
#import "TMMainPageViewController.h"

#import <MediaPlayer/MediaPlayer.h>
#import "TMDetailedBeritaViewController.h"
#import "SMPageControl.h"
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "Article.h"
#import "DataManager.h"
#import "TMSwipeViewTableViewCell.h"
#import "TMBreakingNewsTableViewCell.h"
#import "Reachability.h"
#import "Constants.h"
#import "NSString+Helpers.h"
#import "UIImageView+Helpers.h"
#import "TMWebViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AppDelegate.h"
#import "TMMoviePlayerViewController.h"
#import "TMShareVideoViewController.h"
#import "AnalyticManager.h"
#import "UIView+Toast.h"
#import "TMMoviePlayerViewController.h"
#import "TMWebViewController.h"

@interface TMNewsViewController ()<SwipeViewDataSource, SwipeViewDelegate, UITableViewDelegate, UITableViewDataSource, TMMainPageViewControllerDelegate, TMBreakingNewsTableViewCellDelegate, TMNewsTableViewCellDelagate, TMNewsDetailsViewControllerDelagate, TMNewsPagesViewControllerDelagate, GADBannerViewDelegate, GADAdSizeDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *breakingNewsView;

@property (nonatomic, strong) TMSwipeViewTableViewCell *swipeViewCell;
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, strong) SMPageControl *tnPageControl;
@property (nonatomic, strong) SMPageControl *bnPageControl;
@property (nonatomic, strong) TMMoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) NSMutableArray *sectionsArray;
@property (nonatomic, strong) NSString *topNewsAPI;
@property (nonatomic, strong) NSString *latestNewsAPI;
@property (nonatomic, strong) NSString *cellTitle;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UIView *connectionStatusView;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *topNewsArray;
@property (nonatomic, strong) NSMutableArray *bnArray;
@property (nonatomic, strong) NSMutableArray *tableItemsArray;
@property (nonatomic) BOOL hasadvertorial;
@property (nonatomic, strong) SwipeView *tnSwipeView;
@property (nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, strong) SwipeView *bnSwipeView;
@property (nonatomic) BOOL hasHeader;
@property (nonatomic) BOOL didCloseBN;
@property (nonatomic, strong) UIView *addsView;
- (IBAction)didTapCloseBreakingNewsButton:(id)sender;

@end

@implementation TMNewsViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _tableItemsArray = [[NSMutableArray alloc] init];
    _topNewsArray = [[NSMutableArray alloc] init];
    _bookmarks = [[NSMutableArray alloc] init];
    
    
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadBookmarkedItems];
    
    [[AnalyticManager sharedInstance] trackLandingPage];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadTopNewsWithForce:)
                                                 name:@"reloadDataNotif"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    
    [NSTimer scheduledTimerWithTimeInterval:6.0f target:self selector:@selector(slideBreakingNews) userInfo:nil repeats:YES];
    
    self.connectionStatusView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30.0f)];
    [self.connectionStatusView setBackgroundColor:[UIColor blackColor]];
    
    [NSTimer scheduledTimerWithTimeInterval:Constants.BREAKING_NEWS_REQUEST_INTERVAL target:self selector:@selector(requestBreakingNews) userInfo:nil repeats:YES];
  
    // Do any additional setup after loading the view.
   // self.timer =  [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(autoSlide) userInfo:nil repeats:YES]; << removed for the moment
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:self action:@selector(reloadTableData:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshFeed];

    
   // UINavigationController *nav = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
   // TMMainPageViewController *mpVC = (TMMainPageViewController *)[nav.viewControllers firstObject];
   // mpVC.didFinishRequestDelegate = self;
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:Constants.HAS_BREAKING_NEWS_KEY]) {
        [self requestBreakingNews];
    }
    
    [self loadTopNewsWithForce:NO];
    
    
    [self setUpads];
       
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma  mark Swipeview Delegate

-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    if (swipeView == self.tnSwipeView) {
        self.tnPageControl.currentPage = swipeView.currentPage;
        
    }else if (swipeView == self.bnSwipeView){
        self.bnPageControl.currentPage = swipeView.currentPage;
        [swipeView reloadData];
    }
}

-(void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index
{
    if (swipeView == self.tnSwipeView) {
        Article *article = (Article *)[self.topNewsArray objectAtIndex:index];
        NSArray *mediaGroupArray = article.mediagroupList;
        NSString *feedLink = article.feedlink;
        NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:0];
        NSString *content = [mediaDictionary valueForKey:@"content"];
        NSString *type = article.type;
        NSString *sectionType = article.sectionType;
        NSString *stringUrl = article.sectionURL;
        
        if([type isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants EPISODE_TYPE]]){
            TMNewsPagesViewController *pages = (TMNewsPagesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsPagesID"];
            pages.delegate = self;
            if (_bookmarks.count) {
                for (Article *articleFromBookmark in _bookmarks) {
                    if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                        pages.isBookmark = YES;
                        break;
                    }
                }
            }
            pages.isTopNews = YES;
            pages.article = [self.topNewsArray objectAtIndex:index];
            [[AnalyticManager sharedInstance] trackOpenNewsDetailScreenWithCategoryName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
            [self.navigationController pushViewController:pages animated:YES];
           
        }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){

            TMPhotoBrowserViewController *photoBrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
            photoBrowser.article = article;
            photoBrowser.view.alpha = 1;
            photoBrowser.isTopNews = YES;
            photoBrowser.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [self presentViewController:photoBrowser animated:YES completion:nil];
            
        }else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]){
            Media *media = [Media new];
            media.tittle = article.tittle;
            media.mediaGroup = (NSMutableArray *)article.mediagroupList;
            media.guid = article.guid;
            media.pubDate = article.pubDate;
            media.sectionUrl = article.sectionURL;
            media.feedlink = article.feedlink;
            media.downloadTime = article.downloadTime;
            media.brief = article.brief;
            
            TMShareVideoViewController *svc = (TMShareVideoViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"shareVideoController"];
            svc.index = index;
            svc.videoBrowserList = [[NSMutableArray alloc]initWithObjects:media, nil];
            svc.videoList = [[NSMutableArray alloc]initWithObjects:media, nil];
            [[AnalyticManager sharedInstance] trackOpenVideoWithGuid:media.guid andArticleTitle:media.tittle];
            [self.navigationController pushViewController:svc animated:YES];
            
        }else if([type isEqualToString:[Constants CA_TYPE]]){
            
            [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:feedLink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                if ([array count]) {
                    Article *article = [array objectAtIndex:0];
                    NSArray *sectionsArray = article.sections;
                    TMDetailedBeritaViewController *dbvc = (TMDetailedBeritaViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"detailedBeritaID"];
                    dbvc.newsCategories = sectionsArray;
                    dbvc.selectedIndex = 0;
                    dbvc.isTopNews = YES;
                    dbvc.feedLink = feedLink;
                    [[AnalyticManager sharedInstance] trackOpenCAProgramScreenWithCategoryname:article.enCategory];
                    [self.navigationController pushViewController:dbvc animated:YES];
                }
            }];
            
        }else if ([type isEqualToString:[Constants SPECIAL_REPORT_TYPE]]){
            
            TMDetailedBeritaViewController *dbvc = (TMDetailedBeritaViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"detailedBeritaID"];
            dbvc.specialReportArticle = article;
            dbvc.screenType = SpecialReportScreenType;
            dbvc.isTopNews = YES;
            [[AnalyticManager sharedInstance] trackOpenSpecialReportScreenWithCategoryName:article.tittle];
            [self.navigationController pushViewController:dbvc animated:YES];
        }

    }else if (swipeView == self.bnSwipeView){
        Article *article = (Article *)[self.bnArray objectAtIndex:index];
        NSString *link = article.link;
        if (![link isEqualToString:@""]){
            TMWebViewController *mwvc = (TMWebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            mwvc.title = [Constants LANDING_BREAKING_NEWS];
            mwvc.urlString = link;
            [self.navigationController pushViewController:mwvc animated:YES];
        }
    }
}
- (void)swipeViewWillBeginDragging:(SwipeView *)swipeView
{
  //[self.timer invalidate];
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    
    if ([self.topNewsArray count]) {
        
        float width = [UIScreen mainScreen].bounds.size.width;
        float aheight = 9;
        float awidth = 16;
        [self.swipeViewCell.swipeView setFrame:CGRectMake(0, 0, width, (aheight/awidth * width) +60)];
        self.hasHeader = true;
    }else{
        [self.swipeViewCell.swipeView setFrame:CGRectZero];
        self.hasHeader = false;
    }
    
    if (swipeView == self.tnSwipeView) {
        self.tnPageControl.numberOfPages = [self.topNewsArray count];
        return [self.topNewsArray count];
    }else if(swipeView == self.bnSwipeView){
        self.bnPageControl.numberOfPages = [self.bnArray count];
        return [self.bnArray count];
    }
    return 0;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    float width = [UIScreen mainScreen].bounds.size.width;
    float aheight = 9;
    float awidth = 16;
    if (swipeView == self.tnSwipeView) {
        
        UIImageView *imageView = nil;
        UILabel *tittleLabel = nil;
        UIImageView *playOverlay = nil;
        UIImageView *gradientView = nil;
        NIAttributedLabel *datelabel = nil;
        
        if (view == nil) {
            view = [[UIView alloc] initWithFrame:swipeView.bounds];
            
            [view setBackgroundColor:[UIColor colorWithRed:2.0/225.0 green:19.0/225.0 blue:29.0/225.0 alpha:1]];
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, aheight/awidth * width)];
            imageView.tag = 1;
            gradientView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gradient"]];
            [gradientView setFrame:CGRectMake(0, aheight/awidth * width-42, width, 43)];
            gradientView.tag = 5;
            
            tittleLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, (aheight/awidth * width), width-16, 40)];
            [tittleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:17]];
            [tittleLabel setTextColor:[UIColor whiteColor]];
            [tittleLabel setNumberOfLines:2];
            tittleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [tittleLabel setBackgroundColor:[UIColor clearColor]];
            tittleLabel.tag = 2;
            
            datelabel = [[NIAttributedLabel alloc] initWithFrame:CGRectMake(8, (aheight/awidth * width)+40, width-16, 20)];
            datelabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [datelabel setBackgroundColor:[UIColor clearColor]];
            datelabel.tag = 3;
            
            playOverlay = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"play_overlay"]];
            [playOverlay setFrame:CGRectMake(8, (aheight/awidth * width)-28, 20, 20)];
            playOverlay.tag =4;
            [playOverlay setHidden:YES];
            
            [view addSubview:datelabel];
            [view addSubview:imageView];
            [imageView addSubview:gradientView];
            [imageView addSubview:playOverlay];
            [view addSubview:tittleLabel];
        } else {
            imageView = (UIImageView *)[view viewWithTag:1];
            tittleLabel = (UILabel *)[view viewWithTag:2];
            datelabel = (NIAttributedLabel *)[view viewWithTag:3];
            playOverlay = (UIImageView *)[view viewWithTag:4];
            gradientView =  (UIImageView *)[view viewWithTag:5];
        }
        
        if (self.topNewsArray .count) {
            Article *article = (Article *)[self.topNewsArray objectAtIndex:index];
            
            NSArray *mediaGroupArray = article.mediagroupList;
            NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:0];
            NSString *photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
            NSString *type = [mediaDictionary valueForKey:@"type"];
            NSString *enTitle = article.tittle;
            NSString *enCategory = article.category;
            NSString *datestring = article.pubDate;
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
            NSDate *pubDate = [dateFormatter dateFromString:datestring];
            NSString *formatedDate = [pubDate dateString];
            NSString *date = [NSString stringWithFormat:@"%@ %@",enCategory,formatedDate];
            datelabel.text = date;
            if ([type isEqualToString:@"video"]){
                playOverlay.hidden = false;
                
            }else{
                playOverlay.hidden = true;
                
            }
            
            if (enCategory != nil){
                [datelabel setFont:[UIFont fontWithName:@"HelveticaNeue-bold" size:13] range:[date rangeOfString:enCategory]];
                [datelabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12] range:[date rangeOfString:formatedDate]];
                [datelabel setTextColor:[UIColor colorWithRed:0/255.0 green:151.0/255.0 blue:255.0/255.0 alpha:1.0] range:[date rangeOfString:enCategory]];
                [datelabel setTextColor:[UIColor colorWithRed:175/255.0 green:188.0/255.0 blue:195.0/255.0 alpha:1.0] range:[date rangeOfString:formatedDate]];
            }
            
            
            tittleLabel.text = enTitle;
            self.cellTitle = enTitle;
            [imageView sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                         placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:index == 0 ? SDWebImageRefreshCached : 0];
        }
       

    }else if(swipeView == self.bnSwipeView){
      //  [swipeView setTruncateFinalPage:YES];
        [swipeView setWrapEnabled:YES];
        UILabel *tittleLabel = nil;
        UILabel *breakingNewsLabel = nil;
         if (view == nil) {
             view = [[UIView alloc] initWithFrame:CGRectMake(0, 10, width, 30)];
             [view setBackgroundColor:[UIColor clearColor]];
              tittleLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0,width-16, 40)];
             [tittleLabel setNumberOfLines:2];
             [tittleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
             tittleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
             [tittleLabel setBackgroundColor:[UIColor clearColor]];
             breakingNewsLabel = [[UILabel alloc] init];
             breakingNewsLabel = (UILabel *)[swipeView viewWithTag:999];
             [view addSubview:tittleLabel];
         }else {
             tittleLabel = (UILabel *)[view viewWithTag:22];
             breakingNewsLabel = (UILabel *)[swipeView viewWithTag:999];
         }

        Article *article = (Article *)[self.bnArray objectAtIndex:index];
        NSString *title =article.tittle;
        [tittleLabel setText:title];
        breakingNewsLabel.text = [Constants LANDING_BREAKING_NEWS];

    }
    
    return view;
}


- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    if (swipeView == self.tnSwipeView) {
         return self.tnSwipeView.bounds.size;
    }else if (swipeView == self.bnSwipeView){
        return self.bnSwipeView.bounds.size;
    }
    return CGSizeZero;
}


#pragma mark tableView DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0){
        return 1;
    }else if (section == 1 && self.hasHeader) {
        return 1;
    }else if(section == 2){
        if (self.hasadvertorial) {
            return [self.tableItemsArray count] + 1;
        }
        return [self.tableItemsArray count] ;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   if(indexPath.section == 0){
        if ([[NSUserDefaults standardUserDefaults]boolForKey:Constants.HAS_BREAKING_NEWS_KEY] && self.bnArray.count) {
            [self.bnPageControl reloadInputViews];
            [self.bnSwipeView reloadData];
            return 65;
        }else{
            return 0;
        }
    }else if (indexPath.section == 1) {
        float width = [UIScreen mainScreen].bounds.size.width;
        float aheight = 9;
        float awidth = 16;
        return (aheight/awidth * width)+70;
    }else if (indexPath.section == 2){
        return 85;
    }
    return 0;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        TMBreakingNewsTableViewCell *cell = (TMBreakingNewsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bnCell" forIndexPath:indexPath];
        self.bnSwipeView = cell.bnSwipeview;
         [cell resignFirstResponder];
        cell.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1.0];
        cell.pageControl.backgroundColor = [UIColor clearColor];
        cell.pageControl.alpha = 1;
        cell.pageControl.hidesForSinglePage =YES;
        cell.pageControl.indicatorDiameter = 5.0f;
        cell.pageControl.alignment = SMPageControlAlignmentRight;
        cell.pageControl.indicatorMargin = 2.0f;
        cell.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:208.0f/255.0f green:168.0/255.0 blue:38.0f/255.0 alpha:1];
        self.bnPageControl = cell.pageControl;
        [self.bnPageControl resignFirstResponder];
        cell.delegate = self;
       
        return cell;
    }else if (indexPath.section == 1 && self.hasHeader) {
        TMSwipeViewTableViewCell *cell = (TMSwipeViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"svCell" forIndexPath:indexPath];
        [cell resignFirstResponder];
        cell.swipeView.tag = 111;
        self.tnSwipeView = cell.swipeView;
        [cell.swipeView reloadData];
        cell.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:26.0f/255.0f green:153.0f/255.0f blue:252.0f/255.0f alpha:1.0];
        cell.pageControl.backgroundColor = [UIColor clearColor];
        cell.pageControl.alpha = 1;
        cell.pageControl.hidesForSinglePage =YES;
        cell.pageControl.indicatorDiameter = 5.0f;
        cell.pageControl.alignment = SMPageControlAlignmentRight;
        cell.pageControl.indicatorMargin = 2.0f;
        cell.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:11.0f/255.0f green:92.0f/255.0f blue:147.0f/255.0f alpha:1];
        self.tnPageControl = cell.pageControl;
        //[self.swipeViewCell bringSubviewToFront:self.swipeViewCell.pageControl];
        
        return cell;
    }else if(indexPath.section == 2){
        TMNewsTableViewCell *cell = (TMNewsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
        NSString *photoThumbnail = @"";
        if (self.tableItemsArray.count) {
            if (self.hasadvertorial && indexPath.row == 3) {
                NSDictionary *dictionary = [self.tableItemsArray objectAtIndex:indexPath.row];
                photoThumbnail = dictionary[@"thumbnail"];
                cell.indexPath = indexPath;
                cell.titleLabelLATEST.text = dictionary[@"title"];
                cell.categoryLabelLATEST.text = @"Tajaan";
                cell.dateLabelLATEST.text = dictionary[[@"pubDate" formatDate]];
                [cell.starButton setHidden:YES];
                [cell.playOverlay setHidden:YES];
                [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                  placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                return cell;
            } else {
                Article *article = (Article *)[self.tableItemsArray objectAtIndex:indexPath.row];
                cell.delegate = self;
                cell.indexPath = indexPath;
                cell.titleLabelLATEST.text = article.tittle;
                cell.categoryLabelLATEST.text = article.category;
                cell.dateLabelLATEST.text = [article.pubDate formatDate];
                [cell.thumbnail setBorder];
                
                [cell.thumbnail.layer setBorderWidth:1.0];
                [cell.thumbnail.layer setBorderColor:[[UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1] CGColor]];
                
                
                if (article.mediagroupList) {
                    if (article.mediagroupList.count) {
                        NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                        photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                        NSString *type = [mediaDictionary valueForKey:@"type"];
                        if ([type isEqualToString:@"video"]) {
                            [cell.playOverlay setHidden:NO];
                        } else {
                            [cell.playOverlay setHidden:YES];
                        }
                    }
                }
                
                [cell.starButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:article]];
                [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                  placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                return cell;
            }
        }
        
        
    }
    return nil;

}


#pragma mark tableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int adIndex = 3;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.hasadvertorial && indexPath.row == adIndex) {
        NSDictionary *dictionary = [self.tableItemsArray objectAtIndex:indexPath.row];
        NSString *link = dictionary[@"link"];
        TMWebViewController *webView = (TMWebViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
        webView.urlString = link;
        [self.navigationController pushViewController:webView animated:YES];
    } else {
        TMNewsDetailsViewController *detailednewsVC = (TMNewsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetailsID"];
        detailednewsVC.screenType = LatestNewsScreen;
        detailednewsVC.passedArray = self.tableItemsArray;
        detailednewsVC.index = indexPath.row;
        detailednewsVC.didTapBookmarkButtonDelegate =self;
        detailednewsVC.screenType = LatestNewsScreen;
        
        Article *article = (Article *)[self.tableItemsArray objectAtIndex:indexPath.row];
        if (_bookmarks.count) {
            for (Article *articleFromBookmark in _bookmarks) {
                if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                    detailednewsVC.isBookmark = YES;
                    break;
                }
            }
        }
        
        [self.navigationController pushViewController:detailednewsVC animated:YES];
    }
    
   
}

#pragma mark TMNewsTableViewCellDelagate
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    Article *article = (Article *) [self.tableItemsArray objectAtIndex:indexPath.row];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
        
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [_bookmarks removeObject:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [_bookmarks addObject:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
        }

    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
         [_bookmarks addObject:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
    }
    
    if (!bookmarkArticle){
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    }
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
}

#pragma mark Private Functions



-(void)slideBreakingNews{
    if([self.bnArray count] > 1){
        [self.bnSwipeView scrollByNumberOfItems:1 duration:0];
    }
    
}
- (void)checkNetworkStatus {
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        [self displayNoNetwork];
    } else {
        [self dismissNoNetworkDisplay];
    }
}

-(void)loadBookmarkedItems
{
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.tableView reloadData];
        }
    }];
}

- (void) requestBreakingNews{

    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        for(NSDictionary *dictionary in array){
            NSString *enTitle = [dictionary valueForKey:@"enTitle"];
            if ([enTitle isEqualToString:@"breaking news"]){
                NSString *breakingNewsUrl = [dictionary valueForKey:@"sectionURL"];
                [[LibraryAPI sharedInstance]getArticlesWithURL:breakingNewsUrl isForced:YES withCompletionBlock:^(NSMutableArray *array) {

                    if([array count]){
                        Article *newArticle = [array firstObject];
                        NSString *oldArticleGUID = [[NSUserDefaults standardUserDefaults]objectForKey:Constants.BREAKING_NEWS_GUID];
                        if (oldArticleGUID) {
                            if(![newArticle.guid isEqualToString:oldArticleGUID]){
                                
                                [[NSUserDefaults standardUserDefaults]removeObjectForKey:Constants.BREAKING_NEWS_GUID];
                                [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:Constants.HAS_BREAKING_NEWS_KEY];
                    
                                [[NSUserDefaults standardUserDefaults]setObject:newArticle.guid forKey:Constants.BREAKING_NEWS_GUID];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                self.bnArray = [array mutableCopy];
                                [self.tableView beginUpdates];
                                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                                [self.tableView endUpdates];
                            }else{
                                if ([[NSUserDefaults standardUserDefaults]boolForKey:Constants.HAS_BREAKING_NEWS_KEY]) {
                                    self.bnArray = [array mutableCopy];
                                    [self.tableView beginUpdates];
                                    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                                    [self.tableView endUpdates];
                                }
                            }
                        }else{
                            [[NSUserDefaults standardUserDefaults]removeObjectForKey:Constants.BREAKING_NEWS_GUID];
                            [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:Constants.HAS_BREAKING_NEWS_KEY];
                            [[NSUserDefaults standardUserDefaults]setObject:newArticle.guid forKey:Constants.BREAKING_NEWS_GUID];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            self.bnArray = [array mutableCopy];
                            [self.tableView beginUpdates];
                            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                            [self.tableView endUpdates];
                        }
                      

                    } else {
                        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:Constants.HAS_BREAKING_NEWS_KEY];
                    }
                    
                }];
                break;
            }
        }
    }];
  
    
}



- (void) handleNetworkChanged: (NSNotification *)notification
{
    [self checkNetworkStatus];
}

- (void) displayBreakingNews
{
  
}

- (void) dismissBreakingNews
{

}

-(void) displayNoNetwork {
    
    self.connectionStatusView.frame = CGRectMake(0.0f, -30.0f, self.tableView.frame.size.width, 30.0f);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(self.connectionStatusView.frame.origin.x / 2, (self.connectionStatusView.frame.origin.y +  self.connectionStatusView.frame.size.height), self.connectionStatusView.frame.size.width, 13.0f)];
    label.center = CGPointMake(self.connectionStatusView.center.x, self.connectionStatusView.center.y + 30.0f);
    [label setFont:[UIFont systemFontOfSize:12.0f]];
    label.text = [Constants DIALOG_NETWORK_MESSAGE];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    UIButton *buttonClose = [[UIButton alloc] initWithFrame:CGRectMake(self.connectionStatusView.frame.size.width - 8.0f, self.connectionStatusView.frame.origin.y / 2, 15.0f, 14.0f)];
    buttonClose.center = CGPointMake(self.connectionStatusView.frame.size.width - 16.0f, self.connectionStatusView.center.y + 30.0f);
    [buttonClose setImage:[UIImage imageNamed:@"icon_close_breaking" ] forState:UIControlStateNormal];
    [buttonClose addTarget:self action:@selector(didTapCloseConnectionCell) forControlEvents:UIControlEventTouchUpInside];
    
    [self.connectionStatusView addSubview:label];
    [self.connectionStatusView addSubview:buttonClose];
    
    [self.view addSubview:self.connectionStatusView];
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.tableView setContentInset:UIEdgeInsetsMake(30.0f, 0.0f, 0.0f, 0.0f)];
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, self.tableView.frame.size.width, self.connectionStatusView.frame.size.height) animated:NO];
        self.connectionStatusView.center = CGPointMake(self.connectionStatusView.center.x, self.connectionStatusView.center.y + 30.0f);
    } completion:^(BOOL finished) {
    }];
}

-(void)dismissNoNetworkDisplay {
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.connectionStatusView.center = CGPointMake(self.connectionStatusView.center.x, self.connectionStatusView.center.y - 30.0f);
        [self.tableView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    } completion:^(BOOL finished) {
        [self.connectionStatusView removeFromSuperview];
    }];
}

- (void) didTapCloseConnectionCell
{
    [self dismissNoNetworkDisplay];
}



-(void)autoSlide
{
    if (self.swipeViewCell.swipeView.currentItemIndex != self.swipeViewCell.swipeView.numberOfItems-1) {
        [self.swipeViewCell.swipeView scrollToPage:self.swipeViewCell.swipeView.currentItemIndex +1 duration:0.5];
    }else{
        [self.swipeViewCell.swipeView setCurrentItemIndex:0];
    }
}
-(void)reloadTableData: (UIRefreshControl *)refreshFeed
{
    [self loadTopNewsWithForce:YES];
    //[self loadLatestNewsWithForce:YES];
    if (refreshFeed) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        [refreshFeed endRefreshing];
        
    }
    
}
-(void)loadTopNewsWithForce:(BOOL)isForced{
    [[LibraryAPI sharedInstance]getTopNewsWithForce:isForced andCompletionBlock:^(NSMutableArray *array) {
        
        [self.topNewsArray removeAllObjects];
        for (id object in array) {
            Article *article = (Article *)object;
            NSString *category = article.sectionName;
            if ([category isEqualToString:@"home"]){
                
                [self.topNewsArray addObject:article];
                
            }
        }
        if ([self.topNewsArray count]) {
            self.hasHeader = true;
            [self.tnSwipeView reloadData];
            [self.tableView reloadData];
            
        }
        
        [self loadLatestNewsWithForce:isForced];
        //[self getAdvertorialWithForce:isForced];
        
    }];
    
    
}

-(void)loadLatestNewsWithForce:(BOOL)isForced{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LibraryAPI sharedInstance] getLatestNewsWithForce:isForced andCompletionBlock:^(NSMutableArray *array) {
        self.tableItemsArray = [array mutableCopy];
        
       [self.tableView reloadData];
        [self loadBookmarkedItems];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [self getAdvertorialWithForce:NO];
        [self checkNetworkStatus];
    }];
    
}

- (void)getAdvertorialWithForce: (BOOL)isForced {
    [[LibraryAPI sharedInstance] getAdvertorialWithForce:isForced andCompletionBlock:^(NSError *error, NSDictionary *dict) {
        if (!error) {
            if (self.hasadvertorial) {
                [self.tableItemsArray insertObject:dict atIndex:3];
            }
            self.hasadvertorial = YES;
            [self.tableView reloadData];
            
            
        }
    }];
}

#pragma mark TMBreakingNewsTableViewCellDelegate
-(void)closeBreakingNews{
   [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:Constants.HAS_BREAKING_NEWS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}


#pragma mark TMNewsDetailsViewControllerDelagate
-(void)didTapBookmarkButton
{
    [self loadBookmarkedItems];
}
#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    self.bannerView.adSize = kGADAdSizeBanner;
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       ]];*/
  //  self.bannerView.adSizeDelegate = self;
    
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:request];
}
#pragma mark GADbannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view{
    self.bannerHeight.constant = 50;
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"adBanner: %@",error);
    self.bannerHeight.constant = 0;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end