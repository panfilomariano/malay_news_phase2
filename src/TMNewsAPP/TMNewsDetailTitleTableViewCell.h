//
//  TMNewsDetailTitleTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/22/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMNewsDetailTitleTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *updateLabel;
@end
