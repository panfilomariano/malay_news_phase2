//
//  TMHeaderSectionCollectionReusableView.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/10/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMHeaderSectionCollectionReusableView : UICollectionReusableView
@property (nonatomic, weak) IBOutlet UILabel *sectionTitle;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *height;
@end
