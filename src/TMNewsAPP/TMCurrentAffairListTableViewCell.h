//
//  TMCurrentAffairListTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCurrentAffairListTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *caTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *caPubDateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *caThumbnail;

@end
