//
//  NSString+PercentEscapes.h
//  EightDays
//
//  Created by Payne Chu on 1/2/14.
//  Copyright (c) 2014 PI Entertainment Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(PercentEscapes)

- (NSString *)stringByAddingPercentEscapes;
- (NSString *)stringByReplacingPercentEscapes;

@end
