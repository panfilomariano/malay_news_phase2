//
//  TMPhotosViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/8/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
@interface TMPhotosViewController : UIViewController

typedef enum
{
    OthersPhotosScreenType,
    LatestPhotosScreenType,
    CAPhotosScreenType,
    SpecialReportPhotosScreenType,
    NewsPhotosScreenType,
    MenuPhotosScreenType
} PhotosScreenType;

@property (nonatomic, strong) NSString *photoAPI;
@property (nonatomic, strong) NSString *apistring;
@property (nonatomic, strong) NSString *photoEklusifTitle;
@property (nonatomic, strong) NSString *photoCATitle;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic) BOOL isFromMain;
@property (nonatomic, strong) NSString *sectionURL;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@property (nonatomic, assign) PhotosScreenType screenType;

@end
