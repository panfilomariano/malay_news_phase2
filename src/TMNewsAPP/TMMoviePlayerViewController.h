//
//  TMMoviePlayerViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/14/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface TMMoviePlayerViewController : MPMoviePlayerViewController

@end
