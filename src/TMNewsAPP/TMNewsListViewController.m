//
//  TMNewsListViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/5/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMNewsListViewController.h"
#import "TMNewsCollectionViewLayout.h"
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "TMSquareCollectionViewCell.h"
#import "TMSmallCollectionViewCell.h"
#import "NSString+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+Helpers.h"
#import "TMSwipeCollectionViewCell.h"
#import "Util.h"
#import "SMPageControl.h"
#import "Constants.h"
#import <MediaPlayer/MediaPlayer.h>
#import "TMNewsPagerViewController.h"
#import "TMNewsDetailsPagerViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AppDelegate.h"
#import "TMCAandSRCollectionViewCell.h"
#import "TMExclusiveCollectionViewCell.h"
#import "TMNewsArticleViewController.h"
#import "TMPhotoBrowserViewController.h"
#import "Media.h"
#import "TMiPADVideoDetailsViewController.h"
#import "TMSquareBannerCollectionViewCell.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AnalyticManager.h"
#import "UIView+Toast.h"
#import "TMMoviePlayerViewController.h"

@interface TMNewsListViewController () <TMNewsCollectionViewLayoutDelegate, UICollectionViewDataSource, UICollectionViewDelegate, TMSquareCollectionViewCellDelagate, TMSmallCollectionViewCellDelagate, SwipeViewDataSource, SwipeViewDelegate,TMCAandSRCollectionViewCellDelagate,TMNewsDetailsPagerViewControllerDelagate, GADBannerViewDelegate>
{
    dispatch_group_t group;
}

@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) UINib *squareCell;
@property (nonatomic, strong) UINib *smallCell;
@property (nonatomic, strong) UINib *bigCell;
@property (nonatomic, strong) UINib *squareBannerCell;
@property (nonatomic, strong) UINib *caAndSRCell;
@property (nonatomic, strong) SMPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *newsArray;
@property (nonatomic, strong) NSMutableArray *topNewsArray;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *passedSectionNewsForTab;;
@property (nonatomic, strong) TMMoviePlayerViewController *moviePlayer;

@property (nonatomic, strong) GADBannerView * gADBannerView;
@property (nonatomic, strong) GADBannerView * gADBannerView2;
@property (nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;

@property (nonatomic) BOOL isPortrait;
@property (nonatomic) BOOL hasAds;
@property (nonatomic) BOOL hasPreviousContents;

@end

@implementation TMNewsListViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _newsArray = [[NSMutableArray alloc] init];
    _topNewsArray = [[NSMutableArray alloc] init];
    _bookmarks = [[NSMutableArray alloc] init];
    
    self.squareCell = [UINib nibWithNibName:@"SquareCollectionViewCell" bundle:nil];
    self.smallCell = [UINib nibWithNibName:@"LongCollectionViewCell" bundle:nil];
    self.bigCell = [UINib nibWithNibName:@"BigCollectionViewCell" bundle:nil];
    self.squareBannerCell = [UINib nibWithNibName:@"SquareBannerCollectionViewCell" bundle:nil];
    self.caAndSRCell = [UINib nibWithNibName:@"TMCAandSPfirstItemCollectionViewCell" bundle:nil];
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    self.isPortrait = UIInterfaceOrientationIsPortrait(self.interfaceOrientation);
    
    TMNewsCollectionViewLayout* layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    layout.delegate = self;
    [self loadLayout:layout forOrientation:self.interfaceOrientation];
    
    [self requestContentsWithForce:NO];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.hasAds = YES;
    
    self.collectionView.alwaysBounceVertical = YES;
    [self.collectionView registerNib:self.squareCell  forCellWithReuseIdentifier:@"squareCell"];
    [self.collectionView registerNib:self.smallCell  forCellWithReuseIdentifier:@"smallCell"];
    [self.collectionView registerNib:self.bigCell  forCellWithReuseIdentifier:@"bigCell"];
    [self.collectionView registerNib:self.squareBannerCell  forCellWithReuseIdentifier:@"squareBannerCell"];
    [self.collectionView registerNib:self.caAndSRCell  forCellWithReuseIdentifier:@"CAandSRCell"];
    [self.collectionView setContentInset:UIEdgeInsetsMake(9.0f, 0.0f, 0.0f, 0.0f)];
    
    self.bannerHeight.constant = 0;
    [Util setRefreshControlWithTarget:self inView:self.collectionView withSelector:@selector(reloadTableData:)];
    if (self.type == SpecialReportType){
         self.bannerHeight.constant = 0;
         [self setUpads];
    }
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        self.isPortrait = YES;
    } else {
        self.isPortrait = NO;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CollectionViewLayout Delegate
- (NSUInteger) layoutIndexAtIndexPath: (NSIndexPath *)indexPath
{
    int itemsPerLayout = (self.isPortrait) ? 3 : 5;
    
    if(self.isPortrait) {
        if(self.type ==  SpecialReportType){
            if (indexPath.row == _newsArray.count - 1) {
                return 3;
            }
            return 2;
        }
        if (_topNewsArray.count || self.type == EpisodesType || self.screenType == SpecialReportNewsListScreenType) {
            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
                return 0;
            }
        }
        return 1;
    } else {
        if(self.type ==  SpecialReportType){
            if (indexPath.row == _newsArray.count - 1) {
                return 3;
            }
            return 2;
            
        }
        if (_topNewsArray.count || self.type == EpisodesType || self.screenType == SpecialReportNewsListScreenType){
            if (indexPath.row <= 4) {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

#pragma mark GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"adViewDidReceiveAd");

    if (adView.tag == 555) {
        [self.bannerView setHidden:NO];
        if (self.bannerHeight.constant == 0)
            self.bannerHeight.constant = 50;
    } else {
        if (!self.hasAds) {
            self.hasAds = YES;
            
            [self.collectionView reloadData];
        }
    }
    
}

- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error{

    if (adView.tag == 555) {
        [self.bannerView setHidden:YES];
        self.bannerHeight.constant = 0;
    } else {
        if (self.hasAds) {
            self.hasAds = NO;
            
            [self.collectionView reloadData];
        }
    }
    NSLog(@"adViewDidFailToReceiveAdWithError : %@", [error localizedDescription]);
    
}
#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    int addCount = 0;

    if(self.hasAds && self.type != SpecialReportType){
        
        if(self.isPortrait){
            if (_newsArray.count > 30 ) {// because of add placements need to
                addCount += 3;
            } else if (_newsArray.count > 18 ) {
                addCount += 2;
            } else if (_newsArray.count > 6) {
                addCount += 1;
            }
        }else{
            if (_topNewsArray.count) {
                if (_newsArray.count > 41) {// because of add placements need to
                    addCount += 3;
                } else if (_newsArray.count > 25) {
                    addCount += 2;
                } else if (_newsArray.count > 9) {
                    addCount += 1;
                }
            } else {
                if (_newsArray.count > 40) {// because of add placements need to
                    addCount += 3;
                } else if (_newsArray.count > 24) {
                    addCount += 2;
                } else if (_newsArray.count > 8) {
                    addCount += 1;
                }
            }
        }
    }
    
    if (_topNewsArray.count) {
        return _newsArray.count + addCount + 1;
    }
    
    return _newsArray.count + addCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    int numberOfItemsInLayout1 = self.isPortrait ? 3 : 5;
    int numberOfItemsInLayout2 = self.isPortrait ? 3 : 4;
    int cellId = 0;
    
    if (self.type == SpecialReportType) {
        cellId = 4;
    } else if ((_topNewsArray.count && indexPath.row < numberOfItemsInLayout1) || (self.type == EpisodesType && indexPath.row < numberOfItemsInLayout1) || (self.screenType == SpecialReportNewsListScreenType && indexPath.row < numberOfItemsInLayout1)) {
        cellId = 1;
        if (indexPath.row == 0 && (self.type == EpisodesType || self.screenType == SpecialReportNewsListScreenType)) {
            cellId = 3;
        } else {
            if (indexPath.row == 0)
                cellId = 0;
        }
    
    } else {
        int indexInsideLayout = (indexPath.row - numberOfItemsInLayout1) % numberOfItemsInLayout2;
        

        if (_topNewsArray.count == 0 && !(self.type == EpisodesType || self.screenType == SpecialReportNewsListScreenType))
            indexInsideLayout = indexPath.row % numberOfItemsInLayout2;

        
        if (indexInsideLayout <= (self.isPortrait) ? 0 : 1)  {
            cellId  = 1;
        } else {
            cellId = 2;
        }
        
        if (self.isPortrait) {
            if (indexInsideLayout == 0) {
                cellId  = 1;
            } else {
                cellId = 2;
            }
        } else {
            if (indexInsideLayout < 2) {
                cellId  = 1;
            } else {
                cellId = 2;
            }
        }
    }
    
    UICollectionViewCell *cell = nil;

    switch (cellId) {
        case 0: {
            TMSwipeCollectionViewCell *swipeCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"bigCell" forIndexPath:indexPath];
            swipeCell.pageControl.numberOfPages = _topNewsArray.count;
            swipeCell.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1];
            swipeCell.pageControl.backgroundColor = [UIColor clearColor];
            swipeCell.pageControl.alpha = 1;
            swipeCell.pageControl.hidesForSinglePage = YES;
            swipeCell.pageControl.indicatorDiameter = 5.0f;
            swipeCell.pageControl.alignment = SMPageControlAlignmentRight;
            swipeCell.pageControl.indicatorMargin = 2.0f;
            self.pageControl = swipeCell.pageControl;
  
            swipeCell.swipeView.bounces = NO;
            swipeCell.swipeView.truncateFinalPage = YES;
            swipeCell.swipeView.delegate = self;
            swipeCell.swipeView.dataSource = self;
            [swipeCell.swipeView reloadData];
            
            cell = swipeCell;
            break;
        } case 1: {
            BOOL banner = NO;
            int articleIndex = indexPath.row;
            
            if (self.hasAds) {
                
                if (self.isPortrait) {
                    if ((indexPath.row == 6 ) || (indexPath.row == 18 ) || (indexPath.row == 30)) {
                        banner = YES;
                    }
                    
                    if (indexPath.row > 30 ) {// because of add placements need to
                        articleIndex -= 3;
                    } else if (indexPath.row > 18 ) {
                        articleIndex -=2;
                    } else if (indexPath.row > 6 ) {
                        articleIndex -= 1;
                    }
                    
                } else {
                    if (_topNewsArray.count || self.type == EpisodesType || self.screenType == SpecialReportNewsListScreenType) {
                        if ((indexPath.row == 9) || (indexPath.row == 25) || (indexPath.row == 41)) {
                            banner = YES;
                        }
                        
                        if (indexPath.row > 41) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 25) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 9) {
                            articleIndex -= 1;
                        }
                        
                    } else {
                        if ((indexPath.row == 8) || (indexPath.row == 24) || (indexPath.row == 40)) {
                            banner = YES;
                        }
                        
                        if (indexPath.row > 40) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 24) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 8) {
                            articleIndex -= 1;
                        }
                    }
                }
            }
            
            if (_topNewsArray.count) {
                articleIndex -= 1;
            }
            
            if (banner) {
                
                TMSquareBannerCollectionViewCell *bannerCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"squareBannerCell" forIndexPath:indexPath];
                BOOL loadFirstBanner = YES;
                GADBannerView *banner = nil;
                if (_topNewsArray.count || self.type == EpisodesType || self.screenType == SpecialReportNewsListScreenType) {
                    if (indexPath.row == 18 || indexPath.row == 25) {
                        loadFirstBanner = NO;
                    }
                } else {
                    if ((indexPath.row == 18) || (indexPath.row == 24)) {
                        loadFirstBanner = NO;
                    }
                }
                
                if (loadFirstBanner) {
                    if (!self.gADBannerView.superview) {
                        self.gADBannerView = [[GADBannerView alloc] initWithAdSize:GADAdSizeFromCGSize(CGSizeMake(300, 250))];
                        GADRequest *request = [GADRequest request];
                        request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                                                @"Simulator"
                                                ];
                        
                        self.gADBannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
                        self.gADBannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;
                        self.gADBannerView.autoloadEnabled = YES;
                        self.gADBannerView.delegate = self;
                        [self.gADBannerView loadRequest:request];
                        
                    }
                    banner = self.gADBannerView;
                } else {
                    if (!self.gADBannerView2.superview) {
                        self.gADBannerView2 = [[GADBannerView alloc] initWithAdSize:GADAdSizeFromCGSize(CGSizeMake(300, 250))];
                        GADRequest *request = [GADRequest request];
                        request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                                                @"Simulator"
                                                ];
                        
                        self.gADBannerView2.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
                        self.gADBannerView2.rootViewController = [AppDelegate sharedInstance].window.rootViewController;
                        self.gADBannerView2.autoloadEnabled = YES;
                        self.gADBannerView2.delegate = self;
                        [self.gADBannerView2 loadRequest:request];
                        
                    }
                    
                    banner = self.gADBannerView2;
                }
                
                
                [bannerCell addSubview:banner];
                
                return bannerCell;
                
            } else {
                Article *article = (Article *)[self.newsArray objectAtIndex:articleIndex];
                TMSquareCollectionViewCell *squareCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"squareCell" forIndexPath:indexPath];
                
             
                if((self.type == EpisodesType && articleIndex <= 2)|| (self.screenType == SpecialReportNewsListScreenType && articleIndex <= 2)){
                #ifdef TAMIL
                    squareCell.backgroundColor = [UIColor blackColor];
                #else
                     squareCell.backgroundColor = [UIColor colorWithRed:2/255.0f green:43/255.0f blue:63/255.0f alpha:1];
                #endif
            
                    squareCell.tittle.textColor = [UIColor whiteColor];
                    squareCell.category.textColor =  [UIColor whiteColor];
                    squareCell.pubDate.textColor =  [UIColor whiteColor];
                }else{
                
                    squareCell.backgroundColor = [UIColor whiteColor];
                    squareCell.tittle.textColor = [UIColor blackColor];
                    squareCell.category.textColor =  [UIColor blackColor];
                    squareCell.pubDate.textColor =  [UIColor colorWithRed:153.0f/255.0f green:153.0/255.0f blue:153.0f/255.0f alpha:1];
                }
                
                squareCell.delegate = self;
                squareCell.indexPath = indexPath;
                squareCell.tittle.text = article.tittle;
                
                squareCell.pubdateSpace.constant = -4;
                [squareCell.category setFrame:CGRectZero];
                squareCell.pubDate.text = [article.pubDate formatDate];
                
                [squareCell.playImage setHidden:YES];
                NSString *photoThumbnail = @"";
                if (article.mediagroupList) {
                    if (article.mediagroupList.count) {
                        NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                        photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                        NSString *type = [mediaDictionary valueForKey:@"type"];
                        if ([type isEqualToString:@"video"]) {
                            [squareCell.playImage setHidden:NO];
                        }
                    }
                } else {
                    photoThumbnail = article.thumbnail;
                }
                
                [squareCell.bookmarkButton setSelected:NO];
                if (_bookmarks.count) {
                    for (Article *articleFromBookmark in _bookmarks) {
                        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                            [squareCell.bookmarkButton setSelected:YES];
                            break;
                        }
                    }
                }
                
                
                [squareCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                        placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                
                cell = squareCell;
            }
            
            break;
        } case 2: {
            int articleIndex = (int)indexPath.row;
            
            if (self.hasAds) {
                
                if(self.isPortrait){
                    if (indexPath.row > 30) {// because of add placements need to
                        articleIndex -= 3;
                    } else if (indexPath.row > 18) {
                        articleIndex -= 2;
                    } else if (indexPath.row > 6) {
                        articleIndex -= 1;
                    }
                }else{
                    if (_topNewsArray.count) {
                        if (indexPath.row > 41) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 25) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 9) {
                            articleIndex -= 1;
                        }
                    } else {
                        if (indexPath.row > 40) {// because of add placements need to
                            articleIndex -= 3;
                        } else if (indexPath.row > 24) {
                            articleIndex -= 2;
                        } else if (indexPath.row > 8) {
                            articleIndex -= 1;
                        }
                    }
                    
                }
            }
           
            if (_topNewsArray.count) {
                articleIndex -= 1;
            }
            
            Article *article = (Article *)[self.newsArray objectAtIndex:articleIndex];
            TMSmallCollectionViewCell *smallCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"smallCell" forIndexPath:indexPath];
            smallCell.delegate = self;
            smallCell.indexPath = indexPath;
            smallCell.tittle.text = article.tittle;
            [smallCell.category setHidden:YES];
           // smallCell.category.text = article.category;
            smallCell.categoryHeight.constant = 0;
            smallCell.pubDate.text = [article.pubDate formatDate];
            [smallCell.playImage setHidden:YES];
            NSString *photoThumbnail = @"";
            if (article.mediagroupList) {
                if (article.mediagroupList.count) {
                    NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                    photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                    NSString *type = [mediaDictionary valueForKey:@"type"];
                    if ([type isEqualToString:@"video"]) {
                        [smallCell.playImage setHidden:NO];
                    }
                }
            } else {
                photoThumbnail = article.thumbnail;
            }
            
            [smallCell.bookmarkButton setSelected:NO];
            if (_bookmarks.count) {
                for (Article *articleFromBookmark in _bookmarks) {
                    if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                        [smallCell.bookmarkButton setSelected:YES];
                        break;
                    }
                }
            }
            
            [smallCell.thumbnail setBorder];
            [smallCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                   placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            
            cell = smallCell;
            break;
        }case 3:{
            int articleIndex = (int)indexPath.row;
            
            if (self.hasAds) {
                
                if(self.isPortrait){
                    if (indexPath.row > 30) {// because of add placements need to
                        articleIndex -= 3;
                    } else if (indexPath.row > 18) {
                        articleIndex -= 2;
                    } else if (indexPath.row > 6) {
                        articleIndex -= 1;
                    }
                }else{
                    if (indexPath.row > 41) {// because of add placements need to
                        articleIndex -= 3;
                    } else if (indexPath.row > 25) {
                        articleIndex -= 2;
                    } else if (indexPath.row > 9) {
                        articleIndex -= 1;
                    }
                }
            }
            
            Article *article = (Article *)[self.newsArray objectAtIndex:articleIndex];
            TMCAandSRCollectionViewCell *CAandSRCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CAandSRCell" forIndexPath:indexPath];
            CAandSRCell.delegate = self;
            CAandSRCell.indexPath = indexPath;
            CAandSRCell.tittle.text = article.tittle;
            CAandSRCell.caption.text = article.brief;
            CAandSRCell.pubdate.text = [article.pubDate formatDate];
            
            #ifdef TAMIL
                CAandSRCell.backgroundColor = [UIColor blackColor];
            #else
                CAandSRCell.backgroundColor = [UIColor colorWithRed:2/255.0f green:43/255.0f blue:63/255.0f alpha:1];
            #endif
            NSString *photoThumbnail = @"";
            if (article.mediagroupList) {
                if (article.mediagroupList.count) {
                    NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                    photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                    NSString *type = [mediaDictionary valueForKey:@"type"];
                    if ([type isEqualToString:@"video"]) {
                        [CAandSRCell.playImage setHidden:NO];
                    }
                }
            } else {
                photoThumbnail = article.thumbnail;
            }
            
            if (_bookmarks.count) {
                for (Article *bookmark in _bookmarks) {
                    if ([bookmark.guid isEqualToString:article.guid]) {
                        [CAandSRCell.bookmarkButton setSelected:YES];
                        break;
                    }
                }
            }
          
            [CAandSRCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                   placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            cell = CAandSRCell;
            break;
        }case 4:{
            int articleIndex = (int)indexPath.row;
             Article *article = (Article *)[self.newsArray objectAtIndex:articleIndex];
             TMExclusiveCollectionViewCell *exclusiveCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"exclusiveCell" forIndexPath:indexPath];
                exclusiveCell.tittleLabel.text = article.tittle;
                exclusiveCell.descriptionLabel.text = article.brief;
            
            NSString *photoThumbnail = @"";
            if (article.mediagroupList) {
                if (article.mediagroupList.count) {
                    NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                    photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
               
                }
            }
            
          
            [exclusiveCell.thumbnail sd_setImageWithURL:[NSURL URLWithString:article.thumbnail]
                                       placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            cell = exclusiveCell;
        
        }
    }
    
    
    return cell;
}

#pragma mark - UICollectionView Delegate
- (IBAction)unwindToThisViewController:(UIStoryboardSegue *)unwindSegue
{
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    int index = (int)indexPath.row;
    if (_topNewsArray.count) {
        index = (int)indexPath.row - 1;
    }
    
    
    if(self.hasAds){
        if(self.isPortrait){
            if (indexPath.row > 30) {// because of add placements need to
                index -= 3;
            } else if (indexPath.row > 18) {
                index -= 2;
            } else if (indexPath.row > 6) {
                index -= 1;
            }
        } else {
            if (indexPath.row > 41) {// because of add placements need to
                index -= 3;
            } else if (indexPath.row > 25) {
                index -= 2;
            } else if (indexPath.row > 9) {
                index -= 1;
            }
        }
    }
    
    Article *article = (Article *)[self.newsArray objectAtIndex:index];
    NSString *videoUrl = @"";
    if (article.mediagroupList) {
        if (article.mediagroupList.count) {
            NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
            videoUrl = [mediaDictionary valueForKey:@"content"];
            NSLog(@"%@", mediaDictionary);
        }
    }

    switch (self.type) {
        case NewsType:
        case EpisodesType: {
            TMNewsDetailsPagerViewController *pager = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsPagerViewControllerId"];
            pager.customDelegate = self;
            pager.newsArticles = self.newsArray;
            pager.bookmarks = [_bookmarks mutableCopy];
            pager.activeTabIndex = index;
            pager.pagerTittle = @"";

            if(self.screenType == SpecialReportNewsListScreenType){
                pager.screenType = SpecialReportScreen;
                pager.categoryTitle = self.titleForCategory;
                pager.specialReportArticle = self.specialReportArticle;
            } 
            
            [self.navigationController pushViewController:pager animated:YES];
             break;
        }case PhotosType: {
            TMPhotoBrowserViewController *photobrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
            photobrowser.article = article;
            photobrowser.isNewsPhoto = NO;
            photobrowser.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [self presentViewController:photobrowser animated:NO completion:nil];
            photobrowser.view.alpha = .96;
            
            break;
        }case VideosType: {
            self.moviePlayer = [[TMMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
            [self presentMoviePlayerViewControllerAnimated:self.moviePlayer];
            [self.moviePlayer.moviePlayer play];
            break;
        }case CAType: {
            [[LibraryAPI sharedInstance]getArticlesWithURL:article.sectionURL andFeedLink:article.feedlink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                if ([array count]) {
                    Article *article = [array objectAtIndex:0];
                    TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                    newsPagerVC.pagerTittle = article.tittle;
                    newsPagerVC.newsPages = article.sections;
                    newsPagerVC.activeIndex = 0;
                    newsPagerVC.feedLink = article.feedlink;
                    newsPagerVC.screenType = CANewsPagerScreenType;
                    [self.navigationController pushViewController:newsPagerVC animated:YES];
                }
            }];
            break;
        }case SpecialReportType: {
            [[LibraryAPI sharedInstance]getArticlesWithURL:article.sectionURL andFeedLink:article.feedlink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                if(array){
                    if (array.count) {
                        Article *spArticle = (Article *)[array firstObject];
                        TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                        newsPagerVC.eksklusifArticle = spArticle;
                        newsPagerVC.pagerTittle = article.tittle;
                        newsPagerVC.newsPages = spArticle.sections;
                        newsPagerVC.eklusifEntitle = spArticle.entittle;
                        newsPagerVC.activeIndex = 0;
                        newsPagerVC.screenType = SpecialReportNewsPagerScreenType;
                        newsPagerVC.sectionTitleForCategory = article.tittle;
                        newsPagerVC.screenType = SpecialReportNewsListScreenType;

                        [self.navigationController pushViewController:newsPagerVC animated:YES];
                    }
                }
            }];
            break;
        }
    }
}

#pragma mark SwipeViewDataSOurce
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return _topNewsArray.count;
}

#pragma mark SwipeViewDelegate
-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    self.pageControl.currentPage = swipeView.currentPage;
}
-(void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{
    typedef enum : NSUInteger {
        PhotosType,
        VideosType,
        NewsType,
        EpisodeType,
        CAType,
        SpecialReportType
    } ArticleType;
    
    Article *article = (Article *)[_topNewsArray objectAtIndex:index];
    NSArray *mediaGroupArray = article.mediagroupList;
    NSString *feedLink = article.feedlink;
    NSString *type = article.type;
    NSString *sectionType = article.sectionType;
    NSString *stringUrl = article.sectionURL;
    
    
    
    
    ArticleType articletype;
    
    if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]) {
        NSLog(@"Selected PHOTOS TYPE");
        articletype = PhotosType;
    } else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]) {
        NSLog(@"Selected VIDEOS TYPE");
        articletype = VideosType;
    } else if ([type isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants NEWS_TYPE]]) {
        articletype = NewsType;
    } else if ([type isEqualToString:[Constants EPISODE_TYPE]] || [sectionType isEqualToString:[Constants EPISODE_TYPE]]) {
        articletype = EpisodeType;
    } else if ([type isEqualToString:[Constants CA_TYPE]] || [sectionType isEqualToString:[Constants CA_TYPE]]) {
        articletype = CAType;
    } else if ([type isEqualToString:[Constants SPECIAL_REPORT_TYPE]] || [sectionType isEqualToString:[Constants SPECIAL_REPORT_TYPE]]) {
        articletype = SpecialReportType;
    }
    
    switch (articletype) {
        case PhotosType:{
            Media *media = [Media new];
            TMPhotoBrowserViewController *photobrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
            photobrowser.article = article;
            photobrowser.isNewsPhoto = NO;
            photobrowser.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [[AnalyticManager sharedInstance] trackOpenPhotoGalleryWithAlbumName:media.tittle];
            [self presentViewController:photobrowser animated:NO completion:nil];
            photobrowser.view.alpha = .96;
            break;
            
        }
        case VideosType:{
            Media *media = [Media new];
            media.tittle = article.tittle;
            media.mediaGroup = (NSMutableArray *)article.mediagroupList;
            media.guid = article.guid;
            media.pubDate = article.pubDate;
            media.sectionUrl = article.sectionURL;
            media.feedlink = article.feedlink;
            media.downloadTime = article.downloadTime;
            media.brief = article.brief;
            TMiPADVideoDetailsViewController *vdc = (TMiPADVideoDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videoDetailsVC"];
            vdc.index = (int)index;
            vdc.videoBrowserList = [[NSMutableArray alloc]initWithObjects:media, nil];
            vdc.videoList = [[NSMutableArray alloc]initWithObjects:media, nil];
            [[AnalyticManager sharedInstance] trackOpenVideoWithGuid:media.guid andArticleTitle:media.tittle];
            [self.navigationController pushViewController:vdc animated:YES];
            break;
        }
        case NewsType:{
            BOOL isBookmark = [self checkBookmarkWithArticle:article];
            TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
            newsArticleVC.article = article;
            newsArticleVC.isBookmark = isBookmark;
            newsArticleVC.hasBarItems = YES;
            [[AnalyticManager sharedInstance] trackOpenNewsDetailScreenWithCategoryName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
            [self.navigationController pushViewController:newsArticleVC animated:YES];
            break;
        }
        case EpisodeType: {
            BOOL isBookmark = [self checkBookmarkWithArticle:article];
            TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
            newsArticleVC.article = article;
            newsArticleVC.isBookmark = isBookmark;
            newsArticleVC.hasBarItems = YES;
            [self.navigationController pushViewController:newsArticleVC animated:YES];
            break;
        }
        case CAType:{
            [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:feedLink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                if ([array count]) {
                    Article *article = [array objectAtIndex:0];
                    TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                    newsPagerVC.pagerTittle = article.tittle;
                    newsPagerVC.newsPages = article.sections;
                    newsPagerVC.activeIndex = 0;
                    newsPagerVC.feedLink = article.feedlink;
                    [self.navigationController pushViewController:newsPagerVC animated:YES];
                    [[AnalyticManager sharedInstance] trackOpenCAProgramScreenWithCategoryname:article.enCategory];
                }
            }];
            
            break;
        }
        case SpecialReportType:
            [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:feedLink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                if ([array count]) {
                    Article *article = [array objectAtIndex:0];
                    TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                    newsPagerVC.pagerTittle = [Constants NEWS_LISTING_TITTLE];
                    newsPagerVC.newsPages = article.sections;
                    newsPagerVC.activeIndex = 0;
                    newsPagerVC.feedLink = article.feedlink;
                    [[AnalyticManager sharedInstance] trackOpenSpecialReportScreenWithCategoryName:article.tittle];
                    [self.navigationController pushViewController:newsPagerVC animated:YES];
                }
            }];
            break;
    }

}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
  //  aheight = 248;
  //  awidth = 388;
    float width = 441;
    float aheight = 9;
    float awidth = 16;
    
    if (!self.isPortrait) {
        width =  388;
        aheight = 9;
        awidth = 16;
        
    }
    
    UIImageView *imageView = nil;
    UILabel *tittleLabel = nil;
    UILabel *subTittleLabel = nil;
    UILabel *categoryLabel = nil;
    UILabel *pubDateLabel = nil;
    UIImageView *playOverlay = nil;
    
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 510)];
        [view setBackgroundColor:[UIColor colorWithRed:2.0/225.0 green:19.0/225.0 blue:29.0/225.0 alpha:1]];
        imageView = [[UIImageView alloc] init];
        imageView.tag = 1;
        
        tittleLabel = [[UILabel alloc] init];
        [tittleLabel setFont:[UIFont systemFontOfSize:24]];
        [tittleLabel setTextColor:[UIColor whiteColor]];
        [tittleLabel setNumberOfLines:2];
        [tittleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        tittleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [tittleLabel setBackgroundColor:[UIColor clearColor]];
        tittleLabel.tag = 2;
        
        subTittleLabel = [[UILabel alloc] init];
        [subTittleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
        [subTittleLabel setTextColor:[UIColor colorWithRed:175/255.0f green:188/255.0f blue:195/255.0f alpha:1]];
        [subTittleLabel setNumberOfLines:4];
        subTittleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [subTittleLabel setBackgroundColor:[UIColor clearColor]];
        subTittleLabel.tag = 3;
        
        pubDateLabel = [[UILabel alloc] init];
        [pubDateLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        [pubDateLabel setTextColor:[UIColor colorWithRed:175/255.0f green:188/255.0f blue:195/255.0f alpha:1]];
        [pubDateLabel setNumberOfLines:1];
        pubDateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [pubDateLabel setBackgroundColor:[UIColor clearColor]];
        pubDateLabel.tag = 5;
        
        playOverlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play_overlay"]];
        playOverlay.tag = 6;
        
        [view addSubview:imageView];
        [view addSubview:tittleLabel];
        [view addSubview:subTittleLabel];
     //   [view addSubview:categoryLabel];
        [view addSubview:pubDateLabel];
        [view addSubview:playOverlay];
        
    } else {
        imageView = (UIImageView *)[view viewWithTag:1];
        tittleLabel = (UILabel *)[view viewWithTag:2];
        subTittleLabel = (UILabel *) [view viewWithTag:3];
        categoryLabel = (UILabel *) [view viewWithTag:4];
        pubDateLabel = (UILabel *) [view viewWithTag:5];
        playOverlay = (UIImageView *) [view viewWithTag:6];
    }
    
    if (_topNewsArray.count) {
        Article *article = (Article *)[_topNewsArray objectAtIndex:index];
        NSArray *mediaGroupArray = article.mediagroupList;
        NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:0];
        NSString *photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
        NSString *type = [mediaDictionary valueForKey:@"type"];
        
        [view setFrame:CGRectMake(0, 0, width, 510)];
        
        float imageViewHeight = aheight/awidth * width;
        [imageView setFrame:CGRectMake(0, 0, width, imageViewHeight)];
       // [imageView setBorder];
        [imageView sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                     placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:index == 0 ? SDWebImageRefreshCached : 0];
        
        [playOverlay setFrame:CGRectMake(12, imageViewHeight + imageView.frame.origin.y - (8 + 20), 20, 20)];
        if ([type isEqualToString:@"video"]){
            playOverlay.hidden = NO;
        } else {
            playOverlay.hidden = YES;
        }
        
        CGSize tittleMaxSize = CGSizeMake(width - 48, 70);
        float tittleLabelHeight = [Util expectedHeightFromText:article.tittle withFont:[UIFont systemFontOfSize:27] andMaximumLabelSize:tittleMaxSize];
        [tittleLabel setFrame:CGRectMake(12, imageView.frame.origin.y + imageViewHeight + 17, width - 24, tittleLabelHeight)];
        tittleLabel.text = article.tittle;
        
        CGSize subtittleMaxSize = CGSizeMake(width - 24, 150);
        float subtittleLabelHeight = [Util expectedHeightFromText:article.brief withFont:[UIFont systemFontOfSize:16] andMaximumLabelSize:subtittleMaxSize];
        [subTittleLabel setFrame:CGRectMake(12, tittleLabel.frame.origin.y + tittleLabelHeight + 17, width - 24, subtittleLabelHeight)];
        subTittleLabel.text = article.brief;
        
        float pubLabelWidth = [Util expectedWidthFromText:[article.pubDate formatDate] withFont:[UIFont fontWithName:@"HelveticaNeue" size:13] andMaximumLabelHeight:15];
        [pubDateLabel setFrame:CGRectMake(/*categoryLabelWidth + */categoryLabel.frame.origin.x + 13, subTittleLabel.frame.origin.y + subtittleLabelHeight + 17, pubLabelWidth, 15)];
        pubDateLabel.text = [article.pubDate formatDate];
    }

    return view;
}

#pragma mark TMNewsDetailsPagerViewControllerDelagate
- (void) didTapBookmarkAtIndex: (NSNumber *)indexNumber {
    int index = [indexNumber integerValue];
    int finalIndexPath = index;
    if (_topNewsArray.count ) {
        finalIndexPath = index + 1;
    }
    
    if(self.hasAds){
        if(self.isPortrait){
            if (index > 30) {// because of add placements need to
                finalIndexPath -= 3;
            } else if (index> 18) {
                finalIndexPath -= 2;
            } else if (index> 6) {
                finalIndexPath -= 1;
            }
        }else{
            if (index > 41) {// because of add placements need to
                finalIndexPath -= 3;
            } else if (index> 25) {
                finalIndexPath -= 2;
            } else if (index> 9) {
                finalIndexPath -= 1;
            }
            
        }
    }
    
    /*if(self.hasAds){
        if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
            if (indexx > 30) {// because of add placements need to
                finalIndexPath -= 3;
            } else if (indexx> 18) {
                finalIndexPath -= 2;
            } else if (indexx> 6) {
                finalIndexPath -= 1;
            }
        }else{
            if (indexx > 41) {// because of add placements need to
                finalIndexPath -= 3;
            } else if (indexx> 25) {
                finalIndexPath -= 2;
            } else if (indexx> 9) {
                finalIndexPath -= 1;
            }
        }

    }*/
    
    Article *article = (Article *) [_newsArray objectAtIndex:index];
    if ([self checkBookmarkForIndex:index]) {
        [_bookmarks removeObject:article];
    } else {
        [_bookmarks addObject:article];
    }
    //[[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:finalIndexPath inSection:0] ;
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];

}

- (BOOL)checkBookmarkForIndex: (NSUInteger)index
{
    Article *article = (Article *) [self.newsArray objectAtIndex:index];
    BOOL isBookmark = NO;
    for (Article *articleFromBookmark in _bookmarks) {
        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
            isBookmark = YES;
            break;
        }
    }
    
    return isBookmark;
}


#pragma mark TMSquareCollectionViewCellDelagate | TMSmallCollectionViewCellDelagate

- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    int index = indexPath.row;
    if (_topNewsArray.count) {
        index = indexPath.row - 1;
    }
    
    if(self.hasAds){
        
        if(self.isPortrait){
            if (indexPath.row > 30) {// because of add placements need to
                index -= 3;
            } else if (indexPath.row > 18) {
                index -= 2;
            } else if (indexPath.row > 6) {
                index -= 1;
            }
            
        }else{
            if (indexPath.row > 41) {// because of add placements need to
                index -= 3;
            } else if (indexPath.row > 25) {
                index -= 2;
            } else if (indexPath.row > 9) {
                index -= 1;
            }
        }

    }
    
    
    Article *article = (Article *) [_newsArray objectAtIndex:index];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {

        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            [_bookmarks removeObject:bookmarkArticle];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
            [_bookmarks addObject:article];
        }
        
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
    }
    
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
    
    if (!bookmarkArticle){
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    }
    
}



#pragma mark Notif Observers

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
        self.isPortrait = YES;
    else
        self.isPortrait = NO;
    
    TMNewsCollectionViewLayout *layout = (id)[self.collectionView collectionViewLayout];
    [layout clearLayout];
    [self loadLayout:layout forOrientation:toInterfaceOrientation];
    [layout invalidateLayout];
    [layout prepareLayout];
    //if ([Util checkifPortrait])
        [self.collectionView reloadData];
}

#pragma mark Private Functions
-(void)reloadTableData: (UIRefreshControl *)refreshFeed
{
    //self.firstAddOk = YES;
    //self.secondAddOk =YES;
    //self.thirdAddOk = YES;
    self.hasAds = YES;
    
    [self requestContentsWithForce:YES];
    if (refreshFeed) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        [refreshFeed endRefreshing];
        
    }
    
}
- (BOOL)checkBookmarkWithArticle: (Article*)article
{
    BOOL isBookmark = NO;
    for (Article *articleFromBookmark in _bookmarks) {
        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
            isBookmark = YES;
            break;
        }
    }
    
    return isBookmark;
}

- (void) loadLayout: (TMNewsCollectionViewLayout *)layout forOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (self.isPortrait) {
        [layout addLayoutWithRowWidth:768.f rowHeight:528.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 9, 441, 510.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(459, 9.0f, 300.f, 250.f)],
                                                                       [NSValue valueWithCGRect:CGRectMake(459, 270.0f, 300.f, 250.f)]]];
        
        /*[layout addLayoutWithRowWidth:768.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 9.0f, 300.f, 250.f)],//====Ehwal semasa
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 9.0f, 441, 121)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 130, 441, 121)]]];*/
        [layout addLayoutWithRowWidth:768.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 0.0f, 300, 250)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 0.0f, 441, 121)],
                                                                       [NSValue valueWithCGRect:CGRectMake(318.f, 130, 441, 121)]]];

        
        [layout addLayoutWithRowWidth:768.f rowHeight:178.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 9.0f, 750.0f, 169.f)]]];//====exclusive contents
        
        [layout addLayoutWithRowWidth:768.f rowHeight:187.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 9.0f, 750.0f, 169.f)]]];
        
        //self.isPortrait = YES;
    } else {
        [layout addLayoutWithRowWidth:1024.f rowHeight:528.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 9, 388, 510.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(406, 9.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(715, 9.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(406, 270.0f, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(715, 270.0f, 300.f, 250.f)]
                                                                        ]];
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9, 0, 300.f, 250.f)],
                                                                        [NSValue valueWithCGRect:CGRectMake(318, 0, 300.f, 250.f)],//====Ehwal semasa
                                                                        [NSValue valueWithCGRect:CGRectMake(627, 0, 388, 121)],
                                                                        [NSValue valueWithCGRect:CGRectMake(627.f, 130, 388, 121)]
                                                                        ]];
        
         [layout addLayoutWithRowWidth:1024.f rowHeight:259.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 9.0f, 1005.0f, 250.0f)]]];
        
        [layout addLayoutWithRowWidth:1024.f rowHeight:268.0f layouts:@[[NSValue valueWithCGRect:CGRectMake(9.f, 9.0f, 1005.0f, 250.0f)]]];//====exclusive contents
        //self.isPortrait = NO;
    }
}

- (void) requestContentsWithForce: (BOOL)forced
{
    //self.hasAds = YES;
    if(self.passedSectionNewsForTab == nil){
        self.passedSectionNewsForTab = [[NSMutableArray alloc]init];
    }
    [self.passedSectionNewsForTab removeAllObjects];
    for (Article *article in self.passedLatestNews) {
        if ([self.enTitle isEqualToString:article.sectionName]) {
            [self.passedSectionNewsForTab addObject:article];
        }
    }
    
    if (!group)
        group = dispatch_group_create();
    if (!self.hasPreviousContents)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self getNewsListWithForce:forced];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        if (self.topNewsArray.count == 0 && !self.type == EpisodesType && self.screenType != SpecialReportNewsListScreenType) {
            [self.collectionView setContentInset:UIEdgeInsetsMake(9.0f, 0.0f, 0.0f, 0.0f)];
        } else {
            [self.collectionView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        }
        if (self.newsArray.count || self.topNewsArray.count) {
            [self.collectionView reloadData];
            self.hasPreviousContents = YES;
        }
    });
}

- (void)getNewsListWithForce: (BOOL)forced
{
    NSString *urlString = self.urlString;
    if (self.type == PhotosType) {
        dispatch_group_enter(group);
        [[LibraryAPI sharedInstance]getSectionArticlesFromURL:urlString withFeedUrl:self.feedLink isForced:forced withCompletionBlock:^(NSMutableArray *array) {
            self.newsArray = [array mutableCopy];
            dispatch_group_leave(group);
        }];
        
    } else {
        dispatch_group_enter(group);
        [[LibraryAPI sharedInstance]getArticlesWithURL:urlString isForced:forced withCompletionBlock:^(NSMutableArray *array) {
            self.newsArray = [array mutableCopy];
            if (array.count) {
                Article *article = (Article *)[array firstObject];
                [self getTopNewsForCategory:article.enCategory WithForce:forced];
                [self getBookmarkItems];
                [self.collectionView reloadData];
            }else{
                self.newsArray = self.passedSectionNewsForTab;
            }
            
             dispatch_group_leave(group);
        }];
    }
}

- (void)getTopNewsForCategory: (NSString *)category WithForce:(BOOL)isForced
{
    dispatch_group_enter(group);
    [[LibraryAPI sharedInstance]getTopNewsWithForce:isForced andCompletionBlock:^(NSMutableArray *array) {
        [_topNewsArray removeAllObjects];
        if(array.count){
            for (Article *article in array) {
                if ([category isEqualToString:article.sectionName]){
                    [_topNewsArray addObject:article];
                    [self.collectionView reloadData];
                }
            }
        }else{
            for (Article *article in self.passedSectionNewsForTab) {
                if ([category isEqualToString:article.sectionName]){
                    [_topNewsArray addObject:article];
                    [self.collectionView reloadData];
                }
            }
        }
        
        dispatch_group_leave(group);
    }];
    
}

-(void)getBookmarkItems
{
    dispatch_group_enter(group);
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
           
            
        }
        dispatch_group_leave(group);
    }];
    
}

#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    self.bannerView.adSize = kGADAdSizeBanner;
    self.bannerView.tag = 555;
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeSmartBannerPortrait),
                                       NSValueFromGADAdSize(kGADAdSizeSmartBannerLandscape)
                                       ]];*/
    
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;
    self.bannerView.delegate = self;
    //self.bannerView.adSizeDelegate =self;
    [self.bannerView setAutoloadEnabled:YES];
    [self.bannerView loadRequest:request];
    [self.bannerView setHidden:YES];
}
/*
- (void)adView:(GADBannerView *)bannerView willChangeAdSizeTo:(GADAdSize)gadSize
{
    self.bannerHeight.constant = gadSize.size.height;
}*/

@end
