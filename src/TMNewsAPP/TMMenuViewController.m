//
//  TMMenuViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/8/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMMenuViewController.h"
#import "AspectFitView.h"
#import "TMSettingsTableViewController.h"
#import "TMSearchViewController.h"
#import "Constants.h"
#import "LibraryAPI.h"
#import "Article.h"
#import "TMBookmarksTableViewController.h"
#import "TMTableBeritaListViewController.h"
#import "TMDetailedBeritaViewController.h"
#import "TMNewsViewController.h"
#import "TMBeritaViewController.h"
#import "TMMainPageViewController.h"
#import "TMCurrentAffairViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMSearchTableViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "RadioManager.h"
#import "Util.h"
#import "FPPopoverController.h"
#import "TMNewsPagerViewController.h"
#import "TMSettingsiPadViewController.h"
#import "AnalyticManager.h"
#import "TMWebViewController.h"
#import "TMBookmarkiPadViewController.h"
#import "TMNavigationController.h"
#import "UIView+Toast.h"
#import  "TMBookmarksViewController.h"
#import "TMCurrentAffairPagerViewController.h"

@interface TMMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet AspectFitView *bottonParentAspectFitView;
@property (nonatomic, weak) IBOutlet UIButton *searchButton;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, weak) IBOutlet UIButton *settingsButton;
@property (nonatomic, weak) IBOutlet UIButton *stationSwitchButton;
@property (nonatomic, weak) IBOutlet UIButton *radioPlayPauseButton;
@property (nonatomic, weak) IBOutlet UILabel *liveradiotext1;
@property (nonatomic, weak) IBOutlet UILabel *liveradiotext2;
@property (nonatomic, weak) IBOutlet UISwitch *onOffSwitch;
@property (nonatomic, strong) NSArray *latestNewsList;
@property (nonatomic, strong) NSMutableArray *newsList;
@property (nonatomic, strong) NSArray *caList;
@property (nonatomic, strong) NSString *radioURL;
@property (nonatomic, strong) TMNavigationController *transitionsNavigationController;
@property (nonatomic) BOOL isWaitingForArticle;
@end

@implementation TMMenuViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _latestNewsList = [[NSArray alloc] init];
    _newsList = [[NSMutableArray alloc] init];
    _caList = [[NSArray alloc] init];
    self.isWaitingForArticle = FALSE;
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    
#if TAMIL
    if([[RadioManager sharedInstance]isRadioPlaying]){
        [self.onOffSwitch setOn:YES];
    }else{
        [self.onOffSwitch setOn:NO];
    }
#else
    if([[RadioManager sharedInstance]isRadioPlaying]){
        [self.radioPlayPauseButton setSelected:YES];
    }else{
        [self.radioPlayPauseButton setSelected:NO];
    }
    
    if([RadioManager sharedInstance].radioStation == RIA){
        [self.stationSwitchButton setSelected:YES];
    }else if([RadioManager sharedInstance].radioStation == WARNA){
         [self.stationSwitchButton setSelected:NO];
    }
    
#endif
    
    
}
- (void)viewDidLoad {
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   //  [self.slidingViewCon  if (![Util is_iPad]) {
   
    //self.transitionsNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
    [[AnalyticManager sharedInstance] trackMenuTab];

    self.tableView.dataSource = self;
    self.tableView.delegate = self;
#if TAMIL
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Tamil_Header_Logo_85x44"]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.radioPlayPauseButton setHidden:YES];
    [self.stationSwitchButton setHidden:YES];
    [self.liveradiotext1 setHidden:NO];
    [self.liveradiotext2 setHidden:NO];
    [self.onOffSwitch setHidden:NO];
    
#else
    
 
    [[UINavigationBar appearance] setBarTintColor:[Util headerColor]];

    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"berita_logo"]];
 
    [self.radioPlayPauseButton setHidden:NO];
    [self.stationSwitchButton setHidden:NO];
    [self.liveradiotext1 setHidden:YES];
    [self.liveradiotext2 setHidden:YES];
    [self.onOffSwitch setHidden:YES];
#endif
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setFrame:CGRectMake(0, 0, 28, 29)];
    [closeButton setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(didTapCloseButton) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    self.bottonParentAspectFitView.childView = self.searchButton;
    self.bottonParentAspectFitView.childView = self.bookmarkButton;
    self.bottonParentAspectFitView.childView = self.settingsButton;
    
    [self.settingsButton addTarget:self action:@selector(didTapSettings) forControlEvents:UIControlEventTouchUpInside];
    [self.searchButton addTarget:self action:@selector(didTapSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.bookmarkButton addTarget:self action:@selector(didTapBookmarks) forControlEvents:UIControlEventTouchUpInside];
    
    [[LibraryAPI sharedInstance] getNewsWithCompletionBlock:^(NSMutableArray *array) {
        self.newsList = [array mutableCopy];;
        for(NSDictionary *dict in array){
            NSString *type = [dict valueForKey:@"type"];
            if([type isEqualToString:@"special report"]){
                [self.newsList removeObjectIdenticalTo:dict];
                [self.newsList addObject:dict];
                break;
            }
        }
       
        [self.tableView reloadData];
    }];
    
    [[LibraryAPI sharedInstance] getCAWithForce:YES andCompletionBlock:^(NSMutableArray *array) {
        self.caList = array;
        [self.tableView reloadData];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - tableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0: {
            //return self.latestNewsList.count;
            break;
        } case 1: {
            return self.newsList.count;
        } case 2: {
            return self.caList.count;
        }
            
        default:
            break;
    }
    
    return  0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tittle = @"";
    switch (indexPath.section) {
        case 0: {
            break;
        } case 1: {
            NSDictionary *newsDictionary = (NSDictionary *) self.newsList [indexPath.row];
            tittle = [newsDictionary valueForKey:@"title"];
            break;
        } case 2: {
            Article *article = (Article *) [self.caList objectAtIndex:indexPath.row];
            tittle = article.tittle;
            break;
        }
        default:
            break;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UILabel *tittleLabel = (UILabel *)  [cell.contentView viewWithTag:1];
    tittleLabel.text = tittle;
    return  cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  Constants.TAB_TITTLES.count;
}

#pragma mark - tableView Delegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    switch (indexPath.section) {
        case 0: {
            break;
        } case 1: {
            NSString *tittle = @"";
            NSDictionary *newsDictionary = (NSDictionary *) self.newsList [indexPath.row];
            tittle = [newsDictionary valueForKey:@"enTitle"];
            [[AnalyticManager sharedInstance] trackMenuCategoryWithCategoryName:tittle];
            
            TMNavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"beritaListNavigationID"];
            self.slidingViewController.topViewController = nav;
            TMDetailedBeritaViewController *dbvc = (TMDetailedBeritaViewController *)[nav.viewControllers firstObject];
            dbvc.newsCategories = self.newsList;
            dbvc.selectedIndex = indexPath.row;
            dbvc.screenType = MenuBeritaScreenType;
            
            [self.navigationController dismissViewControllerAnimated:NO completion:nil];
            break;
        } case 2: {
            Article *article = [self.caList objectAtIndex:indexPath.row];
            NSString *stringUrl = article.sectionURL;
            NSString *title = article.tittle;
            if(self.isWaitingForArticle) return;
            self.isWaitingForArticle = TRUE;
            [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:article.feedlink  isForced:NO withCompletionBlock:^(NSMutableArray *array){
                
                if ([array count]) {
                    Article *newsArticle = [array firstObject];
                    NSArray *sectionsArray = newsArticle.sections;
                    if(![Util is_iPad]){
                        TMNavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"currentAffairPagerId"];
                        self.slidingViewController.topViewController = nav;
                        
                        TMCurrentAffairPagerViewController *dbvc = (TMCurrentAffairPagerViewController *)[nav.viewControllers firstObject];
                        dbvc.newsCategories = sectionsArray;
                        dbvc.selectedIndex = 0;
                        dbvc.enTitle = newsArticle.entittle;
                        dbvc.title = title;
                        dbvc.isFromMenu = YES;
                        
                        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                        [[AnalyticManager sharedInstance] trackMenuCategoryWithCategoryName:newsArticle.entittle];
                    }
                }
                self.isWaitingForArticle = FALSE;
            }];
            break;
        }
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect mainScreenFrame = [UIScreen mainScreen].bounds;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mainScreenFrame.size.width, 40.0f)];
    [view setBackgroundColor:[UIColor colorWithRed:0/255.0f green:116.0f/255.0f blue:212.0f/255.0f alpha:1]];
    UILabel *label = [[UILabel alloc] init];
    
    label.frame = CGRectMake(8.0f, 11, 362.0f, 21.0f);
    [label setTextColor: [UIColor whiteColor]];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0f]];
    label.text = [Constants TAB_TITTLES][section];
    [label sizeToFit];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, mainScreenFrame.size.width, 40)];
    [button setBackgroundColor:[UIColor clearColor]];
    button.tag = section;
    
    [button addTarget:self action:@selector(didTapSection:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 39.0f, mainScreenFrame.size.width, 1.0f)];
    [separatorLineView setBackgroundColor:[UIColor colorWithRed:240/255.0f green:240/255.0f blue:240/255.0f alpha:0.4f]];
    
    [view addSubview:label];
    [button addSubview:separatorLineView];
    [button bringSubviewToFront:separatorLineView];
    [view addSubview:button];
    
    return view;
}

#pragma mark Private FUnctions
- (void) didTapCloseButton
{
   // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (void) didTapSettings
{
    TMSettingsTableViewController *settingsVc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsStoryboardId"];
    if ([self.slidingViewController.topViewController isKindOfClass:[TMNavigationController class]]) {
        TMNavigationController *nav = (TMNavigationController *) self.slidingViewController.topViewController;
        [nav pushViewController:settingsVc animated:NO];
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
        //[self.slidingViewController resetTopViewAnimated:NO];
    }

}

- (void) didTapSearch {
    TMSearchViewController *searchVc = [self.storyboard instantiateViewControllerWithIdentifier:@"searchViewController"];
    if ([self.slidingViewController.topViewController isKindOfClass:[TMNavigationController class]]) {
        TMNavigationController *nav = (TMNavigationController *) self.slidingViewController.topViewController;
        [nav pushViewController:searchVc animated:NO];
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
        //[self.slidingViewController resetTopViewAnimated:NO];
       
    }

}

- (void) didTapBookmarks {
    TMBookmarksViewController *bmVc = [self.storyboard instantiateViewControllerWithIdentifier:@"bookmarkViewController"];
    if ([self.slidingViewController.topViewController isKindOfClass:[TMNavigationController class]]) {
        TMNavigationController *nav = (TMNavigationController *) self.slidingViewController.topViewController;
        [nav pushViewController:bmVc animated:NO];
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
     //   [self.slidingViewController resetTopViewAnimated:NO];
    }

}

- (void)didTapSection: (UIButton *)sender {
    
    UIButton *button = (UIButton *) sender;
     [[AnalyticManager sharedInstance] trackMenuCategoryWithCategoryName:[Constants MENU_TAGGING][button.tag]];
    TMNavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"mainPageNavigationID"];
    
    TMMainPageViewController *mainVc = (TMMainPageViewController *) [nav.viewControllers firstObject];
    mainVc.index = button.tag;
    self.slidingViewController.topViewController = nav;
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
  //  [self.slidingViewController resetTopViewAnimated:NO];
  
   
}

#pragma  mark IBActions
-(IBAction)didTapRadioPlayPausebutton:(id)sender{
 
    UIButton *button = (UIButton *) sender;
    [button setSelected:button.isSelected?NO:YES];
    if(button.isSelected){
        [self.view makeToast:[Constants PLAY_RADIO]];
        [[RadioManager sharedInstance] play];
    }else{
        [[RadioManager sharedInstance] stop];
    }

}

-(IBAction)didTapStaionSelectionButton:(id)sender{
    UIButton *button = (UIButton *) sender;
    [button setSelected:button.isSelected?NO:YES];
    if (button.isSelected) {
        [RadioManager sharedInstance].radioStation = RIA;
        
    }else{
        [RadioManager sharedInstance].radioStation = WARNA;
    }
    
    if([self.radioPlayPauseButton isSelected]){
        [[RadioManager sharedInstance] play];
    }
}

-(IBAction)didSwitchRadio:(id)sender{
    [RadioManager sharedInstance].radioStation = OLI;
    UISwitch *button = (UISwitch *) sender;
    if(button.isOn){
        [self.view makeToast:[Constants PLAY_RADIO]];
        [[RadioManager sharedInstance] play];
    }else{
        [[RadioManager sharedInstance] stop];
    }
}
- (IBAction)unwindToMenuViewController:(UIStoryboardSegue *)segue { }

//
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
