//
//  TMDetailedBeritaViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/17/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMDetailedBeritaViewController.h"
#import "TMTableBeritaListViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Constants.h"
#import "TMWebViewController.h"
#import "TMMenuViewController.h"
#import "Util.h"
#import "TMPhotosViewController.h"
#import "TMVideosViewController.h"
#import "TMNavigationController.h"
#import "AnalyticManager.h"
#import "LibraryAPI.h"
#import "MBProgressHUD.h"

@interface TMDetailedBeritaViewController ()<ViewPagerDataSource,ViewPagerDelegate, TMTableBeritaListViewControllerDelegate>

@end

@implementation TMDetailedBeritaViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    NSDictionary *dictionary = [self.newsCategories objectAtIndex:self.activeTabIndex];
    NSString *tittle = @"";
    
    if([dictionary valueForKey:@"title"]){
        tittle = [dictionary valueForKey:@"enTitle"];
        
    }else{
        tittle = [dictionary valueForKey:@"enCategory"];
    }
    
    if (self.screenType != SpecialReportScreenType){
        [[AnalyticManager sharedInstance] trackNewsListingWithCategoryName:tittle];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.dataSource = self;
    self.delegate = self;
    self.activeTabIndex = self.selectedIndex;
    //[self selectTabAtIndex:self.selectedIndex];
   
    if(self.screenType == MenuBeritaScreenType){
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    } else if (self.screenType == SpecialReportScreenType) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[LibraryAPI sharedInstance]getArticlesWithURL:self.specialReportArticle.sectionURL andFeedLink:self.specialReportArticle.feedlink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
            if(array){
                Article *srSectionArticle = (Article *)[array firstObject];
                self.newsCategories = srSectionArticle.sections;
                self.enTitle = srSectionArticle.entittle;
                self.categoryForSR = self.categoryForSR;
                self.activeTabIndex = 0;
                [self reloadData];
                
                [[AnalyticManager sharedInstance] trackSPNewsListingScreen:srSectionArticle.entittle];
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }

    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    
    
}

- (void)didReceiveMemoryWarning {	
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillDisappear:(BOOL)animated{
    
//[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Malay_Header_bg"]  forBarMetrics:UIBarMetricsDefault];

}

#pragma mark ViewPager Datasource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
  
    if(self.isTopNews){
        return 1;
    }else{
        return [self.newsCategories count];
    }
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    NSDictionary *dictionary = (NSDictionary *)[self.newsCategories objectAtIndex:index];
    NSString *type = [dictionary valueForKey:@"type"];
  
    if ([type isEqualToString:@"news"] ||[type isEqualToString:@"special report"]) {
        self.title = [Constants NEWS_LISTING_TITTLE];
        
    }
    
    UILabel *label = [UILabel new];
    NSString *uperCaseLabel = [NSString new];
    label.textAlignment = NSTextAlignmentCenter;
    if([dictionary valueForKey:@"title"]){
        uperCaseLabel = [[dictionary valueForKey:@"title"] uppercaseString];
        label.text = uperCaseLabel;
    }else{
        uperCaseLabel = [[dictionary valueForKey:@"category"] uppercaseString];
        label.text = uperCaseLabel;
    }
    
    [label sizeToFit];
    [label setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [label setTextColor:[UIColor colorWithRed:180.0f/255.0f green:200.0f/255.0f blue:241.0f/255.0f alpha:1]];
    return  label;
   
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
    
    NSDictionary *dictionary = (NSDictionary *)[self.newsCategories objectAtIndex:index];
    NSString *tittle = @"";
    NSString *enTitle = self.enTitle;
    NSString *sectionType = [dictionary valueForKey:@"sectionType"];
    NSString *type = [dictionary valueForKey:@"type"];
    
    if([dictionary valueForKey:@"title"]){
        tittle = [dictionary valueForKey:@"enTitle"];
        
    }else{
        tittle = [dictionary valueForKey:@"enCategory"];
    }
    
 
        if (self.screenType == SpecialReportScreenType){
            if ([type isEqualToString:[Constants HTML_TYPE]]|| [sectionType isEqualToString:[Constants HTML_TYPE]]) {
                [[AnalyticManager sharedInstance] trackSPHtmlPageWithEnCategory:tittle withEnCategory:enTitle];
            }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
                [[AnalyticManager sharedInstance] trackPhotosWithSpecialReport:enTitle];
            }else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]){
                [[AnalyticManager sharedInstance] trackSPVideoListing:enTitle];
            }else {
                 [[AnalyticManager sharedInstance] trackSPNewsListingScreen:self.enTitle];
            }
        } else {
            [[AnalyticManager sharedInstance] trackNewsListingWithCategoryName:tittle];
        }

}


- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index{

    NSMutableDictionary *dictionary = (NSMutableDictionary *)self.newsCategories[index];
    NSString *sectionType = [dictionary valueForKey:@"sectionType"];
    NSString *type = [dictionary valueForKey:@"type"];
    
    
    if ([type isEqualToString:[Constants HTML_TYPE]]|| [sectionType isEqualToString:[Constants HTML_TYPE]]) {
        TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
        webViewController.sectionUrl = dictionary[@"sectionURL"];
        return webViewController;
    
    }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
        TMPhotosViewController *others = (TMPhotosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoViewController"];
        others.sectionURL = [dictionary valueForKey:@"sectionURL"];
        others.isFromMain = NO;
        if (self.screenType == SpecialReportScreenType){
            others.screenType = SpecialReportPhotosScreenType;
            others.photoEklusifTitle = self.specialEnTittle;
        }
        return others;
    }else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]){
        TMVideosViewController *videos = (TMVideosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videosViewController"];
        videos.sectionURL = [dictionary valueForKey:@"sectionURL"];
        if (self.screenType == SpecialReportScreenType){
            videos.isFromSpecialReport = TRUE;
            videos.videosEksklusifTitle = self.specialEnTittle;
        }
        videos.isFromMain = NO;
        return videos;

    }else{
        TMTableBeritaListViewController *tableVC = (TMTableBeritaListViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"tableBeritaID"];
        if ([type isEqualToString:[Constants SPECIAL_REPORT_TYPE]] || [sectionType isEqualToString:[Constants SPECIAL_REPORT_TYPE]]){
            tableVC.screenType = SpecialReportBeritaScreenType;
        }
        
        if (self.screenType == SpecialReportScreenType) {
            tableVC.hasLiveStream = YES;
            tableVC.screenType = SpecialReportBeritaScreenType;
            tableVC.categoryForSR = self.categoryForSR;
            tableVC.specialReportEnTittle = self.specialEnTittle;
        }
        tableVC.dictionary = dictionary;
        tableVC.tabIndex = index;
        tableVC.feedLink = self.feedLink;
        tableVC.delegate =self;
        return tableVC;
}
    
    
}
- (NSNumber *)tabHeight {
    return [NSNumber numberWithFloat:29.0f];
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value{

    switch (option) {

        case ViewPagerOptionTabOffset: {
            if ([Util is_iPad]) {
                return 80.0f;
            } else {
                return 20.0f;
            }
        }
    }
    
    return value;
}

#pragma mark ViewPager Delegate
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerTabsView:
            return [UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}

#pragma mark TMTableBeritaListViewControllerDelegate
-(void)loadViewedItems:(NSString *)UrlString
{
    if (self.detailedBeritaViewControllerdelegate && [self.detailedBeritaViewControllerdelegate respondsToSelector: @selector(loadViewedItems:atIndex:)]) {
        [self.detailedBeritaViewControllerdelegate performSelector:@selector(loadViewedItems:atIndex:) withObject:UrlString withObject:[NSNumber numberWithInteger:self.activeTabIndex ]];
    }

    
}
#pragma mark Private function
- (IBAction)didTapMenu:(id)sender {
    
    TMNavigationController *nav = (TMNavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"menuNavigationStoryboardId"];
    [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setBarTintColor: [Util headerColor]];
    [self.navigationController presentViewController:nav animated:NO completion:nil];
}




@end
