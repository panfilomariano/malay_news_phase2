//
//  TMSquareBannerCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 3/16/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface TMSquareBannerCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet GADBannerView *bannerView;

@end
