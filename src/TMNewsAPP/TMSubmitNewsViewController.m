//
//  TMSubmitNewsViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/2/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSubmitNewsViewController.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Constants.h"
#import "Util.h"

@interface TMSubmitNewsViewController () <UITableViewDataSource, UITableViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) NSArray *newsArray;

@end

@implementation TMSubmitNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.newsArray = [[NSArray alloc]initWithObjects:[Constants SETTINGS_SUBMIT_NEWS], [Constants SETTINGS_SUBMIT_NEWS_PHOTOS_OR_VIDEOS], nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *) [cell.contentView viewWithTag:1];
    label.text = self.newsArray [indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66.0f;
}


#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        if ([MFMailComposeViewController canSendMail]){
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            NSString *emailTitle = [Constants SUBMIT_NEWS_EMAIL_SUBJECT];
            NSString *messageBody = [Constants SUBMIT_NEWS_EMAIL_MESSAGE];
            NSArray *toRecipents = [NSArray arrayWithObject:[Constants SUBMIT_NEWS_EMAIL_TO]];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTranslucent:NO];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            
            [self presentViewController:mc animated:YES completion:^{
                //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            }];
            
        }else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                message:@"Your device doesn't support the composer sheet"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
            [alertView show];
        }

    }else{
        [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
        [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
        [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
        
        UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
        imagepicker.delegate = self;
        imagepicker.allowsEditing = NO;
        imagepicker.navigationBar.translucent = NO;
        [imagepicker.navigationBar setTintColor:[UIColor whiteColor]];
        imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagepicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeMovie, (NSString *) kUTTypeImage,
                                  nil];

        imagepicker.allowsEditing = NO;
        [self presentViewController:imagepicker animated:YES completion:nil];
        
    }
}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if ([MFMailComposeViewController canSendMail]) {
        //[[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor clearColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
        
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        NSString *emailTitle = [Constants SUBMIT_NEWS_EMAIL_SUBJECT];
        NSString *messageBody = [Constants SUBMIT_NEWS_EMAIL_MESSAGE];
        NSArray *toRecipents = [NSArray arrayWithObject:[Constants SUBMIT_NEWS_EMAIL_TO]];
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        
        if ([mediaType isEqualToString:@"public.image"]){
            UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
            NSData* data = UIImageJPEGRepresentation(image, 1.0);
            
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            // Attach Image as Data
            [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            [UINavigationBar appearance].barTintColor = [Util headerColor];
            // Present mail view controller on screen
        }else if ([mediaType isEqualToString:@"public.movie"]){
            NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
            NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
            
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            // Attach Image as Data
            [mc addAttachmentData:videoData mimeType:@"video/mp4" fileName:@"video"];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            [UINavigationBar appearance].barTintColor = [Util headerColor];
        }
        
        [picker dismissViewControllerAnimated:YES completion:^(){
            [self presentViewController:mc animated:YES completion:NULL];
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Operation cannot perform"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
