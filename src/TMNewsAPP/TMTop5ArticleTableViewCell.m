//
//  TMTop5ArticleTableViewCell.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/11/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMTop5ArticleTableViewCell.h"

@implementation TMTop5ArticleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)didTapBookmarkButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}

@end
