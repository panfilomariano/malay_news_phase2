//
//  RadioManager.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/11/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
typedef enum radioStationType
{
    WARNA,
    RIA,
    OLI
} RadioStationType;

@interface RadioManager : NSObject


@property (nonatomic) RadioStationType radioStation;
@property (nonatomic) BOOL isRadioPlaying;
-(void)stop;
-(void)play;
+ (RadioManager *)sharedInstance;

@end
