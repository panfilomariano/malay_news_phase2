//
//  TMNewsDetailsPagerViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/12/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMNewsDetailsPagerViewController.h"
#import "TMNewsArticleViewController.h"
#import "LibraryAPI.h"
#import "Data.h"
#import "TMIPADMenuViewController.h"
#import "Util.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "UIView+Toast.h"


@interface TMNewsDetailsPagerViewController () <ViewPagerDataSource,ViewPagerDelegate, UIPopoverControllerDelegate, TMIPADMenuViewControllerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate, TMNewsDetailsPagerViewControllerDelagate>

@property (nonatomic, strong) UIBarButtonItem *fontBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *bookmarkBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *menuBarButtonItem;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIView *sliderView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIView *dimView;
@property (nonatomic, strong) UIPopoverController *popOver;
@property (nonatomic, strong) UIViewController *currentTabViewController;
@property (nonatomic) bool isTherePopOver;
@property (nonatomic) bool isThereSlider;
@property (nonatomic, strong) NSMutableArray *menuArrayList;
@property (nonatomic, strong) NSMutableArray *newsList;

@end

@implementation TMNewsDetailsPagerViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIButton *button = (UIButton *) _bookmarkBarButtonItem.customView;
    [button setSelected:[self checkBookmarkForIndex:self.activeTabIndex]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.reuseViewControllers = YES;
    self.dataSource = self;
    self.delegate = self;
    [self setUpBarButtonItems];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadViews)
                                                 name:@"didBookmarkChanged"
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"TMNewsDetailsPagerViewController");
}

- (void) touchesBegan:(NSSet *)touches
            withEvent:(UIEvent *)event {
    
    UIButton *bookmarkButton = (UIButton *) _fontBarButtonItem.customView;
    if (bookmarkButton.selected)
        [self dismissSlider];
}


#pragma mark ViewPagerDelegate
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return [self.newsArticles count];
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    return  label;
    
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerTabsView:
            return [UIColor whiteColor];
        default:
            return color;
    }
}
- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index{
    
    Article *article = (Article *)[self.newsArticles objectAtIndex:index];
    TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
    newsArticleVC.article = article;
    newsArticleVC.tableView = self.tableView;
    newsArticleVC.totalPages =[self.newsArticles count];
    newsArticleVC.pageNumber = index+1;

    if (self.screenType == LatestNewsScreen) {
        newsArticleVC.type = LatestNewsArticle;
    } else if(self.screenType == SpecialReportScreen){
        [newsArticleVC setType:SpecialReportArticle];
        newsArticleVC.titleForSpecialReport = self.categoryTitle;
        newsArticleVC.specialReportArticle = self.specialReportArticle;
    }
    self.currentTabViewController = newsArticleVC;
    
    return newsArticleVC;
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    if ([self checkBookmarkForIndex:index]) {
        UIButton *button = (UIButton *) _bookmarkBarButtonItem.customView;
        [button setSelected:YES];
    } else {
        UIButton *button = (UIButton *) _bookmarkBarButtonItem.customView;
        [button setSelected:NO];
    }
    
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    switch (option) {
        case ViewPagerOptionTabHeight:
            return 0.0;
        case ViewPagerOptionTabOffset:
            return 30.0f;
        case ViewPagerOptionTabLocation:
            return 0.0;
        default:
            return value;
    }
}
#pragma mark TMIPADMenuViewControllerDelegate
-(void)didTapItemAtMenu{
    [self.dimView removeFromSuperview];
    [self.popOver dismissPopoverAnimated: YES];
   [self.menuButton setSelected:NO];
    self.isTherePopOver = NO;

}
#pragma  mark UIPopOverDelegate
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.menuButton setSelected:NO];
    [self.dimView removeFromSuperview];
    self.isTherePopOver = NO;

}
#pragma mark Notif Observers

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self setNeedsReloadOptions];
    
    if (self.isTherePopOver) {
        if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
            int ctr = [self.menuArrayList count] +[self.newsList count];
            [self.popOver dismissPopoverAnimated: YES];
            self.popOver = nil;
            
            [self instantiateUIPopoverController];
            [self.popOver setPopoverContentSize:CGSizeMake(320, 275+(40*ctr))];// for ios 7
            self.popOver.contentViewController.preferredContentSize =CGSizeMake(320, 275+(40*ctr));
            [self.popOver presentPopoverFromRect:CGRectMake(453,7, 320, 275+(40*ctr)) inView:self.view permittedArrowDirections:0 animated:YES];
            [self.dimView setFrame:CGRectMake(0, 0, 768, 1024)];
        }else{
            int ctr =  (([self.menuArrayList count] +[self.newsList count])+2-1)/2;
            [self.popOver dismissPopoverAnimated: YES];
            self.popOver = nil;
            
            [self instantiateUIPopoverController];
            [self.popOver setPopoverContentSize:CGSizeMake(400, 275+(ctr*40))];// for ios 7
            self.popOver.contentViewController.preferredContentSize =CGSizeMake(400, 275+(ctr*40));
            [self.popOver presentPopoverFromRect:CGRectMake(702,7, 400, 275+(ctr*40)) inView:self.view permittedArrowDirections:0 animated:YES];
            [self.dimView setFrame:CGRectMake(0, 0, 1024, 768)];
        }
    }

    if(self.isThereSlider){
        [self dismissSlider];
        [self showSlider];
    }
}

#pragma mark Private Functions
-(void)reloadViews{
    UIButton *button = (UIButton *) _bookmarkBarButtonItem.customView;
    [button setSelected:[self checkBookmarkForIndex:self.activeTabIndex]];
}
- (void)instantiateUIPopoverController{
    TMIPADMenuViewController *dbvc = (TMIPADMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"menuStoryboardId"];
    dbvc.delegate = self;
    dbvc.ecSlidingVC = self.slidingViewController;
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:dbvc];
    self.popOver.delegate = self;
}

- (void)didTapMenu:(UIButton *)sender {
    [[AnalyticManager sharedInstance] trackMenuTab];
    [[LibraryAPI sharedInstance]getCAWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
        self.menuArrayList = array;
        [[LibraryAPI sharedInstance] getNewsWithCompletionBlock:^(NSMutableArray *array) {
            self.newsList = [array mutableCopy];
            
        if(self.popOver == nil){
            [self instantiateUIPopoverController];
        }
        
        
        UIButton *button =(UIButton *)sender;
        if(![button isSelected]){
            
            self.dimView = [[UIView alloc]initWithFrame:self.parentViewController.view.bounds];
            [self.dimView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
            [self.parentViewController.view addSubview:self.dimView];
            if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
                int ctr = [self.menuArrayList count] +[self.newsList count];
                [self.popOver setPopoverContentSize:CGSizeMake(320, 275+(40*ctr))];// for ios 7
                self.popOver.contentViewController.preferredContentSize =CGSizeMake(320, 275+(40*ctr));
                [self.popOver presentPopoverFromRect:CGRectMake(453,7, 320, 275+(40*ctr)) inView:self.view permittedArrowDirections:0 animated:YES];

            }else{
                 int ctr =  (([self.menuArrayList count] +[self.newsList count])+2-1)/2;
                [self.popOver setPopoverContentSize:CGSizeMake(400, 275+(ctr*40))];// for ios 7
                self.popOver.contentViewController.preferredContentSize =CGSizeMake(400, 275+(ctr*40));
                [self.popOver presentPopoverFromRect:CGRectMake(702,7, 400, 275+(ctr*40)) inView:self.view permittedArrowDirections:0 animated:YES];
            }
            
            [button setSelected:YES];
        }else{
            
            [self.popOver dismissPopoverAnimated:YES];
            self.popOver = nil;
            [self.dimView removeFromSuperview];
            
            [button setSelected:NO];
        }
        self.isTherePopOver = YES;
            }];
    }];
}

- (void)setUpBarButtonItems
{
    UIButton *bookmarkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bookmarkButton setFrame:CGRectMake(0, 0, 28, 29)];
    [bookmarkButton setImage:[UIImage imageNamed:@"bigStar"] forState:UIControlStateNormal];
    [bookmarkButton setImage:[UIImage imageNamed:@"bigStar_selected"] forState:UIControlStateSelected];
    [bookmarkButton addTarget:self action:@selector(didTapBookmark) forControlEvents:UIControlEventTouchUpInside];
    
    self.menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.menuButton setFrame:CGRectMake(0, 0, 28, 29)];
    [self.menuButton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [self.menuButton setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateSelected];
    [self.menuButton addTarget:self action:@selector(didTapMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *fontResizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fontResizeButton setFrame:CGRectMake(0, 0, 28, 29)];
    [fontResizeButton setImage:[UIImage imageNamed:@"fontButton"] forState:UIControlStateNormal];
    [fontResizeButton setImage:[UIImage imageNamed:@"fontButton_selected"] forState:UIControlStateSelected];
    [fontResizeButton addTarget:self action:@selector(displayfontResizeView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *separatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 28)];
    [separatorImage setImage:[UIImage imageNamed:@"separator_icon"]];
    
    UIImageView *transparentSeparatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 28)];
    [transparentSeparatorImage setImage:[UIImage new]];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setFrame:CGRectMake(0, 0, 28, 29)];
    
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateSelected];
    [shareButton addTarget:self action:@selector(didTapShare) forControlEvents:UIControlEventTouchUpInside];
    
    _menuBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.menuButton];
    UIBarButtonItem *separatorBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:separatorImage];
    UIBarButtonItem *shareBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    _bookmarkBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bookmarkButton];
    _fontBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:fontResizeButton];
    UIBarButtonItem *transparentSeparatorBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:transparentSeparatorImage];
    
    self.navigationItem.rightBarButtonItems = @[_menuBarButtonItem,
                                                separatorBarButtonItem,
                                                shareBarButtonItem,
                                                transparentSeparatorBarButtonItem,
                                                _bookmarkBarButtonItem,
                                                transparentSeparatorBarButtonItem,
                                                _fontBarButtonItem];
}

- (BOOL)checkBookmarkForIndex: (NSUInteger)index
{
    if (index < self.newsArticles.count) {
        Article *article = (Article *) [self.newsArticles objectAtIndex:index];
        return [[LibraryAPI sharedInstance] isBookmarkedArticle:article];
    }
    return NO;
}

- (void)didTapBookmark
{
    UIButton *bookmarkButton = (UIButton *) _bookmarkBarButtonItem.customView;
    
    Article *article = (Article *) [self.newsArticles objectAtIndex:self.activeTabIndex];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
    }
    
    if (bookmarkArticle) {
        [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
        [self.view makeToast:[Constants REMOVE_FROM_FAV]];
        [_bookmarks removeObject:bookmarkArticle];
        [bookmarkButton setSelected:NO];
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
        [bookmarkButton setSelected:YES];
    }
    
    if (self.customDelegate && [self.customDelegate respondsToSelector:@selector(didTapBookmarkAtIndex:)]) {
        NSNumber *indexNumberValue = [NSNumber numberWithInt:self.activeTabIndex];
        [self.customDelegate performSelector:@selector(didTapBookmarkAtIndex:) withObject:indexNumberValue];
    }
    
    if (bookmarkArticle == nil){
        if (self.screenType == CAScreen){
            [[AnalyticManager sharedInstance] trackCABookmarkWithProgram:article.enCategory andEpisodeId:article.guid andEpisodeTitle:article.tittle];
        }else {
            [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"willRefreshSwipeView"
                                                        object:nil
                                                      userInfo:nil];
}



- (void)displayfontResizeView:(UIButton *)sender
{
    if (!sender.selected) {
        sender.selected = YES;
        [self showSlider];
    } else {
        sender.selected = NO;
        [self dismissSlider];
    }

}

- (void)didTapShare
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Facebook",
                                  @"Twitter",
                                  @"Email",
                                  nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];

}



#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
   
            switch (buttonIndex) {
                case 0:
                    [self FBShare];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self emailShare];
                    break;
                default:
                    break;
            }
    
}

- (void)FBShare{
    
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    Article *article = (Article *) [self.newsArticles objectAtIndex:self.activeTabIndex];
    NSString *title = [NSString stringWithFormat:@"%@ %@\n", [Constants SHARE_PRE_TITTLE],article.tittle];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:article.link]];
    [self presentViewController:controller animated:YES completion:Nil];
  
}
- (void)emailShare {
    
    if (self.newsArticles.count) {
        if ([MFMailComposeViewController canSendMail]) {
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
            
            Article *media = [self.newsArticles objectAtIndex:self.activeTabIndex];
            NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE], [Constants SHARE_TITTLE]];
            NSString *messageBody = [NSString stringWithFormat:@"%@\n\n%@", media.tittle ,media.link];
            NSArray *toRecipents = [NSArray arrayWithObject:@""];
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:title];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTranslucent:NO];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            
            NSString *thumbnail = @"";
            if (self.newsArticles.count){
                NSArray *mediaGroup = [media.mediagroupList copy];
                if (mediaGroup.count){
                    NSDictionary *dictionary = [mediaGroup firstObject];
                    thumbnail = [dictionary valueForKey:@"thumbnail"];
                }
            }
            
            NSURL *url = [NSURL URLWithString:thumbnail];
            NSData *data = [NSData dataWithContentsOfURL:url];
            [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
            
            [self presentViewController:mc animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                        message:@"Nothing to share."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }

}

- (void)TwitterShare {
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    Article *media = [self.newsArticles objectAtIndex:self.activeTabIndex];
    //NSString *link = @"";
    if(self.newsArticles.count){
        
        NSArray *mediaGroup = [media.mediagroupList copy];
        if(mediaGroup.count) {
            //NSDictionary *dictionary = [mediaGroup firstObject];
            //link = [dictionary valueForKey:@"link"];
            
        }
    }
    NSString *title = [NSString stringWithFormat:@"%@ %@",[Constants SHARE_PRE_TITTLE],media.tittle];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:media.link]];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)setUpSlider
{
    int value = 0;
    switch ([Data sharedInstance].fontSizeType) {
        case FontSizeTypeXSmall:
            value = 0;
            break ;
        case FontSizeTypeSmall:
            value = 1;
            break ;
        case FontSizeTypeLarge:
            value = 3;
            break ;
        case FontSizeTypeXLarge:
            value = 4;
            break ;
        default:
            value = 2;
    }
    
    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"fontbar_line"];

    float width = 0;

    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        width = 768.0;
    }else{
        width = 1024.0;
    }

    self.sliderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, [UIScreen mainScreen].bounds.size.height)];
    _sliderView.backgroundColor =  [UIColor clearColor];

    
    UIView *viewParent = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 40)];
    viewParent.tag = 555;
    viewParent.backgroundColor = [UIColor colorWithRed:0 green:123/255.0f blue:208/255.0f alpha:1];
    
    [viewParent addGestureRecognizer:[[UIPanGestureRecognizer alloc] init]];
    
    self.slider = [[UISlider alloc] init];
    self.slider.tag = 111;
    self.slider.maximumValue = 4;
    self.slider.value = value;
    [self.slider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.slider setThumbImage:[UIImage imageNamed: @"thumbButton"] forState:UIControlStateNormal];
    [self.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    
    UILabel *small = [UILabel new];
    small.tag = 222;
    small.textAlignment = NSTextAlignmentLeft;
    [small setText:@"A"];
    [small setFont:[UIFont systemFontOfSize:16]];
    [small setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
    
    UILabel *big = [UILabel new];
    big.textAlignment = NSTextAlignmentRight;
    big.tag = 333;
    [big setText:@"A"];
    [big setFont:[UIFont systemFontOfSize:20]];
    [big setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
    
    
    //[_sliderView addSubview:self.slider];
    //[_sliderView addSubview:small];
    //[_sliderView addSubview:big];
    [viewParent addSubview:self.slider];
    [viewParent addSubview:small];
    [viewParent addSubview:big];
    [_sliderView addSubview:viewParent];
    
    
    
    self.slider.frame = CGRectMake(50, 0, _sliderView.frame.size.width - 100, 40);
    [small setFrame:CGRectMake(25, 0, 25, 40)];
    [big setFrame:CGRectMake(self.slider.frame.origin.x + self.slider.frame.size.width - 5, 0, 25, 40)];
    
    [self.view addSubview:_sliderView];
    
    

}



- (void)showSlider
{
    if (self.sliderView) {
        int value = 0;
        switch ([Data sharedInstance].fontSizeType) {
            case FontSizeTypeXSmall:
                value = 0;
                break ;
            case FontSizeTypeSmall:
                value = 1;
                break ;
            case FontSizeTypeLarge:
                value = 3;
                break ;
            case FontSizeTypeXLarge:
                value = 4;
                break ;
            default:
                value = 2;
        }
        float width = 0;
        if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
            width = 768.0;
        }else{
            width = 1024.0;
        }

        self.sliderView.frame = CGRectMake(0, 0, width , [UIScreen mainScreen].bounds.size.height);

        UIView *parentView = (UIView *) [self.sliderView viewWithTag:555];
        parentView.frame = CGRectMake(0, 0, width ,40);
        self.slider.frame = CGRectMake(50, 0, width - 100, 40);
        self.slider.value = value;
        UILabel *small = (UILabel *) [self.sliderView viewWithTag:222];
        [small setFrame:CGRectMake(25, 0, 25, 40)];
        UILabel *big = (UILabel *) [self.sliderView viewWithTag:333];
        [big setFrame:CGRectMake(width - 48, 0, 25, 40)];
    } else {
        [self setUpSlider];
    }
    
    UIButton *bookmarkButton = (UIButton *) _fontBarButtonItem.customView;
    bookmarkButton.selected = YES;
    self.isThereSlider = YES;
}

- (void)dismissSlider
{
    if (self.sliderView) {
        self.sliderView.frame = CGRectZero;
        self.slider.frame = CGRectZero;
        UIView *parentView = (UIView *) [self.sliderView viewWithTag:555];
        parentView.frame = CGRectZero;
        UILabel *small = (UILabel *) [self.sliderView viewWithTag:222];
        [small setFrame:CGRectZero];
        UILabel *big = (UILabel *) [self.sliderView viewWithTag:333];
        [big setFrame:CGRectZero];
    }
    
    UIButton *bookmarkButton = (UIButton *) _fontBarButtonItem.customView;
    bookmarkButton.selected = NO;
    self.isThereSlider = NO;
}

- (void)sliderAction: (id)sender
{
    UISlider *slider = (UISlider *)sender;
    float value = slider.value;
    
    switch (lroundf(value)) {
        case 0: {
            slider.value = 0;
            [Data sharedInstance].fontSizeType = FontSizeTypeXSmall;
            break;
        } case 1: {
            slider.value = 1;
            [Data sharedInstance].fontSizeType = FontSizeTypeSmall;
            break;
        } case 2: {
            slider.value = 2;
            [Data sharedInstance].fontSizeType = FontSizeTypeDefault;
            break;
        } case 3: {
            slider.value = 3;
            [Data sharedInstance].fontSizeType = FontSizeTypeLarge;
            break;
        } case 4: {
            slider.value = 4;
            [Data sharedInstance].fontSizeType = FontSizeTypeXLarge;
            break;
        }
    }
    
    UIViewController *vc = (UIViewController *)[self.viewControllers objectAtIndex:self.activeTabIndex];
    
     if ([vc isKindOfClass:[TMNewsArticleViewController class]]) {
         TMNewsArticleViewController *articleVc = (TMNewsArticleViewController *) vc;
         [articleVc requestWebView];
         [articleVc layoutScrollView];
    }
    
    [self dismissSlider];
}



@end
