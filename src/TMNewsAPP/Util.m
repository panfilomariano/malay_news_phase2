//
//  Util.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "Util.h"
#import <CommonCrypto/CommonHMAC.h>
#import "Weather.h"
#import "AFNetworking.h"
#import "Data.h"
#import "Constants.h"
#import "Article.h"
#import "LibraryAPI.h"

@implementation Util

+ (float) expectedHeightFromText:(NSString *)text withFont:(UIFont *)font andMaximumLabelWidth: (float)labelWidth
{
    CGSize maximumLabelSize = CGSizeMake(labelWidth, FLT_MAX);
    CGRect expectedLabelSize = [text boundingRectWithSize:maximumLabelSize
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:font}
                                                  context:nil];

    return expectedLabelSize.size.height;
    
}

+ (float) expectedWidthFromText:(NSString *)text withFont:(UIFont *)font andMaximumLabelHeight: (float)labelHeight
{
    CGSize maximumLabelSize = CGSizeMake(FLT_MAX, labelHeight);
    CGRect expectedLabelSize = [text boundingRectWithSize:maximumLabelSize
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:font}
                                                  context:nil];
    
    return expectedLabelSize.size.width;
    
}

+ (float) expectedHeightFromText:(NSString *)text withFont:(UIFont *)font andMaximumLabelSize: (CGSize)maxSize
{
    CGRect expectedLabelSize = [text boundingRectWithSize:maxSize
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:font}
                                                  context:nil];
    
    return expectedLabelSize.size.height;
    
}

+ (NSString *)urlWithTokenFromURL:(NSString *)url {
    
    NSString * prefix = @"http://mobile-ss.mediacorp";
    if ( [url rangeOfString:prefix].location == NSNotFound){
        return url;
    }
    
    NSCharacterSet* charSet = [NSCharacterSet characterSetWithCharactersInString:@"?"];
    NSRange characterRange = [url rangeOfCharacterFromSet:charSet];
    BOOL isContainQuery = NO;
    NSString* urlPathWithoutQuery = url;
    
    if (NSNotFound != characterRange.location) {
        isContainQuery = YES;
        urlPathWithoutQuery = [url substringWithRange:NSMakeRange(0, characterRange.location)];
    }
    
    NSRange addressBeginRange = [urlPathWithoutQuery rangeOfString:@"//"];
    
    if (NSNotFound != addressBeginRange.location) {
        urlPathWithoutQuery = [urlPathWithoutQuery substringFromIndex:addressBeginRange.location + 2];
    }
    
    NSCharacterSet* slashCharSet = [NSCharacterSet characterSetWithCharactersInString:@"/"];
    NSRange slashRange = [urlPathWithoutQuery rangeOfCharacterFromSet:slashCharSet];
    NSString* urlLocalPath = urlPathWithoutQuery;
    
    if (NSNotFound != slashRange.location) {
        urlLocalPath = [urlPathWithoutQuery substringFromIndex:slashRange.location];
    }
    
    NSDateFormatter* utcDateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [utcDateFormatter setTimeZone:timeZone];
    [utcDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSLocale* enUSLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [utcDateFormatter setLocale:enUSLocale];
    NSDate *oneHourBefore = [[NSDate date] dateByAddingTimeInterval:-3600];
    NSString* startTimeString = [utcDateFormatter stringFromDate:oneHourBefore];
    
    
    NSDate *twoHoursAfter = [[NSDate date] dateByAddingTimeInterval:7200];
    NSString* endTimeString = [utcDateFormatter stringFromDate:twoHoursAfter];
    
    
    NSString *infoString = [NSString stringWithFormat:@"%@?stime=%@&etime=%@", urlLocalPath, startTimeString, endTimeString];
    NSString* hmacHashString = [Util hmac:infoString withKey:@"aMSyStRoHpHVMflazeGefCofoKoRvDczXHfbyEuRkbNDhDrvmuFNaMWaYrnprhfm"];
    NSString* thefirst20HashString = [hmacHashString substringToIndex:20];
    
    
    NSString* newURLString = [NSString stringWithFormat:@"%@%@&encoded=0%@", [url stringByReplacingOccurrencesOfString:urlLocalPath withString:@""], infoString, thefirst20HashString];
    
    return newURLString;
}

+ (NSString *)hmac:(NSString *)plaintext withKey:(NSString *)key {
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [plaintext cStringUsingEncoding:NSASCIIStringEncoding];
    uint8_t cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSString *hash = @"";
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        hash = [hash stringByAppendingString:[NSString stringWithFormat:@"%02X", cHMAC[i]]];
    }
    
    return hash;
}

+ (BOOL) is_iPad
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? NO : YES;
}

+ (NSString *) urlTokenForFeed: (NSString *)url {
    //NSLog(@"urlTokenForFeed::%@", url);
    NSCharacterSet* charSet = [NSCharacterSet characterSetWithCharactersInString:@"?"];
    NSRange characterRange = [url rangeOfCharacterFromSet:charSet];
    BOOL isContainQuery = NO;
    NSString* urlPathWithoutQuery = url;
    NSString* queryString = nil;
    NSString* httpprotocol = @"";
    
    if (NSNotFound != characterRange.location) {
        isContainQuery = YES;
        urlPathWithoutQuery = [url substringWithRange:NSMakeRange(0, characterRange.location)];
        queryString = [url substringFromIndex:characterRange.location+1];
        //NSLog(@"urlTokenForFeed::queryString::%@", queryString);
    }
    
    NSRange addressBeginRange = [urlPathWithoutQuery rangeOfString:@"//"];
    
    if (NSNotFound != addressBeginRange.location) {
        urlPathWithoutQuery = [urlPathWithoutQuery substringFromIndex:addressBeginRange.location + 2];
        httpprotocol = [url substringToIndex:addressBeginRange.location+2];
    }
    
    NSCharacterSet* slashCharSet = [NSCharacterSet characterSetWithCharactersInString:@"/"];
    NSRange slashRange = [urlPathWithoutQuery rangeOfCharacterFromSet:slashCharSet];
    NSString* urlLocalPath = urlPathWithoutQuery;
    
    if (NSNotFound != slashRange.location) {
        urlLocalPath = [urlPathWithoutQuery substringFromIndex:slashRange.location];
    }
    
    NSDateFormatter* utcDateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [utcDateFormatter setTimeZone:timeZone];
    [utcDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSLocale* enUSLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [utcDateFormatter setLocale:enUSLocale];
    NSDate *oneHourBefore = [[NSDate date] dateByAddingTimeInterval:-3600];
    NSString* startTimeString = [utcDateFormatter stringFromDate:oneHourBefore];
    
    
    NSDate *twoHoursAfter = [[NSDate date] dateByAddingTimeInterval:7200];
    NSString* endTimeString = [utcDateFormatter stringFromDate:twoHoursAfter];
    
    
    NSString *infoString = [NSString stringWithFormat:@"%@?stime=%@&etime=%@", urlLocalPath, startTimeString, endTimeString];
    if ( queryString!=nil )
        infoString = [NSString stringWithFormat:@"%@?stime=%@&etime=%@&%@", urlLocalPath, startTimeString, endTimeString, queryString];
    //NSLog(@"infoString::%@", infoString);
    NSString* hmacHashString = [Util hmac:infoString withKey:@"PafhaiIorDjQnOVkraOgcvjXycbpvlenUPQpEesSkyltNDPjsbLoBXdAPFeESrVw"];
    NSString* thefirst20HashString = [hmacHashString substringToIndex:20];
    
    
    NSString* newURLString = [NSString stringWithFormat:@"%@%@%@&encoded=0%@", httpprotocol, [urlPathWithoutQuery stringByReplacingOccurrencesOfString:urlLocalPath withString:@""], infoString, thefirst20HashString];
    //NSLog(@"newURLString::%@", newURLString);

    return newURLString;
    
}
+ (BOOL) isPortrait
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    switch (orientation)
    {
        case UIDeviceOrientationUnknown:{
             return NO;
        }case UIDeviceOrientationPortrait:{
            
            return YES;
        }case UIDeviceOrientationPortraitUpsideDown:{
            return YES;
        }case  UIDeviceOrientationLandscapeLeft:{
            return NO;
        }case UIDeviceOrientationLandscapeRight:{
            return NO;
        }case UIDeviceOrientationFaceUp:{
            return NO;
        }case UIDeviceOrientationFaceDown:{
            return NO;
        }
    }
}


+(UIRefreshControl *)setRefreshControlWithTarget:(UIViewController *)viewController  inView:(UIView *)view withSelector:(SEL)selector{
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:viewController action:selector forControlEvents:UIControlEventValueChanged];
    [view addSubview:refreshFeed];

    return refreshFeed;
}

+ (UIColor *) headerColor
{
#if TAMIL
    return [UIColor blackColor];
#else
    return [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif
    
}

+ (BOOL) needsUpgradeVersion: (NSString *)serverVersionStr
{
    NSString * appVersionStr = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    if ([serverVersionStr compare:appVersionStr options:NSNumericSearch] == NSOrderedDescending) {
        return YES;
    }
    return NO;
}

+ (NSString *) formatHtmlData: (NSString *)htmldata
{

    return [NSString stringWithFormat:@"%@%@", [Constants HEADER_HTMLDATA],htmldata];

}

+ (NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+ (BOOL)checkURLForPush: (NSString *)urlStr withNavigationType: (UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked ||
        (navigationType == UIWebViewNavigationTypeOther &&
         ![urlStr isEqualToString:@"about:blank"] &&
         ![urlStr isEqualToString:[Constants URL_BASE]] &&
         ![urlStr hasPrefix:@"http://static.ak.facebook.com/"] &&
         ![urlStr hasPrefix:@"https://s-static.ak.facebook.com/"] &&
         ![urlStr hasPrefix:@"https://pubads.g.doubleclick.net/"] &&
         !([urlStr hasPrefix:@"https://www.youtube.com"] && [urlStr rangeOfString:@"embed/"].location != NSNotFound) &&
         !([urlStr hasPrefix:@"https://instagram.com"] && [urlStr rangeOfString:@"embed/captioned"].location != NSNotFound) &&
         !(([urlStr hasPrefix:@"http://www.facebook.com/"] || [urlStr hasPrefix:@"https://www.facebook.com/"] || [urlStr hasPrefix:@"https://m.facebook.com/"]) && [urlStr rangeOfString:@"plugins/"].location != NSNotFound) )
        
        ) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isVersion8Above
{
    BOOL isVersion8Above = YES;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0f) {
        isVersion8Above = NO;
    }
    
    return isVersion8Above;
}

+ (void)updateBookmarkWith: (Article *)article addedArticleSuccess: (void (^)(Article *addedBookmarkArticle))addedArticleSuccess removedArticleSuccess: (void (^)(Article *removedBookmarkArticle))removedArticleSuccess
{
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        Article *bookmarkArticle = nil;
        if (array) {
            for (Article *bookmark in array) {
                if ([bookmark.guid isEqualToString:article.guid]) {
                    bookmarkArticle = bookmark;
                    break;
                }
            }
            
            if (bookmarkArticle) {
                [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
                removedArticleSuccess (bookmarkArticle);
            } else {
                [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
                addedArticleSuccess (article);
            }
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            addedArticleSuccess (article);
        }
        
       
    }];
}

@end
