//
//  TMNewsViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/8/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
@interface TMNewsViewController : UIViewController

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;

@end
