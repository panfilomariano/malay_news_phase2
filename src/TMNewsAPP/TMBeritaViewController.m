//
//  TMBeritaViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/16/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMBeritaViewController.h"
#import "TMBeritaSmallCollectionViewCell.h"
#import "TMDetailedBeritaViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "Article.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "AnalyticManager.h"

@interface TMBeritaViewController ()<GADBannerViewDelegate, GADAdSizeDelegate>
@property (nonatomic, weak) IBOutlet UICollectionView *beritaCollectionView;
@property (nonatomic, strong) NSMutableArray *newsArray;
@property (nonatomic, strong) NSString *beritaUrl;
@property (nonatomic, strong) NSString *latestSectionUrl;
@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (nonatomic, strong) NSString *thumbnailString;
@property (nonatomic, assign) BOOL forImageReload;
@property (nonatomic, strong) NSArray *thumbailsArray;
@property (nonatomic, strong) Article *srFirstArticle;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@property (nonatomic, assign) NSInteger indexx;

@end

@implementation TMBeritaViewController

- (void)viewWillAppear:(BOOL)animated
{
    [[AnalyticManager sharedInstance] trackNewsCategoryTab];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.categoryArray = [[NSMutableArray alloc]init];
    
    [self requestContentWithForce:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestContentWithForce:)
                                                 name:@"reloadDataNotif"
                                               object:nil];
    
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:self action:@selector(reloadTableData:) forControlEvents:UIControlEventValueChanged];
    [self.beritaCollectionView addSubview:refreshFeed];
    
    [self setUpads];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark CollectionView Delegates

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.categoryArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    TMBeritaSmallCollectionViewCell *cell = (TMBeritaSmallCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"smallCell" forIndexPath:indexPath];
    NSDictionary *categoryDict =   [self.categoryArray objectAtIndex:indexPath.row];
    NSString *title = [categoryDict valueForKey:@"title"];
    NSString *enTitle = [categoryDict valueForKey:@"enTitle"];

    [cell.layer setBorderWidth:1.0];
    [cell.layer setBorderColor:[[UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1] CGColor]];
    
    for (Article *article in self.thumbailsArray) {
        
        if ([article.sectionName isEqualToString:enTitle]) {
            NSDictionary *dict = [article.mediagroupList objectAtIndex:0];
            [cell.thumbnailImage sd_setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"thumbnail"]]
                                   placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]]
                                            options:SDWebImageRefreshCached];
            break;
        }
    }
    
    if ([enTitle isEqualToString:@"specialreports"]) {
        [cell.thumbnailImage sd_setImageWithURL:[NSURL URLWithString:self.srFirstArticle.thumbnail]
                               placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]]
                                        options:SDWebImageRefreshCached];
    }
    

    cell.thumbnailLabel.text = title;

    return cell;

}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(([self.categoryArray count]%2) !=0 ){
        if (indexPath.row == 0) {
            return CGSizeMake(screenWidth-10, (screenWidth/2)+56);
        }else{
            return CGSizeMake((screenWidth/2)-8,(screenWidth/2)-36);
        }
    }else{
        return CGSizeMake((screenWidth/2)-8, (screenWidth/2)-36);
    }
        return CGSizeZero;
}

#pragma mark UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TMDetailedBeritaViewController *dbvc = (TMDetailedBeritaViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"detailedBeritaID"];
    dbvc.newsCategories = self.categoryArray;
    dbvc.selectedIndex = indexPath.row;
    
    [self.navigationController pushViewController:dbvc animated:YES];
    
}

#pragma mark private functions

- (void) requestContentWithForce: (BOOL) force
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LibraryAPI sharedInstance]getNewsWithCompletionBlock:^(NSMutableArray *array) {
        if(array){
            self.categoryArray = [array mutableCopy];
            for (NSDictionary *dict in array) {
                if([[dict valueForKey:@"type"] isEqualToString:@"special report"]){
                    [self.categoryArray removeObjectIdenticalTo:dict];
                    [self.categoryArray addObject:dict];
                    break;
                }
            }
            [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
                if (array) {
                    for (NSDictionary *feedItemDict in array) {
                        if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:@"latest section news"]) {
                            
                            NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                            [[LibraryAPI sharedInstance]getSectionArticlesFromURL:sectionURL isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                                if(array){
                                    self.thumbailsArray = [array mutableCopy];
                                }
                                [self.beritaCollectionView reloadData];
                                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                            }];
                        }
                        
                        if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:@"specialreports"]) {
                            NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                            [[LibraryAPI sharedInstance]getArticlesWithURL:sectionURL isForced:NO withCompletionBlock:^(NSMutableArray *array) {
                                if(array){
                                    self.srFirstArticle = [array firstObject];
                                }
                                [self.beritaCollectionView reloadData];
                                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                            }];
                        }
                    }
                } else {
                    [self.beritaCollectionView reloadData];
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                }
            }];
        } else {
            [self.beritaCollectionView reloadData];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        
        
    }];
}

-(void)reloadTableData: (UIRefreshControl *)refreshFeed {
    
    [self requestContentWithForce:YES];
    //[self.beritaCollectionView reloadData];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    refreshFeed.attributedTitle = attributedTitle;
        
    [refreshFeed endRefreshing];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    self.bannerView.adSize = kGADAdSizeBanner;
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       ]];*/
    //self.bannerView.adSizeDelegate = self;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:request];
   
    [self.bannerView setHidden:YES];
}
#pragma mark GADbannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view{
   self.bannerHeight.constant = 50;
    [self.bannerView setHidden:NO];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
    
}
/*
- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)size{
    self.bannerHeight.constant = size.size.height;
}*/
@end
