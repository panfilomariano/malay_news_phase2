//
//  TMVideoListCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/2/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMVideoListCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *descLabel;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;

@end
