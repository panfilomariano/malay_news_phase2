//
//  TMSwipeViewTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/13/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMPageControl.h"
#import "SwipeView.h"
@interface TMSwipeViewTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UILabel *topFiveNewsLabel;
@end
