//
//  MobileBDError.h
//  MobileBDSDK
//
//  Created by Le Vu on 19/3/15.
//  Copyright (c) 2015 Knorex. All rights reserved.
//

#import <Foundation/Foundation.h>

// Mobile Brand Display error domain
extern NSString *const kMobileBDErrorDomain;

typedef NS_ENUM(NSInteger, MobileBDErrorCode) {
    
    // there is no error
    kMobileBDErrorNoError,
    
    // unknown error
    kMobileBDErrorUnknown,
    
    // adUnitId is nil or empty
    kMobileBDErrorInvalidAdUnitId,
    
    // empty response when request an adUnit
    kMobileBDErrorEmptyAdUnitResponse,
    
    // invalid in adUnit response, cannot proceed
    kMobileBDErrorInvalidAdUnitResponse,
    
    // the MobileBDAdView view is invalid because
    //  + it contains no ad or too many views
    //  + size is smaller than the ad size specifications
    kMobileBDErrorInvalidAdView,
};

@interface MobileBDError : NSError

@end
