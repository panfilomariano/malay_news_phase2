//
//  NSString+Helpers.m
//  Cubit
//
//  Created by Wong Johnson on 10/18/12.
//  Copyright (c) 2012 Pace Creative Studio. All rights reserved.
//

#import "NSString+Helpers.h"

@implementation NSString (Helpers)

#pragma mark Helpers
- (NSString *) stringByUrlEncoding
{
	return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,  (CFStringRef)self,  NULL,  (CFStringRef)@"!*'();:@&amp;=+$,/?%#[]",  kCFStringEncodingUTF8));
}

-(NSString *) formatDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
    NSDate *pubDate = [dateFormatter dateFromString:self];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    return [formatter stringFromDate:pubDate];
}

@end
