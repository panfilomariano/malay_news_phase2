//
//  AnalyticManager.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/4/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface AnalyticManager : NSObject
+(AnalyticManager *)sharedInstance;

- (void)trackLandingPage;
- (void)trackNewsCategoryTab;
- (void)trackTopNewsWithCategoryName:(NSString *)categoryName andArticleTitle:(NSString *)articleTitle andGUID:(NSString *)guId;
- (void)trackNewsListingWithCategoryName: (NSString *)categoryName;
- (void)trackContentScreenWithCategoryName: (NSString *)categoryName andArticleTitle: (NSString *)articleTitle andPubDate: (NSString *)pubDate andGUID: (NSString *)guId;
- (void)trackRelatedNewsWithArticleTitle: (NSString *)articleTitle;
- (void)trackPopularNewsWithArticleTitle: (NSString *)articleTitle andGUID: (NSString *)guId;
- (void)trackVideoTab;
- (void)trackVideoPlayWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle andVideoTitle: (NSString *)videoTitle;
- (void)trackPhotoTab;
- (void)trackOpenPhotoAlbumWithAlbumName: (NSString *)albumName andPubDate: (NSString *)pubdate andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle andGalleryTitle: (NSString *)galleryTitle andPhotoTitle: (NSString *)photoTitle andPhotoId: (NSString *)photoId;
- (void)trackCACategoryTab;
- (void)trackCAProgrammeSelectedWithProgram: (NSString *)program;
- (void)trackCADetailScreenWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle;
- (void)trackCABookmarkWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle;
- (void)CALiveStreamingWithProgram: (NSString *)program;
- (void)trackFacebookShareNewsWithCategory: (NSString *)categoryName andArticleTitle: (NSString *)articleTitle andGuid: (NSString *)guId;
- (void)trackTwitterShareNewsWithCategoryName: (NSString *)categoryName andGuId: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackEmailShareNewsWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guid andArticleTitle: (NSString *)articleTitle;
- (void)trackFacebookShareCAWithEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle andProgram: (NSString *)program;
- (void)trackTwitterShareCAWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle;
- (void)trackEmailShareCAWithProgram: (NSString *)program andEpisodeId: (NSString *)episodeId andEpisodeTitle: (NSString *)episodeTitle;
- (void)trackFacebookShareVideoWithGuId: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackTwitterShareVideoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackEmailShareVideoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackFacebookSharePhotoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackTwitterSharePhotoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackEmailSharePhotoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackMenuTab;
- (void)trackRadioWithName: (NSString *)radioName;
- (void)trackSearch;
- (void)trackSearchString;
- (void)trackBookmark;
- (void)trackSettings;
- (void)trackOfficeDownload;
- (void)trackCAAboutScreenWithCategory: (NSString *)categoryName andProgram: (NSString *)program;
- (void)trackSpecialReportHome;
- (void)trackMenuCategoryWithCategoryName: (NSString *)categoryName;
- (void)trackVideosWithVideoId: (NSString *)videoId andVideoTitle: (NSString *)videoTitle;

- (void)trackPhotosWithGalleryTitle: (NSString *)galleryTitle andPhotoId: (NSString *)photoId andPhotoTitle: (NSString *)photoTitle;
- (void)trackNewsSearch;
- (void)trackSearchStringWithCategoryName: (NSString *)categoryName andCount:(NSUInteger)count;
- (void)trackPhotosWithSpecialReport: (NSString *)specialreport;
- (void) trackVideoWithSpecialReports: (NSString *)specialReport;
- (void)trackVideosWithSpecialReports: (NSString *)specialReport andGuid: (NSString *)guId andvideoTitle: (NSString *)videoTitle;
- (void)trackPhotosWithPhotoId: (NSString *)photoId andPhotoTitle: (NSString *)photoTitle andGalleryTitle: (NSString *)galleryTitle andSpecialReport: (NSString *)specialReport;
- (void)trackSPLiveStreaming: (NSString *)specialreport andLiveStreaming: (NSString *)livestream;
- (void)trackSPHtmlPageWithEnCategory: (NSString *)specialReport withEnCategory: (NSString *)enCategory;
- (void) trackSPVideoListing: (NSString *)specialReport;
- (void)trackSPNewsListingScreen: (NSString *)specialreport;

- (void)trackOpenPhotoAlbumWithAlbumName: (NSString *)albumName  andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle andPhotoTitle:(NSString *)photoTitle;
- (void)trackBookmarkWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackClickNewsArticleInSearchWithCategory: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackClickSpecialReportArticleInSearchWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackCAArticleInSearchWithProgramName: (NSString *)programName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackOpenPhotoGalleryWithAlbumName: (NSString *)albumName;
- (void)trackOpenVideoWithGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackOpenSpecialReportPhotoGalleryWithCategoryName: (NSString *)categoryName andAlbumName: (NSString *)albumName;
- (void)trackOpenSpecialReportDetailScreenWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackOpenSpecialReportScreenWithCategoryName: (NSString *)categoryName;
- (void)trackOpenNewsDetailScreenWithCategoryName: (NSString *)categoryName andGuid: (NSString *)guId andArticleTitle: (NSString *)articleTitle;
- (void)trackCAOpenPhotoGalleryWithCategoryName: (NSString *)categoryName andAlbumName: (NSString *)albumName;
- (void)trackCAOpenDetailScreenWithCategoryName: (NSString *)categoryName andGuid: (NSString *) guId andArticleTitle: (NSString *)articleTitle;
- (void)trackOpenCAProgramScreenWithCategoryname: (NSString *)categoryName;
- (void)trackCAProgrammeContentListingWithProgram: (NSString *)program ;
- (void)trackPhotosWithGuid: (NSString *)guId andPhotoTitle: (NSString *)photoTitle andAlbumName: (NSString *)albumName andSpecialReport: (NSString *)specialReport;
- (void)trackCAPhotoListingWithProgram: (NSString *)program;
- (void)trackContentScreenWithCategoryName: (NSString *)categoryName andArticleTitle: (NSString *)articleTitle andGUID: (NSString *)guId;
- (void)trackContentScreenInCAWithCategoryName: (NSString *)categoryName andArticleTitle: (NSString *)articleTitle andGUID: (NSString *)guId;
- (void)trackSpecialReportNewsDetailScreenWithGuId: (NSString *)GuId andArticleTitle: (NSString *)articleTitle  andSpecialReport: (NSString *)specialReport andSpecialReportEnTittle: (NSString *)spEnTittle;




@end
