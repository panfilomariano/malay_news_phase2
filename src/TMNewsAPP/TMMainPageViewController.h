//
//  TMMainPageViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/9/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "ViewPagerController.h"
#import <UIKit/UIKit.h>

@protocol TMMainPageViewControllerDelegate <NSObject>
@optional
-(void)getAPIitems;
@end

@interface TMMainPageViewController : ViewPagerController<UIPopoverControllerDelegate>
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, weak) id <TMMainPageViewControllerDelegate> didFinishRequestDelegate;
@property (nonatomic) float index;


@end
