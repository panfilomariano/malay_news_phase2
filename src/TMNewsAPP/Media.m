//
//  Media.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/12/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "Media.h"

@implementation Media

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.mediaGroup = [decoder decodeObjectForKey:@"mediaGroup"];
    self.tittle = [decoder decodeObjectForKey:@"tittle"];
    self.guid = [decoder decodeObjectForKey:@"guid"];
    self.pubDate = [decoder decodeObjectForKey:@"pubDate"];
    self.feedlink = [decoder decodeObjectForKey:@"feedlink"];
    self.sectionUrl = [decoder decodeObjectForKey:@"sectionUrl"];
    self.downloadTime = [decoder decodeObjectForKey:@"downloadTime"];
    self.brief = [decoder decodeObjectForKey:@"brief"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.mediaGroup forKey:@"mediaGroup"];
    [encoder encodeObject:self.tittle forKey:@"tittle"];
    [encoder encodeObject:self.guid forKey:@"guid"];
    [encoder encodeObject:self.pubDate forKey:@"pubDate"];
    [encoder encodeObject:self.feedlink forKey:@"feedlink"];
    [encoder encodeObject:self.sectionUrl forKey:@"sectionUrl"];
    [encoder encodeObject:self.downloadTime forKey:@"downloadTime"];
    [encoder encodeObject:self.brief forKey:@"brief"];
}

@end
