//
//  NSString+Helpers.h
//  Cubit
//
//  Created by Wong Johnson on 10/18/12.
//  Copyright (c) 2012 Pace Creative Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Helpers)

- (NSString *) stringByUrlEncoding;
- (NSString *) formatDate;

@end
