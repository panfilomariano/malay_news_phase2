
//
//  TMTableBeritaListViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/17/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMTableBeritaListViewController.h"
#import "TMBeritaListTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Helpers.h"
#import "TMNewsDetailsViewController.h"
#import "TMBeritaTopNewsTableViewCell.h"
#import "SwipeView.h"
#import "NIAttributedLabel.h"
#import "TMDetailedBeritaViewController.h"
#import "TMPhotoBrowserViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "TMCurrentAffairsDetailsViewController.h"
#import "SMPageControl.h"
#import "TMBeritaImageTableViewCell.h"
#import "TMNewsPagesWebTableViewCell.h"
#import "LibraryAPI.h"
#import "Article.h"
#import "TMSwipeViewTableViewCell.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "TMLongCollectionViewCell.h"
#import "Util.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"
#import "AnalyticManager.h"
#import "TMMoviePlayerViewController.h"
#import "Media.h"
#import "TMNewsPagesViewController.h"
#import "TMLiveStreamingSubTableViewCell.h"
#import "TMLiveStreamMainTableViewCell.h"
@interface TMTableBeritaListViewController ()<SwipeViewDataSource,SwipeViewDelegate,UIWebViewDelegate, TMBeritaListTableViewCellDelagate, TMNewsDetailsViewControllerDelagate, GADBannerViewDelegate, GADAdSizeDelegate, TMNewsPagesViewControllerDelagate>
{
    float webViewHeight;
    BOOL authed;
}
@property (nonatomic, weak) IBOutlet UITableView *newsListTable;
@property (nonatomic, strong)  TMSwipeViewTableViewCell *swipeViewCell;
@property (nonatomic, strong)  NSMutableArray *newsListArray;
@property (nonatomic, strong) NSString *compareString;
@property (nonatomic, strong) NSMutableArray *swipeviewArray;
@property (nonatomic, strong) TMMoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) SMPageControl *pageControl;
@property (nonatomic, strong) NSString *tabType;
@property (nonatomic, strong) NSString *sectionType;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic) BOOL hasHeader;

@property (nonatomic) BOOL didTapLiveStream;
@end

@implementation TMTableBeritaListViewController
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    _bookmarks = [[NSMutableArray alloc] init];
    
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
  
        
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable)
                                                 name:@"didBookmarkChanged"
                                               object:nil];
    
    self.swipeviewArray = [[NSMutableArray alloc]init];
   
    self.tabType = [self.dictionary valueForKey:@"type"];
    self.sectionType = [self.dictionary valueForKey:@"sectionType"];
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:self action:@selector(reloadTableData:) forControlEvents:UIControlEventValueChanged];
    [self.newsListTable addSubview:refreshFeed];

    [self getArticleWithForce:NO];

    if([self.tabType isEqualToString:[Constants SPECIAL_REPORT_TYPE]]){
         [self.newsListTable setContentInset:UIEdgeInsetsMake(0, 0, 6, 0)];
    }
    
    self.swipeViewCell.swipeView.pagingEnabled = YES;
    [self.swipeViewCell.swipeView setCurrentItemIndex:0];
    [self.swipeViewCell.swipeView setFrame:CGRectZero];
   
    [self setUpads];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark tableView DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        if (self.hasHeader || self.hasLiveStream) {
            if (self.didTapLiveStream){
                return 4;//to change
            }
            return 1;
        }
    }else if(section == 1){
        return [self.newsListArray count];
    }
    
    return 0;
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

        if (indexPath.section == 0) {
            if ( self.hasHeader) {
                TMSwipeViewTableViewCell *cell = (TMSwipeViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"svCell" forIndexPath:indexPath];
                [cell resignFirstResponder];
                cell.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:26/255.0 green:153.0/255.0 blue:252.0/255.0 alpha:1.0];
                cell.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:11/255.0 green:92/255.0 blue:147/255.0 alpha:1];
                cell.pageControl.backgroundColor = [UIColor clearColor];
                cell.pageControl.alpha = 1;
                cell.pageControl.indicatorDiameter = 5.0f;
                cell.pageControl.alignment = SMPageControlAlignmentRight;
                cell.pageControl.indicatorMargin = 2.0f;
                cell.pageControl.hidesForSinglePage = YES;
                self.pageControl = cell.pageControl;
                return cell;
            } else if (self.hasLiveStream){
                if (indexPath.row == 0) {
                    TMLiveStreamMainTableViewCell *cell = (TMLiveStreamMainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LSmainCell" forIndexPath:indexPath];
                    return cell;
                } else {
                    TMLiveStreamingSubTableViewCell *cell = (TMLiveStreamingSubTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LSSubCell" forIndexPath:indexPath];
                    return cell;
                }
            }
        }else{
            Article *article = (Article *)[self.newsListArray objectAtIndex:indexPath.row];
            if (article) {
        
                    if([self.tabType isEqualToString:[Constants SPECIAL_REPORT_TYPE]] || [self.sectionType isEqualToString:[Constants SPECIAL_REPORT_TYPE]]){
                        
                        TMBeritaImageTableViewCell *cell = (TMBeritaImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bigCell" forIndexPath:indexPath];
                        NSString *photoThumbnail = article.thumbnail;
                        cell.titleLabel.text = article.tittle;
                        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                  placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                
                        return cell;
                
                    }else if([self.tabType isEqualToString:[Constants HTML_TYPE]] || [self.sectionType isEqualToString:[Constants HTML_TYPE]]){
                        TMNewsPagesWebTableViewCell *cell = (TMNewsPagesWebTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"webcell" forIndexPath:indexPath];
                       
                        NSString *htmlstring = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %f\">%@</span>",
                                                @"Helvetica Neue",
                                                14.0f,article.desc];
                        
                        cell.webview.delegate = self;
                        [cell.webview setUserInteractionEnabled:NO];
                        [cell.webview loadHTMLString:htmlstring baseURL:nil];

                        return cell;
                        
                    }else{
                        TMBeritaListTableViewCell *cell = (TMBeritaListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                        NSString *title = article.tittle;
                        cell.title.text = title;
                        cell.delegate = self;
                        NSString *datestring = article.pubDate;
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
                        NSDate *pubDate = [dateFormatter dateFromString:datestring];
                        NSString *formatedDate = [pubDate dateString];
                        cell.date.text = formatedDate;
                        NSArray *mediaGroupArray = article.mediagroupList;
                        NSDictionary *mediaDictionary = (NSDictionary *) [mediaGroupArray objectAtIndex:0];
                        NSString *mediaType = [mediaDictionary valueForKey:@"type"];
                        NSString *photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                        
                        if([mediaType isEqualToString:@"video"]){
                            [cell.playOverlay setHidden:NO];
                        }else{
                            [cell.playOverlay setHidden:YES];
                        }
                        
                        [cell.starButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:article]];
                        cell.indexPath = indexPath;
                        [cell.thumbnail.layer setBorderColor:[[UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1] CGColor]];
                        [cell.thumbnail.layer setBorderWidth: 1.0];
                        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                                  placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                        return cell;
                    }
            }
        }
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 ) {
        if (self.hasHeader) {
            
            float width = [UIScreen mainScreen].bounds.size.width;
            float aheight = 9;
            float awidth = 16;
            return (aheight/awidth * width)+70;

        } else if(self.hasLiveStream){
            if (indexPath.row == 0) {
                return 300;
            } else {
                return 93;
            }
        }
    }else if(indexPath.section == 1){
        if ([self.tabType isEqualToString:[Constants SPECIAL_REPORT_TYPE]] || [self.sectionType isEqualToString:[Constants SPECIAL_REPORT_TYPE]]) {
            float width = [UIScreen mainScreen].bounds.size.width;
            float aheight = 9;
            float awidth = 16;
            return (aheight/awidth * width)+30;
        }else if([self.tabType isEqualToString:[Constants HTML_TYPE]] || [self.sectionType isEqualToString:[Constants HTML_TYPE]]){
            return webViewHeight+50;
        }else{
            return 85;
        }
    }
    
    return 0;
}
#pragma mark tableView Delegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section == 0) {
        if (indexPath.row == 0 && self.hasLiveStream) {
            self.didTapLiveStream =YES;
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        }
    } else {
        NSString *type = [self.dictionary valueForKey:@"type"];
        NSString *sectionType = [self.dictionary valueForKey:@"sectionType"];
        Article *article = (Article *)[self.newsListArray objectAtIndex:indexPath.row];
        NSArray *mediaGroupArray = article.mediagroupList;
        NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:0];
        NSString *content = [mediaDictionary valueForKey:@"content"];
        
        if([type isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants EPISODE_TYPE]]){
            TMNewsDetailsViewController *detailednewsVC = (TMNewsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetailsID"];;
            if (_bookmarks.count) {
                for (Article *articleFromBookmark in _bookmarks) {
                    if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                        detailednewsVC.isBookmark = YES;
                        break;
                    }
                }
            }
            detailednewsVC.didTapBookmarkButtonDelegate =self;
            detailednewsVC.passedArray = self.newsListArray;
            detailednewsVC.categoryForSR = article.category;
            detailednewsVC.index = indexPath.row;
            if (self.screenType == SpecialReportBeritaScreenType) {
                detailednewsVC.screenType = SpecialReportScreen;
                detailednewsVC.categoryForSR = self.categoryForSR;
                detailednewsVC.specialReportEnTittle = self.specialReportEnTittle;
            } else if (self.screenType == CABeritaType) {
                detailednewsVC.screenType = CAScreen;
            }
            
            [self.navigationController pushViewController:detailednewsVC animated:YES];
            
        }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
            TMPhotoBrowserViewController *photoBrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
            photoBrowser.article = article;
            photoBrowser.view.alpha = 1;
            photoBrowser.isNewsPhoto = NO;
            photoBrowser.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:photoBrowser animated:YES completion:nil];
            
        }else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]){
            
            self.moviePlayer = [[TMMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:content]];
            [self presentMoviePlayerViewControllerAnimated:self.moviePlayer];
            [self.moviePlayer.moviePlayer play];
            
        }/*else if([type isEqualToString:[Constants CA_TYPE]]){
          
          TMCurrentAffairsDetailsViewController *cadv = (TMCurrentAffairsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"currentAffairDetails"];
          NSString *stringUrl = article.sectionURL;
          cadv.stringApiUrl = stringUrl;
          [self.navigationController pushViewController:cadv animated:YES];
          
          }*/else if ([type isEqualToString:[Constants SPECIAL_REPORT_TYPE]]){
              
              TMDetailedBeritaViewController *dbvc = (TMDetailedBeritaViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"detailedBeritaID"];
              dbvc.selectedIndex = 0;
              dbvc.title = article.tittle;
              dbvc.categoryForSR = article.tittle;
              dbvc.specialReportArticle = article;
              if (self.screenType == SpecialReportBeritaScreenType) {
                  dbvc.screenType = SpecialReportScreenType;
                  dbvc.specialEnTittle = article.entittle;
              }
              
              [self.navigationController pushViewController:dbvc animated:YES];;
              
          }

    }
}

#pragma mark WebView Delegates
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    CGRect frame = webView.frame;
    frame.size.height = 5.0f;
    webView.frame = frame;
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    CGSize mWebViewTextSize = [webView sizeThatFits:CGSizeMake(1.0f, 1.0f)];  // Pass about any size
    CGRect mWebViewFrame = webView.frame;
    mWebViewFrame.size.height = mWebViewTextSize.height;
    webView.frame = mWebViewFrame;
    if(webViewHeight != webView.frame.size.height){
        webViewHeight = webView.frame.size.height;
        
     
    }
    //Disable bouncing in webview
    for (id subview in webView.subviews)
    {
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
        {
            [subview setBounces:NO];
        }
    }
}
#pragma mark privateFunctions
-(void)getBookmarkedItems
{
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.newsListTable reloadData];
        }
    }];

}
-(void)getTopNewsWithForce:(BOOL)isForced{
    if (!self.hasLiveStream) {
        [[LibraryAPI sharedInstance]getTopNewsWithForce:isForced andCompletionBlock:^(NSMutableArray *array) {
            [self.swipeviewArray removeAllObjects];
            for (id object in array) {
                Article *article = (Article *)object;
                NSString *category = article.sectionName;
                if (![category isEqualToString:@"home"]){
                    if ([self.compareString isEqualToString:category]){
                        [self.swipeviewArray addObject:article];
                    }
                }
            }
            if ([self.swipeviewArray count]) {
                self.hasHeader = true;
                [self.newsListTable reloadData];
                
            }
            
        }];
    }
}

-(void)getArticleWithForce:(BOOL)isForced{
     NSString *urlString = [self.dictionary valueForKey:@"sectionURL"];
     NSString *sectionType = [self.dictionary valueForKey:@"sectionType"];
    NSString *type = [self.dictionary valueForKey:@"type"];
    if ([sectionType isEqualToString:[Constants PHOTOS_TYPE]] || [type isEqualToString:[Constants PHOTOS_TYPE]] ) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[LibraryAPI sharedInstance]getSectionArticlesFromURL:urlString withFeedUrl:self.feedLink isForced:isForced withCompletionBlock:^(NSMutableArray *array) {
            self.newsListArray = [array mutableCopy];
            if([self.newsListArray count]) {
                Article *article = (Article *)[self.newsListArray firstObject];
                self.compareString = article.enCategory;
             
            }
             [self.newsListTable reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        }];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[LibraryAPI sharedInstance]getArticlesWithURL:urlString isForced:isForced withCompletionBlock:^(NSMutableArray *array) {
            self.newsListArray = [array mutableCopy];
            if([self.newsListArray count]) {
                Article *article = (Article *)[self.newsListArray firstObject];
                NSDictionary *mediaDict = [article.mediagroupList objectAtIndex:0];
                NSString *thumbnailUrl = [mediaDict valueForKey:@"thumbnail"];
                if (self.delegate && [self.delegate respondsToSelector: @selector(loadViewedItems:)]) {
                    [self.delegate performSelector:@selector(loadViewedItems:) withObject:thumbnailUrl];
                }
                self.compareString = article.enCategory;
                [self getTopNewsWithForce:NO];
                [self getBookmarkedItems];
            }
            [self.newsListTable reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            
        }];
    }
    

}

-(void)reloadTableData: (UIRefreshControl *)refreshFeed
{
    [self getArticleWithForce:YES];
    [self.newsListTable reloadData];
    if (refreshFeed) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        
        [refreshFeed endRefreshing];
    }
}


#pragma mark TMBeritaListTableViewCellDelagate
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    Article *article = (Article *) [self.newsListArray objectAtIndex:indexPath.row];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
        
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            [_bookmarks removeObject:bookmarkArticle];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
            [_bookmarks addObject:article];
        }
        
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
    }
    
    
    
    [self.newsListTable beginUpdates];
    [self.newsListTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.newsListTable endUpdates];
  
    if (!bookmarkArticle){
        if (self.screenType == CABeritaType){
            [[AnalyticManager sharedInstance] trackCABookmarkWithProgram:article.enCategory andEpisodeId:article.guid andEpisodeTitle:article.tittle];
        }else {
            [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
        }
    }
    
}


#pragma mark swipeView Delegate
- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeViewCell.swipeView.bounds.size;
}
-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    
   self.pageControl.currentPage = swipeView.currentPage;
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
   self.pageControl.numberOfPages = [self.swipeviewArray count];
    if ([self.swipeviewArray count]) {
        float width = [UIScreen mainScreen].bounds.size.width;
        float aheight = 9;
        float awidth = 16;
        [self.swipeViewCell.swipeView setFrame:CGRectMake(0, 0, width, (aheight/awidth * width) +60)];
        self.hasHeader = true;
    }else{
        [self.swipeViewCell.swipeView setFrame:CGRectZero];
        self.hasHeader = false;
    }
    return [self.swipeviewArray count];
    
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
   
    UIImageView *imageView = nil;
    UILabel *label = nil;
    UILabel *datelabel = nil;
    UIImageView *playOverlay = nil;
    UIImageView *gradientView = nil;
    if (view == nil) {
        float width = [UIScreen mainScreen].bounds.size.width;
        float aheight = 9;
        float awidth = 16;
        
        
        view = [[UIView alloc] initWithFrame:swipeView.bounds];
        [view setBackgroundColor:[UIColor colorWithRed:2.0/225.0 green:19.0/225.0 blue:29.0/225.0 alpha:1]];
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, aheight/awidth * width)];
        imageView.tag = 1;
        
        gradientView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gradient"]];
        [gradientView setFrame:CGRectMake(0, aheight/awidth * width-42, width, 43)];
        gradientView.tag =5;
        
        playOverlay = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"play_overlay"]];
        [playOverlay setFrame:CGRectMake(8, (aheight/awidth * width)-28, 20, 20)];
        playOverlay.tag =4;
        [playOverlay setHidden:YES];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(8, aheight/awidth * width, width-20, 40)];
        [label setFont:[UIFont fontWithName:@"HelveticaNeue" size:17]];
        [label setTextColor:[UIColor whiteColor]];
        [label setNumberOfLines:2];
        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [label setBackgroundColor:[UIColor clearColor]];
        label.tag = 2;
        
        datelabel = [[UILabel alloc] initWithFrame:CGRectMake(8, (aheight/awidth * width)+40, width-20, 20)];
        [datelabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        
        datelabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [datelabel setTextColor:[UIColor colorWithRed:175/255.0 green:188.0/255.0 blue:195.0/255.0 alpha:1.0]];
        [datelabel setBackgroundColor:[UIColor clearColor]];
        datelabel.tag = 3;
        
        [view addSubview:datelabel];
        [view addSubview:imageView];
        [view addSubview:label];
        [imageView addSubview:gradientView];
        [imageView addSubview:playOverlay];

        
    } else {
        imageView = (UIImageView *)[view viewWithTag:1];
        label = (UILabel *)[view viewWithTag:2];
        datelabel = (NIAttributedLabel *)[view viewWithTag:3];
        playOverlay = (UIImageView *)[view viewWithTag:4];
        gradientView =  (UIImageView *)[view viewWithTag:5];
    }
    
    if (self.swipeviewArray.count) {
        Article *article = (Article *) [self.swipeviewArray objectAtIndex:index];
        NSArray *mediaGroupArray = article.mediagroupList;
        
        if (mediaGroupArray.count) {
            
            NSDictionary *mediaDictionary = [mediaGroupArray firstObject];
            
            NSString *type = [mediaDictionary valueForKey:@"type"];
            if ([type isEqualToString:[Constants VIDEOS_TYPE]]){
                playOverlay.hidden = false;
            } else {
                playOverlay.hidden = true;
            }
            
            NSString *photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
            [imageView sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                         placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == 0 ? SDWebImageRefreshCached : 0];
            
            NSString *enTitle = article.tittle;
            NSString *datestring = article.pubDate;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
            NSDate *pubDate = [dateFormatter dateFromString:datestring];
            NSString *formatedDate = [pubDate dateString];
            datelabel.text = formatedDate;
            label.text = enTitle;
            
            
            
        }
    }
    
    return view;
}

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{

    Article *article = (Article *)[self.swipeviewArray objectAtIndex:index];
    NSArray *mediaGroupArray = article.mediagroupList;
    NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:0];
    NSString *content = [mediaDictionary valueForKey:@"content"];
    NSString *type = article.type;
    NSString *sectionType = article.sectionType;
    NSString *feedLink = article.feedlink;
    NSString *sectionUrl = article.sectionURL;
    
    if([type isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants NEWS_TYPE]] || [sectionType isEqualToString:[Constants EPISODE_TYPE]]){
        
        TMNewsDetailsViewController *detailednewsVC = (TMNewsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetailsID"];
        detailednewsVC.passedArray = @[article];//self.swipeviewArray;
        detailednewsVC.isTopNews = YES;
        detailednewsVC.index = 0;
        if (_bookmarks.count) {
            for (Article *articleFromBookmark in _bookmarks) {
                if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                    detailednewsVC.isBookmark = YES;
                    break;
                }
            }
        }
        [[AnalyticManager sharedInstance] trackOpenNewsDetailScreenWithCategoryName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
        detailednewsVC.didTapBookmarkButtonDelegate =self;
        [self.navigationController pushViewController:detailednewsVC animated:YES];
        
    }else if ([type isEqualToString:[Constants PHOTOS_TYPE]] || [sectionType isEqualToString:[Constants PHOTOS_TYPE]]){
        Media *media = [Media new];
        TMPhotoBrowserViewController *photoBrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
        photoBrowser.article = article;
        photoBrowser.view.alpha = 1;
        photoBrowser.isNewsPhoto = NO;
        photoBrowser.modalPresentationStyle = UIModalPresentationFullScreen;
        [[AnalyticManager sharedInstance] trackOpenPhotoGalleryWithAlbumName:media.tittle];
        [self presentViewController:photoBrowser animated:YES completion:nil];
        
    }else if ([type isEqualToString:[Constants VIDEOS_TYPE]] || [sectionType isEqualToString:[Constants VIDEOS_TYPE]]){
        
        self.moviePlayer = [[TMMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:content]];
        [self presentMoviePlayerViewControllerAnimated:self.moviePlayer];
        [self.moviePlayer.moviePlayer play];
        
    }else if([type isEqualToString:[Constants CA_TYPE]]){
        
        TMCurrentAffairsDetailsViewController *cadv = (TMCurrentAffairsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"currentAffairDetails"];
        NSString *stringUrl = article.sectionURL;
        cadv.stringApiUrl = stringUrl;
        [[AnalyticManager sharedInstance] trackOpenCAProgramScreenWithCategoryname:article.enCategory];
        [self.navigationController pushViewController:cadv animated:YES];
        
    }else if ([type isEqualToString:[Constants SPECIAL_REPORT_TYPE]]){
        

        [[LibraryAPI sharedInstance]getArticlesWithURL:sectionUrl andFeedLink:feedLink isForced:NO withCompletionBlock:^(NSMutableArray *array) {
            if ([array count]) {
                Article *article = [array objectAtIndex:0];
                NSArray *sectionsArray = article.sections;
                TMDetailedBeritaViewController *dbvc = (TMDetailedBeritaViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"detailedBeritaID"];
                dbvc.newsCategories = sectionsArray;
                dbvc.selectedIndex = 0;
                dbvc.feedLink = feedLink;
                [[AnalyticManager sharedInstance] trackOpenSpecialReportScreenWithCategoryName:article.tittle];
                [self.navigationController pushViewController:dbvc animated:YES];
            }
        }];

    }
}
#pragma mark TMNewsDetailsViewControllerDelagate
-(void)didTapBookmarkButton
{
    [self getBookmarkedItems];
}

#pragma mark admob
- (void)setUpads
{
    if(![self.tabType isEqualToString:[Constants HTML_TYPE]]){
    
    
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    self.bannerView.adSize = kGADAdSizeBanner;
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       ]];*/
  //  self.bannerView.adSizeDelegate = self;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:request];
    
    }
}
#pragma mark GADbannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view{
    self.bannerHeight.constant = 50;
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
    
}

- (void)reloadTable{
    [self getArticleWithForce:NO];
    [self.newsListTable reloadData];
}
@end
