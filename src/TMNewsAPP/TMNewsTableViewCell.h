//
//  TMNewsTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/15/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMNewsTableViewCellDelagate <NSObject>

- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMNewsTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *titleLabelLATEST;
@property (nonatomic, weak) IBOutlet UILabel *dateLabelLATEST;
@property (nonatomic, weak) IBOutlet UILabel *categoryLabelLATEST;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UIImageView *playOverlay;
@property (nonatomic, weak) IBOutlet UIButton *starButton;
@property (nonatomic, weak) id <TMNewsTableViewCellDelagate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end
