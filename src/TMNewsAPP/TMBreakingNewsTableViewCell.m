//
//  TMBreakingNewsTableViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMBreakingNewsTableViewCell.h"

@implementation TMBreakingNewsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)didTapClose:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector: @selector(closeBreakingNews)]) {
        [self.delegate performSelector:@selector(closeBreakingNews)];
    }
    
}
@end
