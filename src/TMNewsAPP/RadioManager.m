//
//  RadioManager.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/11/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "RadioManager.h"
#import "Constants.h"
#import "AnalyticManager.h"

NSString * const FormatType_toString[] = {
    [WARNA] = @"warna",
    [RIA] = @"ria",
    [OLI] = @"oli"
    
};

@interface RadioManager()
@property (nonatomic, strong) MPMoviePlayerController *radioPlayer;
@property (nonatomic) RadioStationType currentStation;
@end

@implementation RadioManager


static RadioManager *_instance;
+ (RadioManager *) sharedInstance
{
    if (_instance == nil) {
        _instance = [[RadioManager alloc] init];
    }
    
    return  _instance;
}

- (id) init {
    
    if (self=[super init]) {
        
       // _radioPlayerWarna =
       // _radioPlayerRia = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[Constants ]]];
    }
    
    return self;
}

-(void)play {
    NSString *str = FormatType_toString[self.radioStation];
    [[AnalyticManager sharedInstance] trackRadioWithName:str];
    
    if(self.currentStation == self.radioStation && self.radioPlayer){
        self.isRadioPlaying = YES;
        [self.radioPlayer play];
        return;
    }
    
    switch (self.radioStation) {
        case WARNA: {
            self.radioPlayer =[[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[Constants MALAY_RADIO_WARNA_URL]]];
            self.currentStation = WARNA;
            self.isRadioPlaying = YES;
            [self.radioPlayer play];
            break;
        } case RIA: {
            self.radioPlayer =[[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[Constants MALAY_RADIO_RIA_URL]]];
            self.currentStation = RIA;
            self.isRadioPlaying = YES;
            [self.radioPlayer play];
            break;
        } case OLI: {
            self.radioPlayer =[[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[Constants TAMIL_RADIO_OLI_URL]]];
            self.currentStation = OLI;
            self.isRadioPlaying = YES;
            [self.radioPlayer play];
            break;
        }
    }
}

-(void)stop {
    self.isRadioPlaying = NO;
    [self.radioPlayer stop];
}

@end
