//
//  TMSearchCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 2/25/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSearchCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIButton *starButton;



@end
