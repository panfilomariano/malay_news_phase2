//
//  Data.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/23/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, FontSizeType)
{
    FontSizeTypeDefault = 0,
    FontSizeTypeXSmall = 1,
    FontSizeTypeSmall = 2,
    FontSizeTypeLarge = 3,
    FontSizeTypeXLarge = 4
};

@interface Data : NSObject

@property (nonatomic, assign) FontSizeType fontSizeType;

+ (Data *)sharedInstance;
- (void)downloadAllArticlesWithImages: (BOOL)withImages andCompletionBlock: (void (^)()) completionBlock;

@end
