//
//  TMiPADVideoDetailsViewController.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/17/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMiPADVideoDetailsViewController : UIViewController
@property (nonatomic, strong) NSMutableArray *videoBrowserList;
@property (nonatomic) BOOL isFromMain;
@property (nonatomic, strong) NSArray *videoList;
@property (nonatomic) int index;
@end
