//
//  TMPhotoBrowserViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/11/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"

@interface TMPhotoBrowserViewController : UIViewController

@property (nonatomic, strong) Article *article;
@property (nonatomic, assign) NSUInteger currentPage;
@property (assign) BOOL isNewsPhoto;
@property (nonatomic) BOOL isFromExclusive;
@property (nonatomic) BOOL isFromCA;
@property (nonatomic, strong) NSString *photosFromEksklusif;
@property (nonatomic, strong) NSString *photosFromCA;
@property (nonatomic) BOOL isTopNews;

@end
