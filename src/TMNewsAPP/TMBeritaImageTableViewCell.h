//
//  TMBeritaImageTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMBeritaImageTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@end
