//
//  TMSImageSliderTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSImageSliderTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *caImage;

@end
