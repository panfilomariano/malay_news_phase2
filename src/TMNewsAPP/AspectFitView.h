//
//  AspectFitView.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/8/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AspectFitView : UIView

@property (nonatomic, strong) UIView *childView;

@end
