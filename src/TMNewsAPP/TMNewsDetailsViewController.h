//
//  TMNewsDetailsViewController.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/15/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPagerController.h"
#import "Article.h"

@protocol TMNewsDetailsViewControllerDelagate <NSObject>
- (void) didTapBookmarkButton;
@end

typedef enum
{
    Others,
    LatestNewsScreen,
    CAScreen,
    SpecialReportScreen,
    NewsScreen,
    
} ScreenType;

@interface TMNewsDetailsViewController : ViewPagerController

@property (nonatomic, weak) id <TMNewsDetailsViewControllerDelagate> didTapBookmarkButtonDelegate;
@property (nonatomic, strong) NSMutableArray *passedArray;
@property (nonatomic, strong) NSString *specialReportEnTittle;
@property (nonatomic) NSInteger index;
@property (nonatomic, assign) BOOL isBookmark;
@property (nonatomic, assign) BOOL isTopNews;
@property (nonatomic, strong) NSString *categoryForSR;
@property (nonatomic, assign)  ScreenType screenType;

@end
