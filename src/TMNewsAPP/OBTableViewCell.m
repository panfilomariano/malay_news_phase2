//
//  OBTableViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/6/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "OBTableViewCell.h"

@implementation OBTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
