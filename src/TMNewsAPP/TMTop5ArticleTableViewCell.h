//
//  TMTop5ArticleTableViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/11/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMTop5ArticleTableViewCellDelegate <NSObject>
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMTop5ArticleTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *tittle;
@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIImageView *playImage;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;

@property (nonatomic, weak) id <TMTop5ArticleTableViewCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end
