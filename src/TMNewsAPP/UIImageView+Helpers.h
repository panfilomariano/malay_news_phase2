//
//  UIImageView+Helpers.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/22/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Helpers)

- (void) setBorder;
- (void) hideBorder;

@end
