//
//  TMBeritaTopNewsTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/6/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMBeritaTopNewsTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@end
