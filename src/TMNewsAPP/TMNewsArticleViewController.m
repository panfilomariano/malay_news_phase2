//
//  TMNewsArticleViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMNewsArticleViewController.h"
#import "NSString+Helpers.h"
#import "Util.h"
#import "Data.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIImageView+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"
#import "SwipeView.h"
#import "SMPageControl.h"
#import <OutbrainSDK/OutbrainSDK.h>
#import "OBClassicRecommendationsView.h"
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "TMTop5ArticleTableViewCell.h"
#import "TMNewsDetailsPagerViewController.h"
#import "TMIPADMenuViewController.h"
#import "TMWebViewController.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "Media.h"
#import "Constants.h"
#import "AnalyticManager.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"
#import "TMMoviePlayerViewController.h"

@interface TMNewsArticleViewController () <UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, SwipeViewDataSource, SwipeViewDelegate, UIScrollViewDelegate, OBWidgetViewDelegate, TMTop5ArticleTableViewCellDelegate, TMNewsDetailsPagerViewControllerDelagate, TMIPADMenuViewControllerDelegate, UIPopoverControllerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, GADBannerViewDelegate, GADAdSizeDelegate>

{
    float webViewHeight;
    float scrollViewWidth;
    float tableViewWidth;
    BOOL canTapBookmark;
}

@property (nonatomic, strong) SwipeView *swipeView;
@property (nonatomic, strong) SMPageControl *pageControl;
@property (nonatomic, strong) OBRecommendationResponse *recommendationResponse;
@property (nonatomic, strong) UILabel *categoryLabel;
@property (nonatomic, strong) UILabel *tittleLabel;
@property (nonatomic, strong) UILabel *pubDateLabel;
//@property (nonatomic, strong) UILabel *pagerLabel;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIView *captionView;
@property (nonatomic, strong) TMMoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) NSMutableArray *top5newsArray;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) UIBarButtonItem *fontBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *bookmarkBarButtonItem;
@property (nonatomic, strong) UIView *sliderView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIPopoverController *popOverController;
@property (nonatomic, strong) UIView *dimView;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIActivityIndicatorView *outbrainActivityIndicator;
@property (nonatomic, strong) NSMutableArray *menListArray;
@property (nonatomic, strong) NSMutableArray *newsList;

@property (nonatomic) bool isTherePopOver;
@property (nonatomic) bool isThereSlider;
@property (nonatomic) BOOL isPortrait;
@property (nonatomic) BOOL hasEmbededVideo;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *scrollViewWidthLC;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableViewWidthLC;
@property (nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@property (nonatomic) dispatch_group_t group;

@end

@implementation TMNewsArticleViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _top5newsArray = [[NSMutableArray alloc] init];
    _bookmarks = [[NSMutableArray alloc] init];
    canTapBookmark = YES;
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isPortrait = UIInterfaceOrientationIsPortrait(self.interfaceOrientation);
    [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    tableViewWidth = 244.0f;
    scrollViewWidth = 524.0f;
    if (!self.isPortrait) {
        scrollViewWidth = 706;
        tableViewWidth = 318.0f;
    }
    
    self.tableViewWidthLC.constant = tableViewWidth;
    self.scrollViewWidthLC.constant = scrollViewWidth;
    [self requestWebView];
    [self trackTagging];
    
    if (self.hasBarItems) {
        [self setUpBarButtonItems];
        UIButton *bookmarkButton = (UIButton *) _bookmarkBarButtonItem.customView;
        [bookmarkButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]];
    }
    
    [self requestBookmarksWithoutDispatch];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
    
    if (self.hasEmbededVideo)
        [self.webView loadHTMLString:nil baseURL:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    float newHeight = self.webView.scrollView.contentSize.height;
    NSLog(@"newHeight=%f", newHeight);
    if (webViewHeight != newHeight) {
        webViewHeight = newHeight;
        [self layoutScrollView];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isPortrait = UIInterfaceOrientationIsPortrait(self.interfaceOrientation);

    self.bannerHeight.constant = 0;
    tableViewWidth = 244.0f;
    scrollViewWidth = 524.0f;
    if (!self.isPortrait) {
        scrollViewWidth = 706;
        tableViewWidth = 318.0f;
    }
    
    [self setUpads];
    [self loadData];
    [self layoutScrollView];
    
    self.bannerHeight.constant = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadSwipeView)
                                                 name:@"willRefreshSwipeView"
                                               object:nil];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UIButton *bookmarkButton = (UIButton *) _fontBarButtonItem.customView;
    if (bookmarkButton.selected)
        [self dismissSlider];
}

- (BOOL)validateEmail:(NSString *)candidate
{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
    /*
    NSString *emailRegex = @"[A-Z0-9a-z._%+-:]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];*/
}

- (void)trackTagging
{
    if (self.type == SpecialReportArticle/*[self.article.type isEqualToString:@"special report"]*/){
        [[AnalyticManager sharedInstance] trackSpecialReportNewsDetailScreenWithGuId:self.article.guid andArticleTitle:self.article.tittle  andSpecialReport:self.article.enCategory andSpecialReportEnTittle:self.specialReportArticle.entittle];
    }else if([self.article.type isEqualToString:[Constants CA_TYPE]] || self.type == CAArticle){
        [[AnalyticManager sharedInstance] trackContentScreenInCAWithCategoryName:self.article.enCategory andArticleTitle:self.article.tittle andGUID:self.article.guid];
    }else if (self.type == LatestNewsArticle){
        [[AnalyticManager sharedInstance] trackContentScreenWithCategoryName:@"latest news" andArticleTitle:self.article.tittle andGUID:self.article.guid];
    }else{
        [[AnalyticManager sharedInstance] trackContentScreenWithCategoryName:self.article.enCategory andArticleTitle:self.article.tittle andGUID:self.article.guid];
    }
}

#pragma mark WebView Delegates

-(BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    NSLog(@"navigationType=%ld urlString=%@", navigationType, urlString);
    if ([urlString hasPrefix:@"https://www.youtube.com/"]) {
        self.hasEmbededVideo = YES;
    }
    
    if ([Util checkURLForPush:urlString withNavigationType:navigationType]) {
        
        if ([urlString hasPrefix:@"https://"] || [urlString hasPrefix:@"http://"] ) {
            TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            webViewController.urlString = urlString;
            [self.navigationController pushViewController:webViewController animated:YES];
            return NO;
        }
    }
    return YES;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 && self.recommendationResponse) {
        return 1;
    } else if (section == 1) {
        return _top5newsArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (self.recommendationResponse) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OutbrainCell" forIndexPath:indexPath];
            UIView *view = [cell.contentView viewWithTag:111];
            if (view) {
                [view removeFromSuperview];
            }
    
            OBClassicRecommendationsView *recomView = [[OBClassicRecommendationsView alloc] init];
            recomView.tag = 111;
            recomView.frame = cell.bounds;
            recomView.showImages = NO;
            recomView.widgetDelegate = self;
            recomView.recommendationResponse = self.recommendationResponse;
            recomView.alpha = 1;
            [cell.contentView addSubview:recomView];
            recomView.frame = CGRectMake(0, 18, cell.frame.size.width, cell.frame.size.height);
            
            if (!self.outbrainActivityIndicator.superview) {
                [cell.contentView addSubview:self.outbrainActivityIndicator];
            }
            
            return cell;
        }
    } else {
        
        Article *article = (Article *) [_top5newsArray objectAtIndex:indexPath.row];
        TMTop5ArticleTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"top5Cell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.indexPath = indexPath;
        cell.tittle.text = article.tittle;
        cell.category.text = article.category;
        cell.pubDate.text = [article.pubDate formatDate];
        
        NSString *photoThumbnail = @"";
        if (article.mediagroupList) {
            if (article.mediagroupList.count) {
                NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                NSString *type = [mediaDictionary valueForKey:@"type"];
                if ([type isEqualToString:@"video"]) {
                    [cell.playImage setHidden:NO];
                } else {
                    [cell.playImage setHidden:YES];
                }
            }
        }
        
        [cell.bookmarkButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:article]];
        
        [cell.thumbnail setBorder];
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                          placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
        
        return cell;
        
        
    }
    return nil;
}

#pragma mark UITableViewDelegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Article *article = (Article *)[self.top5newsArray objectAtIndex:indexPath.row];
    BOOL isBookmark = [self checkBookmarkWithArticle:article];
    TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
    newsArticleVC.article = article;
    newsArticleVC.isBookmark = isBookmark;
    newsArticleVC.hasBarItems = YES;
    [[AnalyticManager sharedInstance] trackPopularNewsWithArticleTitle:article.tittle andGUID:article.guid];
    [self.navigationController pushViewController:newsArticleVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (self.recommendationResponse) {
            return 320.0f;
        }
    } else {
        if (self.isPortrait)
            return 250.0f;
        else
            return 292.0f;
    }
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
        return 42.0f;
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 42.0f)];
        view.backgroundColor = [UIColor colorWithRed:0.969 green:0.969 blue:0.969 alpha:1.000];
        UIView *lineview = [[UIView alloc] initWithFrame:CGRectMake(9, 0, self.tableView.frame.size.width-18, 1.0f)];
        lineview.backgroundColor = [UIColor colorWithRed:227.0f/255.0f green:228.0f/255.0f blue:230.0f/255.0f alpha:1];
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont boldSystemFontOfSize:16.0f];
        label.textColor = [UIColor colorWithRed:0/255.0f green:45/255.0f blue:75/255.0f alpha:1];
        label.text = [Constants TOP5_NEWS];
        [view addSubview:label];
        [view addSubview:lineview];
        label.frame = CGRectMake(9, 5, self.tableView.frame.size.width - 18, 37.0f);
        return view;
    }
    return nil;
}

#pragma mark SwipeViewDataSOurce
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return self.article.mediagroupList.count;
}

#pragma mark SwipeViewDelegate
-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    
    self.pageControl.currentPage = swipeView.currentItemIndex;
    [self layoutScrollView];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    float height = 0;
    
    float imageViewHeight = 294;
    float imageViewWidth = scrollViewWidth;
    
    if (!self.isPortrait) {
        imageViewHeight = 396;
        imageViewWidth = 706;
    }
    
    UIImageView *imageView = nil;
    UIImageView *playOverlay = nil;
    
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, scrollViewWidth, height)];

        imageView = [[UIImageView alloc] init];
        imageView.tag = 1;
    
        playOverlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play_overlay"]];
        playOverlay.tag = 6;
        
        [view addSubview:imageView];
        [view addSubview:playOverlay];
        
    } else {
        imageView = (UIImageView *)[view viewWithTag:1];
        playOverlay = (UIImageView *) [view viewWithTag:6];
    }
    
    NSDictionary *dict = (NSDictionary *) [self.article.mediagroupList objectAtIndex:index];
    NSString *type = [dict valueForKey:@"type"];
    NSString *thumbnail = @"";
    if ([type isEqualToString:@"video"/*[Constants VIDEOS_TYPE]*/]) {
        playOverlay.hidden = NO;
         thumbnail = [dict valueForKey:@"thumbnail"];
    } else {
        playOverlay.hidden = YES;
        thumbnail = [dict valueForKey:@"content"];
    }
   
    height += imageViewHeight;
    [view setFrame:CGRectMake(0, 0, scrollViewWidth, height)];
    
    [imageView setFrame:CGRectMake(0, 0, imageViewWidth, imageViewHeight)];
    [imageView setBorder];
    [imageView sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                 placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:index == 0 ? SDWebImageRefreshCached : 0];
    
    [playOverlay setFrame:CGRectMake(12, imageViewHeight + imageView.frame.origin.y - 40, 30, 30)];
    [view bringSubviewToFront:playOverlay];
    return view;
}

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index
{
    if (self.article.mediagroupList.count) {
        NSDictionary *mediaDictionary = (NSDictionary *) [self.article.mediagroupList objectAtIndex:index];
        NSString *content = [mediaDictionary valueForKey:@"content"];
        NSString *type = [mediaDictionary valueForKey:@"type"];
        if([type isEqualToString:@"video"]){
            self.moviePlayer = [[TMMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:content]];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(MPMoviePlayerDidExitFullscreen:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:nil];
            [self presentMoviePlayerViewControllerAnimated:self.moviePlayer];
            [self.moviePlayer.moviePlayer play];
        }
    }
}

#pragma mark MoviePlayerNofication

- (void)MPMoviePlayerDidExitFullscreen:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
    
    [self.moviePlayer.moviePlayer stop];
    self.moviePlayer = nil;
    [self dismissMoviePlayerViewControllerAnimated];
    
}
#pragma mark Outbrain Delegate
- (void)widgetView:(UIView<OBWidgetViewProtocol> *)widgetView tappedRecommendation:(OBRecommendation *)recommendation {
    // This recommendations was tapped.
    // Here is where we register the click with outbrain for this piece of content
    NSURL * url = [Outbrain getOriginalContentURLAndRegisterClickForRecommendation:recommendation];
    
    // Now we have a url that we can show in a webview, or if it's a piece of our native content
    // Then we can inspect [url hash] to get the mobile_id
    
    TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
    webViewController.title = [Constants MORE_FROM_NEWS];
    webViewController.urlString = [url absoluteString];
    
    [self.navigationController pushViewController:webViewController animated:YES];
}

#pragma mark TMSquareCollectionViewCellDelagate | TMSmallCollectionViewCellDelagate

- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    Article *article = (Article *) [_top5newsArray objectAtIndex:indexPath.row];
    [Util updateBookmarkWith:article addedArticleSuccess:^(Article *addedBookmarkArticle) {
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    } removedArticleSuccess:^(Article *addedBookmarkArticle) {
        [self.view makeToast:[Constants REMOVE_FROM_FAV]];
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                [_bookmarks removeObject:bookmark];
                break;
            }
        }
    }];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didBookmarkChanged"
                                                        object:nil
                                                      userInfo:nil];
    
    UIButton *bookmarkButton = (UIButton *) _bookmarkBarButtonItem.customView;
    [bookmarkButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]];
    
}

#pragma mark Notif Observers

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    self.isPortrait = UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    tableViewWidth = 244.0f;
    scrollViewWidth = 524.0f;
    if (!self.isPortrait) {
        scrollViewWidth = 706;
        tableViewWidth = 318.0f;
    }
   
    self.tableViewWidthLC.constant = tableViewWidth;
    self.scrollViewWidthLC.constant = scrollViewWidth;
    self.webView.scrollView.contentSize = CGSizeMake(1.0f, 1.0f);
    [self layoutScrollView];
    [self requestWebView];
    [self.tableView reloadData];

    if (self.isTherePopOver) {
        if (self.isPortrait) {
            int ctr = [self.menListArray count] +[self.newsList count];
            [self.popOverController setPopoverContentSize:CGSizeMake(320, 275+(40*ctr))];
            self.popOverController.contentViewController.preferredContentSize = CGSizeMake(320, 275+(40*ctr));
            [self.popOverController presentPopoverFromRect:CGRectMake(453, 7, 320, 275+(40*ctr)) inView:self.parentViewController.view permittedArrowDirections:0 animated:YES];
            [self.dimView setFrame:CGRectMake(0, 0, 768, 1024)];
            
        }else{
            int ctr =  (([self.menListArray count] +[self.newsList count])+2-1)/2;
            [self.popOverController setPopoverContentSize:CGSizeMake(400, 275+(ctr*40))];
            self.popOverController.contentViewController.preferredContentSize =CGSizeMake(400, 275+(ctr*40));
            [self.popOverController presentPopoverFromRect:CGRectMake(702,7, 400, 275+(ctr*40)) inView:self.parentViewController.view permittedArrowDirections:0 animated:YES];
            [self.dimView setFrame:CGRectMake(0, 0, 1024, 768)];
        }
    }
    
    if(self.isThereSlider){
        [self dismissSlider];
        if (self.isPortrait) {
            [self showSlider];
        }else{
            [self showSlider];
        }
    }
}

#pragma mark Public Function

- (void)layoutScrollView
{
    int addent = 0;
    switch ([Data sharedInstance].fontSizeType) {
        case FontSizeTypeXSmall:
            addent = -3;
            break ;
        case FontSizeTypeSmall:
            addent = -2;
            break ;
        case FontSizeTypeLarge:
            addent = 2;
            break ;
        case FontSizeTypeXLarge:
            addent = 3;
            break ;
            
        default:
            addent = 0;
    }
    
    
    if (!self.categoryLabel) {
        UILabel *category = [[UILabel alloc] init];
        category.numberOfLines = 1;
        category.textColor = [UIColor colorWithRed:0 green:123/255.0f blue:128/255.0f alpha:1];
        
        self.categoryLabel = category;
        [self.scrollView addSubview:category];
    }
#ifdef TAMIL
    UIFont *categoryFont = [UIFont boldSystemFontOfSize:18 + addent];
    int categoryHeight=20+addent;
#else
    UIFont *categoryFont = [UIFont boldSystemFontOfSize:16 + addent];
    int categoryHeight=18+addent;
#endif
    self.categoryLabel.frame = CGRectMake(18, 18, scrollViewWidth - 36, categoryHeight);
    self.categoryLabel.numberOfLines = 1;
    self.categoryLabel.font = categoryFont;
    self.categoryLabel.textColor = [UIColor colorWithRed:0 green:123/255.0f blue:208/255.0f alpha:1];
    
    if(self.type == SpecialReportArticle){
        self.categoryLabel.text = self.titleForSpecialReport;
    }else{
        self.categoryLabel.text = self.article.category;
    }
    
    if (!self.tittleLabel) {
        UILabel *tittle = [[UILabel alloc] init];
        tittle.numberOfLines = 99;
        
        self.tittleLabel = tittle;
        self.tittleLabel.numberOfLines = 99;
        self.tittleLabel.lineBreakMode =NSLineBreakByWordWrapping;
        [self.scrollView addSubview:tittle];
    }
    self.tittleLabel.numberOfLines = 99;
    self.tittleLabel.lineBreakMode =NSLineBreakByWordWrapping;
#ifdef TAMIL
    UIFont *tittleFont = [UIFont boldSystemFontOfSize:28 + addent];
#else
    UIFont *tittleFont = [UIFont boldSystemFontOfSize:24 + addent];
#endif
    //CGSize tittleLabelSize = CGSizeMake(scrollViewWidth - 36, 80);
    float tittleLabelHeight = [Util expectedHeightFromText:self.article.tittle withFont:tittleFont andMaximumLabelWidth:scrollViewWidth - 36];//[Util expectedHeightFromText:self.article.tittle withFont:tittleFont andMaximumLabelSize:tittleLabelSize];
    self.tittleLabel.frame = CGRectMake(18, self.categoryLabel.frame.origin.y + self.categoryLabel.frame.size.height + 5, scrollViewWidth - 36, tittleLabelHeight+5);
    self.tittleLabel.font = tittleFont;
    
    self.tittleLabel.text = self.article.tittle;
    
    if (!self.pubDateLabel) {
        UILabel *pub = [[UILabel alloc] init];
        self.pubDateLabel = pub;
        [self.scrollView addSubview:pub];
    }
    NSString *lastUpdateString = @"";
    NSString *pubDate = @"";
    UIFont *pubFont = [UIFont systemFontOfSize:14 + addent];
    if(self.article.lastUpdateDate){
        lastUpdateString = [self.article.lastUpdateDate formatDate];
    }
   
    if (lastUpdateString){
        pubDate = [NSString stringWithFormat:@"%@  Update: %@", [self.article.pubDate formatDate], lastUpdateString];
    }else{
        pubDate = [self.article.pubDate formatDate];
    }
    
    
    self.pubDateLabel.frame = CGRectMake(18, self.tittleLabel.frame.origin.y + self.tittleLabel.frame.size.height + 8, scrollViewWidth - 36, 16);
    self.pubDateLabel.numberOfLines = 1;
    self.pubDateLabel.font = pubFont;
    self.pubDateLabel.textColor = [UIColor colorWithRed:153/255.0f green:153/255.0f blue:153/255.0f alpha:1];
    self.pubDateLabel.text = pubDate;

    if (!self.swipeView) {
        SwipeView *sv = [[SwipeView alloc] init];
        sv.dataSource = self;
        sv.delegate = self;
        self.swipeView = sv;
        self.swipeView.truncateFinalPage = YES;
        [self.scrollView addSubview:sv];
        
       
    }
    
    float swipViewHeight = 294.0f;
    if (!self.isPortrait) {
        swipViewHeight = 396.0f;
    }
    
    self.swipeView.frame = CGRectMake(0, self.pubDateLabel.frame.origin.y + self.pubDateLabel.frame.size.height + 16, scrollViewWidth, swipViewHeight);
    [self.swipeView reloadData];
    
    
    if (!self.captionView) {
        
        UIView *view = [[UIView alloc] init];
        [view setBackgroundColor:[UIColor colorWithRed:2.0/225.0 green:19.0/225.0 blue:29.0/225.0 alpha:1]];
        
        UILabel *label = [[UILabel alloc] init];
        label.tag = 111;
        
        [view addSubview:label];
        self.captionView = view;
        [self.scrollView addSubview:view];
    }

    NSDictionary *dict = (NSDictionary *) [self.article.mediagroupList objectAtIndex:self.swipeView.currentItemIndex];
    NSString *caption = [dict valueForKey:@"caption"];
    float captionLabelHeight = [Util expectedHeightFromText:caption withFont:[UIFont systemFontOfSize:14+addent] andMaximumLabelWidth:scrollViewWidth - 36];
    if (caption.length) {
        self.captionView.frame = CGRectMake(0, self.swipeView.frame.origin.y + self.swipeView.frame.size.height - 1, scrollViewWidth, captionLabelHeight + 28);
        UILabel *captionLabel = (UILabel *) [self.captionView viewWithTag:111];
        captionLabel.frame = CGRectMake(18, 8, scrollViewWidth - 36, captionLabelHeight+8);
        [captionLabel setFont:[UIFont systemFontOfSize:14 + addent]];
        [captionLabel setTextColor:[UIColor whiteColor]];
        [captionLabel setNumberOfLines:0];
        captionLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [captionLabel setBackgroundColor:[UIColor clearColor]];
        captionLabel.text = caption;
    } else {
        self.captionView.frame = CGRectMake(0, self.swipeView.frame.origin.y + self.swipeView.frame.size.height - 1, scrollViewWidth, 0);
    }
    
    float fontSize = 17;
    fontSize += addent;

    if (webViewHeight == 0)
        webViewHeight = .5;
    
    if (!self.webView) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(18, self.captionView.frame.origin.y + self.captionView.frame.size.height + 18, scrollViewWidth - 38, webViewHeight)];
        webView.dataDetectorTypes = UIDataDetectorTypeLink;
        webView.delegate = self;
        webView.scrollView.scrollEnabled = NO;
        webView.scrollView.bounces = NO;
        [webView setUserInteractionEnabled:NO];
        [webView setBackgroundColor:[UIColor clearColor]];
        [webView setOpaque:NO];
        self.webView = webView;
        [self.scrollView addSubview:webView];
    }

    [self.webView setUserInteractionEnabled:YES];
    self.webView.frame = CGRectMake(14, self.captionView.frame.origin.y + self.captionView.frame.size.height + 18, scrollViewWidth - 38, webViewHeight);
   
    
    if (!self.pageControl) {
        SMPageControl *pc = [[SMPageControl alloc] initWithFrame:CGRectMake(12, self.swipeView.frame.origin.y + self.swipeView.frame.size.height - 35, scrollViewWidth - 24, 50)];
        pc.numberOfPages = self.article.mediagroupList.count;
        pc.pageIndicatorTintColor = [UIColor whiteColor];
        pc.currentPageIndicatorTintColor = [UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1];
        pc.backgroundColor = [UIColor clearColor];
        pc.alpha = 1;
        pc.hidesForSinglePage = YES;
        pc.indicatorDiameter = 5.0f;
        pc.alignment = SMPageControlAlignmentRight;
        pc.indicatorMargin = 2.0f;
        
        self.pageControl = pc;
        [self.scrollView bringSubviewToFront:pc];
        [self.scrollView addSubview:pc];
    }
    
    /*if(!self.pagerLabel){
        UILabel *pageLabel = [[UILabel alloc] init] ;
        pageLabel.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
        pageLabel.text = [NSString stringWithFormat:@"%ld / %ld",(long)self.pageNumber,(long)self.totalPages];
        pageLabel.tag = 999;
        self.pagerLabel = pageLabel;
        [self.scrollView addSubview:pageLabel];
        
    }*/
    /*if(self.totalPages <= 1){
        [self.pagerLabel setHidden:YES];
    }else {
        [self.pagerLabel setHidden:NO];
    }*/
/*#ifdef TAMIL
 
    [self.pagerLabel setFrame:CGRectMake(scrollViewWidth - 75, self.captionView.frame.origin.y + self.captionView.frame.size.height+webViewHeight - ((webViewHeight > 20.0f) ? 20.0f : webViewHeight) + 18, 100, 40)];
#else
    [self.pagerLabel setFrame:CGRectMake(scrollViewWidth - 75, self.captionView.frame.origin.y + self.captionView.frame.size.height+webViewHeight - ((webViewHeight > 20.0f) ? 20.0f : webViewHeight), 100, 40)];
#endif*/

    self.pageControl.frame = CGRectMake(12, self.swipeView.frame.origin.y + self.swipeView.frame.size.height - 35, scrollViewWidth - 24, 50);
    
    
    [self updateScrollViewContentSize];
    [self.scrollView setNeedsLayout];
}
#pragma mark admob

- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    self.bannerView.autoloadEnabled = YES;
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    self.bannerView.adSize = kGADAdSizeBanner;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;
    [self.bannerView loadRequest:request];
    self.bannerView.delegate = self;
    self.bannerView.backgroundColor = [UIColor clearColor];
    [self.bannerView setHidden:YES];
}

#pragma mark GADbannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view{
    NSLog(@"adViewDidReceiveAd in article detail at Page %i", self.pageNumber);
    if (self.bannerHeight.constant == 0) {
        self.bannerHeight.constant = 50;
    }
    [view setHidden:NO];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
    [view setHidden:YES];
}
/*
- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)gadSize{
    self.bannerHeight.constant = gadSize.size.height;
}*/

#pragma  mark UIPopOverDelegate
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.menuButton setSelected:NO];
    self.isTherePopOver = NO;
    [self.dimView removeFromSuperview];
    
}
#pragma mark TMIPADMenuViewControllerDelegate
-(void)didTapItemAtMenu{
    [self.dimView removeFromSuperview];
    [self.popOverController dismissPopoverAnimated: YES];
    [self.menuButton setSelected:NO];
    self.isTherePopOver = NO;
    
}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark Private
- (void)reloadSwipeView{
    [self.tableView reloadData];
}
- (void)setUpSlider
{
    int value = 0;
    switch ([Data sharedInstance].fontSizeType) {
        case FontSizeTypeXSmall:
            value = 0;
            break ;
        case FontSizeTypeSmall:
            value = 1;
            break ;
        case FontSizeTypeLarge:
            value = 3;
            break ;
        case FontSizeTypeXLarge:
            value = 4;
            break ;
        default:
            value = 2;
    }
    
    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"fontbar_line"];
    self.sliderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    _sliderView.backgroundColor =  [UIColor clearColor];
    
    UIView *viewParent = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    viewParent.tag = 555;
    viewParent.backgroundColor = [UIColor colorWithRed:0 green:123/255.0f blue:208/255.0f alpha:1];
    
    [viewParent addGestureRecognizer:[[UIPanGestureRecognizer alloc] init]];
    
    self.slider = [[UISlider alloc] init];
    self.slider.tag = 111;
    self.slider.maximumValue = 4;
    self.slider.value = value;
    [self.slider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.slider setThumbImage:[UIImage imageNamed: @"thumbButton"] forState:UIControlStateNormal];
    [self.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    
    UILabel *small = [UILabel new];
    small.tag = 222;
    small.textAlignment = NSTextAlignmentLeft;
    [small setText:@"A"];
    [small setFont:[UIFont systemFontOfSize:16]];
    [small setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
    
    UILabel *big = [UILabel new];
    big.textAlignment = NSTextAlignmentRight;
    big.tag = 333;
    [big setText:@"A"];
    [big setFont:[UIFont systemFontOfSize:20]];
    [big setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
    
    
    //[_sliderView addSubview:self.slider];
    //[_sliderView addSubview:small];
    //[_sliderView addSubview:big];
    [viewParent addSubview:self.slider];
    [viewParent addSubview:small];
    [viewParent addSubview:big];
    [_sliderView addSubview:viewParent];
    
    
    
    self.slider.frame = CGRectMake(50, 0, _sliderView.frame.size.width - 100, 40);
    [small setFrame:CGRectMake(25, 0, 25, 40)];
    [big setFrame:CGRectMake(self.slider.frame.origin.x + self.slider.frame.size.width - 5, 0, 25, 40)];
    
    [self.view addSubview:_sliderView];
}

- (void)showSlider
{
    if (self.sliderView) {
        int value = 0;
        switch ([Data sharedInstance].fontSizeType) {
            case FontSizeTypeXSmall:
                value = 0;
                break ;
            case FontSizeTypeSmall:
                value = 1;
                break ;
            case FontSizeTypeLarge:
                value = 3;
                break ;
            case FontSizeTypeXLarge:
                value = 4;
                break ;
            default:
                value = 2;
        }
        
        float width = 0;
        if(self.isPortrait){
            width = 768.0;
        }else{
            width = 1024.0;
        }
        
        self.sliderView.frame = CGRectMake(0, 0, width, [UIScreen mainScreen].bounds.size.height);
        
        UIView *parentView = (UIView *) [self.sliderView viewWithTag:555];
        parentView.frame = CGRectMake(0, 0, width, 40);
        self.slider.frame = CGRectMake(50, 0, width - 100, 40);
        self.slider.value = value;
        UILabel *small = (UILabel *) [self.sliderView viewWithTag:222];
        [small setFrame:CGRectMake(25, 0, 25, 40)];
        UILabel *big = (UILabel *) [self.sliderView viewWithTag:333];
        [big setFrame:CGRectMake(width - 48, 0, 25, 40)];
    } else {
        [self setUpSlider];
    }
    
    UIButton *bookmarkButton = (UIButton *) _fontBarButtonItem.customView;
    bookmarkButton.selected = YES;
    self.isThereSlider = YES;
}

- (void)dismissSlider
{
    if (self.sliderView) {
        self.sliderView.frame = CGRectZero;
        self.slider.frame = CGRectZero;
        UIView *parentView = (UIView *) [self.sliderView viewWithTag:555];
        parentView.frame = CGRectZero;
        UILabel *small = (UILabel *) [self.sliderView viewWithTag:222];
        [small setFrame:CGRectZero];
        UILabel *big = (UILabel *) [self.sliderView viewWithTag:333];
        [big setFrame:CGRectZero];
    }
    
    UIButton *bookmarkButton = (UIButton *) _fontBarButtonItem.customView;
    bookmarkButton.selected = NO;
    self.isThereSlider = NO;
}

- (void)sliderAction: (id)sender
{
    UISlider *slider = (UISlider *)sender;
    float value = slider.value;
    if (value != [Data sharedInstance].fontSizeType) {
        switch (lroundf(value)) {
            case 0: {
                slider.value = 0;
                [Data sharedInstance].fontSizeType = FontSizeTypeXSmall;
                break;
            } case 1: {
                 slider.value = 1;
                [Data sharedInstance].fontSizeType = FontSizeTypeSmall;
                break;
            } case 2: {
                 slider.value = 2;
                [Data sharedInstance].fontSizeType = FontSizeTypeDefault;
                break;
            } case 3: {
                 slider.value = 3;
                [Data sharedInstance].fontSizeType = FontSizeTypeLarge;
                break;
            } case 4: {
                 slider.value = 4;
                [Data sharedInstance].fontSizeType = FontSizeTypeXLarge;
                break;
            }
        }
        [self layoutScrollView];
        [self requestWebView];
        [self dismissSlider];
    }
}

- (BOOL)checkBookmarkWithArticle: (Article*)article
{
    BOOL isBookmark = NO;
    for (Article *articleFromBookmark in _bookmarks) {
        if ([articleFromBookmark.guid isEqualToString:article.guid]) {
            isBookmark = YES;
            break;
        }
    }
    
    return isBookmark;
}

- (void)setUpBarButtonItems
{
    UIButton *bookmarkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bookmarkButton setFrame:CGRectMake(0, 0, 28, 29)];
    [bookmarkButton setImage:[UIImage imageNamed:@"bigStar"] forState:UIControlStateNormal];
    [bookmarkButton setImage:[UIImage imageNamed:@"bigStar_selected"] forState:UIControlStateSelected];
    [bookmarkButton addTarget:self action:@selector(didTapBookmark) forControlEvents:UIControlEventTouchUpInside];
    
    self.menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.menuButton setFrame:CGRectMake(0, 0, 28, 29)];
    [self.menuButton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [self.menuButton addTarget:self action:@selector(didTapMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *fontResizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fontResizeButton setFrame:CGRectMake(0, 0, 28, 29)];
    [fontResizeButton setImage:[UIImage imageNamed:@"fontButton"] forState:UIControlStateNormal];
    [fontResizeButton setImage:[UIImage imageNamed:@"fontButton_selected"] forState:UIControlStateSelected];
    [fontResizeButton addTarget:self action:@selector(displayfontResizeView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *separatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 28)];
    [separatorImage setImage:[UIImage imageNamed:@"separator_icon"]];
    
    UIImageView *transparentSeparatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 28)];
    [transparentSeparatorImage setImage:[UIImage new]];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setFrame:CGRectMake(0, 0, 28, 29)];
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateSelected];
    [shareButton addTarget:self action:@selector(didTapShare) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *menuBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.menuButton];
    UIBarButtonItem *separatorBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:separatorImage];
    UIBarButtonItem *shareBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    UIBarButtonItem *transparentSeparatorBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:transparentSeparatorImage];
    _bookmarkBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bookmarkButton];
    _fontBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:fontResizeButton];
    
    self.navigationItem.rightBarButtonItems = @[menuBarButtonItem,
                                                separatorBarButtonItem,
                                                shareBarButtonItem,
                                                transparentSeparatorBarButtonItem,
                                                _bookmarkBarButtonItem,
                                                transparentSeparatorBarButtonItem,
                                                _fontBarButtonItem];
}

- (void)loadData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.group = dispatch_group_create();
    
    //[self layoutScrollView];
    [self requestOutbrain];
    [self requestTop5news];
    [self requestBookmarks];
    
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
    });
}

- (void)requestOutbrain
{
    //dispatch_group_enter(self.group);
    [self.outbrainActivityIndicator startAnimating];
    OBRequest * request = [OBRequest requestWithURL:self.article.link widgetID:[Constants OUTBRAIN_WIDGET_ID]];
    [Outbrain fetchRecommendationsForRequest:request withCallback:^(OBRecommendationResponse *response) {
        if(response.error){
            NSLog(@"Got error from recommendations request %@", response.error);
        } else {
            self.recommendationResponse = response;
        }
        
        [self.outbrainActivityIndicator stopAnimating];
        [self.tableView reloadData];
        //dispatch_group_leave(self.group);
    }];
}

- (void)requestTop5news
{
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getTopFiveNewsWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            self.top5newsArray = [array mutableCopy];
        }
        dispatch_group_leave(self.group);
    }];
}

- (void)requestBookmarks
{
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
        }
        dispatch_group_leave(self.group);
    }];
}

- (void)requestBookmarksWithoutDispatch
{
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.tableView reloadData];
        }
    }];
}
- (void)updateScrollViewContentSize
{
    self.scrollView.contentSize = CGSizeMake(scrollViewWidth, [self checkScrollViewHeight]);
}

- (float)checkScrollViewHeight
{
   float height = 18 + self.categoryLabel.frame.size.height + 12 + self.tittleLabel.frame.size.height + 12 + self.pubDateLabel.frame.size.height + 18 + self.swipeView.frame.size.height + self.captionView.frame.size.height + 18 + self.webView.frame.size.height;
    return height;
}


- (void)didTapBookmark
{
    if (!canTapBookmark) return;
    
    UIButton *bookmarkButton = (UIButton *) _bookmarkBarButtonItem.customView;
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:self.article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
    }
    
    canTapBookmark = NO;
    
    if (!bookmarkArticle) bookmarkArticle = self.article;
    
    [Util updateBookmarkWith:bookmarkArticle addedArticleSuccess:^(Article *addedBookmarkArticle) {
        [self.view makeToast:[Constants ADD_TO_FAV]];
        canTapBookmark = YES;

        if (self.type == NewsArticle) {
            [[AnalyticManager sharedInstance] trackBookmarkWithGuid:self.article.guid  andArticleTitle:self.article.tittle];
        } else if (self.type == CAArticle) {
            [[AnalyticManager sharedInstance] trackCABookmarkWithProgram:bookmarkArticle.enCategory andEpisodeId:bookmarkArticle.guid andEpisodeTitle:bookmarkArticle.tittle];
        }
    } removedArticleSuccess:^(Article *addedBookmarkArticle) {
        [self.view makeToast:[Constants REMOVE_FROM_FAV]];
        canTapBookmark = YES;
    }];
    
    
    [bookmarkButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]];
    [self.tableView reloadData];
}

- (void)didTapMenu:(UIButton *)sender {
      [[AnalyticManager sharedInstance] trackMenuTab];
    [[LibraryAPI sharedInstance] getCAWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
        self.menListArray = array;
        [[LibraryAPI sharedInstance] getNewsWithCompletionBlock:^(NSMutableArray *array) {
            self.newsList = [array mutableCopy];
        
        if(self.popOverController == nil){
            [self instantiateUIPopoverController];
        }
        
        
        UIButton *button =(UIButton *)sender;
            if(![button isSelected]){
                
                self.dimView = [[UIView alloc]initWithFrame:self.parentViewController.view.bounds];
                [self.dimView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
                [self.parentViewController.view addSubview:self.dimView];
                if (self.isPortrait){
                    int ctr = [self.menListArray count] +[self.newsList count];
                    [self.popOverController setPopoverContentSize:CGSizeMake(320, 275+(40*ctr))];// for ios 7
                    self.popOverController.contentViewController.preferredContentSize = CGSizeMake(320, 275+(40*ctr));// for ios 8
                    [self.popOverController presentPopoverFromRect:CGRectMake(453,7, 320, 560+(40*ctr)) inView:self.view permittedArrowDirections:0 animated:YES];
                }else{
                    int ctr =  (([self.menListArray count] +[self.newsList count])+2-1)/2;
                    [self.popOverController setPopoverContentSize:CGSizeMake(400, 275+(ctr*40))];// for ios 7
                    self.popOverController.contentViewController.preferredContentSize = CGSizeMake(400, 275+(ctr*40));
                    [self.popOverController presentPopoverFromRect:CGRectMake(702,7, 400, 275+(ctr*40)) inView:self.view permittedArrowDirections:0 animated:YES];
                }
                
                [button setSelected:YES];
            }else{

            
            [self.popOverController dismissPopoverAnimated:YES];
            self.popOverController = nil;
            [self.dimView removeFromSuperview];
            
            [button setSelected:NO];
        }
        
        self.isTherePopOver = YES;
              }];
    }];
}

- (void)displayfontResizeView:(UIButton *)sender
{
    if (!sender.selected) {
        [self showSlider];
    } else {
        [self dismissSlider];
    }
}

- (void)didTapShare
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Facebook",
                                  @"Twitter",
                                  @"Email",
                                  nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
   
            switch (buttonIndex) {
                case 0:
                    [self FBShare];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self emailShare];
                    break;
                default:
                    break;
            }
 
}

- (void)FBShare{

    NSString *title = [NSString stringWithFormat:@"%@ %@\n", [Constants SHARE_PRE_TITTLE],self.article.tittle];
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:self.article.link]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultDone){
            [[AnalyticManager sharedInstance] trackFacebookShareNewsWithCategory:self.article.enCategory andArticleTitle:self.article.tittle andGuid:self.article.guid];
        }
    };
    [self presentViewController:controller animated:YES completion:Nil];

}

- (void)TwitterShare {
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *content = @"";
    
        
        NSArray *mediaGroup = [self.article.mediagroupList copy];
        if(mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            content = [dictionary valueForKey:@"content"];
            
        }
    
    NSString *title = [NSString stringWithFormat:@"%@ %@ ",[Constants SHARE_PRE_TITTLE],self.article.tittle];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:self.article.link]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultDone){
            [[AnalyticManager sharedInstance] trackTwitterShareNewsWithCategoryName:self.article.enCategory andGuId:self.article.guid andArticleTitle:self.article.tittle];
        }
    };
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)emailShare {
    if ([MFMailComposeViewController canSendMail]) {
       [UINavigationBar appearance].barTintColor = [Util headerColor]; 
/*
#if TAMIL
        [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
        [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
        
        NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE], [Constants SHARE_TITTLE]];
        NSString *messageBody = [NSString stringWithFormat:@"%@\n\n%@", self.article.tittle ,self.article.link];
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:title];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        [mc.navigationBar setTranslucent:NO];
        [mc.navigationBar setTintColor:[UIColor whiteColor]];
        
        NSString *thumbnail = @"";
        
            NSArray *mediaGroup = [self.article.mediagroupList copy];
            if (mediaGroup.count){
                NSDictionary *dictionary = [mediaGroup firstObject];
                thumbnail = [dictionary valueForKey:@"thumbnail"];
            }
      
        
        NSURL *url = [NSURL URLWithString:thumbnail];
        NSData *data = [NSData dataWithContentsOfURL:url];
        [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
        
        
        
        [self presentViewController:mc animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
}

- (void)instantiateUIPopoverController{
    
    TMIPADMenuViewController *dbvc = (TMIPADMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"menuStoryboardId"];
    dbvc.delegate = self;
    dbvc.ecSlidingVC = self.slidingViewController;
    self.popOverController = [[UIPopoverController alloc] initWithContentViewController:dbvc];
    self.popOverController.delegate = self;
}

- (void) requestWebView
{
    int addent = 0;
    switch ([Data sharedInstance].fontSizeType) {
        case FontSizeTypeXSmall:
            addent = -3;
            break ;
        case FontSizeTypeSmall:
            addent = -2;
            break ;
        case FontSizeTypeLarge:
            addent = 2;
            break ;
        case FontSizeTypeXLarge:
            addent = 4;
            break ;
            
        default:
            addent = 0;
    }
    
    float fontSize = 20;
    fontSize += addent;

    NSString *copyright = @"";
    if (self.article.copyright.length) {

        copyright = [NSString stringWithFormat:@"<span style=\"float: left;font-family:%@; font-size:%fpx;\">%@</span>", @"Helvetica Neue",
                     fontSize, self.article.copyright];
    }
    
    NSString *pagination = @"";
    if (self.totalPages > 1) {
        
        pagination = [NSString stringWithFormat:@"<span style=\"float: right;font-family: %@; font-size:%fpx;\"><font color=\"#ADADAD\">%ld / %ld</font></span>", @"Helvetica Neue", fontSize, (long)self.pageNumber, (long)self.totalPages];
    }
#ifdef TAMIL
    NSString *htmlstring = [NSString stringWithFormat:@"<body><span style=\"font-family: %@; font-size: %f\"><p><p>%@</p></p></span></body>", @"Helvetica Neue", fontSize , self.article.desc];
#else
    NSString *htmlstring = [NSString stringWithFormat:@"<body><span style=\"font-family: %@; font-size: %f\"><p><p>%@</p></p></span></body>", @"Helvetica Neue", fontSize - 2, self.article.desc];
#endif
    
    NSString *finalHTML = [NSString stringWithFormat:@"%@ %@ %@", htmlstring, copyright, pagination];
    
    self.webView.delegate = self;
    [self.webView loadHTMLString:[Util formatHtmlData:finalHTML] baseURL:[NSURL URLWithString:[Constants URL_BASE]]];
}

@end
