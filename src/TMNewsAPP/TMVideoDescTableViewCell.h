//
//  TMVideoDescTableViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/14/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMVideoDescTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *tittle;
+ (float) heightFromText:(NSString *)text;

@end
