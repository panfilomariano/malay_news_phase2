//
//  TMCurrentAffairPagerViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/31/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "ViewPagerController.h"

@protocol TMCurrentAffairPagerViewControllerDelegate <NSObject>
@optional
-(void)loadViewedItems:(NSString *)UrlString atIndex:(NSNumber*)index;
@end

@interface TMCurrentAffairPagerViewController : ViewPagerController
@property (nonatomic, weak) id <TMCurrentAffairPagerViewControllerDelegate> currentAffairViewControllerdelegate;
@property (nonatomic, strong) NSArray *newsCategories;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, strong) NSString *feedLink;
@property (nonatomic, strong) NSString *title;
@property (nonatomic) BOOL isFromMenu;
@property (nonatomic) BOOL isTopNews;
@property (nonatomic, strong) NSString *enTitle;

@end
