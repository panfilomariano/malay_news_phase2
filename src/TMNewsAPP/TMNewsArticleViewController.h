//
//  TMNewsArticleViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

typedef NS_OPTIONS(NSUInteger, articleType)
{
    NewsArticle = 0,
    EpisodeArticle = 1,
    SpecialReportArticle = 2,
    PhotosArticle = 3,
    VideosArticle = 4,
    CAArticle = 5,
    LatestNewsArticle
};

@interface TMNewsArticleViewController : UIViewController
typedef NS_OPTIONS(NSUInteger, ENTitle)
{
    LatestNews = 0,
    LatestSectionNews = 1,
    MostPopular = 2,
    TopNews = 3,
    Singapore = 4,
    World = 5,
    Business = 6,
    SpecialReports = 7,
    CA = 8,
    Sport = 9,
    Lifestyle = 10,
    Entertainment = 11,
    BreakingNews = 12,
    Advertorial = 13,
    LatestEpisodes = 14,
    Videos = 15,
    Photos = 16,
    Search = 17,
};


@property (nonatomic, strong) Article *article;
@property (nonatomic, strong) Article *specialReportArticle;
@property (nonatomic, assign) ENTitle enTitlea;
@property (nonatomic, assign) BOOL isBookmark;
@property (nonatomic, assign) BOOL hasBarItems;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *paginationLabel;
@property (nonatomic) NSInteger pageNumber;
@property (nonatomic) NSInteger totalPages;
@property (nonatomic, strong) NSString *titleForSpecialReport;
//@property (nonatomic, strong) NSString *enTitleForSpecialReport;
@property (nonatomic, assign) articleType type;

- (void)requestWebView;
- (void)layoutScrollView;

@end
