//
//  TMSearchTableViewCell.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/2/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSearchTableViewCell.h"

@implementation TMSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(IBAction)didTapStarButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}


@end
