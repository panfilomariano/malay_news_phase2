//
//  TMSearchiPadCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/6/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSearchiPadCollectionViewCellDelegate <NSObject>
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMSearchiPadCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UILabel *searchTitle;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UIImageView *playButton;
@property (nonatomic, strong) id <TMSearchiPadCollectionViewCellDelegate>delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;



@end
