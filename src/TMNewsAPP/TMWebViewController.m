//
//  TMWebViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/13/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMWebViewController.h"
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "Util.h"
#import "Constants.h"
@interface TMWebViewController () <UIWebViewDelegate>

@property (nonatomic, strong) NSMutableArray *webViewItemArray;
//@property (nonatomic, strong) NSString *customPageSrc;
@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation TMWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.barTintColor = [Util headerColor];
    self.navigationController.navigationBar.translucent = NO;
    
    if (self.forPushNotif) {
        NSLog(@"=========Back button Created==========");
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 15, 23)];
        [button setImage:[UIImage imageNamed:@"right-btn-s"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(didTapBackButton) forControlEvents:UIControlEventTouchUpInside];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        self.title = [Constants PUSH_HEADER];
    }
    
    self.webView.delegate = self;
    [self requestContents];
}

#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    /*NSString *source = [webView stringByEvaluatingJavaScriptFromString:
                        @"document.getElementsByTagName('html')[0].outerHTML"];
    if (!self.customPageSrc.length) {
       
        self.customPageSrc = [source stringByReplacingOccurrencesOfString:@", initial-scale=1.0, user-scalable=no" withString:@", user-scalable=yes"];
        NSURL *url = [NSURL URLWithString:[Constants URL_BASE]];
        [webView loadHTMLString:self.customPageSrc baseURL:url];
    }*/
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
}

#pragma mark Private
- (void) didTapBackButton
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) requestContents
{
    //[self.webView scalesPageToFit];
    self.webView.scalesPageToFit = YES;
    if (self.htmlString.length) {
        NSString *htmlstring = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %f\">%@</span>",
                                @"Helvetica Neue",
                                14.0f,self.htmlString];
        [self.webView loadHTMLString:htmlstring baseURL:nil];
    } else if (self.sectionUrl) {
        
        [[LibraryAPI sharedInstance]getArticlesWithURL:self.sectionUrl isForced:NO withCompletionBlock:^(NSMutableArray *array) {
            if(array) {
                if (array.count) {
                    //NSString *htmlstring = @"";
                    Article *article = (Article *)[array firstObject];
                    NSString *htmlstring = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %f\">%@</span>",
                                            @"Helvetica Neue",
                                            14.0f,article.desc];
                    
                    [self.webView loadHTMLString:htmlstring baseURL:nil];
                }
            }
        }];
    } else {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
        [self.webView loadRequest:request];
    }
}

@end
