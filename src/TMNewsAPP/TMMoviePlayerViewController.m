//
//  TMMoviePlayerViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/14/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMMoviePlayerViewController.h"
#import "Util.h"
#import "RadioManager.h"
#import "Constants.h"

@interface TMMoviePlayerViewController ()

@end

@implementation TMMoviePlayerViewController

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.moviePlayer stop];
    BOOL playing = [[RadioManager sharedInstance] isRadioPlaying];
//    NSLog(@"TMMoviePlayerViewController need resume ? %d", playing);
//    [[[UIAlertView alloc] initWithTitle:@"TMMoviePlayerViewController" message:[NSString stringWithFormat:@"TMMoviePlayerViewController need resume ? %d", playing] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    if(playing){
        [[RadioManager sharedInstance] play];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

@end
