//
//  TMSquareCollectionViewCell.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/29/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSquareCollectionViewCell.h"

@implementation TMSquareCollectionViewCell

-(IBAction)didTapBookmarkButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}

@end
