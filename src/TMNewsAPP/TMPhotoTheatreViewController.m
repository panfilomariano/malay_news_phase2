//
//  TMPhotoTheatreViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 2/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMPhotoTheatreViewController.h"
#import "SwipeView.h"
#import "LibraryAPI.h"
#import "Article.h"
#import "NSString+Helpers.h"
#import "MBProgressHUD.h"

@interface TMPhotoTheatreViewController () <SwipeViewDataSource, SwipeViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *galleryTitle;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UILabel *photoTitle;
@property (nonatomic, weak) IBOutlet UILabel *caption;
@property (nonatomic, weak) IBOutlet UILabel *navTittleLabel;
@property (nonatomic, strong) NSMutableArray *photoList;
@property (nonatomic, assign) NSUInteger *currentPage;
@property (nonatomic) BOOL isPortrait;
- (IBAction)didTapShareButton:(id)sender;
- (IBAction)didTapBackButton:(id)sender;

@end

@implementation TMPhotoTheatreViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _photoList = [[NSMutableArray alloc]init];
    
    return self;

}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.currentPage = 0;
    
    
    if(self.isNewsPhoto){
        self.navTittleLabel.text = self.article.tittle;
        self.galleryTitle.text = @"";
        self.pubDate.text = @"";
    }else{
        self.galleryTitle.text = self.article.tittle;
        self.pubDate.text = [self.article.pubDate formatDate];
        self.navTittleLabel.text = @"";
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LibraryAPI sharedInstance]getMediaItemsFromURL:self.article.sectionURL andFeedLink:self.article.feedlink withCompletionBlock:^(NSMutableArray *array) {
        self.photoList = [array mutableCopy];
        [self.collectionView reloadData];
        [self.swipeView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];

    
}

#pragma mark SwipeView DataSource

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    
    float width = 441;
    float aheight = 248;
    float awidth = 441;
    
    if (![self checkPortrait]) {
        width =  388;
        aheight = 248;
        awidth = 388;
    }

    
    UILabel *photoTitle = nil;
    UILabel *categoryTitle = nil;
    UIImageView *thumnail = nil;
    
    if(view == nil) {
        view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, 510)];
        [view setBackgroundColor:[UIColor colorWithRed:2.0/225.0 green:19.0/225.0 blue:29.0/225.0 alpha:1]];
        thumnail = [[UIImageView alloc] init];
        thumnail.tag = 1;
        
        photoTitle = [[UILabel alloc]init];
       // photoTitle setFont:[UIFont systemFontOfSize:<#(CGFloat)#>]
        
        
    }
    
    
    return view;
}

- (BOOL) checkPortrait
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            return YES;
        }
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            return NO;
        }
        case UIInterfaceOrientationUnknown:break;
    }
    
    return YES;
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
