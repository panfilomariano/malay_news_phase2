//
//  LibraryAPI.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"
#import "AFNetworking.h"

@interface LibraryAPI : NSObject

+ (LibraryAPI *) sharedInstance;
+ (AFHTTPRequestOperationManager *) httpClient;

- (void) requestIndexListItems:(void (^)()) completionBlock;
- (void) getIndexListItems:(void (^)(NSArray *array)) completionBlock;
- (void) getLatestNewsWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getNewsWithCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getCAWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getPhotosWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getVideosWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getTopNewsWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getArticlesWithURL: (NSString *)url isForced: (BOOL) forced withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getSectionArticlesFromURL: (NSString *)url withFeedUrl: (NSString *)feedUrl isForced: (BOOL) forced  withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getArticlesWithURL: (NSString *)url andFeedLink:(NSString *)feedLink isForced: (BOOL) forced withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getMediaItemsFromURL: (NSString *)url andFeedLink: (NSString *)feedLink withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getArticlesWithFeedURL: (NSString *)url isForced: (BOOL) forced withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) requestWeatherWithCompletionBlock:(void (^)(NSError *error, id response))completionBlock;
- (void) getSectionArticlesFromURL: (NSString *)url isForced: (BOOL) forced  withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getBookmarksWithCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) addBookmarkWithArticle: (Article *)article;
- (void) deleteBookmarkWithArticle: (Article *)article;
- (BOOL) isBookmarkedArticle:(Article *)article;
- (void) getWeatherWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) reloadWeatherWithCompletionBlock:(void (^)(NSError *error, id response))completionBlock;
- (void)requestAppVersionWithURLString: (NSString *)url;
- (void) getSectionMediaFromURL: (NSString *)url withFeedUrl:(NSString *)feedUrl isForced: (BOOL) forced  withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void) getTopFiveNewsWithForce: (BOOL) forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
- (void)getAdvertorialWithForce: (BOOL)force andCompletionBlock: (void (^)(NSError *error, NSDictionary *dict)) completionBlock;
@end
