//
//  LibraryAPI.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "LibraryAPI.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "Media.h"
#import "PersitenceManager.h"
#import "DataManager.h"
#import "Reachability.h"
#import "Util.h"
#import "Weather.h"
#import "LibraryAPI.h"

static AFHTTPRequestOperationManager *_afhttpRequestOperationManager;
static Util *utility;



@interface LibraryAPI ()

@property (nonatomic, strong) DataManager *dataManager;
@property (nonatomic, strong) PersitenceManager *persistencyManager;

@end

@implementation LibraryAPI

+ (AFHTTPRequestOperationManager *)httpClient
{
    if (!_afhttpRequestOperationManager) {
        _afhttpRequestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:Constants.URL_BASE]];
        [_afhttpRequestOperationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
        [_afhttpRequestOperationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"UfinityUser" password:@"Acc*Web@2013"];
        //NSURL *url = [NSURL URLWithString:[Util urlTokenForFeed:Constants.URL_BASE]];
        //_afhttpRequestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    }
    
    return _afhttpRequestOperationManager;
}

static LibraryAPI *_instance;

+ (LibraryAPI *) sharedInstance
{
    if (_instance == nil) {
        _instance = [[LibraryAPI alloc] init];
    }
    
    return  _instance;
}

- (id) init {
    
    if (self=[super init]) {
        
        _dataManager = [[DataManager alloc] init];
        _persistencyManager = [[PersitenceManager alloc] init];
    }
    
    return self;
}

- (void) requestIndexListItems:(void (^)()) completionBlock
{
    [[LibraryAPI httpClient] GET:[Util urlTokenForFeed:Constants.INDEX_API] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        _dataManager.indexItemList = [(NSArray *) [responseObject objectForKey:@"items"] copy];
        [_persistencyManager saveObject:_dataManager.indexItemList withKey: Constants.INDEX_ITEM_LIST_KEY];
        completionBlock ();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        _dataManager.indexItemList = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey: Constants.INDEX_ITEM_LIST_KEY];
        completionBlock ();
    }];
}

- (void) getIndexListItems:(void (^)(NSArray *array)) completionBlock
{
    if (_dataManager.indexItemList.count) {
        completionBlock (_dataManager.indexItemList);
    } else {
        if (![self isOnLine]) {
            _dataManager.indexItemList = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey: Constants.INDEX_ITEM_LIST_KEY];
            completionBlock (_dataManager.indexItemList);
        } else {
            [self requestIndexListItems:^{
                completionBlock (_dataManager.indexItemList);
            }];
        }
    }
    
}

- (void) getLatestNewsWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSLog(@"START: %@", [NSDate date]);
    BOOL request = true;
    NSMutableArray *tempContainer = [NSMutableArray new];
    
    if (_dataManager.latestNews.count) {
        NSLog(@"AA: %@", [NSDate date]);
        Article *firstArticle = [_dataManager.latestNews firstObject];
        
        if (![self reDownloadArticle:firstArticle]) {
            if (!forced) {
                tempContainer = [_dataManager.latestNews mutableCopy];
                NSLog(@"AA DONE: %@", [NSDate date]);
                completionBlock (tempContainer);
                return;
            }
        }
        
    } else {
        if (!forced) {
            NSLog(@"BB: %@", [NSDate date]);
            NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.LATEST_NEWS_KEY] mutableCopy];
            
            if (articleDataFromDisk.count) {
                NSData *firstData = [articleDataFromDisk firstObject];
                Article *firstArticle = [NSKeyedUnarchiver unarchiveObjectWithData:firstData];
                
                if (![self reDownloadArticle:firstArticle]) {
                    //use the old data
                    for (NSData *data in articleDataFromDisk) {
                        Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        [tempContainer addObject:article];
                    }
                    NSLog(@"BB DONE: %@", [NSDate date]);
                    _dataManager.latestNews = [tempContainer mutableCopy];
                    completionBlock (tempContainer);
                    return;
                }
                
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            
            if (_dataManager.latestNews.count == 0) {
                //NSLog(@"CC: %@", [NSDate date]);
                NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.LATEST_NEWS_KEY] mutableCopy];
                for (NSData *data in articleDataFromDisk) {
                    Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [tempContainer addObject:article];
                }
                //NSLog(@"CC: DONE %@", [NSDate date]);
                _dataManager.latestNews = [tempContainer mutableCopy];
                completionBlock (tempContainer);
                return;
            } else {
                completionBlock (_dataManager.latestNews);
                return;
            }
        } else {
            //NSLog(@"DD: %@", [NSDate date]);
            [self requestIndexListItems:^() {
                
                for (NSDictionary *feedItemDict in _dataManager.indexItemList) {
                    if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:Constants.LATEST_NEWS]) {
                        NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                        
                        [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:sectionURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            NSArray *array = [responseObject objectForKey:@"items"];
                            
                            NSMutableArray *articleItems = [NSMutableArray new];
                            NSDate *date = [NSDate date];
                            double downloadDateInDouble = [date timeIntervalSince1970];
                            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
                            for (NSDictionary *itemDict  in array) {
                                
                                Article *article = [Article new];
                                
                                article.category = [itemDict valueForKey:@"category"];
                                article.enCategory = [itemDict valueForKey:@"enCategory"];
                                article.byLine = [itemDict valueForKey:@"byline"];
                                article.type = [itemDict valueForKey:@"type"];
                                article.sectionURL = [itemDict valueForKey:@"sectionURL"];
                                article.mediagroupList = [[itemDict objectForKey:@"mediaGroup"] copy];
                                article.tittle = [itemDict valueForKey:@"title"];
                                article.brief = [itemDict valueForKey:@"brief"];
                                article.link = [itemDict valueForKey:@"link"];
                                article.desc = [itemDict valueForKey:@"description"];
                                article.guid = [itemDict valueForKey:@"guid"];
                                article.lastUpdateDate = [itemDict valueForKey:@"lastUpdateDate"];
                                article.pubDate = [itemDict valueForKey:@"pubDate"];
                                article.sectionType = [itemDict valueForKey:@"sectionType"];
                                article.sections = [[itemDict objectForKey:@"sections"] copy];
                                article.related = [[itemDict objectForKey:@"related"] copy];
                                article.feedlink = sectionURL;
                                article.totalNum = [[itemDict valueForKey:@"totalNum"] stringValue];
                                article.downloadTime = downloadDateInNumber;
                                article.copyright = [itemDict valueForKey:@"copyright"];
                                
                                [articleItems addObject:article];
                            }
                            
                            _dataManager.latestNews = [articleItems mutableCopy];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.LATEST_NEWS_KEY];
                            [_persistencyManager saveArticles:articleItems withKey:Constants.LATEST_NEWS_KEY];
                            //NSLog(@"DD: DONE %@", [NSDate date]);
                            completionBlock (_dataManager.latestNews);
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            completionBlock (_dataManager.latestNews);
                            NSLog(@"getTopNews Error: %@", error);
                        }];
                        
                        break;
                    }
                }
            }];
        }
    }
    
}

- (void)getAdvertorialWithForce: (BOOL)force andCompletionBlock: (void (^)(NSError *error, NSDictionary *dict)) completionBlock {
    
    [self requestIndexListItems:^{
         NSString *sectionUrl = @"";
        for (NSDictionary *dictionary in _dataManager.indexItemList) {
            if ([dictionary[@"enTitle"] isEqualToString:Constants.ADVERTORIAL]) {
                sectionUrl = dictionary[@"sectionURL"];
                break;
            }
        }
        
        [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:sectionUrl] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *dictionary = nil;
            if (responseObject) {
                NSDictionary *tempDict = responseObject[@"items"];
               
                if (tempDict) {
                    NSString *guId = tempDict[@"guid"];
                    if (guId.length > 0) {
                        dictionary = responseObject[@"items"];
                    }
                }
            }
            completionBlock (nil,dictionary);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            completionBlock (error, nil);
        }];
    }];

}


- (void) getNewsWithCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    if (_dataManager.indexItemList.count == 0) {
        [self requestIndexListItems:^() {
            
            [_dataManager.feedNewsItems removeAllObjects];
            for (NSDictionary *feedItemDict in _dataManager.indexItemList) {
                
                if (([[feedItemDict valueForKey:@"type"] isEqualToString:Constants.NEWS_TYPE] && ![[feedItemDict valueForKey:@"enTitle"] isEqualToString:Constants.LATEST_NEWS]) || [[feedItemDict valueForKey:@"type"] isEqualToString:Constants.SPECIAL_REPORT]) {
                    [_dataManager.feedNewsItems addObject:feedItemDict];
                }
                
            }
            completionBlock (_dataManager.feedNewsItems);
        }];
    } else {
        if (_dataManager.feedNewsItems.count == 0) {
            for (NSDictionary *feedItemDict in _dataManager.indexItemList) {
                
                if (([[feedItemDict valueForKey:@"type"] isEqualToString:Constants.NEWS_TYPE] && ![[feedItemDict valueForKey:@"enTitle"] isEqualToString:Constants.LATEST_NEWS]) || [[feedItemDict valueForKey:@"type"] isEqualToString:Constants.SPECIAL_REPORT]) {
                    [_dataManager.feedNewsItems addObject:feedItemDict];
                }
            }
            completionBlock (_dataManager.feedNewsItems);
        } else {
            completionBlock (_dataManager.feedNewsItems);
        }
    }
}

- (void) getCAWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    BOOL request = true;
    NSMutableArray *tempContainer = [NSMutableArray new];
    
    if (_dataManager.caNewsItems.count) {
        Article *firstArticle = [_dataManager.caNewsItems firstObject];
        if (![self reDownloadArticle:firstArticle]) {
            if (!forced) {
                tempContainer = [_dataManager.caNewsItems mutableCopy];
                completionBlock (tempContainer);
                return;
            }
        }
        
    } else {
        if (!forced) {
            NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.CA_KEY] mutableCopy];
            
            if (articleDataFromDisk.count) {
                NSData *firstData = [articleDataFromDisk firstObject];
                Article *firstArticle = [NSKeyedUnarchiver unarchiveObjectWithData:firstData];
                
                if (![self reDownloadArticle:firstArticle]) {
                    //use the old data
                    for (NSData *data in articleDataFromDisk) {
                        Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        [tempContainer addObject:article];
                    }
                    //NSLog(@"BB DONE: %@", [NSDate date]);
                    _dataManager.caNewsItems = [tempContainer mutableCopy];
                    completionBlock (tempContainer);
                    return;
                }
                
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            
            if (_dataManager.caNewsItems.count == 0) {
                //NSLog(@"CC: %@", [NSDate date]);
                NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.CA_KEY] mutableCopy];
                for (NSData *data in articleDataFromDisk) {
                    Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [tempContainer addObject:article];
                }
                //NSLog(@"CC: DONE %@", [NSDate date]);
                _dataManager.caNewsItems = [tempContainer mutableCopy];
                completionBlock (tempContainer);
                return;
            } else {
                completionBlock (_dataManager.caNewsItems);
                return;
            }
        } else {
            //NSLog(@"DD: %@", [NSDate date]);
            [self requestIndexListItems:^() {
                
                for (NSDictionary *feedItemDict in _dataManager.indexItemList) {
                    if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:Constants.CA]) {
                        NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                        
                        [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:sectionURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            NSArray *array = [responseObject objectForKey:@"items"];
                            
                            NSMutableArray *articleItems = [NSMutableArray new];
                            NSDate *date = [NSDate date];
                            double downloadDateInDouble = [date timeIntervalSince1970];
                            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
                            for (NSDictionary *itemDict  in array) {
                                
                                Article *article = [Article new];
                                
                                article.category = [itemDict valueForKey:@"category"];
                                article.enCategory = [itemDict valueForKey:@"enCategory"];
                                article.byLine = [itemDict valueForKey:@"byline"];
                                article.type = [itemDict valueForKey:@"type"];
                                article.sectionURL = [itemDict valueForKey:@"sectionURL"];
                                article.mediagroupList = [[itemDict objectForKey:@"mediaGroup"] copy];
                                article.tittle = [itemDict valueForKey:@"title"];
                                article.brief = [itemDict valueForKey:@"brief"];
                                article.link = [itemDict valueForKey:@"link"];
                                article.desc = [itemDict valueForKey:@"description"];
                                article.guid = [itemDict valueForKey:@"guid"];
                                article.lastUpdateDate = [itemDict valueForKey:@"lastUpdateDate"];
                                article.pubDate = [itemDict valueForKey:@"pubDate"];
                                article.sectionType = [itemDict valueForKey:@"sectionType"];
                                article.sections = [[itemDict objectForKey:@"sections"] copy];
                                article.related = [[itemDict objectForKey:@"related"] copy];
                                article.thumbnail = [itemDict valueForKey:@"thumbnail"];
                                article.feedlink = sectionURL;
                                article.totalNum = [[itemDict valueForKey:@"totalNum"] stringValue];
                                article.downloadTime = downloadDateInNumber;
                                article.thumbnail = [itemDict valueForKey:@"thumbnail"];
                                article.copyright = [itemDict valueForKey:@"copyright"];
                                
                                [articleItems addObject:article];
                            }
                            
                            _dataManager.caNewsItems = [articleItems mutableCopy];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.CA_KEY];
                            [_persistencyManager saveArticles:articleItems withKey:Constants.CA_KEY];
                            //NSLog(@"DD: DONE %@", [NSDate date]);
                            completionBlock (_dataManager.caNewsItems);
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            NSLog(@"getCA Error: %@", error);
                        }];
                        
                        break;
                    }
                }
            }];
        }
    }
}

- (void) getPhotosWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    BOOL request = true;
    
    if (_dataManager.photosItems.count) {
        //NSLog(@"AA: %@", [NSDate date]);
        
        Article *firstArticle = [_dataManager.photosItems firstObject];
        
        if (![self reDownloadArticle:firstArticle]) {
            if (!forced) {
                //NSLog(@"AA DONE: %@", [NSDate date]);
                completionBlock (_dataManager.photosItems);
                return;
            }
        }
        
    } else {
        if (!forced) {
            //NSLog(@"A: %@", [NSDate date]);
            NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.PHOTOS_KEY] mutableCopy];
            
            if (articleDataFromDisk.count) {
                [_dataManager.photosItems removeAllObjects];
                for (NSData *data in articleDataFromDisk) {
                    Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [_dataManager.photosItems addObject:article];
                }
                
                Article *firstArticle = [_dataManager.photosItems firstObject];
                if (![self reDownloadArticle:firstArticle] || ![self isOnLine]) {
                    if (!forced) {
                        completionBlock (_dataManager.photosItems);
                        return;
                    }
                }
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            
            if (_dataManager.photosItems.count == 0) {
                //NSLog(@"C: %@", [NSDate date]);
                NSArray *articleDataFromDisk = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.PHOTOS_KEY];
                for (NSData *data in articleDataFromDisk) {
                    Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [_dataManager.photosItems addObject:article];
                }
                
                completionBlock (_dataManager.photosItems);
                //NSLog(@"D: %@", [NSDate date]);
                return;
            } else {
                completionBlock (_dataManager.photosItems);
                return;
            }
        } else {
            [self getIndexListItems:^(NSArray *array) {
                for (NSDictionary *feedItemDict in array) {
                    if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:Constants.PHOTOS]) {
                        NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                        
                        [self requestSectionArticlesFromURL:[Util urlTokenForFeed:sectionURL] withCompletionBlock:^(NSMutableArray *array) {
                            _dataManager.photosItems = [array mutableCopy];
                            
                            [_persistencyManager deleteArticlesWithFeedURL:sectionURL];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.PHOTOS_KEY];
                            [_persistencyManager saveArticles:array withKey:Constants.PHOTOS_KEY];
                            
                            completionBlock (array);
                        }];
                        
                        break;
                    }
                }
                
            }];
        }
    }
    
}

- (void) getVideosWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    //NSLog(@"START: %@", [NSDate date]);
    BOOL request = true;
    NSMutableArray *tempContainer = [NSMutableArray new];
    
    if (_dataManager.videosItems.count) {
        
        Media *firstMedia = [_dataManager.videosItems firstObject];
        
        if (![self reDownloadMedia:firstMedia]) {
            if (!forced) {
                tempContainer = [_dataManager.videosItems mutableCopy];
                completionBlock (tempContainer);
                return;
            }
        }
        
    } else {
        if (!forced) {
            //NSLog(@"Date: AA %@", [NSDate date]);
            NSArray *videoDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.VIDEOS_KEY] mutableCopy];
            
            if (videoDataFromDisk.count) {
                NSData *firstData = [videoDataFromDisk firstObject];
                Media *firstMedia = [NSKeyedUnarchiver unarchiveObjectWithData:firstData];
                
                if (![self reDownloadMedia:firstMedia]) {
                    //use the old data
                    for (NSData *data in videoDataFromDisk) {
                        Media *media = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        [tempContainer addObject:media];
                    }
                    //NSLog(@"Date: AA DONE %@", [NSDate date]);
                    _dataManager.videosItems = [tempContainer mutableCopy];
                    completionBlock (tempContainer);
                    return;
                }
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            if (_dataManager.videosItems.count == 0) {
                //NSLog(@"Date: BB %@", [NSDate date]);
                NSArray *videoDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.VIDEOS_KEY] mutableCopy];
                for (NSData *data in videoDataFromDisk) {
                    Media *media = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [tempContainer addObject:media];
                }
                
                //NSLog(@"Date: BB DONE %@", [NSDate date]);
                _dataManager.videosItems = [tempContainer mutableCopy];
                completionBlock (tempContainer);
                return;
            } else {
                completionBlock (_dataManager.videosItems);
                return;
            }
        } else {
            //NSLog(@"Date: CC %@", [NSDate date]);
            [self requestIndexListItems:^() {
                
                //NSLog(@"Date: CC DONE %@", [NSDate date]);
                for (NSDictionary *feedItemDict in _dataManager.indexItemList) {
                    if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:Constants.VIDEOS]) {
                        NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                        
                        [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:sectionURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            //NSLog(@"Date: DD %@", [NSDate date]);
                            NSArray *array = [[responseObject objectForKey:@"items"] copy];
                            NSMutableArray *mediaItems = [NSMutableArray new];
                            NSDate *date = [NSDate date];
                            double downloadDateInDouble = [date timeIntervalSince1970];
                            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
                            for (NSDictionary *itemDict  in array) {
                                Media *media = [Media new];
                                media.mediaGroup = [itemDict objectForKey:@"mediaGroup"];
                                media.tittle = [itemDict objectForKey:@"title"];
                                media.guid = [itemDict objectForKey:@"guid"];
                                media.pubDate = [itemDict objectForKey:@"pubDate"];
                                media.feedlink = sectionURL;
                                media.downloadTime = downloadDateInNumber;
                                
                                [mediaItems addObject:media];
                            }
                            
                            //NSLog(@"Date: DD DONE %@", [NSDate date]);
                            _dataManager.videosItems = [mediaItems mutableCopy];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.VIDEOS_KEY];
                            [_persistencyManager saveMediaList:mediaItems withKey:Constants.VIDEOS_KEY];
                            //[_persistencyManager saveMediaList:mediaItems withKey:Constants.VIDEOS_KEY];
                            
                            completionBlock (_dataManager.videosItems);
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            NSLog(@"getVideos Error: %@", error);
                            completionBlock (_dataManager.videosItems);
                        }];
                        
                        
                        break;
                    }
                }
            }];
        }
    }
}

- (void) getTopNewsWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    BOOL request = true;
    NSMutableArray *tempContainer = [NSMutableArray new];
    
    if (_dataManager.topNews.count) {
        //NSLog(@"AA: %@", [NSDate date]);
        Article *firstArticle = [_dataManager.topNews firstObject];
        
        if (![self reDownloadArticle:firstArticle]) {
            if (!forced) {
                tempContainer = [_dataManager.topNews mutableCopy];
                completionBlock (tempContainer);
                return;
            }
        }
        
        //NSLog(@"BB: %@", [NSDate date]);
    } else {
        if (!forced) {
            //NSLog(@"A: %@", [NSDate date]);
            NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.TOP_NEWS_KEY] mutableCopy];
            
            if (articleDataFromDisk.count) {
                NSData *firstData = [articleDataFromDisk firstObject];
                Article *firstArticle = [NSKeyedUnarchiver unarchiveObjectWithData:firstData];
                
                if (![self reDownloadArticle:firstArticle]) {
                    //use the old data
                    for (NSData *data in articleDataFromDisk) {
                        Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        [tempContainer addObject:article];
                    }
                    
                    _dataManager.topNews = [tempContainer mutableCopy];
                    //NSLog(@"B: %@", [NSDate date]);
                    completionBlock (tempContainer);
                    return;
                    
                }
                
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            if (_dataManager.topNews.count == 0) {
                //NSLog(@"C: %@", [NSDate date]);
                NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.TOP_NEWS_KEY] mutableCopy];
                for (NSData *data in articleDataFromDisk) {
                    Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [tempContainer addObject:article];
                }
                
                _dataManager.topNews = [tempContainer mutableCopy];
                completionBlock (tempContainer);
                //NSLog(@"D: %@", [NSDate date]);
                return;
            } else {
                completionBlock (_dataManager.topNews);
                return;
            }
        } else {
            [self requestIndexListItems:^() {
                //NSLog(@"E: %@", [NSDate date]);
                for (NSDictionary *feedItemDict in _dataManager.indexItemList) {
                    if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:Constants.TOP_NEWS]) {
                        NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                        
                        [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:sectionURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            NSArray *array = [[responseObject objectForKey:@"sections"] copy];
                            NSMutableArray *articleItems = [NSMutableArray new];
                            
                            for (NSDictionary *dictionary in array) {
                                NSString *sectionName = (NSString *) [dictionary objectForKey:@"name"];
                                NSArray *items =  [[dictionary objectForKey:@"items"] copy];
                                NSDate *date = [NSDate date];
                                double downloadDateInDouble = [date timeIntervalSince1970];
                                NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
                                for (NSObject *obj in items) {
                                    if ([obj isKindOfClass:[NSDictionary class]]) {
                                        NSDictionary *itemDict = (NSDictionary *) obj;
                                        Article *article = [Article new];
                                        article.category = [itemDict valueForKey:@"category"];
                                        article.enCategory = [itemDict valueForKey:@"enCategory"];
                                        article.byLine = [itemDict valueForKey:@"byline"];
                                        article.type = [itemDict valueForKey:@"type"];
                                        article.sectionURL = [itemDict valueForKey:@"sectionURL"];
                                        article.mediagroupList = [[itemDict objectForKey:@"mediaGroup"] copy];
                                        article.tittle = [itemDict valueForKey:@"title"];
                                        article.brief = [itemDict valueForKey:@"brief"];
                                        article.link = [itemDict valueForKey:@"link"];
                                        article.desc = [itemDict valueForKey:@"description"];
                                        article.guid = [itemDict valueForKey:@"guid"];
                                        article.lastUpdateDate = [itemDict valueForKey:@"lastUpdateDate"];
                                        article.pubDate = [itemDict valueForKey:@"pubDate"];
                                        article.sectionType = [itemDict valueForKey:@"sectionType"];
                                        article.sections = [[itemDict objectForKey:@"sections"] copy];
                                        article.related = [[itemDict objectForKey:@"related"] copy];
                                        article.feedlink = sectionURL;
                                        article.sectionName = sectionName;
                                        article.totalNum = [[itemDict valueForKey:@"totalNum"] stringValue];
                                        article.downloadTime = downloadDateInNumber;
                                        article.copyright = [itemDict valueForKey:@"copyright"];
                                        [articleItems addObject:article];
                                    } else {
                                        NSLog(@"Obj %@", [obj class]);
                                    }
                                    
                                }
                            }
                            _dataManager.topNews = [articleItems mutableCopy];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.TOP_NEWS_KEY];
                            [_persistencyManager saveArticles:articleItems withKey:Constants.TOP_NEWS_KEY];
                            //NSLog(@"F: %@", [NSDate date]);
                            completionBlock (_dataManager.topNews);
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            NSLog(@"getTopNews Error: %@", error);
                            completionBlock (_dataManager.topNews);
                        }];
                        
                        break;
                    }
                }
            }];
        }
    }
    
}


- (void) getArticlesWithURL: (NSString *)url isForced: (BOOL) forced withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    //NSLog(@"START: %@==%@",url, [NSDate date]);
    BOOL request = true;
    NSMutableArray *articleList = [NSMutableArray new];
    if (_dataManager.articles.count) {
        
        for (Article *article in _dataManager.articles) {
            if ([article.feedlink isEqualToString:url] && article.sectionURL) {
                [articleList addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
        
    } else {
        NSArray *articleListFromDisk = [_persistencyManager getArticles];
        for (Article *article in articleListFromDisk) {
            if ([article.feedlink isEqualToString:url] && article.sectionURL) {
                [articleList addObject:article];
                if (!request)
                    [_dataManager.articles addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
    }
    
    if (request) {
        NSMutableArray *articleListForRequest = [NSMutableArray new];
        if (![self isOnLine]) {
            NSArray *articleListFromDisk = [_persistencyManager getArticles];
            for (Article *article in articleListFromDisk) {
                if ([article.feedlink isEqualToString:url] && article.sectionURL) {
                    [articleListForRequest addObject:article];
                    [_dataManager.articles addObject:article];
                }
            }
            
            //if (articleList.count) {
                completionBlock (articleListForRequest);
                return;
            //}
        } else {
            //NSLog(@"Request URL: %@", url);
            [self requestArticlesFromURL:url withCompletionBlock:^(NSMutableArray *array) {
                if (array) {
                    [_persistencyManager deleteArticlesWithFeedURL:url];
                    for (Article *article in array) {
                        [_dataManager.articles addObject:article];
                    }
                    [_persistencyManager saveArticles:array];
                }
                completionBlock (array);
            }];
            
        }
    }
}

- (void) getTopFiveNewsWithForce: (BOOL) forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    BOOL request = true;
    NSMutableArray *tempContainer = [NSMutableArray new];
    
    if (_dataManager.topFiveNews.count) {
        //NSLog(@"AA: %@", [NSDate date]);
        Article *firstArticle = [_dataManager.topFiveNews firstObject];
        
        if (![self reDownloadTopFiveNews:firstArticle]) {
            if (!forced) {
                tempContainer = [_dataManager.topFiveNews mutableCopy];
                completionBlock (tempContainer);
                return;
            }
        }
        
        //NSLog(@"BB: %@", [NSDate date]);
    } else {
        if (!forced) {
            //NSLog(@"A: %@", [NSDate date]);
            NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:[Constants TOP_FIVE_NEWS_KEY]] mutableCopy];
            
            if (articleDataFromDisk.count) {
                NSData *firstData = [articleDataFromDisk firstObject];
                Article *firstArticle = [NSKeyedUnarchiver unarchiveObjectWithData:firstData];
                
                if (![self reDownloadTopFiveNews:firstArticle]) {
                    //use the old data
                    for (NSData *data in articleDataFromDisk) {
                        Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        [tempContainer addObject:article];
                    }
                    
                    _dataManager.topFiveNews = [tempContainer mutableCopy];
                    //NSLog(@"B: %@", [NSDate date]);
                    completionBlock (tempContainer);
                    return;
                    
                }
                
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            if (_dataManager.topFiveNews.count == 0) {
                //NSLog(@"C: %@", [NSDate date]);
                NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:[Constants TOP_FIVE_NEWS_KEY]] mutableCopy];
                for (NSData *data in articleDataFromDisk) {
                    Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [tempContainer addObject:article];
                }
                
                _dataManager.topFiveNews = [tempContainer mutableCopy];
                completionBlock (tempContainer);
                //NSLog(@"D: %@", [NSDate date]);
                return;
            } else {
                completionBlock (_dataManager.topFiveNews);
                return;
            }
        } else {
            [self requestIndexListItems:^() {
                //NSLog(@"E: %@", [NSDate date]);
                for (NSDictionary *feedItemDict in _dataManager.indexItemList) {
                    if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:[Constants MOST_POPULAR]]) {
                        NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                        
                        [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:sectionURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            NSArray *array = [responseObject objectForKey:@"items"];
                            
                            NSMutableArray *articleItems = [NSMutableArray new];
                            NSDate *date = [NSDate date];
                            double downloadDateInDouble = [date timeIntervalSince1970];
                            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
                            for (NSDictionary *itemDict  in array) {
                                
                                Article *article = [Article new];
                                
                                article.category = [itemDict valueForKey:@"category"];
                                article.enCategory = [itemDict valueForKey:@"enCategory"];
                                article.byLine = [itemDict valueForKey:@"byline"];
                                article.type = [itemDict valueForKey:@"type"];
                                article.sectionURL = [itemDict valueForKey:@"sectionURL"];
                                article.mediagroupList = [[itemDict objectForKey:@"mediaGroup"] copy];
                                article.tittle = [itemDict valueForKey:@"title"];
                                article.brief = [itemDict valueForKey:@"brief"];
                                article.link = [itemDict valueForKey:@"link"];
                                article.desc = [itemDict valueForKey:@"description"];
                                article.guid = [itemDict valueForKey:@"guid"];
                                article.lastUpdateDate = [itemDict valueForKey:@"lastUpdateDate"];
                                article.pubDate = [itemDict valueForKey:@"pubDate"];
                                article.sectionType = [itemDict valueForKey:@"sectionType"];
                                article.sections = [[itemDict objectForKey:@"sections"] copy];
                                article.related = [[itemDict objectForKey:@"related"] copy];
                                article.thumbnail = [itemDict valueForKey:@"thumbnail"];
                                article.feedlink = sectionURL;
                                article.totalNum = [[itemDict valueForKey:@"totalNum"] stringValue];
                                article.downloadTime = downloadDateInNumber;
                                article.thumbnail = [itemDict valueForKey:@"thumbnail"];
                                article.copyright = [itemDict valueForKey:@"copyright"];
                                
                                [articleItems addObject:article];
                            }
                            
                            _dataManager.topFiveNews = [articleItems mutableCopy];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[Constants TOP_FIVE_NEWS_KEY]];
                            [_persistencyManager saveArticles:articleItems withKey:[Constants TOP_FIVE_NEWS_KEY]];
                            //NSLog(@"DD: DONE %@", [NSDate date]);
                            completionBlock (_dataManager.topFiveNews);
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            NSLog(@"getCA Error: %@", error);
                        }];
                        
                        break;
                    }
                }
            }];
        }
    }
}

- (void) getArticlesWithFeedURL: (NSString *)url isForced: (BOOL) forced withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    //NSLog(@"START: %@==%@",url, [NSDate date]);
    BOOL request = true;
    NSMutableArray *mediaList = [NSMutableArray new];
    if (_dataManager.articles.count) {
        
        for (Article *media in _dataManager.articles) {
            if ([media.feedlink isEqualToString:url]) {
                [mediaList addObject:media];
            }
        }
        
        if (mediaList.count) {
            Article *firstMedia = [mediaList firstObject];
            if (![self reDownloadArticle:firstMedia]) {
                completionBlock (mediaList);
                return;
            }
        }
        
    } else {
        NSArray *mediaListFromDisk = [_persistencyManager getArticles];
        for (Article *media in mediaListFromDisk) {
            if ([media.feedlink isEqualToString:url]) {
                [mediaList addObject:media];
                [_dataManager.articles addObject:media];
            }
        }
        
        if (mediaList.count) {
            Article *firstMedia = [mediaList firstObject];
            if (![self reDownloadArticle:firstMedia]) {
                completionBlock (mediaList);
                return;
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            NSArray *mediaListFromDisk = [_persistencyManager getArticles];
            for (Article *media in mediaListFromDisk) {
                if ([media.feedlink isEqualToString:url]) {
                    [mediaList addObject:media];
                    [_dataManager.articles addObject:media];
                }
            }
            
            if (mediaList.count) {
                completionBlock (mediaList);
                return;
            }
        } else {
            //NSLog(@"Request URL: %@", url);
            [self requestArticlesFromFeedURL:url withCompletionBlock:^(NSMutableArray *array) {
                if (array) {
                    
                    [_persistencyManager deleteArticlesWithFeedURL:url];
                    for (Article *media in array) {
                        [_dataManager.articles addObject:media];
                    }
                    [_persistencyManager saveArticles:array];
                }
                completionBlock (array);
            }];
            
        }
    }
}

- (void) getArticlesWithURL: (NSString *)url andFeedLink:(NSString *)feedLink isForced: (BOOL) forced withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    //NSLog(@"START: %@==%@",url, [NSDate date]);
    BOOL request = true;
    //[_dataManager.articles removeAllObjects];
    NSMutableArray *articleList = [NSMutableArray new];
    if (_dataManager.articles.count) {
        
        for (Article *article in _dataManager.articles) {
            if ([article.parentUrl isEqualToString:url] && [article.feedlink isEqualToString:feedLink]) {
                [articleList addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
        
    } else {
        NSArray *articleListFromDisk = [_persistencyManager getArticles];
        for (Article *article in articleListFromDisk) {
            if ([article.parentUrl isEqualToString:url]  && [article.feedlink isEqualToString:feedLink]) {
                [articleList addObject:article];
                [_dataManager.articles addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            NSArray *articleListFromDisk = [_persistencyManager getArticles];
            for (Article *article in articleListFromDisk) {
                if ([article.parentUrl isEqualToString:url]  && [article.feedlink isEqualToString:feedLink]) {
                    [articleList addObject:article];
                    [_dataManager.articles addObject:article];
                }
            }
            
            //if (articleList.count) {
                completionBlock (articleList);
                return;
            //}
        } else {
            [self requestArticlesFromURL:url andFeedLink:feedLink withCompletionBlock:^(NSMutableArray *array) {
                if (array) {
                    [_persistencyManager deleteArticlesWithFeedURL:feedLink];
                    for (Article *article in array) {
                        [_dataManager.articles addObject:article];
                    }
                    [_persistencyManager saveArticles:array];
                }
                completionBlock (array);
            }];
            
            
        }
    }
    
    
}

- (void) getMediaItemsFromURL: (NSString *)url andFeedLink: (NSString *)feedLink withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    
    //NSLog(@"START: %@==%@",url, [NSDate date]);
    BOOL request = true;
    NSMutableArray *mediaList = [NSMutableArray new];
    if (_dataManager.mediaItems.count) {
        
        for (Media *media in _dataManager.mediaItems) {
            if ([media.sectionUrl isEqualToString:url]) {
                [mediaList addObject:media];
            }
        }
        
        if (mediaList.count) {
            Media *firstMedia = [mediaList firstObject];
            if (![self reDownloadMedia:firstMedia]) {
                completionBlock (mediaList);
                return;
            }
        }
        
    } else {
        NSArray *mediaListFromDisk = [_persistencyManager getMediaList];
        for (Media *media in mediaListFromDisk) {
            if ([media.sectionUrl isEqualToString:url]) {
                [mediaList addObject:media];
                [_dataManager.mediaItems addObject:media];
            }
        }
        
        if (mediaList.count) {
            Media *firstMedia = [mediaList firstObject];
            if (![self reDownloadMedia:firstMedia]) {
                completionBlock (mediaList);
                return;
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            NSArray *mediaListFromDisk = [_persistencyManager getMediaList];
            for (Media *media in mediaListFromDisk) {
                if ([media.sectionUrl isEqualToString:url]) {
                    [mediaList addObject:media];
                    [_dataManager.mediaItems addObject:media];
                }
            }
            
            //if (mediaList.count) {
                completionBlock (mediaList);
                return;
            //}
        } else {
            [self requestMediaFromURL:url andFeedLink:feedLink withCompletionBlock:^(NSMutableArray *array) {
                if (array) {
                    [_persistencyManager deleteMediaListWithSectionUrl:url];
                    for (Media *media in array) {
                        [_dataManager.mediaItems addObject:media];
                    }
                    [_persistencyManager saveMediaList:array];
                }
                completionBlock (array);
            }];
        }
    }
}
- (void) getSectionArticlesFromURL: (NSString *)url isForced: (BOOL) forced  withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    //NSLog(@"START: %@==%@",url, [NSDate date]);
    BOOL request = true;
    NSMutableArray *articleList = [NSMutableArray new];
    if (_dataManager.articles.count) {
        
        for (Article *article in _dataManager.articles) {
            if ([article.sectionURL isEqualToString:url] ) {
                [articleList addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
        
    } else {
        NSArray *articleListFromDisk = [_persistencyManager getArticles];
        for (Article *article in articleListFromDisk) {
            if ([article.sectionURL isEqualToString:url]) {
                [articleList addObject:article];
                if (!request)
                    [_dataManager.articles addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
    }
    
    if (request) {
        NSMutableArray *articleListForRequest = [NSMutableArray new];
        if (![self isOnLine]) {
            NSArray *articleListFromDisk = [_persistencyManager getArticles];
            for (Article *article in articleListFromDisk) {
                if ([article.sectionURL isEqualToString:url]) {
                    [articleListForRequest addObject:article];
                    [_dataManager.articles addObject:article];
                }
            }
            
            //if (articleList.count) {
                completionBlock (articleListForRequest);
                return;
            //}
        } else {
            [self requestlatestNewsSectionsFromURL:url withCompletionBlock:^(NSMutableArray *array) {
                if (array) {
                    [_persistencyManager deleteArticlesWithFeedURL:url];
                    for (Article *article in array) {
                        [_dataManager.articles addObject:article];
                    }
                    [_persistencyManager saveArticles:array];
                }
                completionBlock (array);
            }];
            
        }
    }
    
    
}

- (void) getSectionArticlesFromURL: (NSString *)url withFeedUrl:(NSString *)feedUrl isForced: (BOOL) forced  withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    BOOL request = true;
    NSMutableArray *articleList = [NSMutableArray new];
    if (_dataManager.articles.count) {
        
        for (Article *article in _dataManager.articles) {
            if ([article.sectionURL isEqualToString:url] && [article.feedlink isEqualToString:feedUrl]) {
                [articleList addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
        
    } else {
        NSArray *articleListFromDisk = [_persistencyManager getArticles];
        for (Article *article in articleListFromDisk) {
            if ([article.sectionURL isEqualToString:url]  && [article.feedlink isEqualToString:feedUrl]) {
                [articleList addObject:article];
                if (!request)
                    [_dataManager.articles addObject:article];
            }
        }
        
        if (articleList.count) {
            Article *firstArticle = [articleList firstObject];
            if (![self reDownloadArticle:firstArticle]) {
                completionBlock (articleList);
                return;
            }
        }
    }
    
    if (request) {
        NSMutableArray *articleListForRequest = [NSMutableArray new];
        if (![self isOnLine]) {
            NSArray *articleListFromDisk = [_persistencyManager getArticles];
            for (Article *article in articleListFromDisk) {
                if ([article.sectionURL isEqualToString:url]  && [article.feedlink isEqualToString:feedUrl]) {
                    [articleListForRequest addObject:article];
                    [_dataManager.articles addObject:article];
                }
            }
            
            //if (articleList.count) {
                completionBlock (articleListForRequest);
                return;
            //}
        } else {
            [self requestSectionArticlesFromURL:url andFeedLink:feedUrl withCompletionBlock:^(NSMutableArray *array) {
                if (array) {
                    [_persistencyManager deleteArticlesWithFeedURL:feedUrl];
                    for (Article *article in array) {
                        [_dataManager.articles addObject:article];
                    }
                    [_persistencyManager saveArticles:array];
                }
                completionBlock (array);
            }];
            
        }
    }
    
}
- (void) getSectionMediaFromURL: (NSString *)url withFeedUrl:(NSString *)feedUrl isForced: (BOOL) forced  withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    BOOL request = true;
    NSMutableArray *mediaList = [NSMutableArray new];
    if (_dataManager.mediaItems.count) {
        
        for (Media *media in _dataManager.mediaItems) {
            if ([media.sectionUrl isEqualToString:url] && [media.feedlink isEqualToString:feedUrl]) {
                [mediaList addObject:media];
            }
        }
        
        if (mediaList.count) {
            Media *firstArticle = [mediaList firstObject];
            if (![self reDownloadMedia:firstArticle]) {
                completionBlock (mediaList);
                return;
            }
        }
        
    } else {
        NSArray *articleListFromDisk = [_persistencyManager getMediaList];
        for (Media *media in articleListFromDisk) {
            if ([media.sectionUrl isEqualToString:url]  && [media.feedlink isEqualToString:feedUrl]) {
                [mediaList addObject:media];
                [_dataManager.articles addObject:media];
            }
        }
        
        if (mediaList.count) {
            Media *firstArticle = [mediaList firstObject];
            if (![self reDownloadMedia:firstArticle]) {
                completionBlock (mediaList);
                return;
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            NSArray *articleListFromDisk = [_persistencyManager getMediaList];
            for (Media *media in articleListFromDisk) {
                if ([media.sectionUrl isEqualToString:url]  && [media.feedlink isEqualToString:feedUrl]) {
                    [mediaList addObject:media];
                    [_dataManager.articles addObject:media];
                }
            }
            
            //if (mediaList.count) {
                completionBlock (mediaList);
                return;
           //}
        } else {
            [self requestSectionMediaFromURL:url andFeedLink:feedUrl withCompletionBlock:^(NSMutableArray *array) {
                if (array) {
                    [_persistencyManager deleteMediaListWithSectionUrl:feedUrl];
                    for (Media *media in array) {
                        [_dataManager.articles addObject:media];
                    }
                    [_persistencyManager saveMediaList:array];
                }
                completionBlock (array);
            }];
            
        }
    }
    
}

- (void) getBookmarksWithCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock;
{
    if (_dataManager.bookmarks.count) {
        NSMutableArray *tempHolder = [_dataManager.bookmarks mutableCopy];
        for (Article *article in tempHolder) {
            if (![self checkBookmarkArticleValid:article]) {
                [_dataManager.bookmarks removeObject:article];
            }
        }
        completionBlock (_dataManager.bookmarks);
        return;
    } else {
        NSArray *articleDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:[Constants BOOKMARKS_KEY]] mutableCopy];
        for (NSData *data in articleDataFromDisk) {
            Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            if (![self checkBookmarkArticleValid:article]) {
                [self deleteBookmarkWithArticle:article];
            } else {
                [_dataManager.bookmarks addObject:article];
            }
            
        }
        completionBlock (_dataManager.bookmarks);
        return;
    }
    /*_dataManager.latestNews = [articleItems mutableCopy];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.LATEST_NEWS_KEY];
     [_persistencyManager saveArticles:articleItems withKey:Constants.LATEST_NEWS_KEY];*/
}

- (void) addBookmarkWithArticle: (Article *)article
{
    NSDate *date = [NSDate date];
    double downloadDateInDouble = [date timeIntervalSince1970];
    NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
    article.downloadTime = downloadDateInNumber;
    
    [_dataManager.bookmarks addObject:article];
    [_persistencyManager saveArticle:article withKey:[Constants BOOKMARKS_KEY]];
}

- (void) deleteBookmarkWithArticle: (Article *)article
{
    if (_dataManager.bookmarks.count) {
        for (Article *bookmarkArticle in _dataManager.bookmarks) {
            if ([bookmarkArticle.guid isEqualToString:article.guid]) {
                [_dataManager.bookmarks removeObject:bookmarkArticle];
                break;
            }
        }
    }
    
    [_persistencyManager deleteBookmarkWithArticle:article];
}

- (BOOL)isBookmarkedArticle:(Article *)article
{
    if (_dataManager.bookmarks.count) {
        for (Article *bookmarkArticle in _dataManager.bookmarks) {
            if ([bookmarkArticle.guid isEqualToString:article.guid]) {
                return YES;
            }
        }
    }
    return NO;
}

- (void) deleteWeather: (Weather *)weather
{
    if (_dataManager.weather.count) {
        for (Weather *weather in _dataManager.weather) {
            if ([weather.date isEqualToString:weather.date]) {
                [_dataManager.weather removeObject:weather];
                break;
            }
        }
    }
    
    [_persistencyManager deleteWeather:weather];
}



#pragma mark Private Functions

- (void) requestMediaFromURL: (NSString *)url andFeedLink: (NSString *)feedLink withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSMutableArray *mediaItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:url] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *array = [[responseObject objectForKey:@"items"] copy];
        NSMutableArray *mediaItems = [NSMutableArray new];
        NSDate *date = [NSDate date];
        double downloadDateInDouble = [date timeIntervalSince1970];
        NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
        for (NSDictionary *itemDict  in array) {
            Media *media = [Media new];
            media.mediaGroup = [itemDict objectForKey:@"mediaGroup"];
            media.tittle = [itemDict objectForKey:@"title"];
            media.guid = [itemDict objectForKey:@"guid"];
            media.pubDate = [itemDict objectForKey:@"pubDate"];
            media.sectionUrl = url;
            media.feedlink = feedLink;
            media.downloadTime = downloadDateInNumber;
            
            [mediaItems addObject:media];
        }
        
        completionBlock (mediaItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completionBlock (mediaItems);
        NSLog(@"getMEDIA Error: %@", error);
    }];
}

- (void) requestArticlesFromURL: (NSString *)url andFeedLink:(NSString *)feedLink withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSMutableArray *articleItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:url]parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *array = [[responseObject objectForKey:@"items"] copy];
        
        for (NSDictionary *itemDict  in array) {
            Article *article = [_dataManager articleFromDictioary:itemDict withParentUrl:url andFeedlink:feedLink];
            [articleItems addObject:article];
        }
        
        [_persistencyManager deleteArticlesWithFeedURL:feedLink];
        [_persistencyManager saveArticles:articleItems];
        
        completionBlock (articleItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Request Article Error: %@", error);
        completionBlock (articleItems);
    }];
}
- (void) requestArticlesFromURL: (NSString *)url withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSMutableArray *articleItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:url] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *array = [[responseObject objectForKey:@"items"] copy];
        
        for (NSDictionary *itemDict  in array) {
            Article *article = [_dataManager articleFromDictioary:itemDict andFeedlink:url];
            [articleItems addObject:article];
        }
        
        [_persistencyManager deleteArticlesWithFeedURL:url];
        [_persistencyManager saveArticles:articleItems];
        
        completionBlock (articleItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Request Articles Error: %@", error);
        completionBlock (articleItems);
    }];
}

- (void) requestArticlesFromFeedURL: (NSString *)url withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSMutableArray *articleItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *array = [[responseObject objectForKey:@"items"] copy];

        for (NSDictionary *itemDict  in array) {
            Article *article = [_dataManager feedArticleFromDictioary:itemDict];
            [articleItems addObject:article];
        }
        
        [_persistencyManager deleteArticlesWithFeedURL:url];
        [_persistencyManager saveArticles:articleItems];
        
        completionBlock (articleItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"requestArticlesFromFeedURL Error: %@", error);
        completionBlock (articleItems);
    }];
}

- (void) requestSectionArticlesFromURL: (NSString *)url withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSMutableArray *sectionArticleItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *sectionList = [[responseObject objectForKey:@"sections"] copy];
        if (!sectionList) {
            sectionList = [[responseObject objectForKey:@"section"] copy];
        }
        
        for (NSDictionary *dictionary in sectionList) {
            NSString *sectionName = (NSString *) [dictionary objectForKey:@"name"];
            NSArray *items =  [[dictionary objectForKey:@"items"] copy];
            NSDate *date = [NSDate date];
            double downloadDateInDouble = [date timeIntervalSince1970];
            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
            for (NSDictionary *itemDict in items) {
                
                Article *article = [Article new];
                article.category = [itemDict valueForKey:@"category"];
                article.enCategory = [itemDict valueForKey:@"enCategory"];
                article.byLine = [itemDict valueForKey:@"byline"];
                article.type = [itemDict valueForKey:@"type"];
                
                article.sectionURL = [itemDict valueForKey:@"sectionURL"];
                
                article.mediagroupList = [[itemDict objectForKey:@"mediaGroup"] copy];
                article.tittle = [itemDict valueForKey:@"title"];
                article.brief = [itemDict valueForKey:@"brief"];
                article.link = [itemDict valueForKey:@"link"];
                article.desc = [itemDict valueForKey:@"description"];
                article.guid = [itemDict valueForKey:@"guid"];
                article.lastUpdateDate = [itemDict valueForKey:@"lastUpdateDate"];
                article.pubDate = [itemDict valueForKey:@"pubDate"];
                article.sectionType = [itemDict valueForKey:@"sectionType"];
                article.sections = [[itemDict objectForKey:@"sections"] copy];
                article.related = [[itemDict objectForKey:@"related"] copy];
                article.feedlink = url;
                article.sectionName = sectionName;
                article.totalNum = [[itemDict valueForKey:@"totalNum"] stringValue];
                article.downloadTime = downloadDateInNumber;
                article.copyright = [itemDict valueForKey:@"copyright"];
                [sectionArticleItems addObject:article];
                
            }
        }
        completionBlock (sectionArticleItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"requestSectionArticlesFromURL Error: %@", error);
        completionBlock (sectionArticleItems);
    }];
}
- (void) requestlatestNewsSectionsFromURL: (NSString *)url withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSMutableArray *sectionArticleItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:url] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *sectionList = [[responseObject objectForKey:@"sections"] copy];
        if (!sectionList) {
            sectionList = [[responseObject objectForKey:@"section"] copy];
        }
        
        for (NSDictionary *dictionary in sectionList) {
            NSString *sectionName = (NSString *) [dictionary objectForKey:@"name"];
            NSArray *items =  [[dictionary objectForKey:@"items"] copy];
            NSDate *date = [NSDate date];
            double downloadDateInDouble = [date timeIntervalSince1970];
            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
            for (NSDictionary *itemDict in items) {
                
                Article *article = [Article new];
                article.category = [itemDict valueForKey:@"category"];
                article.enCategory = [itemDict valueForKey:@"enCategory"];
                article.byLine = [itemDict valueForKey:@"byline"];
                article.type = [itemDict valueForKey:@"type"];
                
                article.sectionURL = url;
                
                article.mediagroupList = [[itemDict objectForKey:@"mediaGroup"] copy];
                article.tittle = [itemDict valueForKey:@"title"];
                article.brief = [itemDict valueForKey:@"brief"];
                article.link = [itemDict valueForKey:@"link"];
                article.desc = [itemDict valueForKey:@"description"];
                article.guid = [itemDict valueForKey:@"guid"];
                article.lastUpdateDate = [itemDict valueForKey:@"lastUpdateDate"];
                article.pubDate = [itemDict valueForKey:@"pubDate"];
                article.sectionType = [itemDict valueForKey:@"sectionType"];
                article.sections = [[itemDict objectForKey:@"sections"] copy];
                article.related = [[itemDict objectForKey:@"related"] copy];
                article.feedlink = url;
                article.sectionName = sectionName;
                article.totalNum = [[itemDict valueForKey:@"totalNum"] stringValue];
                article.downloadTime = downloadDateInNumber;
                article.copyright = [itemDict valueForKey:@"copyright"];
                [sectionArticleItems addObject:article];
                
            }
        }
        completionBlock (sectionArticleItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"requestlatestNewsSectionsFromURL Error: %@", error);
        completionBlock (sectionArticleItems);
    }];
}

- (void) requestSectionArticlesFromURL: (NSString *)url andFeedLink:(NSString *)feedLink withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    
    NSMutableArray *sectionArticleItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *sectionList = [[responseObject objectForKey:@"sections"] copy];
        if (!sectionList) {
            sectionList = [[responseObject objectForKey:@"section"] copy];
            
        }
        
        for (NSDictionary *dictionary in sectionList) {
            NSString *sectionName = (NSString *) [dictionary objectForKey:@"name"];
            NSArray *items =  [[dictionary objectForKey:@"items"] copy];
            NSDate *date = [NSDate date];
            double downloadDateInDouble = [date timeIntervalSince1970];
            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
            for (NSDictionary *itemDict in items) {
                
                Article *article = [_dataManager articleFromDictioary:itemDict andFeedlink:feedLink];
                article.category = [itemDict valueForKey:@"category"];
                article.enCategory = [itemDict valueForKey:@"enCategory"];
                article.byLine = [itemDict valueForKey:@"byline"];
                article.type = [itemDict valueForKey:@"type"];
                article.sectionURL = [itemDict valueForKey:@"sectionURL"];
                article.mediagroupList = [[itemDict objectForKey:@"mediaGroup"] copy];
                article.tittle = [itemDict valueForKey:@"title"];
                article.brief = [itemDict valueForKey:@"brief"];
                article.link = [itemDict valueForKey:@"link"];
                article.desc = [itemDict valueForKey:@"description"];
                article.guid = [itemDict valueForKey:@"guid"];
                article.lastUpdateDate = [itemDict valueForKey:@"lastUpdateDate"];
                article.pubDate = [itemDict valueForKey:@"pubDate"];
                article.sectionType = [itemDict valueForKey:@"sectionType"];
                article.sections = [[itemDict objectForKey:@"sections"] copy];
                article.related = [[itemDict objectForKey:@"related"] copy];
                article.feedlink = feedLink;
                article.sectionName = sectionName;
                article.totalNum = [[itemDict valueForKey:@"totalNum"] stringValue];
                article.downloadTime = downloadDateInNumber;
                article.copyright = [itemDict valueForKey:@"copyright"];
                
                if ([itemDict valueForKey:@"sectionUrl"])
                    article.sectionURL = [itemDict valueForKey:@"sectionUrl"];
                else
                    article.sectionURL = [itemDict valueForKey:@"sectionURL"];
                
                [sectionArticleItems addObject:article];
                
            }
            
            
        }
        [_persistencyManager deleteArticlesWithFeedURL:feedLink];
        [_persistencyManager saveArticles:sectionArticleItems];
        completionBlock (sectionArticleItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"requestSectionArticlesFromURL Error: %@", error);
        completionBlock (sectionArticleItems);
    }];
}
- (void) requestSectionMediaFromURL: (NSString *)url andFeedLink:(NSString *)feedLink withCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock
{
    NSMutableArray *mediaItems = [NSMutableArray new];
    [[LibraryAPI httpClient] POST:[Util urlTokenForFeed:url] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *sectionList = [[responseObject objectForKey:@"sections"] copy];
        if (!sectionList) {
            sectionList = [[responseObject objectForKey:@"section"] copy];
            
        }
        
        for (NSDictionary *dictionary in sectionList) {
            //NSString *sectionName = (NSString *) [dictionary objectForKey:@"name"];
            NSArray *items =  [[dictionary objectForKey:@"items"] copy];
            NSDate *date = [NSDate date];
            double downloadDateInDouble = [date timeIntervalSince1970];
            NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
            for (NSDictionary *itemDict in items) {
                
                Media *media = [Media new];
                media.mediaGroup = [itemDict objectForKey:@"mediaGroup"];
                media.tittle = [itemDict objectForKey:@"title"];
                media.guid = [itemDict objectForKey:@"guid"];
                media.pubDate = [itemDict objectForKey:@"pubDate"];
                media.sectionUrl = url;
                media.feedlink = feedLink;
                media.downloadTime = downloadDateInNumber;
                
                [mediaItems addObject:media];
                
                if ([itemDict valueForKey:@"sectionUrl"])
                    media.sectionUrl = [itemDict valueForKey:@"sectionUrl"];
                else
                    media.sectionUrl = [itemDict valueForKey:@"sectionURL"];
                
                [mediaItems addObject:media];
                
            }
            
            
        }
        [_persistencyManager deleteArticlesWithFeedURL:feedLink];
        [_persistencyManager saveArticles:mediaItems];
        completionBlock (mediaItems);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"requestSectionArticlesFromURL Error: %@", error);
        completionBlock (mediaItems);
    }];
}
- (BOOL) reDownloadMedia: (Media *)media
{
    double downloadTimeDoubleValue = [media.downloadTime doubleValue];
    
    NSDate *downloadDate = [NSDate dateWithTimeIntervalSince1970:downloadTimeDoubleValue];
    NSDate *date = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date timeIntervalSinceDate:downloadDate];
    
    //NSLog(@"distanceBetweenDates: %f", distanceBetweenDates);
    //NSLog(@"DOWNLOAD_EXP_LENGHT: %f", Constants.DOWNLOAD_EXP_LENGHT);
    if (Constants.DOWNLOAD_EXP_LENGHT < distanceBetweenDates) {
        //need to download again
        return true;
    }
    
    return false;
}

- (BOOL) reDownloadArticle: (Article *)article
{
    double downloadTimeDoubleValue = [article.downloadTime doubleValue];
    
    NSDate *downloadDate = [NSDate dateWithTimeIntervalSince1970:downloadTimeDoubleValue];
    NSDate *date = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date timeIntervalSinceDate:downloadDate];
    
    //NSLog(@"distanceBetweenDates: %f", distanceBetweenDates);
    //NSLog(@"DOWNLOAD_EXP_LENGHT: %f", Constants.DOWNLOAD_EXP_LENGHT);
    if ([Constants DOWNLOAD_EXP_LENGHT] < distanceBetweenDates) {
        //need to download again
        return true;
    }
    
    return false;
}
- (BOOL) reDownloadTopFiveNews: (Article *)article
{
    double downloadTimeDoubleValue = [article.downloadTime doubleValue];
    
    NSDate *downloadDate = [NSDate dateWithTimeIntervalSince1970:downloadTimeDoubleValue];
    NSDate *date = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date timeIntervalSinceDate:downloadDate];
    
    //NSLog(@"distanceBetweenDates: %f", distanceBetweenDates);
    //NSLog(@"DOWNLOAD_EXP_LENGHT: %f", Constants.DOWNLOAD_EXP_LENGHT);
    if ([Constants TOP_5_NEWS_DOWNLOAD_EXP_LENGHT] < distanceBetweenDates) {
        //need to download again
        return true;
    }
    
    return false;
}
- (BOOL) reDownloadWeather: (Weather *)weather
{
    double downloadTimeDoubleValue = [weather.lastBuildDate doubleValue];
    
    NSDate *downloadDate = [NSDate dateWithTimeIntervalSince1970:downloadTimeDoubleValue];
    NSDate *date = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date timeIntervalSinceDate:downloadDate];
    
    //NSLog(@"distanceBetweenDates: %f", distanceBetweenDates);
    //NSLog(@"DOWNLOAD_EXP_LENGHT: %f", Constants.DOWNLOAD_EXP_LENGHT);
    if ([Constants WEATHER_EXP_LENGHT] < distanceBetweenDates) {
        //need to download again
        return true;
    }
    
    return false;
}
- (BOOL) checkBookmarkArticleValid: (Article *)article
{
    double downloadTimeDoubleValue = [article.downloadTime doubleValue];
    
    NSDate *downloadDate = [NSDate dateWithTimeIntervalSince1970:downloadTimeDoubleValue];
    NSDate *date = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date timeIntervalSinceDate:downloadDate];
    
    //NSLog(@"distanceBetweenDates: %f", distanceBetweenDates);
    //NSLog(@"DOWNLOAD_EXP_LENGHT: %f", Constants.DOWNLOAD_EXP_LENGHT);
    if ([Constants BOOKMARKS_EXP_LENGHT] < distanceBetweenDates) {
        //need to download again
        return false;
    }
    
    return true;
}

- (BOOL) checkWeatherValid: (Weather *)weather {
    double lastBuildDate = [weather.lastBuildDate doubleValue];
    NSDate *downloadDate = [NSDate dateWithTimeIntervalSince1970:lastBuildDate];
    NSDate *date = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date timeIntervalSinceDate:downloadDate];
    if ([Constants WEATHER_EXP_LENGHT] < distanceBetweenDates) {
        //need to download again
        return false;
    }
    return true;
}

- (BOOL) isOnLine
{
    Reachability *networkReachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    } else {
        return true;
    }
}

- (void) getWeatherWithForce: (BOOL)forced andCompletionBlock: (void (^)(NSMutableArray *array)) completionBlock{
    
    //NSLog(@"START: %@", [NSDate date]);
    BOOL request = true;
    NSMutableArray *tempContainer = [NSMutableArray new];
    
    if (_dataManager.weather.count) {
        //NSLog(@"AA: %@", [NSDate date]);
        Weather *firstWeather = [_dataManager.weather firstObject];
        
        if (![self reDownloadWeather:firstWeather]) {
            if (!forced) {
                tempContainer = [_dataManager.weather mutableCopy];
                completionBlock (tempContainer);
                return;
            }
        }
        
    } else {
        if (!forced) {
            //NSLog(@"BB: %@", [NSDate date]);
            NSArray *weatherDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.WEATHER_KEY] mutableCopy];
            if (weatherDataFromDisk.count) {
                NSData *firstData = [weatherDataFromDisk firstObject];
                Weather *firstWeather = [NSKeyedUnarchiver unarchiveObjectWithData:firstData];
                
                if (![self reDownloadWeather:firstWeather]) {
                    //use the old data
                    for (NSData *data in weatherDataFromDisk) {
                        Weather *weather = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        [tempContainer addObject:weather];
                    }
                    //NSLog(@"BB DONE: %@", [NSDate date]);
                    _dataManager.weather = [tempContainer mutableCopy];
                    completionBlock (tempContainer);
                    return;
                }
                
            }
        }
    }
    
    if (request) {
        if (![self isOnLine]) {
            
            if (_dataManager.weather.count == 0) {
                //NSLog(@"CC: %@", [NSDate date]);
                NSArray *weatherDataFromDisk = [[[NSUserDefaults standardUserDefaults] objectForKey:Constants.WEATHER_KEY] mutableCopy];
                for (NSData *data in weatherDataFromDisk) {
                    Weather *weather = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [tempContainer addObject:weather];
                }
                //NSLog(@"CC: DONE %@", [NSDate date]);
                _dataManager.weather = [tempContainer mutableCopy];
                completionBlock (tempContainer);
                return;
            } else {
                completionBlock (_dataManager.weather);
                return;
            }
        } else {
            //NSLog(@"DD: %@", [NSDate date]);
            NSMutableArray *weatherItems = [NSMutableArray new];
            
            [[LibraryAPI httpClient] GET:[Util urlWithTokenFromURL:Constants.WEATHER_URL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                Weather *weather = [[Weather alloc]init];
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    id object = responseObject[@"Weather"];
                    if ([object isKindOfClass:[NSArray class]]) {
                        NSArray *array = object;
                        if (array.count > 0) {
                            object = array[0];
                            if ([object isKindOfClass:[NSDictionary class]]) {
                                object = object[@"days"];
                                if ([object isKindOfClass:[NSArray class]]) {
                                    array = object;
                                    for (int i = 0; i < array.count; i++) {
                                        object = array[i];
                                        
                                        if ([object isKindOfClass:[NSDictionary class]]) {
                                            NSDate *today = [NSDate date];
                                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                            [formatter setDateFormat:@"dd-MM-yyyy"];
                                            NSString *s = [formatter stringFromDate:today];
                                            NSString *date = object[@"date"];
                                            
                                            if (date && date.length >= 10 && [[date substringToIndex:10] isEqualToString:s]) {
                                                weather.time = object[@"psi_time"];
                                                weather.period = object[@"period"];
                                                weather.min = object[@"min"];
                                                weather.max = object[@"max"];
                                                weather.rh = object[@"rh"];
                                                weather.psi = object[@"psi"];
                                                weather.psi_3h = object[@"psi_3h"];
                                                weather.psi_24h = object[@"psi_24h"];
                                                weather.tide = object[@"tide"];
                                                weather.condition = object[@"condition"];
                                                weather.thumbnail = [[object[@"thumbnail_lo"] lastPathComponent] stringByDeletingPathExtension];
                                                
                                                [weatherItems addObject:weather];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
                
                _dataManager.weather = [weatherItems mutableCopy];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:Constants.WEATHER_KEY];
                [_persistencyManager saveWeather:weatherItems withKey:Constants.WEATHER_KEY];
                //NSLog(@"F: %@", [NSDate date]);
                completionBlock (_dataManager.weather);
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                completionBlock(weatherItems);
            }];
        }
    }
    
}
/*
- (void)requestAppVersionWithURLString: (NSString *)url {
    
    NSString * encryptedURL = [Util urlWithTokenFromURL:url];
    
    [[AFHTTPSessionManager manager] GET:encryptedURL parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSMutableArray *mutableArray = nil;
        
        if ([[Util appVersion] floatValue] )
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dictionary = responseObject;
            
            id object = [dictionary objectForKey:@"item"];
            
            if ([object isKindOfClass:[NSArray class]]) {
                mutableArray = [object mutableCopy];
                NSMutableArray * indexArr = [[NSMutableArray alloc] init];
                for (int i = 0; i < mutableArray.count; i++) {
                    if ([mutableArray[i] isKindOfClass:[NSDictionary class]]) {
                        NSMutableDictionary *mutableDictionary = [mutableArray[i] mutableCopy];
                        if ( [mutableDictionary[@"platform"] isEqualToString:@"ios"] ){
                            [indexArr addObject:mutableDictionary];
                            break;
                        }
                    }
                }
                
                NSMutableDictionary * dictStatus = [[NSMutableDictionary alloc]init];
                [dictStatus setValue:@"STATUS_OK" forKey:@"SIGNALDATA_STATUS"];
                
                NSMutableDictionary * dictAppData = [[NSMutableDictionary alloc]init];
                [dictAppData setValue:indexArr forKey:@"APP_VERSION_LIST"];
                
                NSMutableArray *array = [NSMutableArray new];
                [array addObject:dictStatus];
                [array addObject:dictAppData];
                
                NSMutableDictionary * userInfo = [[NSMutableDictionary alloc]init];
                [userInfo setValue:array forKey:@"DATA_APPVERSION"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SIGNAL_APPVERSION" object:self userInfo:userInfo];
            }else{
                
                NSMutableDictionary * dictStatus = [[NSMutableDictionary alloc]init];
                [dictStatus setValue:@"STATUS_ERROR" forKey:@"SIGNALDATA_STATUS"];
                
                NSMutableDictionary * dictAppData = [[NSMutableDictionary alloc]init];
                [dictAppData setValue:responseObject forKey:@"APP_VERSION_LIST"];
                
                NSMutableArray *array = [NSMutableArray new];
                [array addObject:dictStatus];
                [array addObject:dictAppData];
                
                NSMutableDictionary * userInfo = [[NSMutableDictionary alloc]init];
                [userInfo setValue:array forKey:@"DATA_APPVERSION"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SIGNAL_APPVERSION" object:self userInfo:userInfo];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSMutableDictionary * dictStatus = [[NSMutableDictionary alloc]init];
        [dictStatus setValue:@"STATUS_ERROR" forKey:@"SIGNALDATA_STATUS"];
        
        NSMutableDictionary * dictAppData = [[NSMutableDictionary alloc]init];
        [dictAppData setValue:[NSArray new] forKey:@"APP_VERSION_LIST"];
        
        NSMutableArray *array = [NSMutableArray new];
        [array addObject:dictStatus];
        [array addObject:dictAppData];
        
        NSMutableDictionary * userInfo = [[NSMutableDictionary alloc]init];
        [userInfo setValue:array forKey:@"DATA_APPVERSION"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SIGNAL_APPVERSION" object:self userInfo:userInfo];
    }];
    
}
*/



@end
