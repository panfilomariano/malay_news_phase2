//
//  TMDetailedBeritaViewController.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/17/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPagerController.h"
#import "Article.h"

@protocol TMDetailedBeritaViewControllerDelegate <NSObject>
@optional
-(void)loadViewedItems:(NSString *)UrlString atIndex:(NSNumber*)index;
@end

typedef enum
{
    OthersBeritaScreenType,
    LatestNewsBeritaScreenType,
    CABeritaScreenType,
    SpecialReportScreenType,
    NewsBeritaScreenType,
    MenuBeritaScreenType,
    TopNewsBeritaScreenType,
    
} DetailBeritaScreenType;

@interface TMDetailedBeritaViewController : ViewPagerController

@property (nonatomic, weak) id <TMDetailedBeritaViewControllerDelegate> detailedBeritaViewControllerdelegate;
@property (nonatomic, strong) NSArray *newsCategories;
@property (nonatomic, strong) NSString *feedLink;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *enTitle;
@property (nonatomic, strong) Article *specialReportArticle;
@property (nonatomic, strong) NSString *specialEnTittle;
@property (nonatomic, strong) NSString *categoryForSR;
@property (nonatomic, strong) NSString *stringApiUrl;
@property (nonatomic, assign) DetailBeritaScreenType screenType;
@property (nonatomic) BOOL isTopNews;
@property (nonatomic) NSInteger selectedIndex;
@end
