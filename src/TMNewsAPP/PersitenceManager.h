//
//  PersitenceManager.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"
#import "Media.h"
#import "Weather.h"

@interface PersitenceManager : NSObject

- (void) saveArticles: (NSArray *) articles;
- (void) saveArticle: (Article *) article;
- (void) saveArticle: (Article *) article withKey: (NSString *)key;
- (void) saveMediaList: (NSArray *) mediaList;
- (void) saveMediaList: (NSArray *) mediaList withKey: (NSString *)key;
- (void) saveObject:(id)object withKey:(NSString *)key;
- (void) saveArticles: (NSArray *) articles withKey: (NSString *)key;
- (void) deleteArticlesWithFeedURL: (NSString *)feedlink;
- (void) deleteMediaListWithSectionUrl: (NSString *)sectionUrl;
- (void) deleteArticlesWithKey: (NSString *)key;
- (void) deleteBookmarkWithArticle: (Article *)article;
- (void) saveWeather:(Weather *)weather;
- (void) saveWeather: (NSArray *) weather withKey: (NSString *)key;
- (void) deleteWeather: (Weather *)weather;
- (NSArray *) getArticles;
- (NSArray *) getMediaList;

@end
