//
//  TMLongCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/11/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMLongCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *descLabel;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *imageCounter;

@end
