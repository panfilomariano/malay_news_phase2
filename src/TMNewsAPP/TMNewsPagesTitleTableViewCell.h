//
//  TMNewsPagesTitleTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/19/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMNewsPagesTitleTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@end
