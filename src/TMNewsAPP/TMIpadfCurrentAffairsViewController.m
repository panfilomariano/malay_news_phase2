
//  TMIpadfCurrentAffairsViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMIpadfCurrentAffairsViewController.h"
#import "TMbigCurrentAffairsTableViewCell.h"
#import "TMsmallCurrentAffairsTableViewCell.h"
#import "LibraryAPI.h"
#import "Article.h"
#import "Util.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+Helpers.h"
#import "TMNewsPagerViewController.h"
#import "TMNewsDetailsPagerViewController.h"
#import "TMCAandSpecialReportListViewController.h"
#import "TMNewsPagerViewController.h"
#import "MBProgressHUD.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AnalyticManager.h"
#import "Constants.h"
#import "AnalyticManager.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"

@interface TMIpadfCurrentAffairsViewController () <LatestEpisodesCellDelegate, TMNewsDetailsPagerViewControllerDelagate, GADBannerViewDelegate, GADAdSizeDelegate>
{
    float width;
}

@property (nonatomic, weak) IBOutlet UITableView *bigTableView;
@property (nonatomic, weak) IBOutlet UITableView *smallTableView;
@property (nonatomic, strong) NSMutableArray *currentAffairArray;
@property (nonatomic, strong) NSMutableArray *episodeArray;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bigCellWidth;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *smallCellWidth;
@property (nonatomic, strong) NSString *tittleString;
@property (nonatomic, strong) NSMutableDictionary *titleDictionary;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *removedBookmarks;
@property (nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;


@end

@implementation TMIpadfCurrentAffairsViewController
static dispatch_group_t group;

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    self.currentAffairArray = [[NSMutableArray alloc] init];
    self.episodeArray = [[NSMutableArray alloc] init];
    self.bookmarks = [[NSMutableArray alloc]init];
   
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadBookmarks];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        self.bigCellWidth.constant = 488.0f;
        self.smallCellWidth.constant = 244.0f;
    }else{
        self.bigCellWidth.constant = 688.0f;
        self.smallCellWidth.constant = 300.0f;
    }
    
    
    [[AnalyticManager sharedInstance] trackCACategoryTab];
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    
    self.bannerHeight.constant = 0;
    
    /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestContentsWithForce:)
                                                 name:@"reloadDataNotif"
                                               object:nil];*/
    
    [self.bigTableView setContentInset:UIEdgeInsetsMake(0, 0, 10, 0)];
    [self setUpads];

    [Util setRefreshControlWithTarget:self inView:self.smallTableView withSelector:@selector(reloadSmallTableData:)];
    [Util setRefreshControlWithTarget:self inView:self.bigTableView withSelector:@selector(reloadBigTableData:)];
    
    group = dispatch_group_create();
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self requestContentsWithForce:YES];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.smallTableView reloadData];
        [self.bigTableView reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    });

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Notif Observers
/*- (void)deviceOrientationDidChangeNotification:(NSNotification*)note
{
 //   float width = [UIScreen mainScreen].bounds.size.width;
    
    
}*/

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        self.bigCellWidth.constant = 488.0f;
        self.smallCellWidth.constant = 244.0f;
    }else{
        self.bigCellWidth.constant = 688.0f;
        self.smallCellWidth.constant = 300.0f;
    }
    
    [self.bigTableView reloadData];
    [self.smallTableView reloadData];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark tableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.bigTableView) {
        return  [self.currentAffairArray count];
    }else if (tableView == self.smallTableView){
        return [self.episodeArray count];
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.bigTableView){
        TMbigCurrentAffairsTableViewCell *cell = (TMbigCurrentAffairsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bigCAcell" forIndexPath:indexPath];
        Article *article = (Article *)[self.currentAffairArray objectAtIndex:indexPath.row];
        
        [cell.imageview sd_setImageWithURL:[NSURL URLWithString:article.thumbnail] placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached:0 ];
        return cell;
    } else if (tableView == self.smallTableView){
        TMsmallCurrentAffairsTableViewCell *cell = (TMsmallCurrentAffairsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"smallCAcell" forIndexPath:indexPath];
        Article *article = (Article *)[self.episodeArray objectAtIndex:indexPath.row];
        cell.delegate =self;
        cell.categoryLabel.text = article.category;
        cell.dateLabel.text = [article.pubDate formatDate];
        cell.tittleLabel.text = article.tittle;
        cell.indexPath = indexPath;
        [self.titleDictionary setObject:article.tittle forKey:[NSString stringWithFormat: @"%ld",(long)indexPath.row]];
        NSString *thumbnail = @"";
        NSString *type = @"";
        if (article.mediagroupList) {
            NSDictionary *dictionary = [article.mediagroupList firstObject];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
            type = [dictionary valueForKey:@"type"];
        }
        if ([type isEqualToString:@"video"]){
            [cell.playIcon setHidden:NO];
        }else {
            [cell.playIcon setHidden:YES];
        }
        [cell.bookmarkButton setSelected:NO];
        if (_bookmarks.count) {
            for (Article *bookmark in _bookmarks) {
                if ([bookmark.guid isEqualToString:article.guid]) {
                    [cell.bookmarkButton setSelected:YES];
                    break;
                }
            }
        }
        
    [cell.imageview sd_setImageWithURL:[NSURL URLWithString:thumbnail] placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached:0];
       
        
        return cell;
    }
    
    return nil;
}

#pragma mark tableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float widthBig;
    float widthsmall;

    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        widthBig = 488.0f;
        widthsmall = 244.0f;

    }else{
        widthBig = 688.0f;
        widthsmall = 300.0f;
    }
   
    float aheight = 9;
    float awidth = 16;
   
    if (tableView == self.bigTableView) {
         return (aheight/awidth * widthBig);
        
    }else if(tableView == self.smallTableView){
       
        float initialheight = (aheight/awidth * widthsmall);
     
        return initialheight+95;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView== self.smallTableView) {
         return [Constants LATEST_EPISODE];
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.smallTableView){
        return 40.0f;
    }return 0;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.bigTableView) {
        Article *article = [self.currentAffairArray objectAtIndex:indexPath.row];
        [[LibraryAPI sharedInstance]getArticlesWithURL:article.sectionURL andFeedLink:article.feedlink  isForced:NO withCompletionBlock:^(NSMutableArray *array){
            if ([array count]) {
                Article *newarticle = [array firstObject];
              
                TMNewsPagerViewController *newsPagerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsPagerViewControllerId"];
                newsPagerVC.pagerTittle = article.tittle;
                newsPagerVC.newsPages = newarticle.sections;
                newsPagerVC.screenType = CANewsPagerScreenType;
                newsPagerVC.activeIndex = 0;
                newsPagerVC.enTitle = newarticle.entittle;
                [self.navigationController pushViewController:newsPagerVC animated:YES];
            }
        }];
    }else if(tableView == self.smallTableView) {
        TMNewsDetailsPagerViewController *pager = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsPagerViewControllerId"];
        pager.newsArticles = self.episodeArray;
        pager.bookmarks = [_bookmarks mutableCopy];
        pager.activeTabIndex = indexPath.row;
        pager.pagerTittle = @"";
        [self.navigationController pushViewController:pager animated:YES];
    }

    
}

#pragma mark LatestEpisodesCellDelegate

- (void)didTapBookMarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    
    Article *article = (Article *) [self.episodeArray objectAtIndex:indexPath.row];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
       
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            [_bookmarks removeObject:bookmarkArticle];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
            [_bookmarks addObject:article];
        }
        
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
    }

    
    [self.smallTableView beginUpdates];
    [self.smallTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.smallTableView endUpdates];
    
    if (!bookmarkArticle){
        [[AnalyticManager sharedInstance] trackCABookmarkWithProgram:article.enCategory andEpisodeId:article.guid andEpisodeTitle:article.tittle];
    }
    

    
}


- (void)loadBookmarks {
    [[LibraryAPI sharedInstance]getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array){
            self.bookmarks = [array mutableCopy];
            [self.smallTableView reloadData];
        }
    }];
}

#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    self.bannerView.adSize = kGADAdSizeBanner;
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       ]];*/
    //self.bannerView.adSizeDelegate = self;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    [self.bannerView setAutoloadEnabled:YES];
    [self.bannerView loadRequest:request];
    [self.bannerView setHidden:YES];
    
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
    [self.bannerView setHidden:YES];
}

- (void)adViewDidReceiveAd:(GADBannerView *)view {
    if (self.bannerHeight.constant == 0)
        self.bannerHeight.constant = 50;
    [self.bannerView setHidden:NO];
}
/*- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)gadSize{
     self.bannerHeight.constant = gadSize.size.height;
}*/

#pragma mark Private Functions
-(void)reloadBigTableData: (UIRefreshControl *)refreshFeed
{
    [self getCurrentAffairWithForce:YES];
    
    if (refreshFeed) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        [refreshFeed endRefreshing];
        
    }
    
}
-(void)reloadSmallTableData: (UIRefreshControl *)refreshFeed
{
    [self getIndexListWithForce:YES];
    /*[[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        for(NSDictionary *dictionary in array){
            NSString *enTitle = [dictionary valueForKey:@"enTitle"];
            if([enTitle isEqualToString:@"latest episodes"]){
                [self getEpisodeWithURL:[dictionary valueForKey:@"sectionURL"] withForce:YES];
                
            }
        }
    }];*/
    
    if (refreshFeed) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        [refreshFeed endRefreshing];
        
    }
    
}
- (void)getEpisodeWithURL: (NSString *)urlString withForce: (BOOL) forced {
    dispatch_group_enter(group);
    [[LibraryAPI sharedInstance] getArticlesWithURL:urlString isForced:forced withCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            self.episodeArray = [array mutableCopy];
        }
        [self loadBookmarks];
        [self.smallTableView reloadData];
        dispatch_group_leave(group);
    }];
    
}

- (void)requestContentsWithForce: (BOOL) forced
{
    [self getIndexListWithForce:forced];
    [self getCurrentAffairWithForce:forced];
}

-(void)getIndexListWithForce:(BOOL) forced{
    dispatch_group_enter(group);
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        for(NSDictionary *dictionary in array){
            NSString *enTitle = [dictionary valueForKey:@"enTitle"];
             if([enTitle isEqualToString:@"latest episodes"]){
                [self getEpisodeWithURL:[dictionary valueForKey:@"sectionURL"] withForce:forced];
             }
        }
        dispatch_group_leave(group);
    }];
    
}
- (void)getCurrentAffairWithForce: (BOOL)forced {
   dispatch_group_enter(group);
    [[LibraryAPI sharedInstance] getCAWithForce:forced andCompletionBlock:^(NSMutableArray *array) {
        self.currentAffairArray = [array mutableCopy];
        [self.bigTableView reloadData];
        dispatch_group_leave(group);
    }];
    
    
}

@end
