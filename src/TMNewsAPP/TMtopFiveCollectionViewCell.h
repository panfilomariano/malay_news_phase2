//
//  TMtopFiveCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/5/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMPageControl.h"
@interface TMtopFiveCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *playOverlay;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;
@end

