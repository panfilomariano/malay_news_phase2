//
//  DataManager.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/10/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"
#import "Media.h"
#import "Weather.h"

@interface DataManager : NSObject

@property (nonatomic, strong) NSMutableArray *articles;
@property (nonatomic, strong) NSArray *indexItemList;
@property (nonatomic, strong) NSMutableArray *latestNews;
@property (nonatomic, strong) NSMutableArray *advertorial;
@property (nonatomic, strong) NSMutableArray *topNews;
@property (nonatomic, strong) NSMutableArray *feedNewsItems;
@property (nonatomic, strong) NSMutableArray *caNewsItems;
@property (nonatomic, strong) NSMutableArray *photosItems;
@property (nonatomic, strong) NSMutableArray *videosItems;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableArray *weather;
@property (nonatomic, strong) NSMutableArray *topFiveNews;
@property (nonatomic, strong) NSMutableArray *mediaItems;
- (Article *) articleFromDictioary: (NSDictionary *)dictionary andFeedlink: (NSString *)feedLink;
- (Article *) feedArticleFromDictioary: (NSDictionary *)dictionary;
- (Article *) articleFromDictioary: (NSDictionary *)dictionary withParentUrl:(NSString *)url andFeedlink: (NSString *)feedLink;
- (Weather *) weatherFromDictionary: (NSDictionary *) dictionary;
@end
