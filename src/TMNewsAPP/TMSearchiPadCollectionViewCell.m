//
//  TMSearchiPadCollectionViewCell.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 3/6/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSearchiPadCollectionViewCell.h"

@implementation TMSearchiPadCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(IBAction)didTapBookmarkButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}


@end
