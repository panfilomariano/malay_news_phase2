//
//  TMSearchViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/21/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSearchViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "DataManager.h"
#import "TMBeritaListTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+Helpers.h"
#import "UIImageView+Helpers.h"
#import "TMNewsTableViewCell.h"
#import "TMSearchTableViewCell.h"
#import "TMNewsPagesViewController.h"
#import "TMSquareCollectionViewCell.h"
#import "Util.h"
#import "TMNewsDetailsPagerViewController.h"
#import "TMSearchiPadCollectionViewCell.h"
#import "TMNewsArticleViewController.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "NSString+PercentEscapes.h"

@interface TMSearchViewController () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, TMNewsTableViewCellDelagate, TMNewsPagesViewControllerDelagate, TMSearchTableViewCellDelegate, TMSquareCollectionViewCellDelagate, TMSearchiPadCollectionViewCellDelegate>

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) UINib *squareCell;
@property (nonatomic, strong) NSMutableArray *searchItems;
@property (nonatomic, strong) NSString *searchURL;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, weak) IBOutlet UIView *searchLineView;


@end

@implementation TMSearchViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _bookmarks = [[NSMutableArray alloc] init];
    _searchItems = [[NSMutableArray alloc] init];
    self.squareCell = [UINib nibWithNibName:@"TMSearchiPadCollectionViewCell" bundle:nil];
    
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getBookmarkedItems];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.barTintColor = [Util headerColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.title = [Constants MENU_SEARCH];
    [self.noresultsLabel setHidden:YES];
    [self.searchResultsLabel setHidden:YES];
    
    [self.searchBar becomeFirstResponder];
    self.searchBar.placeholder = [Constants SEARCH_PLACEHOLDER];
    self.searchBar.delegate = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LibraryAPI sharedInstance] getIndexListItems:^(NSArray *array) {
        for (NSDictionary *dictionary in array) {
            if ([[dictionary valueForKey:@"enTitle"] isEqualToString:@"search"]) {
                self.searchURL = dictionary[@"sectionURL"];
                break;
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    [self getBookmarkedItems];
    
    if ([Util is_iPad]){
        [self.collectionView registerNib:self.squareCell forCellWithReuseIdentifier:@"squareSearchCell"];
    }
    [[AnalyticManager sharedInstance] trackSearch];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //CGRect screenBounds = [[UIScreen mainScreen] bounds];
    float rW = 224.0f;
    float rH = 137.0f;
    
    float width = (768 / 3) - 11;
    if (!UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        width = (1024 / 4) - 11;
    }
    
    float height = (rH / rW * width) + 115;
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(9, 9, 9, 9);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 4.5f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.0f;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark UISearchBarDelegate
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (searchBar.text.length >= 30)
        return NO;
    
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    if (self.searchItems.count)
        [self.searchItems removeAllObjects];
    
    if (self.searchURL.length) {

        
        NSString *finalKeyword = [searchBar.text stringByReplacingPercentEscapes];
        NSString *replaceSpecialCharStr1 = [finalKeyword stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *replaceSpecialCharStr2 = [replaceSpecialCharStr1 stringByReplacingOccurrencesOfString:@"%" withString:@""];
        NSString *replaceSpecialCharStr3 = [replaceSpecialCharStr2 stringByReplacingOccurrencesOfString:@"#" withString:@""];
        NSString *replaceSpecialCharStr4 = [replaceSpecialCharStr3 stringByReplacingOccurrencesOfString:@"?" withString:@""];
        finalKeyword = replaceSpecialCharStr4;
        
        finalKeyword = [finalKeyword stringByAddingPercentEscapes];
        
        //NEW API: change the search API for to http://...../<keyword>/search.json
        
        NSArray *arr = [self.searchURL componentsSeparatedByString:@"/"];
        NSString *str =  [arr lastObject];
        NSString *suffix = [NSString stringWithFormat:@"%@/%@", finalKeyword, str];
        NSString *finalUrl = [self.searchURL stringByReplacingOccurrencesOfString:str withString:suffix];
         
        
        //old API. to be change to new API
        //NSString *finalUrl = [NSString stringWithFormat:@"%@?query=%@", self.searchURL ,finalKeyword];

        NSLog(@"search URL::%@", finalUrl);
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:[Util urlTokenForFeed:finalUrl] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if (responseObject) {
                NSArray *array = [[responseObject objectForKey:@"items"] copy];
                
                for (NSDictionary *dictionary  in array) {
                    Article *article = [Article new];
                    article.category = dictionary[@"category"];
                    article.enCategory = dictionary[@"enCategory"];
                    article.byLine = dictionary[@"byline"];
                    article.type = dictionary[@"type"];
                    article.sectionURL = dictionary[@"sectionURL"];
                    article.mediagroupList = [dictionary[@"mediaGroup"] copy];
                    article.tittle = dictionary[@"title"];
                    article.entittle = dictionary[@"enTitle"];
                    article.brief = dictionary[@"brief"];
                    article.link = dictionary[@"link"];
                    article.desc = dictionary[@"description"];
                    article.guid = dictionary[@"guid"];
                    article.lastUpdateDate = dictionary[@"lastUpdateDate"];
                    article.pubDate = dictionary[@"pubDate"];
                    article.sectionType = dictionary[@"sectionType"];
                    article.sections = [dictionary[@"sections"] copy];
                    article.related = [dictionary[@"related"] copy];
                    article.sectionName = dictionary[@"name"];
                    article.thumbnail = dictionary[@"thumbnail"];
                    article.totalNum = dictionary[@"totalNum"];
                    article.copyright = dictionary[@"copyright"];
                    [_searchItems addObject:article];
                    
                }
                
                [[AnalyticManager sharedInstance] trackSearchStringWithCategoryName:searchBar.text andCount:self.searchItems.count];
                
                if (array.count == 0) {
                    [self.noresultsLabel setHidden:NO];
                    [self.noresultsLabel setText:[Constants NO_SEARCH_RESULT]];
                    self.noresultsHeight.constant = 20;
                }else{
                    [self.noresultsLabel setHidden:YES];
                    self.noresultsHeight.constant = 0;
                    
                }
                
            }
           
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.collectionView reloadData];
            [self.tableView reloadData];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self.noresultsLabel setHidden:NO];
            [self.noresultsLabel setText:[Constants NO_SEARCH_RESULT]];
            self.noresultsHeight.constant = 20;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.collectionView reloadData];
            [self.tableView reloadData];
        }];
    }
}

#pragma mark UITableViewDatasource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 30)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *bottom = [[UIView alloc]initWithFrame:CGRectMake(0, 5, self.tableView.bounds.size.width, 1)];
    [bottom setBackgroundColor:[UIColor colorWithRed:227/255.0f green:228/255.0f blue:230/255.0f alpha:1]];
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(9, -23, 312, 30)];
    [title setText:[Constants SEARCH_RESULTS]];
    //[title setText:[tableView.dataSource tableView:tableView titleForHeaderInSection:section]];
    [title setTextColor:[UIColor blackColor]];
    UIFont *fontName = [UIFont fontWithName:@"Helvetica-Nue" size:15.0];
    [title setFont:fontName];
    
    [headerView addSubview:title];
    [headerView addSubview:bottom];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_searchItems.count == 0){
        return 0;
    }else {
        return 5;
    }
}

- (NSInteger)numberOfSections {
    return 1;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [Constants SEARCH_RESULTS];
}*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    //if (section == 0){
        //return 1;
    //}else {
        return _searchItems.count;
   // }

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    //if (indexPath.row == 0){
       // return 22;
   // }else {
        return 86.0f;
   // }

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    TMSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
    
    if (self.searchItems.count) {
        Article *article = (Article *)[self.searchItems objectAtIndex:indexPath.row];
        
        NSString *title = article.tittle;
        NSString *category = article.category;
        cell.titleLabel.text = title;
        cell.category.text = category;
        cell.delegate = self;
        cell.pubDate.text = [article.pubDate formatDate];
        NSArray *mediaGroupArray = article.mediagroupList;
        NSDictionary *mediaDictionary = (NSDictionary *) [mediaGroupArray objectAtIndex:0];
        NSString *mediaType = [mediaDictionary valueForKey:@"type"];
        NSString *photoThumbnail = [mediaDictionary valueForKey:@"thumbnail"];
        
        if([mediaType isEqualToString:@"video"]){
            [cell.playOverlay setHidden:NO];
        }else{
            [cell.playOverlay setHidden:YES];
        }
        [cell.starButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:article]];
        
        cell.indexPath = indexPath;
        [cell.thumbnail setBorder];
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:photoThumbnail]
                          placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
        
      
    }
      return cell;
 

}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TMNewsPagesViewController *pages = (TMNewsPagesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsPagesID"];
    Article *article = [self.searchItems objectAtIndex:indexPath.row];
    pages.delegate = self;
    if (_bookmarks.count) {
        for (Article *articleFromBookmark in _bookmarks) {
            if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                pages.isBookmark = YES;
                break;
            }
        }
    }
    pages.article = [self.searchItems objectAtIndex:indexPath.row];
    if ([article.type isEqualToString:@"special report"]){
        [[AnalyticManager sharedInstance] trackClickSpecialReportArticleInSearchWithCategoryName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
    }else if ([article.type isEqualToString:@"current affair"]){
        [[AnalyticManager sharedInstance] trackCAArticleInSearchWithProgramName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
    }else {
        [[AnalyticManager sharedInstance] trackClickNewsArticleInSearchWithCategory:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
    }
    [self.navigationController pushViewController:pages animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 40;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}


#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  _searchItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TMSearchiPadCollectionViewCell *cell = (TMSearchiPadCollectionViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"squareSearchCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    if (self.searchItems.count) {
        Article *article = (Article *) [self.searchItems objectAtIndex:indexPath.row];
        cell.searchTitle.text = article.tittle;
        cell.category.text = article.category;
        cell.delegate = self;
        cell.indexPath = indexPath;
        if (article.mediagroupList.count) {
            NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
            NSString *thumbnail = [mediaDictionary valueForKey:@"thumbnail"];
            NSString *type = [mediaDictionary valueForKey:@"type"];
            
            cell.pubDate.text = [article.pubDate formatDate];
            if([type isEqualToString:@"video"]){
                [cell.playButton setHidden:NO];
            }else{
                [cell.playButton setHidden:YES];
            }
            
            
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                              placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            
            [cell.bookmarkButton setSelected:NO];
            if (_bookmarks.count) {
                for (Article *articleFromRemovedBookmarks in _bookmarks) {
                    if ([articleFromRemovedBookmarks.guid isEqualToString:article.guid]) {
                        [cell.bookmarkButton setSelected:YES];
                        break;
                    }
                }
                
                [cell.thumbnail.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
                [cell.thumbnail.layer setBorderWidth: 1.0];
            }
        }
        
    }
    return cell;
    
}

#pragma mark UICollectionViewDelegate

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //  BOOL isBookmark = [self checkBookmarkWithArticle:article];
    Article *article = [self.searchItems objectAtIndex:indexPath.row];
    TMNewsArticleViewController *newsArticleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsArticleViewControllerId"];
    newsArticleVC.article = [self.searchItems objectAtIndex:indexPath.row];
    //  newsArticleVC.isBookmark = isBookmark;
    newsArticleVC.hasBarItems = YES;
    [self.navigationController pushViewController:newsArticleVC animated:YES];
    
    if ([article.type isEqualToString:@"special report"]){
        [[AnalyticManager sharedInstance] trackClickSpecialReportArticleInSearchWithCategoryName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
    }else if ([article.type isEqualToString:@"current affair"]){
        [[AnalyticManager sharedInstance] trackCAArticleInSearchWithProgramName:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
    }else {
        [[AnalyticManager sharedInstance] trackClickNewsArticleInSearchWithCategory:article.enCategory andGuid:article.guid andArticleTitle:article.tittle];
    }
    /*
     TMNewsDetailsPagerViewController *newsDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsDetailsPagerViewControllerId"];
     newsDetails.newsArticles = self.searchItems;
     newsDetails.activeTabIndex = indexPath.row;
     newsDetails.pagerTittle = @"";
     [self.navigationController pushViewController:newsDetails animated:YES];*/
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableView = (UICollectionReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *) [reusableView viewWithTag:111];
    [label setText:[Constants SEARCH_RESULTS]];
    if (!_searchItems.count){
        [reusableView setHidden:YES];
        
    }
    
    return reusableView;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if(!_searchItems.count){
        
        return CGSizeZero;
    }else{
        
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), 30);
    }
    
}
#pragma mark TMNewsTableViewCellDelagate | TMSearchiPadCollectionViewCellDelegate
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath
{
    Article *article = [self.searchItems objectAtIndex:indexPath.row];
    Article *bookmarkArticle = nil;
    if (_bookmarks.count) {
        
        for (Article *bookmark in _bookmarks) {
            if ([bookmark.guid isEqualToString:article.guid]) {
                bookmarkArticle = bookmark;
                break;
            }
        }
        
        if (bookmarkArticle) {
            [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:bookmarkArticle];
            [self.view makeToast:[Constants REMOVE_FROM_FAV]];
            [_bookmarks removeObject:bookmarkArticle];
        } else {
            [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
            [self.view makeToast:[Constants ADD_TO_FAV]];
            [_bookmarks addObject:article];
        }
        
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
    }
    
    
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
    [self.tableView endUpdates];
    
    
    if (!bookmarkArticle){
        if ([article.type isEqualToString:@"current affair"]){
            [[AnalyticManager sharedInstance] trackCABookmarkWithProgram:article.enCategory andEpisodeId:article.guid andEpisodeTitle:article.tittle];
        }else {
            [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
        }
    }
   
}

#pragma mark privateFunctions
-(void)getBookmarkedItems
{
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.tableView reloadData];
            [self.collectionView reloadData];
        }
    }];
    
}



@end
