//
//  TMShareVideoViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMShareVideoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MediaPlayer/MediaPlayer.h>
#import "TMVideoTableViewCell.h"
#import "TMHeaderViewTableViewCell.h"
#import "TMVideoDescTableViewCell.h"
#import "Media.h"
#import "TMMoviePlayerViewController.h"
#import "NSString+Helpers.m"
#import "Util.h"
#import "UIImageView+Helpers.h"
#import <Social/Social.h>
#import "UIViewController+ECSlidingViewController.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import <Twitter/Twitter.h>
#import <STTwitter.h>
#import <MessageUI/MessageUI.h>
#import "Constants.h"

@interface TMShareVideoViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIBarPositioningDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSString *caption;

@property (nonatomic, strong) TMMoviePlayerViewController *moviePlayer;
@end

@implementation TMShareVideoViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBarTintColor:[Util headerColor]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    Media *media = [self.videoBrowserList firstObject];
    NSDictionary *dictionary = [media.mediaGroup firstObject];
    NSString *movieUrl = [dictionary valueForKey:@"content"];
   
   [self playMovieWithURL:movieUrl];

    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shareButton.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didPressShare) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 32, 32)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

    UIButton *customBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBack setImage:[UIImage imageNamed:@"right-btn-s"] forState:UIControlStateNormal];
    [customBack addTarget:self action:@selector(didTapBackbutton) forControlEvents:UIControlEventTouchUpInside];
    customBack.frame = CGRectMake(-75, 0, 150, 44);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(-75, 0, 150, 44)];
    [backButtonView addSubview:customBack];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:backButtonView];
     self.navigationItem.leftBarButtonItem = item;
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.videoBrowserList count] > 1) {
        return self.videoBrowserList.count + 2;
    }else{
        return self.videoBrowserList.count + 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.row == 0) {
        Media *media = [self.videoBrowserList firstObject];
        TMHeaderViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath: indexPath];
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        cell.tittle.text = media.tittle;
        cell.date.text = [media.pubDate formatDate];
       
        NSString *thumbnail = @"";
        if (media.mediaGroup.count) {
            NSDictionary *dictionary = [media.mediaGroup firstObject];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
        }

        
        [cell.videoImageView sd_setImageWithURL:[NSURL URLWithString:thumbnail]
    placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];
        
        return cell;
    } else if (indexPath.row == 1) {
        TMVideoDescTableViewCell *cellDesc = [tableView dequeueReusableCellWithIdentifier:@"descCell" forIndexPath: indexPath];
        Media *media = [self.videoBrowserList firstObject];
        NSString *cation = @"";

        if (media.mediaGroup.count) {
            NSDictionary *dictionary = [media.mediaGroup firstObject];
            cation = [dictionary valueForKey:@"caption"];
        }
        self.caption = cation;
        cellDesc.tittle.text = cation;
        return cellDesc;
    } else if (indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"staticCell" forIndexPath: indexPath];
        UILabel *label =  (UILabel *) [cell.contentView viewWithTag:111];
        label.text = [Constants YOU_MIGHT_ALSO_BE_INTERESTED_IN];
        
    } else {
        TMVideoTableViewCell *vcell = [tableView dequeueReusableCellWithIdentifier:@"videoCell" forIndexPath:indexPath];
       
        Media *media = [self.videoBrowserList objectAtIndex:indexPath.row - 2];
        vcell.tittle.text = media.tittle;
        vcell.date.text = [media.pubDate formatDate];

        NSString *thumbnail = @"";
        if (media.mediaGroup.count) {
            NSDictionary *dictionary = [media.mediaGroup firstObject];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
        }
        
        [vcell.thumbnail setBorder];
        [vcell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                  placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]]
                           options:SDWebImageRefreshCached];
        
        return vcell;
    }
    
    return cell;
}



#pragma UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     Media *media = [self.videoBrowserList firstObject];
    if (indexPath.row == 0) {
       
        UIFont *font = [UIFont systemFontOfSize:19.0f];
        float width = [UIScreen mainScreen].bounds.size.width;
        float aheight = 9.0f;
        float awidth = 16.0f;
        float additionalWidth = [Util expectedHeightFromText:media.tittle withFont:font andMaximumLabelWidth:width - 16.0f]+36;
        return (aheight/awidth * width) + additionalWidth;
    }else if (indexPath.row == 1) {
        
        
        NSString *caption = @"";
        
        if (media.mediaGroup.count) {
            NSDictionary *dictionary = [media.mediaGroup firstObject];
            caption = [dictionary valueForKey:@"caption"];
        }
        
        float additionalWidth = [Util expectedHeightFromText:caption withFont:[UIFont systemFontOfSize:12.0f] andMaximumLabelWidth:[UIScreen mainScreen].bounds.size.width - 16.0f]+16;
        return additionalWidth;
    } else if (indexPath.row == 2)
            return 30.0f;
    else
        return 85.0f;
    
    return 0.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    

    if (indexPath.row == 0) {
        Media *media = [self.videoBrowserList firstObject];
        NSDictionary *dictionary = [media.mediaGroup firstObject];
        NSString *movieUrl = [dictionary valueForKey:@"content"];
        [self playMovieWithURL:movieUrl];
        [[AnalyticManager sharedInstance] trackVideoPlayWithGuid:media.guid andArticleTitle:media.tittle andVideoTitle:media.tittle];
    } else {
        NSUInteger indexSelected = (indexPath.row - 2) + self.index;
        if (indexSelected > self.videoList.count - 1) {
            indexSelected = indexSelected - self.videoList.count;
            /*if (i == 0)
                indexSelected = 0;
            else
                indexSelected = i;*/
        }
        NSMutableArray *videosToPass = [NSMutableArray new];
        NSUInteger totalItemsToPass = 0;
        NSUInteger lastIndex = (int) indexSelected + 6;
        NSUInteger excessIndex =  lastIndex - (self.videoList.count - 1);
        if (indexSelected == self.videoList.count - 1) {
            totalItemsToPass = 1;
        } else {
            if (lastIndex > self.videoList.count - 1) {
                lastIndex -= excessIndex;
            }
            totalItemsToPass = (self.videoList.count ) - indexSelected;
        }
        
        
        if (totalItemsToPass > 1) {
            if (totalItemsToPass > 7) {
                totalItemsToPass = 7;
            }
            
            for (NSUInteger i = 0; i < totalItemsToPass; i++) {
                NSUInteger index = indexSelected + i;
                Media *media = [self.videoList objectAtIndex:index];
                [videosToPass addObject:media];
            }
            
        } else {
            Media *media = [self.videoList objectAtIndex:indexSelected];
            [videosToPass addObject:media];
        }
        
        if (excessIndex > 0) {
            
            for (NSUInteger i = 0; i < excessIndex; i++) {
                if (totalItemsToPass < 7) {
                    Media *media = [self.videoList objectAtIndex:i];
                    [videosToPass addObject:media];
                    
                } else {
                    break;
                }
                totalItemsToPass++;
                
            }
        }
        
        Media *media = [self.videoBrowserList objectAtIndex:indexPath.row-2];
        TMShareVideoViewController *svc = (TMShareVideoViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"shareVideoController"];
        svc.index = indexSelected;
        svc.isfromMain = TRUE;
        svc.videoBrowserList = videosToPass;
        svc.videoList = self.videoList;
        [self.navigationController pushViewController:svc animated:YES];
        [[AnalyticManager sharedInstance] trackVideoPlayWithGuid:media.guid andArticleTitle:media.tittle andVideoTitle:media.tittle];
        
    }
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark Private Functions

-(void)didTapBackbutton{
    if (self.isfromMain){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    //[self.navigationController popToViewController:self.parentViewController animated:YES];
    
   // [self.navigationController popViewControllerAnimated:YES];
  //  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) playMovieWithURL: (NSString *)url
{
    [UINavigationBar appearance].barTintColor = [Util headerColor];
    TMMoviePlayerViewController *player = [[TMMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:url]];
    [self presentMoviePlayerViewControllerAnimated:player];
    [player.moviePlayer play];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self FBShare];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self emailShare];
                    break;
                default:
                    break;
            }break;
    }
        default:
            break;
    }
}

- (void)didPressShare {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Facebook",
                            @"Twitter",
                            @"Email",
                            nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
    
    

}

- (void)FBShare {
    
    
    Media *media = [self.videoList objectAtIndex:self.index];
    NSString *thumbnail= @"";
    NSString *caption = @"";
    NSString *content = @"";
    if(self.videoList.count){
        
        NSArray *mediaGroup = [media.mediaGroup copy];
        if(mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            caption = [dictionary valueForKey:@"caption"];
            content = [dictionary valueForKey:@"content"];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
        }
    }
    
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    NSString *title = [NSString stringWithFormat:@"%@ %@\n\n%@", [Constants SHARE_PRE_TITTLE],caption, content];
    
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:thumbnail]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultDone){
           [[AnalyticManager sharedInstance] trackFacebookShareVideoWithGuId:media.guid andArticleTitle:media.tittle];
        }
    
    };
    [self presentViewController:controller animated:YES completion:Nil];
    
}



- (void)emailShare{
    
    if (self.videoList.count || [self.videoList count]<=self.index) {
        if ([MFMailComposeViewController canSendMail]) {
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
             //if ( self.videoList==nil  || [self.videoList count]<=self.index )
                 //return;
            Media *media = [self.videoList objectAtIndex:self.index];
            NSString *thumbnail = @"";
            NSString *content = @"";
            if (self.videoList.count){
                NSArray *mediaGroup = [media.mediaGroup copy];
                if (mediaGroup.count){
                    NSDictionary *dictionary = [mediaGroup firstObject];
                    thumbnail = [dictionary valueForKey:@"thumbnail"];
                    content = [dictionary valueForKey:@"content"];
                }
            }
            
            
            NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE], [Constants SHARE_TITTLE]];
            NSString *messageBody = [NSString stringWithFormat:@"%@\n%@", media.tittle ,content];
            NSArray *toRecipents = [NSArray arrayWithObject:@""];
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:title];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTranslucent:NO];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            
            
            NSURL *url = [NSURL URLWithString:thumbnail];
            NSData *data = [NSData dataWithContentsOfURL:url];
            [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
            
            
            [self presentViewController:mc animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                        message:@"Nothing to share."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }

}


- (void)TwitterShare {
    Media *media = [self.videoList objectAtIndex:(self.index)];
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    NSString *caption = @"";
    NSString *content = @"";
    if(self.videoList.count){
        NSArray *mediaGroup = [media.mediaGroup copy];
        if(mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            caption = [dictionary valueForKey:@"caption"];
            content = [dictionary valueForKey:@"content"];
        }
    }
    NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE],caption];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:content]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
        if(result == SLComposeViewControllerResultDone){
            [[AnalyticManager sharedInstance] trackTwitterShareVideoWithGuid:media.guid andArticleTitle:media.tittle];
        }
    };
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    if(result == MFMailComposeResultSent){
        Media *media = [self.videoList objectAtIndex:self.index];
        [[AnalyticManager sharedInstance] trackEmailSharePhotoWithGuid:media.guid andArticleTitle:media.tittle];
   }

    [self dismissViewControllerAnimated:YES completion:NULL];
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
