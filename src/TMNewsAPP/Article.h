//
//  Article.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Article : NSObject


@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *enCategory;
@property (nonatomic, strong) NSString *byLine;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *sectionURL;
@property (nonatomic, strong) NSArray *mediagroupList;
@property (nonatomic, strong) NSString *tittle;
@property (nonatomic, strong) NSString *entittle;
@property (nonatomic, strong) NSString *brief;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic, strong) NSString *lastUpdateDate;
@property (nonatomic, strong) NSString *pubDate;
@property (nonatomic, strong) NSString *sectionType;
@property (nonatomic, strong) NSString *sectionName;
@property (nonatomic, strong) NSString *feedlink;
@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) NSArray *related;
@property (nonatomic, strong) NSString *totalNum;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic, strong) NSNumber *downloadTime;
@property (nonatomic, strong) NSString *parentUrl;
@property (nonatomic, strong) NSString *copyright;
@property (nonatomic, strong) NSString *thumbnailz;

@end
