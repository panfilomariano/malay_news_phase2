//
//  TMNewsDetailsPagerViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/12/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPagerController.h"
#import "Article.h"

@protocol TMNewsDetailsPagerViewControllerDelagate <NSObject>
- (void) didTapBookmarkAtIndex: (NSNumber *)index;
@end

typedef enum
{
    Others,
    LatestNewsScreen,
    CAScreen,
    SpecialReportScreen,
    NewsScreen
    
} ScreenType;

@interface TMNewsDetailsPagerViewController : ViewPagerController

@property (nonatomic, strong) NSString *pagerTittle;
@property (nonatomic, strong) NSArray *newsArticles;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, weak) id <TMNewsDetailsPagerViewControllerDelagate> customDelegate;
@property (nonatomic) int index;
@property (nonatomic, strong) NSString *categoryTitle;
@property (nonatomic, assign) BOOL isBookmark;
@property (nonatomic, strong) Article *specialReportArticle;
@property (nonatomic, assign) ScreenType screenType;

@end
