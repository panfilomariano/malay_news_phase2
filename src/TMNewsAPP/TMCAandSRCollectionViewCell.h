//
//  TMCAandSRCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/26/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMCAandSRCollectionViewCellDelagate <NSObject>
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMCAandSRCollectionViewCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *tittle;
@property (nonatomic, weak) IBOutlet UILabel *caption;
@property (nonatomic, weak) IBOutlet UILabel *pubdate;
@property (nonatomic, weak) IBOutlet UIImageView *playImage;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, strong) id <TMCAandSRCollectionViewCellDelagate>delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end
