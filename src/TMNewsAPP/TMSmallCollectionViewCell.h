//
//  TMSmallCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/30/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMSmallCollectionViewCellDelagate <NSObject>
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMSmallCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *tittle;
@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIImageView *playImage;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *categoryHeight;
@property (nonatomic, weak) id <TMSmallCollectionViewCellDelagate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end
