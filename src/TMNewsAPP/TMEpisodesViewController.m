//
//  TMEpisodesViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/8/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMEpisodesViewController.h"
#import "TMCAListTableViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>
//#import "TMDetailsViewController.h"
#import "NSDate+Helpers.h"
#import "TMPhotoBrowserViewController.h"
#import "TMNewsPagesViewController.h"
#import "TMNewsDetailsViewController.h"
#import "LibraryAPI.h"
#import "Constants.h"

@interface TMEpisodesViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *itemArray;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) TMCAListTableViewCell *listViewCell;
@property (nonatomic, strong) TMPhotoBrowserViewController *photoBrowser;
@property (nonatomic, strong) NSMutableArray *itemsDictionary;
@property (nonatomic, strong) NSMutableArray *imageArray;

@end

@implementation TMEpisodesViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    self.itemArray = [[NSMutableArray alloc] init];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self getSectionDetailsWithImage];
    [self getItemDetails];
    
    
    
}



-(void)getItemDetails {
   /* self.itemArray = [[NSMutableArray alloc]init];
    [[APIManager sharedInstance] getCurrentAffairsWithURL:self.sectionUrlApi completion:^(NSError *error, id response) {
        if(!error){
            NSLog(@"response: %@", response);
            
            self.itemsDictionary = [response valueForKey:@"items"];
            for(NSDictionary *dictionary in [response valueForKey:@"items"]){
                NSString *title = [dictionary valueForKey:@"title"];
                NSString *pubDate = [dictionary valueForKey:@"pubDate"];
                NSArray *mediaGroup = [dictionary valueForKey:@"mediaGroup"];
                NSDictionary *dict = [mediaGroup objectAtIndex:0];
                NSString *type = [dict valueForKey:@"type"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
                NSDate *date = [dateFormatter dateFromString:pubDate];
                NSString *formatedDate = [date dateString];
                
                NSString *image = [dict valueForKey:@"thumbnail"];
                
                TMCAList *ca = [TMCAList new];
                ca.titleCA  = title;
                ca.pubDateCA = formatedDate;
                ca.image = image;
                ca.type = type;
                
                
                [self.itemArray addObject:ca];
                
            }
            [self.tableView reloadData];
        }else{
            NSLog(@"error%@", error);
        }
    }];*/
    
    [[LibraryAPI sharedInstance]getArticlesWithURL:self.sectionUrlApi isForced:NO withCompletionBlock:^(NSMutableArray *array) {
        self.itemArray = [array mutableCopy];
        [self.tableView reloadData];
    }];

}

- (void)getSectionDetailsWithImage {
    self.itemArray = [[NSMutableArray alloc] init];
    /*[[APIManager sharedInstance] getCurrentAffairsWithURL:self.sectionUrlApi completion:^(NSError *error, id response) {
        if(!error){
            
            self.imageArray = [response valueForKey:@"section"];
            for(NSDictionary *dict in [response valueForKey:@"section"]){
                NSArray *arrayItems = [dict valueForKey:@"items"];
                NSString *CAsectionUrl = [dict valueForKey:@"sectionUrl"];
                NSDictionary *dictItems = [arrayItems objectAtIndex:0];
                NSString *stringTitle = [dictItems valueForKey:@"title"];
                NSString *pubDate = [dictItems valueForKey:@"pubDate"];
                NSArray *mediaGroup = [dict valueForKey:@"mediaGroup"];
                NSDictionary *mediaDictionary = [mediaGroup objectAtIndex:0];
                NSString *stringImage = [mediaDictionary valueForKey:@"thumbnail"];
                NSString *type = [mediaDictionary valueForKey:@"type"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
                NSDate *date = [dateFormatter dateFromString:pubDate];
                NSString *formatedDate = [date dateString];
                
                TMCAList *list = [TMCAList new];
                list.titleCA = stringTitle;
                list.pubDateCA = formatedDate;
                list.image = stringImage;
                list.type = type;
                list.sectionURL = CAsectionUrl;
                
                [self.itemArray addObject:list];
                
                
            }[self.tableView reloadData];
        }
    }];*/
    
    
    //[LibraryAPI sharedInstance]getSectionArticlesFromURL:self.sectionUrlApi withFeedUrl:<#(NSString *)#> isForced:<#(BOOL)#> withCompletionBlock:<#^(NSMutableArray *array)completionBlock#>

}

#pragma mark TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.itemArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Article *article = (Article *) [self.itemArray objectAtIndex:indexPath.row];
    //TMCAList *list = [self.itemArray objectAtIndex:indexPath.row];
    TMCAListTableViewCell *cell = (TMCAListTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    if([article.type isEqualToString:@"video"]){
        [cell.playOver setHidden:NO];
    }else{
        [cell.playOver setHidden:YES];
        
    }
    
    cell.pubDate.text = article.pubDate;
    cell.titleList.text = article.tittle;
    [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:article.thumbnail]
                    placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index ==  (0) ? SDWebImageRefreshCached : 0];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  80.0f;
}


#pragma mark tableview delegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    
    /*
    NSDictionary *dictionary = [self.imageArray objectAtIndex:indexPath.row];
    NSArray *itemsDict = [dictionary valueForKey:@"items"];
    
    NSDictionary *dict = [itemsDict objectAtIndex:0];
    NSString *sectionURL = [dict valueForKey:@"sectionUrl"];
    NSString *title = [dict valueForKey:@"title"];
    NSArray *mediaGroup = [dict valueForKey:@"mediaGroup"];
    NSDictionary *mediaDictionary = [mediaGroup objectAtIndex:0];
    NSString *type = [mediaDictionary valueForKey:@"type"];
    
    if([type isEqualToString:@"photo"]){
        self.photoBrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
        self.photoBrowser.photoBrowserApi = sectionURL;
        self.photoBrowser.pTitle = title;
        [self.navigationController presentViewController:self.photoBrowser animated:YES completion:nil];
    }else{
        TMNewsDetailsViewController *detailednewsVC = (TMNewsDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetailsID"];
        detailednewsVC.passedArray = self.itemsDictionary;
        detailednewsVC.index = indexPath.row;
        [self.navigationController pushViewController:detailednewsVC animated:YES];
    }
    */
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


















- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
