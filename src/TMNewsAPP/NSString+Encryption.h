//
//  NSString+Encryption.h
//  Cubit
//
//  Created by Wong Johnson on 9/27/12.
//  Copyright (c) 2012 Pace Creative Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Encryption)

- (NSString *)md5;

@end
