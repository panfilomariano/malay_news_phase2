//
//  TMNewsTableViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/15/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMNewsTableViewCell.h"

@implementation TMNewsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark delegate
-(IBAction)didTapStarButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}


- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(8,7,127,71);
    float limgW =  self.imageView.image.size.width;
    if(limgW > 0) {
        self.textLabel.frame = CGRectMake(55,self.textLabel.frame.origin.y,self.textLabel.frame.size.width,self.textLabel.frame.size.height);
        self.detailTextLabel.frame = CGRectMake(55,self.detailTextLabel.frame.origin.y,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);
    }
}



@end
