//
//  TMHeaderViewTableViewCell.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMHeaderViewTableViewCell.h"

@implementation TMHeaderViewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    float width = [UIScreen mainScreen].bounds.size.width;
    float aheight = 9.0f;
    float awidth = 16.0f;
    aheight = (aheight/awidth * width);
    self.imageView.frame = CGRectMake(0, 0, width, aheight);
    /*float limgW =  self.imageView.image.size.width;
     if(limgW > 0) {
     self.textLabel.frame = CGRectMake(55,self.textLabel.frame.origin.y,self.textLabel.frame.size.width,self.textLabel.frame.size.height);
     self.detailTextLabel.frame = CGRectMake(55,self.detailTextLabel.frame.origin.y,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);
     }*/
}

@end
