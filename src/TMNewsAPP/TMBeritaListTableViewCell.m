//
//  TMBeritaListTableViewCell.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/17/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMBeritaListTableViewCell.h"

@implementation TMBeritaListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark delegate
-(IBAction)didTapStarButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBookmarkButtonAtIndexPath:)]) {
        [self.delegate didTapBookmarkButtonAtIndexPath:self.indexPath];
    }
}
@end
