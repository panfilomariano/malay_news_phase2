//
//  Constants.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "Constants.h"
#import "Util.h"

@implementation Constants

#pragma mark Public

+ (NSString *) INDEX_API
{
    return [Constants INDEX_API_LIST] [1];
}

+ (NSString *) URL_BASE
{
#ifdef TAMIL
    return  @"http://seithi.mediacorp.sg/";
#else
    return  @"http://berita.mediacorp.sg/";
#endif
}


+ (NSString *) HEADER_HTMLDATA
{
    if ([Util is_iPad]){
        return
        @"<head>"
        @"<link rel=\"stylesheet\" href=\"http://mobile.mediacorp.com.sg/tmnews/scripts/style_ipad.css\" />"
        @"<script type=\"text/javascript\" src=\"http://mobile.mediacorp.com.sg/tmnews/scripts/desc.js\"></script>"
        @"<script type=\"text/javascript\" src=\"http://mobile.mediacorp.com.sg/tmnews/scripts/picturefill.min.js\"></script>"
        @"</head>";
    }
    return
    @"<head>"
    @"<link rel=\"stylesheet\" href=\"http://mobile.mediacorp.com.sg/tmnews/scripts/style_iphone.css\" />"
    @"<script type=\"text/javascript\" src=\"http://mobile.mediacorp.com.sg/tmnews/scripts/desc.js\"></script>"
    @"<script type=\"text/javascript\" src=\"http://mobile.mediacorp.com.sg/tmnews/scripts/picturefill.min.js\"></script>"
    @"</head>";
}

+ (NSString *) URL_REGULATION
{
    return @"http://www.mediacorp.sg/en/termsofuse";
}

+ (NSString *) URL_PRIVACY
{
    return @"http://www.mediacorp.sg/en/privacypolicy";
}

+ (NSString *) URL_ABOUT
{
#ifdef TAMIL
    return @"http://seithi.mediacorp.sg/mobilet/aboutus";
#endif
    return @"http://berita.mediacorp.sg/mobilem/aboutus";
    
}

+ (NSString *) URL_GLOSSARY
{
    return @"http://www.channel8news.sg/html/app/about";
}

+ (NSString *) APPVERSION_URL
{
#ifdef TAMIL
    return @"http://54.254.105.235/tmnews/notification/APNS/ios_update.json";
#endif
    return @"http://54.254.105.235/mlnews/notification/APNS/ios_update.json";
}

+ (NSString *)  ITUNES_APP_URL
{
#ifdef TAMIL
    return @"https://itunes.apple.com/sg/app/id982161322";
#endif
    return @"https://itunes.apple.com/sg/app/id982151128";
}

+ (NSString *) PUSHNOTIF_URL
{
#ifdef TAMIL
    return @"http://54.254.105.235/tmnews/notification/APNS/apns.php?";
#endif
    return @"http://54.254.105.235/mlnews/notification/APNS/apns.php?";
}

+ (NSString *) WEATHER_URL
{
    return @"http://mobile-ss.mediacorp.com.sg/CNA/json/weather_sg.json";
}

+ (NSString *) MALAY_RADIO_WARNA_URL
{
    return @"http://mediacorp.rastream.com/942fm.iphone";
}
+ (NSString *) MALAY_RADIO_RIA_URL
{
    return @"http://mediacorp.rastream.com/897fm.iphone";
}
+ (NSString *) TAMIL_RADIO_OLI_URL
{
    return @"http://mediacorp.rastream.com/968fm.iphone";
}

+ (NSString *) COMSCORE_APPNAME
{
#ifdef TAMIL
    return @"Seithi MediaCorp";
#endif
    return @"Berita MediaCorp";
}

+ (NSArray *) MENU_TAGGING
{
    NSArray *array = [[NSArray alloc] initWithObjects:@"latest news",
                      @"news",
                      @"ca",
                      @"photos",
                      @"videos",
                      nil];
    return  array;
}

+ (NSArray *) TAB_TITTLES
{
    if ([Util is_iPad]) {
#ifdef TAMIL
        NSArray *array = [[NSArray alloc] initWithObjects:@"புதிய செய்தி",
                          @"செய்தி",
                          @"நடப்பு விவகாரம்",
                          @"படங்கள்",
                          @"காணொளி",
                          nil];
        return  array;
#else
        NSArray *array = [[NSArray alloc] initWithObjects:@"TERKINI",
                          @"BERITA",
                          @"EHWAL SEMASA",
                          @"GAMBAR",
                          @"VIDEO",
                          nil];
        return  array;
#endif
    } else {
#ifdef TAMIL
        NSArray *array = [[NSArray alloc] initWithObjects:@"புதிய செய்தி",
                          @"LIVE STREAMING",
                          @"செய்தி",
                          @"நடப்பு விவகாரம்",
                          @"படங்கள்",
                          @"காணொளி",
                          nil];
        return  array;
#else
        NSArray *array = [[NSArray alloc] initWithObjects:@"TERKINI",
                          @"LIVE STREAMING",
                          @"BERITA",
                          @"EHWAL SEMASA",
                          @"GAMBAR",
                          @"VIDEO",
                          nil];
        return  array;
#endif

    }

    
}

//START ---- swa added here. please handle

+ (NSString *) CONTACT_US_EMAIL_TO
{
#ifdef TAMIL
    return  @"mcntamilnews@mediacorp.com.sg";
#else
    return @"berita@mediacorp.com.sg";
#endif
}
+ (NSString *) CONTACT_US_EMAIL_SUBJECT
{
#ifdef TAMIL
    return  @"Contact Seithi MediaCorp";
#else
    return @"Hubungi Berita MediaCorp";
#endif
}

+ (NSString *) CONTACT_US_EMAIL_MESSAGE
{
    return @"Description:\n\n\nName：\nMobile Number：\n\n\n";
}

//message to display in dialog before start download articles
+ (NSString *) DOWNLOAD_PROMPT
{
#ifdef TAMIL
    return  @"Depending on the data plan you use, mobile operator charges may apply. Continue?";
#else
    return @"Depending on the data plan you use, mobile operator charges may apply. Continue?";
#endif
}

//toast message when start radio
+ (NSString *) PLAY_RADIO
{
#ifdef TAMIL
    return  @"Data charges may apply. To switch to Wi-Fi connection.";
#else
    return @"Data charges may apply. To switch to Wi-Fi connection.";
#endif
}

+ (NSString *) XPHOTO
{
#ifdef TAMIL
    return  @"photo";
#else
    return @"gambar";
#endif
}

+ (NSString *) XPHOTOS
{
#ifdef TAMIL
    return  @"photos";
#else
    return @"gambar";
#endif
}

+ (NSString *) PUSH_HEADER
{
#ifdef TAMIL
    return  @"சுடச்சுட";
#else
    return @"Berita Terkini";
#endif
}

+ (NSString *) NO_SEARCH_RESULT
{
#ifdef TAMIL
    return @"எதுவும் கிடைக்கவில்லை";
#endif
    return @"Tiada";
}

+ (NSString *) NO_BOOKMARK_ARTICLES
{
#ifdef TAMIL
    return @"விருப்பக் கட்டுரை எதுவும் கிடையாது. விருப்பமான கட்டுரைகள் இங்கு இடம்பெறும்.";
#endif
    return @"Tiada artikel dalam simpanan.";
}

+ (NSString *) ADD_TO_FAV
{
#ifdef TAMIL
    return @"விருப்பப் பட்டியலில் சேர்க்கப்பட்டுள்ளது. 3 நாள் சேமித்துவைக்கப்படும";
#endif
    return @"Artikel disimpan selama 3 hari.";
}

+ (NSString *) REMOVE_FROM_FAV
{
#ifdef TAMIL
    return @"விருப்பப் பட்டியலில் இருந்து அகற்றவும";
#endif
    return @"Keluarkan";
}
//END ---- swa added here. please handle

+ (NSString *) LATEST_NEWS_PHOTOS
{
#ifdef TAMIL
    return  @"அண்மைச் செய்திப் படம்";
#else
    return  @"GAMBAR BERITA TERKINI";
#endif
}

+ (NSString *) LATEST_PHOTO_GALLERIES
{
#ifdef TAMIL
    return  @"அண்மைப் படத்தொகுப்பு";
#else
    return  @"GALERI GAMBAR";
#endif
}

+ (NSString *)TOP5_NEWS
{
#ifdef TAMIL
    return  @"அதிகம் வாசிக்கப்பட்டவை";
#else
    return  @"PALING POPULAR";
#endif
}

+ (NSString *) LATEST_EPISODE
{
#ifdef TAMIL
    return  @"அண்மை பாகங்கள்";
#else
    return  @"EPISOD TERKINI";
#endif
}

+ (NSString *) YOU_MIGHT_ALSO_BE_INTERESTED_IN
{

#ifdef TAMIL
    return  @"செய்தியில் மேலும்";
#else
    return  @"ANDA MUNGKIN JUGA BERMINAT DENGAN..";
#endif

}

+ (NSString *) MORE_FROM_NEWS
{
#ifdef TAMIL
    return @"தமிழ்ச் செய்தியில் மேலும்";
#else
    return @"Lebih banyak dari Berita";
#endif
}

+ (NSString *) LANDING_BREAKING_NEWS
{
#ifdef TAMIL
    return  @"சுடச்சுட";
#else
    return @"TERKINI";
#endif
}

+ (NSString *) OK_DIALOG_MESSAGE
{
#ifdef TAMIL
    return @"Ok";
#else
    return @"Ok";
#endif
}

+(NSString *) CLOSE_DIALOG_MESSAGE
{
#ifdef TAMIL
    return @"Close";
#else
    return @"Close";
#endif
}

+ (NSString *) OPEN_DIALOG_MESSAGE
{
#ifdef TAMIL
    return @"Open";
#else
    return @"Open";
#endif
}

+ (NSString *) SEARCH_PLACEHOLDER
{
#ifdef TAMIL
    return  @"தேடுக";
#else
    return @"Kata Kunci";
#endif
    
}

+ (NSString *) SEARCH_RESULTS
{
    
#ifdef TAMIL
    return  @"தேடலில் கிடைத்தது";
#else
    return @"HASIL CARIAN";
#endif
}

+ (NSString *) WEATHER_24HR_PSI
{
    return @"24-hr PSI:";
}

+ (NSString *) WEATHER_3HR_PSI
{
    return @"3-hr PSI:";
}

+ (NSString *) SETTING_CONTACT_US
{
#ifdef TAMIL
    return  @"தொடர்பு கொள்க";
#else
    return @"Hubungi Kami";
#endif
}
+ (NSString *) SETTING_SUBMIT_NEWS
{
#ifdef TAMIL
    return  @"ெய்தியை அனுப்ப";
#else
    return @"Sumbangkan Berita";
#endif
}
+ (NSString *) SETTING_ABOUT_US
{
#ifdef TAMIL
    return  @"எங்களைப் பற்றி";
#else
    return @"Mengenai Kami";
#endif
}

+ (NSString *) SETTING_PRIVACY_POLICY
{
#ifdef TAMIL
    return  @"பாதுகாப்புக் கொள்கை";
#else
    return @"Privasi";
#endif
}

+ (NSString *) SETTING_REGULATIONS
{
#ifdef TAMIL
    return  @"விதிமுறைகளும் நிபந்தனைகளும்";
#else
    return @"Syarat dan Penggunaan";
#endif
}

+ (NSString *) SETTING_FONT_SIZE
{
#ifdef TAMIL
    return  @"எழுத்து அளவ";
#else
    return @"Saiz Perkataan";
#endif
}

+ (NSString *) SETTING_VERSION
{
#ifdef TAMIL
    return  @"Version";
#else
    return @"Versi";
#endif
}

+ (NSString *) SETTING_GLOSSARY
{
#ifdef TAMIL
    return  @"சொற்களஞ்சியம்";
#else
    return @"Glossary";
#endif
}

+ (NSString *) SETTINGS_DOWNLOAD_ALL
{
#ifdef TAMIL
    return  @"அனைத்தையும் பதிவிறக்கம் செய்ய";
#else
    return @"Muat Turun Berita";
#endif
}

+ (NSString *) SETTINGS_DOWNLOADING
{
    return @"Downloading...";
}

+ (NSString *) SETTINGS_DOWNLOAD_COMPLETED
{
    return @"Download completed.";
}

+ (NSString *) SETTINGS_DOWNLOAD_OPTION1
{
#ifdef TAMIL
    return  @"அனைத்தையும் பதிவிறக்கம் செய்ய";
#else
    return @"Muat Turun Berita (Tanpa Gambar)";
#endif
}

+ (NSString *) SETTINGS_DOWNLOAD_OPTION2
{
#ifdef TAMIL
    return @"படங்கள் இல்லாமல் பதிவிறக்கம் செய்ய";
#else
    return @"Muat Turun Berita (Dengan Gambar)";
#endif
}

+ (NSString *) SETTINGS_SUBMIT_NEWS
{
#ifdef TAMIL
    return  @"செய்தியை அனுப்ப";
#else
    return @"Sumbangkan Berita";
#endif
}

+ (NSString *) SETTINGS_SUBMIT_NEWS_PHOTOS_OR_VIDEOS
{
#ifdef TAMIL
    return  @"படம், வீடியோவுடன் செய்தியை அனுப்ப.";
#else
    return @"Sumbangkan Berita Dengan Gambar/Video";
#endif
}

+ (NSString *) SETTINGS_FONTSIZE_XSMALL
{
    return @"X-Small";
}

+ (NSString *) SETTINGS_FONTSIZE_SMALL
{
    return @"Small";
}

+ (NSString *) SETTINGS_FONTSIZE_MEDIUM
{
    return @"Medium";
}

+ (NSString *) SETTINGS_FONTSIZE_LARGE
{
    return @"Large" ;
}

+ (NSString *) SETTINGS_FONTSIZE_XLARGE
{
    return @"X-large";
}

+ (NSString *) SUBMIT_NEWS_EMAIL_TO
{
#ifdef TAMIL
    return @"mcntamilnews@mediacorp.com.sg";
#else
    return @"berita@mediacorp.com.sg";
#endif
}
+ (NSString *) SUBMIT_NEWS_EMAIL_SUBJECT
{
#ifdef TAMIL
    return  @"செய்தியை அனுப்ப";
#else
    return @"Sumbangkan Berita";
#endif
}

+ (NSString *) SUBMIT_NEWS_EMAIL_MESSAGE
{
    return @"Description:\n\n\nName：\nMobile Number：\n\n\n";
}

+ (NSString *) DIALOG_NETWORK_MESSAGE
{
    return @"No network. You can continue reading offline.";
    
}

+ (NSString *) SHARE_TITTLE
{
    return @"Share";
}

+ (NSString *) SHARE_PRE_TITTLE
{
#ifdef TAMIL
    return @"Seithi MediaCorp:";
#endif
    return @"Berita MediaCorp:";
}

+ (NSString *) NEWS_LISTING_TITTLE
{
#ifdef TAMIL
    return  @"செய்தி";
#else
    return  @"Berita";
#endif
}
+ (NSString *) MENU_SEARCH
{
#ifdef TAMIL
    return  @"தேடுக";
#else
    return  @"Cari";
#endif
}

+ (NSString *) MENU_BOOKMARKS
{
#ifdef TAMIL
    return  @"குறியீடு";
#else
    return  @"Simpan";
#endif
}

+ (NSString *) PLACEHOLDER
{
#ifdef TAMIL
    return @"placeholder_iphone_small_Tamil";
#endif
    return @"placeholder_iphone_small";
}

+ (NSString *) PLACEHOLDER_BIG
{
#ifdef TAMIL
    return @"placeholder_iphone_big_Tamil";
#endif
    return @"placeholder_iphone_big";
}

+ (NSString *) LATEST_NEWS
{
    return @"latest news";
}

+ (NSString *) LATEST_SECTION_NEWS
{
    return @"latest section news";
}

+ (NSString *) MOST_POPULAR
{
    return @"most popular";
}

+ (NSString *) TOP_NEWS
{
    return @"top news";
}

+ (NSString *) SINGAPORE
{
    return @"singapore";
}

+ (NSString *) WORLD
{
    return @"world";
}

+ (NSString *) BUSINESS
{
    return @"business";
}

+ (NSString *) SPECIAL_REPORT
{
    return @"special report";
}

+ (NSString *) SPORT
{
    return @"sport";
}

+ (NSString *) LIFE_STYLE
{
    return @"lifestyle";
}

+ (NSString *) ENTERTAINMENT
{
    return @"entertainment";
}

+ (NSString *) BREAKING_NEWS
{
    return @"Terkini";
}

+ (NSString *) ADVERTORIAL
{
    return @"advertorial";
}

+ (NSString *) LATEST_EPISODES
{
    return @"latest episodes";
}

+ (NSString *) VIDEOS
{
    return @"videos";
}

+ (NSString *) CA
{
    return @"ca";
}

+ (NSString *) PHOTOS
{
    return @"photos";
}

+ (NSString *) NEWS_TYPE
{
    return @"news";
}

+ (NSString *) OTHERS_TYPE
{
    return @"others";
}

+ (NSString *) SPECIAL_REPORT_TYPE
{
    return @"special report";
}

+ (NSString *) PHOTOS_TYPE
{
    return @"photos";
}

+ (NSString *) VIDEOS_TYPE
{
    return @"videos";
}

+ (NSString *) CA_TYPE
{
    return @"current affair";
}

+ (NSString *) EPISODE_TYPE
{
    return @"episode";
}

+ (NSString *) HTML_TYPE
{
    return @"html";
    
}

+ (NSString *) NEWS_KEY
{
    return @"NEWS_KEY";
}

+ (NSString *) TOP_NEWS_KEY
{
    return @"TOP_NEWS_KEY";
}
+ (NSString *) TOP_FIVE_NEWS_KEY
{
    return @"TOP_FIVE_NEWS_KEY";
}

+ (NSString *) HAS_BREAKING_NEWS_KEY
{
    return @"HAS_BREAKING_NEWS_KEY";
}

+ (NSString *) RADIO_CURRENT_STATION_KEY
{
    return @"RADIO_CURRENT_STATION_KEY";
}

+ (NSString *) LATEST_NEWS_KEY
{
    return @"LATEST_NEWS_KEY";
}

+ (NSString *) ADVERTORIAL_KEY
{
    return @"ADVERTORIAL_KEY";
}


+ (NSString *) ARTICLES_KEY
{
    return @"ARTICLES_KEY";
}

+ (NSString *) INDEX_ITEM_LIST_KEY
{
    return @"INDEX_ITEM_LIST_KEY";
}

+ (NSString *) CA_KEY
{
    return @"CA_KEY";
}

+ (NSString *) PHOTOS_KEY
{
    return @"PHOTOS_KEY";
}

+ (NSString *) VIDEOS_KEY
{
    return @"VIDEOS_KEY";
}

+ (NSString *) MEDIA_KEY
{
    return @"MEDIA_KEY";
}

+ (NSString *) BOOKMARKS_KEY
{
    return @"BOOKMARKS_KEY";
}
+ (NSString *) WEATHER_KEY
{
    return @"WEATHER_KEY";
}

+ (double) BOOKMARKS_EXP_LENGHT
{
    return 129600;
}

+ (double) DOWNLOAD_EXP_LENGHT
{
    return 300.0;
}
+ (double) TOP_5_NEWS_DOWNLOAD_EXP_LENGHT
{
    return 1800.0;
}
+ (double) WEATHER_EXP_LENGHT
{
    return 300.0;
}

+ (double) BREAKING_NEWS_REQUEST_INTERVAL
{
    return 300.0;
}
+ (double) INDEX_LIST_REQUEST_INTERVAL
{
    return 60.0;
}

+ (NSString *) FB_APP_ID
{
    return @"639419456190863";
}

+ (NSString *) OUTBRAIN_WIDGET_ID
{
    return @"SDK_1";
}

+ (NSString *) ADMOB_UNITID_SPLASHAD
{
#ifdef TAMIL
    return @"/4654/TamilNews/splash-ios";
#else
    return @"/4654/MalayNews/splash-ios";
#endif
}

+ (NSString *) ADMOB_UNITID_BANNERAD
{
#ifdef TAMIL
    return @"/4654/TamilNews/IOSAPP-banner";
#else
    return @"/4654/MalayNews/IOSAPP-banner";
#endif
}

+ (NSString *) BREAKING_NEWS_GUID
{
    return @"BREAKING_NEWS_GUID";
}

#pragma mark Private

+ (NSArray *) INDEX_API_LIST
{
#ifdef TAMIL
    return [NSArray arrayWithObjects:@"http://beta.tamilseithi.mediacorp.sg/tmnewsfeeds/1170192/index.json",
            @"http://seithi.mediacorp.sg/tmnewsfeeds/1516600/index.json",
            nil];
#else
    return [NSArray arrayWithObjects:@"http://beta.berita.mediacorp.sg/tmnewsfeeds/1025642/index.json",
            @"http://berita.mediacorp.sg/tmnewsfeeds/1517136/index.json",
            nil];
#endif
    
}































@end
