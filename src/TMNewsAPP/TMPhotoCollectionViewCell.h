//
//  TMPhotoCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/10/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMPhotoCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *keepingCountLabel;

@end
