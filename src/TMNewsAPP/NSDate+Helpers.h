//
//  NSDate+Helpers.h
//  Cubit
//
//  Created by Wong Johnson on 10/19/12.
//  Copyright (c) 2012 Pace Creative Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate(Helpers)

- (NSString *)dateStringByISO8601;
- (NSString *)dateString;

@end
