//
//  MobileBDConfigs.h
//  MobileBDSDK
//
//  Created by Le Vu on 18/3/15.
//  Copyright (c) 2015 Knorex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobileBDConfigs : NSObject

@property (nonatomic) BOOL logEnabled;

+ (MobileBDConfigs *)sharedConfigs;

- (NSString *)sdkVersion;
- (NSString *)googleSdkVersion;

@end
