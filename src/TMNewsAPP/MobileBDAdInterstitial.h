//
//  KNXAdInterstitial.h
//  TheNationMobile
//
//  Created by Le Vu on 10/12/14.
//  Copyright (c) 2014 Knorex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MobileBDAdInterstitialDelegate.h"

@interface MobileBDAdInterstitial : NSObject

@property (nonatomic, strong) id<MobileBDAdInterstitialDelegate> delegate;
@property (nonatomic, strong) NSString *adUnitId;
@property (nonatomic, strong) UIViewController *presentViewController;

- (void)loadAdInterstitialFromAdUnitId:(NSString *)adUnitId;
- (void)show;

@end
