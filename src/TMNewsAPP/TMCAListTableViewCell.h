//
//  TMCAListTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/19/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCAListTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *titleList;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UIImageView *playOver;


@end
