//
//  DataManager.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/10/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "DataManager.h"
#import "AFNetworking.h"
#import "Weather.h"

@implementation DataManager

- (id) init {
    
    if (self=[super init]) {
        
        _indexItemList = [[NSArray alloc] init];
        _articles = [[NSMutableArray alloc] init];
        _latestNews = [[NSMutableArray alloc] init];
        _feedNewsItems = [[NSMutableArray alloc] init];
        _topNews = [[NSMutableArray alloc] init];
        _caNewsItems = [[NSMutableArray alloc] init];
        _photosItems = [[NSMutableArray alloc] init];
        _mediaItems = [[NSMutableArray alloc] init];
        _videosItems = [[NSMutableArray alloc] init];
        _bookmarks = [[NSMutableArray alloc] init];
        _topFiveNews = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark Public Functions
- (Article *) articleFromDictioary: (NSDictionary *)dictionary andFeedlink: (NSString *)feedLink
{
    NSDate *date = [NSDate date];
    double downloadDateInDouble = [date timeIntervalSince1970];
    NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
    Article *article = [Article new];
    article.category = [dictionary valueForKey:@"category"];
    article.enCategory = [dictionary valueForKey:@"enCategory"];
    article.byLine = [dictionary valueForKey:@"byline"];
    article.type = [dictionary valueForKey:@"type"];
    article.sectionURL = [dictionary valueForKey:@"sectionURL"];
    article.mediagroupList = [[dictionary objectForKey:@"mediaGroup"] copy];
    article.tittle = [dictionary valueForKey:@"title"];
    article.entittle = [dictionary valueForKey:@"enTitle"];
    article.brief = [dictionary valueForKey:@"brief"];
    article.link = [dictionary valueForKey:@"link"];
    article.desc = [dictionary valueForKey:@"description"];
    article.guid = [dictionary valueForKey:@"guid"];
    article.lastUpdateDate = [dictionary valueForKey:@"lastUpdateDate"];
    article.pubDate = [dictionary valueForKey:@"pubDate"];
    article.sectionType = [dictionary valueForKey:@"sectionType"];
    article.sections = [[dictionary objectForKey:@"sections"] copy];
    article.related = [[dictionary objectForKey:@"related"] copy];
    article.feedlink = feedLink;
    article.sectionName = [dictionary valueForKey:@"name"];
    article.thumbnail = [dictionary valueForKey:@"thumbnail"];
    article.downloadTime = downloadDateInNumber;
    article.totalNum = [dictionary valueForKey:@"totalNum"];
    article.copyright = [dictionary valueForKey:@"copyright"];
    
    return article;
    
}
- (Article *) articleFromDictioary: (NSDictionary *)dictionary withParentUrl:(NSString *)url andFeedlink: (NSString *)feedLink
{
    NSDate *date = [NSDate date];
    double downloadDateInDouble = [date timeIntervalSince1970];
    NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
    Article *article = [Article new];
    article.category = [dictionary valueForKey:@"category"];
    article.enCategory = [dictionary valueForKey:@"enCategory"];
    article.byLine = [dictionary valueForKey:@"byline"];
    article.type = [dictionary valueForKey:@"type"];
    article.sectionURL = [dictionary valueForKey:@"sectionURL"];
    article.mediagroupList = [[dictionary objectForKey:@"mediaGroup"] copy];
    article.tittle = [dictionary valueForKey:@"title"];
    article.entittle = [dictionary valueForKey:@"enTitle"];
    article.brief = [dictionary valueForKey:@"brief"];
    article.link = [dictionary valueForKey:@"link"];
    article.desc = [dictionary valueForKey:@"description"];
    article.guid = [dictionary valueForKey:@"guid"];
    article.lastUpdateDate = [dictionary valueForKey:@"lastUpdateDate"];
    article.pubDate = [dictionary valueForKey:@"pubDate"];
    article.sectionType = [dictionary valueForKey:@"sectionType"];
    article.sections = [[dictionary objectForKey:@"sections"] copy];
    article.related = [[dictionary objectForKey:@"related"] copy];
    article.feedlink = feedLink;
    article.sectionName = [dictionary valueForKey:@"name"];
    article.thumbnail = [dictionary valueForKey:@"thumbnail"];
    article.downloadTime = downloadDateInNumber;
    article.parentUrl = url;
    article.totalNum = [[dictionary valueForKey:@"totalNum"] stringValue];
    article.copyright = [dictionary valueForKey:@"copyright"];
    return article;
    
}

- (Article *) feedArticleFromDictioary: (NSDictionary *)dictionary
{
    NSDate *date = [NSDate date];
    double downloadDateInDouble = [date timeIntervalSince1970];
    NSNumber *downloadDateInNumber = [NSNumber numberWithDouble:downloadDateInDouble];
    Article *article = [Article new];
    article.category = [dictionary valueForKey:@"category"];
    article.enCategory = [dictionary valueForKey:@"enCategory"];
    article.byLine = [dictionary valueForKey:@"byline"];
    article.type = [dictionary valueForKey:@"type"];
    article.sectionURL = [dictionary valueForKey:@"sectionURL"];
    article.mediagroupList = [[dictionary objectForKey:@"mediaGroup"] copy];
    article.tittle = [dictionary valueForKey:@"title"];
    article.entittle = [dictionary valueForKey:@"enTitle"];
    article.brief = [dictionary valueForKey:@"brief"];
    article.link = [dictionary valueForKey:@"link"];
    article.desc = [dictionary valueForKey:@"description"];
    article.guid = [dictionary valueForKey:@"guid"];
    article.lastUpdateDate = [dictionary valueForKey:@"lastUpdateDate"];
    article.pubDate = [dictionary valueForKey:@"pubDate"];
    article.sectionType = [dictionary valueForKey:@"sectionType"];
    article.sections = [[dictionary objectForKey:@"sections"] copy];
    article.related = [[dictionary objectForKey:@"related"] copy];
    article.sectionName = [dictionary valueForKey:@"name"];
    article.thumbnail = [dictionary valueForKey:@"thumbnail"];
    article.downloadTime = downloadDateInNumber;
    article.totalNum = [[dictionary valueForKey:@"totalNum"] stringValue];
    article.copyright = [dictionary valueForKey:@"copyright"];
    return article;
    
}

- (Weather *) weatherFromDictionary: (NSDictionary *) dictionary {
    
    NSDate *date = [NSDate date];
    double lastBuildDateInDouble = [date timeIntervalSince1970];
    NSNumber *lastBuildDateInNumber = [NSNumber numberWithDouble:lastBuildDateInDouble];
    double lastDownloadDateInDouble = [date timeIntervalSince1970];
    NSNumber *lastDownloadDateInNumber = [NSNumber numberWithDouble:lastDownloadDateInDouble];
    Weather *weather = [Weather new];
    weather.period = [dictionary valueForKey:@"period"];
    weather.min = [dictionary valueForKey:@"min"];  
    weather.max = [dictionary valueForKey:@"max"];
    weather.rh = [dictionary valueForKey:@"rh"];
    weather.psi = [dictionary valueForKey:@"psi"];
    weather.psi_3h = [dictionary valueForKey:@"psi_3h"];
    weather.psi_24h = [dictionary valueForKey:@"psi_24h"];
    weather.tide = [dictionary valueForKey:@"tide"];
    weather.condition = [dictionary valueForKey:@"condition"];
    weather.time = [dictionary valueForKey:@"time"];
    weather.thumbnail = [dictionary valueForKey:@"thumbnail"];
    weather.lastBuildDate = lastBuildDateInNumber;
    weather.lastDownloadDate = lastDownloadDateInNumber;
    
    return weather;
}



#pragma mark Private Functions

@end
