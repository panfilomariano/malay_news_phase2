//
//  TMVideosViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/8/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMVideosViewController.h"
#import "TMVideosViewController.h"
#import "TMVideoTableViewCell.h"
#import "TMWebViewController.h"
#import "TMShareVideoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Helpers.h"
#import "MBProgressHUD.h"
#import "LibraryAPI.h"
#import "Media.h"
#import "MBProgressHUD.h"
#import "NSString+Helpers.h"
#import "UIImageView+Helpers.h"
#import "Util.h"
#import "TMVideoListCollectionViewCell.h"
#import "TMiPADVideoDetailsViewController.h"
#import "Constants.h"
#import "AnalyticManager.h"
#import "AppDelegate.h"

@interface TMVideosViewController () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, GADBannerViewDelegate, GADAdSizeDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *videoArray;

@end

@implementation TMVideosViewController

- (void)viewWillAppear:(BOOL)animated
{
    [self loadVideosWithForce:NO];
    if (self.isFromMain){
        [[AnalyticManager sharedInstance] trackVideoTab];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([Util is_iPad]){
        self.bannerHeight.constant = 0;
    }else {
        self.bannerHeight.constant = 50;
    }
    
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:self action:@selector(reloadDataFeed:) forControlEvents:UIControlEventValueChanged];
   
    if ([Util is_iPad]) {
        [self.collectionView addSubview:refreshFeed];
    } else {
         [self.tableView addSubview:refreshFeed];
    }

    [self setUpads];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark tableview data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.videoArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TMVideoTableViewCell *cell = (TMVideoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"videoCell" forIndexPath:indexPath];
    
    NSString *thumbnail = @"";
    if (self.videoArray.count) {
        Media *media = (Media *) [self.videoArray objectAtIndex:indexPath.row];
        cell.tittle.text = media.tittle;
        cell.date.text = [media.pubDate formatDate];
        NSArray *mediaGroup = [media.mediaGroup copy];
        if (mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
        }
    }
    [cell.thumbnail setBorder];
    
    [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                            placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];

    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  86.0f;
}

#pragma mark uitableview delegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Media *media = [self.videoArray objectAtIndex:indexPath.row];
    NSMutableArray *videosToPass = [NSMutableArray new];
    NSUInteger totalItemsToPass = 0;
    NSUInteger lastIndex = (int) indexPath.row + 6;
    NSUInteger excessIndex =  lastIndex - (self.videoArray.count - 1);
    
    if (indexPath.row == self.videoArray.count - 1) {
        totalItemsToPass = 1;
    } else {
        if (lastIndex > self.videoArray.count - 1) {
            lastIndex -= excessIndex;
        }
        totalItemsToPass = (self.videoArray.count ) - indexPath.row;
    }
    
    
    if (totalItemsToPass > 1) {
        if (totalItemsToPass > 7) {
            totalItemsToPass = 7;
        }
        
        for (NSUInteger i = 0; i < totalItemsToPass; i++) {
            NSUInteger index = indexPath.row + i;
            Media *media = [self.videoArray objectAtIndex:index];
            [videosToPass addObject:media];
        }
    } else {
        [videosToPass addObject:media];
    }
    
    if (excessIndex > 0) {
        for (NSUInteger i = 0; i < excessIndex; i++) {
            if (totalItemsToPass < 7) {
                if (self.videoArray.count > 1) {
                    if (i < self.videoArray.count) {
                        Media *media = [self.videoArray objectAtIndex:i];
                        [videosToPass addObject:media];
                    }
                }
            } else {
                break;
            }
            totalItemsToPass++;
        }
    }
    
    [UINavigationBar appearance].barTintColor = [Util headerColor];
    
    TMShareVideoViewController *svc = (TMShareVideoViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"shareVideoController"];
    svc.index = indexPath.row;
    svc.isfromMain = FALSE;
    svc.videoBrowserList = videosToPass;
    svc.videoList = self.videoArray;
    if (self.isFromSpecialReport){
        [[AnalyticManager sharedInstance] trackVideosWithSpecialReports:self.videosEksklusifTitle andGuid:media.guid andvideoTitle:media.tittle];
    }else {
        [[AnalyticManager sharedInstance] trackVideoPlayWithGuid:media.guid andArticleTitle:media.tittle andVideoTitle:media.tittle];
    }
    
    [self.navigationController pushViewController:svc animated:YES];
    
}

#pragma mark CollectionView datasource

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f blue:237/255.0f alpha:.5];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    float rW = 224.0f;
    float rH = 137.0f;

    float width = (768 / 3) - 11;
    if (!UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        width = (1024 / 4) - 11;
    }
    
    float height = (rH / rW * width) + 115;
    return CGSizeMake(width, height);
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.videoArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    float width = 441;
    CGSize titleSize = CGSizeMake(width - 24, 70);
    
    TMVideoListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSString *thumbnail = @"";
    if (self.videoArray.count) {
        Media *media = (Media *) [self.videoArray objectAtIndex:indexPath.row];
        //float pubLabelWidth = [Util expectedWidthFromText:[media.pubDate formatDate] withFont:[UIFont fontWithName:@"HelveticaNeue" size:13] andMaximumLabelHeight:15];
        //float titleHeight = [Util expectedHeightFromText:media.tittle withFont:[UIFont systemFontOfSize:27] andMaximumLabelSize:titleSize];
        //[cell.pubDate setFrame:CGRectMake(titleHeight + cell.descLabel.frame.origin.x +3, 17.5, pubLabelWidth, 15)];
        cell.descLabel.text = media.tittle;
        cell.pubDate.text = [media.pubDate formatDate];
        NSArray *mediaGroup = [media.mediaGroup copy];
        
        if (mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
        }
        
    }
    
    [cell.thumbnail setBorder];
    [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                      placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:index == /* DISABLES CODE */ (0) ? SDWebImageRefreshCached : 0];
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(9, 9, 9, 9);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 4.5f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.0f;
}

#pragma mark CollectionView delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Media *media = [self.videoArray objectAtIndex:indexPath.row];
    NSMutableArray *videosToPass = [NSMutableArray new];
    int itemCount =[self.videoArray count];
    int numberofSuggestions = 0;
    if(itemCount>6){
        numberofSuggestions = 6;
    }else{
        numberofSuggestions = itemCount-1;
    }
    if(itemCount>1){
        if(itemCount >= indexPath.row+numberofSuggestions){
            int ctr = 0;
            while(ctr<=numberofSuggestions){
                [videosToPass addObject:[self.videoArray objectAtIndex:indexPath.row +ctr]];
                ctr++;
            }
        }else{
            int ctr = (indexPath.row +numberofSuggestions)-itemCount+1;
            int index = 0;
            while(ctr<=numberofSuggestions){
                [videosToPass addObject:[self.videoArray objectAtIndex:indexPath.row +index]];
                ctr++;
                index++;
            }
            
            int extraindex = (indexPath.row+numberofSuggestions)-itemCount+1;
            int extraCtr = 0;
            while(extraindex<=(indexPath.row +numberofSuggestions)-itemCount+1 && extraindex!=0){
                [videosToPass addObject:[self.videoArray objectAtIndex:0+extraCtr]];
                extraindex--;
                extraCtr++;
            }
        }
    }else{
        [videosToPass addObject:[self.videoArray objectAtIndex:indexPath.row ]];
    }
    
    TMiPADVideoDetailsViewController *vdc = (TMiPADVideoDetailsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videoDetailsVC"];
    vdc.index = indexPath.row;
    vdc.isFromMain = FALSE;
    vdc.videoBrowserList = videosToPass;
    vdc.videoList = self.videoArray;
    if (self.isFromSpecialReport){
        [[AnalyticManager sharedInstance] trackVideosWithSpecialReports:self.videosEksklusifTitle andGuid:media.guid andvideoTitle:media.tittle];
    }else {
        [[AnalyticManager sharedInstance] trackVideoPlayWithGuid:media.guid andArticleTitle:media.tittle andVideoTitle:media.tittle];
    }
    [self.navigationController pushViewController:vdc animated:YES];
    
    
}

#pragma mark PrivateFunctions
- (void)loadVideosWithForce:(BOOL)forced{
    self.videoArray = [[NSMutableArray alloc] init];
    if (self.isFromMain) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[LibraryAPI sharedInstance] getVideosWithForce:forced andCompletionBlock:^(NSMutableArray *array) {
            if (array) {
                self.videoArray = [array mutableCopy];
            } else {
                NSLog(@"No array list");
            }
            
            if ([Util is_iPad]) {
                [self.collectionView reloadData];
            } else {
                [self.tableView reloadData];
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[LibraryAPI sharedInstance]getMediaItemsFromURL:self.sectionURL andFeedLink:nil withCompletionBlock:^(NSMutableArray *array) {
            if (array) {
                self.videoArray = [array mutableCopy];
            } else {
                NSLog(@"No array list");
            }
            if ([Util is_iPad]) {
                [self.collectionView reloadData];
            } else {
                [self.tableView reloadData];
            }
            [MBProgressHUD hideHUDForView:self.view animated:NO];
        }];
        
    }
}



- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
     [self.collectionView reloadData];
}



-(void)reloadDataFeed: (UIRefreshControl *)feedRefresh {

    [self loadVideosWithForce:YES];
    if (feedRefresh) {
       
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        feedRefresh.attributedTitle = attributedTitle;
        
        [feedRefresh endRefreshing];
    }
}


#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    self.bannerView.adSize = kGADAdSizeBanner;
    //[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       //NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       //]];
    //self.bannerView.adSizeDelegate = self;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    
    [self.bannerView loadRequest:request];
    
    [self.bannerView setHidden:YES];
}
#pragma mark GADbannerViewDelegate
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
    [self.bannerView setHidden:YES];
}

- (void)adViewDidReceiveAd:(GADBannerView *)view {
    //if (self.bannerHeight.constant == 0) {
        //if ([Util is_iPad]){
            //self.bannerHeight.constant = 100;
        //}else {
            self.bannerHeight.constant = 50;
        //}
    //}
    [self.bannerView setHidden:NO];
}

/*- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)gadSize{
    self.bannerHeight.constant = gadSize.size.height;
}*/

@end
