//
//  TMExclusiveCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 2/3/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMExclusiveCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *tittleLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;

@end
