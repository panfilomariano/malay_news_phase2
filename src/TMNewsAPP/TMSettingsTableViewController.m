//
//  TMSettingsTableViewController.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/8/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMSettingsTableViewController.h"
#import "Constants.h"
#import "TMWebViewController.h"
#import "TMFontSizeTableViewController.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Data.h"
#import "MBProgressHUD.h"
#import "AnalyticManager.h"
#import "Constants.h"
#import "Util.h"

@interface TMSettingsTableViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>
@property (nonatomic) NSInteger index;
@property (nonatomic, weak) IBOutlet UILabel *aboutUsLabel;
@property (nonatomic, weak) IBOutlet UILabel *submitNewsLabel;
@property (nonatomic, weak) IBOutlet UILabel *downloadLabel;
@property (nonatomic, weak) IBOutlet UILabel *termsConditionLabel;
@property (nonatomic, weak) IBOutlet UILabel *privacyPolicyLabel;
@property (nonatomic, weak) IBOutlet UILabel *fontLabel;
@property (nonatomic, weak) IBOutlet UILabel *versionLabel;
@property (nonatomic, weak) IBOutlet UILabel *contactUsLabel;
@property (nonatomic, weak) IBOutlet UILabel *appVersionLabel;

@end


@implementation TMSettingsTableViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {
        self.title = @"Settings";
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.title = @"Settings";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UINavigationBar appearance] setBarTintColor:[Util headerColor]];
    [[AnalyticManager sharedInstance] trackSettings];
    
    [self.aboutUsLabel setText:[Constants SETTING_ABOUT_US]];
    [self.submitNewsLabel setText:[Constants SETTING_SUBMIT_NEWS]];
    [self.downloadLabel setText:[Constants SETTINGS_DOWNLOAD_ALL]];
    [self.termsConditionLabel setText:[Constants SETTING_REGULATIONS]];
    [self.privacyPolicyLabel setText:[Constants SETTING_PRIVACY_POLICY]];
    [self.fontLabel setText:[Constants SETTING_FONT_SIZE]];
    [self.versionLabel setText:[Constants SETTING_VERSION]];
    [self.contactUsLabel setText:[Constants SETTING_CONTACT_US]];
    [self.appVersionLabel setText:[Util appVersion]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSLog(@"Selected: %ld", (long)indexPath.row);
    
    
    switch (indexPath.row) {
        case 0: {
            TMWebViewController *webVieController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            webVieController.urlString = [Constants URL_ABOUT];
            webVieController.title = [Constants SETTING_ABOUT_US];
            [self.navigationController pushViewController:webVieController animated:YES];
            break;
        } case 1: {

            if ([MFMailComposeViewController canSendMail]){
                [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
                [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
                [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                NSString *emailTitle = [Constants CONTACT_US_EMAIL_SUBJECT];
                NSString *messageBody = [Constants CONTACT_US_EMAIL_MESSAGE];
                NSArray *toRecipents = [NSArray arrayWithObject:[Constants CONTACT_US_EMAIL_TO]];
                mc.mailComposeDelegate = self;
                [mc setSubject:emailTitle];
                [mc setMessageBody:messageBody isHTML:NO];
                [mc setToRecipients:toRecipents];
                [mc.navigationBar setTranslucent:NO];
                [mc.navigationBar setTintColor:[UIColor whiteColor]];
                mc.navigationBar.translucent = NO;

                [self presentViewController:mc animated:YES completion:^{
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                }];

            }else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                    message:@"Your device doesn't support the composer sheet"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles: nil];
                [alertView show];
            }
            break;
        } case 2: {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[Constants SETTINGS_SUBMIT_NEWS] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil];
            [actionSheet buttonTitleAtIndex:[actionSheet addButtonWithTitle:[Constants SETTINGS_SUBMIT_NEWS]]];
            [actionSheet buttonTitleAtIndex:[actionSheet addButtonWithTitle:[Constants SETTINGS_SUBMIT_NEWS_PHOTOS_OR_VIDEOS]]];
            actionSheet.tag = 1;
            actionSheet.delegate = self;
            [actionSheet showInView: self.view];
            break;
        } case 3: {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Download" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil];
            [actionSheet buttonTitleAtIndex:[actionSheet addButtonWithTitle:[Constants SETTINGS_DOWNLOAD_OPTION1]]];
            [actionSheet buttonTitleAtIndex:[actionSheet addButtonWithTitle:[Constants SETTINGS_DOWNLOAD_OPTION2]]];
            actionSheet.tag = 2;
            
            [actionSheet showInView: self.view];
            break;
        } case 4: {
            TMWebViewController *webVieController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            webVieController.urlString = [Constants URL_REGULATION];
            webVieController.title = [Constants SETTING_REGULATIONS];
            [self.navigationController pushViewController:webVieController animated:YES];
            break;
        } case 5: {
            TMWebViewController *webVieController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            webVieController.urlString = [Constants URL_PRIVACY];
            webVieController.title = [Constants SETTING_PRIVACY_POLICY];
            [self.navigationController pushViewController:webVieController animated:YES];
            break;
        } case 6: {
            TMFontSizeTableViewController *fontTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"fontTableVIewControllerStoryboardid"];
            fontTableViewController.title = [Constants SETTING_FONT_SIZE];
            [self.navigationController pushViewController:fontTableViewController animated:YES];
            /*TMWebViewController *webVieController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            webVieController.urlString = Constants.URL_GLOSSARY;
            webVieController.title = @"Glossary";
            [self.navigationController pushViewController:webVieController animated:YES];*/
            break;
        } case 7: {
           
            break;
        }
    }
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (actionSheet.tag == 1){
        if (buttonIndex == 1){
            if ([MFMailComposeViewController canSendMail]){
                [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
                [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
                [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
                
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                
                NSString *emailTitle = [Constants SUBMIT_NEWS_EMAIL_SUBJECT];
                
                NSString *messageBody = [Constants SUBMIT_NEWS_EMAIL_MESSAGE];
                
                NSArray *toRecipents = [NSArray arrayWithObject:[Constants SUBMIT_NEWS_EMAIL_TO]];
                mc.mailComposeDelegate = self;
                
                [mc setSubject:emailTitle];
                [mc setMessageBody:messageBody isHTML:NO];
                [mc setToRecipients:toRecipents];
                [mc.navigationBar setTranslucent:NO];
                [mc.navigationBar setTintColor:[UIColor whiteColor]];
                
                [self presentViewController:mc animated:YES completion:^{
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
                }];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Alert"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        }else if (buttonIndex == 2){
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
            
            UIImagePickerController *imagepicker = [[UIImagePickerController alloc]init];
            imagepicker.delegate = self;
            [imagepicker.navigationBar setTranslucent:NO];
            imagepicker.navigationBar.tintColor = [UIColor whiteColor];
            imagepicker.allowsEditing = NO;
            imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagepicker.mediaTypes = [NSArray arrayWithObjects:
                                      (NSString *) kUTTypeMovie, (NSString *) kUTTypeImage,
                                      nil];
            imagepicker.allowsEditing = NO;
            
            [self presentViewController:imagepicker animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
        }
        
        
        
        
    } else if (actionSheet.tag == 2) {
        if (buttonIndex != actionSheet.cancelButtonIndex){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download"
                                                        message:[Constants DOWNLOAD_PROMPT]
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK",nil];
        
        self.index = buttonIndex;
        alert.tag = 1;
        [alert show];
        }
    }

    
}
#pragma mark UIalertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag ==1){
        if(buttonIndex != alertView.cancelButtonIndex){
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //hud.mode = MBProgressHUDModeAnnularDeterminate;
            hud.labelText = [Constants SETTINGS_DOWNLOADING];
            if (self.index == 0) {
                [[Data sharedInstance] downloadAllArticlesWithImages:NO andCompletionBlock:^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[Constants SETTINGS_DOWNLOAD_COMPLETED] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                    [hud hide:YES];
                }];
            }else {
                [[Data sharedInstance] downloadAllArticlesWithImages:YES andCompletionBlock:^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[Constants SETTINGS_DOWNLOAD_COMPLETED] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                    [hud hide:YES];
            
                }];
            }
            
        }
    }
   
    
}
#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if ([MFMailComposeViewController canSendMail]) {
        [UINavigationBar appearance].barTintColor = [Util headerColor];
        
/*#if TAMIL
        [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
        [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/

//        [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor clearColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
        
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        // Email Subject
        NSString *emailTitle = [Constants SUBMIT_NEWS_EMAIL_SUBJECT];
        // Email Content
        NSString *messageBody = [Constants SUBMIT_NEWS_EMAIL_MESSAGE];
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:[Constants SUBMIT_NEWS_EMAIL_TO]];
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        [mc.navigationBar setTranslucent:NO];
        
        if ([mediaType isEqualToString:@"public.image"]){
            UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
            NSData* data = UIImageJPEGRepresentation(image, 1.0);
            
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            // Attach Image as Data
            [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            //[mc.navigationBar setBarTintColor:[UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1.0]];
            // Present mail view controller on screen
        }else if ([mediaType isEqualToString:@"public.movie"]){
            NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
            NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
            
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            // Attach Image as Data
            [mc addAttachmentData:videoData mimeType:@"video/mp4" fileName:@"video"];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            //[mc.navigationBar setBarTintColor:[UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1.0]];
        }
        
        [picker dismissViewControllerAnimated:YES completion:^(){
            [self presentViewController:mc animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Alert"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
