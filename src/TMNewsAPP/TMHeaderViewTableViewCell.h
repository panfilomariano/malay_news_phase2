//
//  TMHeaderViewTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMHeaderViewTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *tittle;
@property (nonatomic, weak) IBOutlet  UILabel *date;
@property (nonatomic, weak) IBOutlet UIImageView *videoImageView;

@end
