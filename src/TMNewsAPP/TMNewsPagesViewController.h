//
//  TMNewsPagesViewController.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/15/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPagerController.h"
#import "Article.h"



@protocol TMNewsPagesViewControllerDelagate <NSObject>
- (void) didTapBookmarkButton;
@end

typedef enum
{
    OthersPagesScreenType,
    LatestNewsPagesScreenType,
    CAPagesScreenType,
    SpecialReportPagesScreenType
    
} PagesScreenType;

@interface TMNewsPagesViewController : UIViewController

@property (nonatomic, weak) id <TMNewsPagesViewControllerDelagate> delegate;
@property (nonatomic) NSInteger index;
@property (nonatomic, strong) Article *article;
@property (nonatomic, strong) NSString *specialReportEnTittle;
@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, assign) BOOL isBookmark;
@property (nonatomic) NSInteger pageNumber;
@property (nonatomic) NSInteger totalPages;
@property (nonatomic, strong) NSString *titleForSpecialReport;
@property (nonatomic, assign) PagesScreenType screenType;
@property (nonatomic, assign) BOOL isTopNews;

@end
