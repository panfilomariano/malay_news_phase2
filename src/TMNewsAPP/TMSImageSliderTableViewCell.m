//
//  TMSImageSliderTableViewCell.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMSImageSliderTableViewCell.h"

@implementation TMSImageSliderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
