//
//  TMLiveStreamingViewController.m
//  TMNewsAPP
//
//  Created by Ace  on 7/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "TMLiveStreamingViewController.h"
#import "TMLiveStreamMainTableViewCell.h"
#import "TMLiveStreamingSubTableViewCell.h"
#import "PNChart.h"
@interface TMLiveStreamingViewController ()
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL didTapOnCell;
@property (nonatomic) NSInteger tappedCell;
@property (nonatomic, strong) UIView *dimView;
@end

@implementation TMLiveStreamingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark tableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.didTapOnCell) {
        return 1;
    } else {
        if (section == self.tappedCell) {
             return 4;
        }else {
            return 1;
        }
    }
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 ) {
        return 300;
    } else {
        return 93;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        TMLiveStreamMainTableViewCell *cell = (TMLiveStreamMainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        return cell;
    } else {
        TMLiveStreamingSubTableViewCell *cell = (TMLiveStreamingSubTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        return cell;
    }
    return nil;
}
#pragma mark tableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if (self.didTapOnCell) {
            if (indexPath.section != self.tappedCell) {
                self.didTapOnCell = NO;
                 [tableView reloadSections:[NSIndexSet indexSetWithIndex:self.tappedCell] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
        self.didTapOnCell = YES;
        self.tappedCell = indexPath.section;
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(unDimDisplay)];
        tapGesture.numberOfTapsRequired = 1;
        tapGesture.numberOfTouchesRequired = 1;
        
        PNCircleChart * circleChart = [[PNCircleChart alloc] initWithFrame:CGRectMake((self.view.bounds.size.width/2)-50, (self.view.bounds.size.height/2)-50, 100, 100) total:[NSNumber numberWithInt:100] current:[NSNumber numberWithInteger:8] clockwise:YES shadow:NO shadowColor:PNGrey];
        circleChart.backgroundColor = [UIColor clearColor];
        [circleChart setStrokeColor:[UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1]];
        [circleChart setLineWidth:[NSNumber numberWithInt:4]];
        [circleChart growChartByAmount:[NSNumber numberWithInt:1]];
        [circleChart strokeChart];
        
        self.dimView = [[UIView alloc]initWithFrame:self.view.frame];
        [self.dimView addGestureRecognizer:tapGesture];
        self.dimView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.9];
        [self.dimView addSubview:circleChart];
        [self.view addSubview:self.dimView];
    }
}

-(void)unDimDisplay
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.dimView.alpha = 0;
                     }];
    
    [self.dimView removeFromSuperview];
    self.dimView = nil;
    
}
@end
