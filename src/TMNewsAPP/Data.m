//
//  Data.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/23/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "Data.h"
#import "LibraryAPI.h"
#import "SDWebImageManager.h"
#import "objc/runtime.h"
#import "Constants.h"

@interface Data ()

@property (nonatomic) dispatch_group_t group;

@end

@implementation Data

static Data *_instance;
static dispatch_once_t oncePredicate;

+ (Data *)sharedInstance
{
    dispatch_once(&oncePredicate, ^{
        _instance = [[Data alloc] init];
    });
    
    return _instance;
}

- (void)downloadAllArticlesWithImages: (BOOL)withImages andCompletionBlock: (void (^)()) completionBlockz
{
    self.group = dispatch_group_create();

#pragma mark TOP NEWS
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getTopNewsWithForce:YES andCompletionBlock:^(NSMutableArray *array) {
        if (withImages) {
            for (Article *article in array) {
                NSString *thumbnail = @"";
                NSString *content = @"";
                if (article.mediagroupList) {
                    if (article.mediagroupList.count) {
                        NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                        thumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                        
                        NSString *type = [mediaDictionary valueForKey:@"type"];
                        if (![type isEqualToString:@"video"]) {
                            content = [mediaDictionary valueForKey:@"content"];
                        }
                    }
                } else {
                    thumbnail = article.thumbnail;
                }
                
                
                if (thumbnail.length) {
                    dispatch_group_enter(self.group);
                    NSURL *url = [NSURL URLWithString:thumbnail];
                    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        if (finished) {
                            dispatch_group_leave(self.group);
                        }
                        
                    }];
                }
                
                if (content.length) {
                    dispatch_group_enter(self.group);
                    NSURL *url = [NSURL URLWithString:content];
                    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        if (finished) {
                            dispatch_group_leave(self.group);
                        }
                        
                    }];
                }
            }
        }
        
        dispatch_group_leave(self.group);
    }];
    
#pragma mark LATEST NEWS
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance] getLatestNewsWithForce:YES andCompletionBlock:^(NSMutableArray *array) {
        if (withImages) {
            for (Article *article in array) {
                NSString *thumbnail = @"";
                NSString *content = @"";
                
                if (article.mediagroupList) {
                    if (article.mediagroupList.count) {
                        NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                        thumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                        
                        NSString *type = [mediaDictionary valueForKey:@"type"];
                        if (![type isEqualToString:@"video"]) {
                            content = [mediaDictionary valueForKey:@"content"];
                        }
                    }
                } else {
                    thumbnail = article.thumbnail;
                }

                
                if (thumbnail.length) {
                    dispatch_group_enter(self.group);
                    NSURL *url = [NSURL URLWithString:thumbnail];
                    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        if (finished) {
                            dispatch_group_leave(self.group);
                        }
                        
                    }];
                }
                
                if (content.length) {
                    dispatch_group_enter(self.group);
                    NSURL *url = [NSURL URLWithString:content];
                    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        if (finished) {
                            dispatch_group_leave(self.group);
                        }
                        
                    }];
                }
                
            }
        }
        dispatch_group_leave(self.group);
    }];
    
#pragma mark NEWS BERITA
    
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getNewsWithCompletionBlock:^(NSMutableArray *array) {
        
            for (NSDictionary *dictionary in array) {
                NSString *urlString = [dictionary valueForKey:@"sectionURL"];
                dispatch_group_enter(self.group);
                [[LibraryAPI sharedInstance]getArticlesWithURL:urlString isForced:YES withCompletionBlock:^(NSMutableArray *array) {
                    if (withImages) {
                    for (Article *article in array) {
                        NSString *thumbnail = @"";
                        NSString *content = @"";
                        
                        if (article.mediagroupList) {
                            if (article.mediagroupList.count) {
                                NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                                thumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                                
                                NSString *type = [mediaDictionary valueForKey:@"type"];
                                if (![type isEqualToString:@"video"]) {
                                    content = [mediaDictionary valueForKey:@"content"];
                                }
                            }
                        } else {
                            thumbnail = article.thumbnail;
                        }
                        
                        
                        if (thumbnail.length) {
                            dispatch_group_enter(self.group);
                            NSURL *url = [NSURL URLWithString:thumbnail];
                            [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (finished) {
                                    dispatch_group_leave(self.group);
                                }
                                
                            }];
                        }
                        
                        if (content.length) {
                            dispatch_group_enter(self.group);
                            NSURL *url = [NSURL URLWithString:content];
                            [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (finished) {
                                    dispatch_group_leave(self.group);
                                }
                                
                            }];
                        }
                    }
                    }
                    dispatch_group_leave(self.group);
                }];
            }
        
        dispatch_group_leave(self.group);
    }];
    
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        
        for (NSDictionary *feedItemDict in array) {
            if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:@"latest section news"]) {
                
                NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                dispatch_group_enter(self.group);
                
                [[LibraryAPI sharedInstance]getSectionArticlesFromURL:sectionURL isForced:YES withCompletionBlock:^(NSMutableArray *array) {
                    if (withImages) {
                        for (Article *article in array) {
                            
                            NSString *thumbnail = @"";
                            NSString *content = @"";
                            
                            if (article.mediagroupList) {
                                if (article.mediagroupList.count) {
                                    NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                                    thumbnail = [mediaDictionary valueForKey:@"thumbnail"];
                                    
                                    NSString *type = [mediaDictionary valueForKey:@"type"];
                                    if (![type isEqualToString:@"video"]) {
                                        content = [mediaDictionary valueForKey:@"content"];
                                    }
                                }
                            } else {
                                thumbnail = article.thumbnail;
                            }
                            
                            
                            if (thumbnail.length) {
                                dispatch_group_enter(self.group);
                                NSURL *url = [NSURL URLWithString:thumbnail];
                                [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (finished) {
                                        dispatch_group_leave(self.group);
                                    }
                                    
                                }];
                            }
                            
                            if (content.length) {
                                dispatch_group_enter(self.group);
                                NSURL *url = [NSURL URLWithString:content];
                                [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (finished) {
                                        dispatch_group_leave(self.group);
                                    }
                                    
                                }];
                            }
                        }
                    }
                    dispatch_group_leave(self.group);
                }];
            }
            
        }
        dispatch_group_leave(self.group);
    }];
    
    /*dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        if (withImages) {
            for (NSDictionary *feedItemDict in array) {
                if ([[feedItemDict valueForKey:@"enTitle"] isEqualToString:@"specialreports"]) {
                    dispatch_group_enter(self.group);
                    NSString *sectionURL = [feedItemDict valueForKey:@"sectionURL"];
                    [[LibraryAPI sharedInstance]getArticlesWithURL:sectionURL isForced:YES withCompletionBlock:^(NSMutableArray *array) {
                        for (Article *article in array) {
                            
                            NSString *urlStr = @"";
                            if (article.mediagroupList) {
                                if (article.mediagroupList.count) {
                                    NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                                    urlStr = [mediaDictionary valueForKey:@"thumbnail"];
                                }
                            } else {
                                urlStr = article.thumbnail;
                            }
                            dispatch_group_enter(self.group);
                            NSURL *url = [NSURL URLWithString:urlStr];
                            [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                
                            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (finished) {
                                    NSLog(@"LEFT");
                                    dispatch_group_leave(self.group);
                                }
                            }];
                        }
                        dispatch_group_leave(self.group);
                    }];
                }
            }
        }
        dispatch_group_leave(self.group);
    }];
     */
/*
#pragma mark CA
    NSLog(@"ENTERED");
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance] getCAWithForce:YES andCompletionBlock:^(NSMutableArray *array) {
        
        for (Article *caArticle in array) {
            if (withImages) {
                NSString *urlStr = @"";
                if (caArticle.mediagroupList) {
                    if (caArticle.mediagroupList.count) {
                        NSDictionary *mediaDictionary = [caArticle.mediagroupList objectAtIndex:0];
                        urlStr = [mediaDictionary valueForKey:@"thumbnail"];
                    }
                } else {
                    urlStr = caArticle.thumbnail;
                }
                NSLog(@"ENTERED");
                dispatch_group_enter(self.group);
                NSURL *url = [NSURL URLWithString:urlStr];
                [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                    
                } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                    if (finished) {
                        NSLog(@"LEFT");
                        dispatch_group_leave(self.group);
                    }
                }];
            }
            
            NSString *stringUrl = caArticle.sectionURL;
            [[LibraryAPI sharedInstance]getArticlesWithURL:stringUrl andFeedLink:caArticle.feedlink  isForced:NO withCompletionBlock:^(NSMutableArray *array){
                
                if ([array count]) {
                    Article *article = [array firstObject];
                    NSArray *sectionsArray = article.sections;
                    for (NSMutableDictionary *dictionary in sectionsArray) {
                        NSString *urlString = [dictionary valueForKey:@"sectionURL"];
                        NSString *sectionType = [dictionary valueForKey:@"sectionType"];
                        NSString *type = [dictionary valueForKey:@"type"];
                        if ([sectionType isEqualToString:@"photos"] || [type isEqualToString:@"photos"] ) {
                            dispatch_group_enter(self.group);
                            [[LibraryAPI sharedInstance]getSectionArticlesFromURL:urlString withFeedUrl:caArticle.feedlink isForced:YES withCompletionBlock:^(NSMutableArray *array) {
                                for (Article *article in array) {
                                    if (withImages) {
                                        NSString *urlStr = @"";
                                        if (article.mediagroupList) {
                                            if (article.mediagroupList.count) {
                                                NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                                                urlStr = [mediaDictionary valueForKey:@"thumbnail"];
                                            }
                                        } else {
                                            urlStr = article.thumbnail;
                                        }
                                        NSLog(@"ENTERED");
                                        dispatch_group_enter(self.group);
                                        NSURL *url = [NSURL URLWithString:urlStr];
                                        [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            
                                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            if (finished) {
                                                NSLog(@"LEFT");
                                                dispatch_group_leave(self.group);
                                            }
                                        }];
                                    }
                                }
                                
                                dispatch_group_leave(self.group);
                            }];
                        } else{
                            dispatch_group_enter(self.group);
                            [[LibraryAPI sharedInstance]getArticlesWithURL:urlString isForced:YES withCompletionBlock:^(NSMutableArray *array) {
                                for (Article *article in array) {
                                    if (withImages) {
                                        NSString *urlStr = @"";
                                        if (article.mediagroupList) {
                                            if (article.mediagroupList.count) {
                                                NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                                                urlStr = [mediaDictionary valueForKey:@"thumbnail"];
                                            }
                                        } else {
                                            urlStr = article.thumbnail;
                                        }
                                        NSLog(@"ENTERED");
                                        dispatch_group_enter(self.group);
                                        NSURL *url = [NSURL URLWithString:urlStr];
                                        [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                            
                                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            if (finished) {
                                                NSLog(@"LEFT");
                                                dispatch_group_leave(self.group);
                                            }
                                        }];
                                    }

                                }
                                
                                dispatch_group_leave(self.group);
                            }];
                        }
                    }
                }
                
            }];
            
        }
        NSLog(@"LEFT");
        dispatch_group_leave(self.group);
    }];
    
    dispatch_group_enter(self.group);
    [[LibraryAPI sharedInstance]getIndexListItems:^(NSArray *array) {
        for(NSDictionary *dictionary in array){
            NSString *enTitle = [dictionary valueForKey:@"enTitle"];
            if([enTitle isEqualToString:@"latest episodes"]){
                dispatch_group_enter(self.group);
                [[LibraryAPI sharedInstance] getArticlesWithURL:[dictionary valueForKey:@"sectionURL"] isForced:YES withCompletionBlock:^(NSMutableArray *array) {
                    if (withImages) {
                        for (Article *article in array) {
                            
                            NSString *urlStr = @"";
                            if (article.mediagroupList) {
                                if (article.mediagroupList.count) {
                                    NSDictionary *mediaDictionary = [article.mediagroupList objectAtIndex:0];
                                    urlStr = [mediaDictionary valueForKey:@"thumbnail"];
                                }
                            } else {
                                urlStr = article.thumbnail;
                            }
                            dispatch_group_enter(self.group);
                            NSURL *url = [NSURL URLWithString:urlStr];
                            [[SDWebImageManager sharedManager] downloadImageWithURL:url options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                
                            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (finished) {
                                    NSLog(@"LEFT");
                                    dispatch_group_leave(self.group);
                                }
                            }];
                        }
                        
                    }
                    dispatch_group_leave(self.group);
                }];
                
            }
        }
        dispatch_group_leave(self.group);
    }];
*/
    //DISPATHC BLOCK
    dispatch_group_notify(self.group, dispatch_get_main_queue(), ^{
        completionBlockz ();
    });
    
}

@end
