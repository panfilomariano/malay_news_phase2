//
//  NSDate+Helpers.m
//  Cubit
//
//  Created by Wong Johnson on 10/19/12.
//  Copyright (c) 2012 Pace Creative Studio. All rights reserved.
//

#import "NSDate+Helpers.h"

@implementation NSDate(Helpers)

- (NSString *)dateStringByISO8601
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    //[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
    return [formatter stringFromDate:self];
}
- (NSString *)dateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    return [formatter stringFromDate:self];
}

@end
