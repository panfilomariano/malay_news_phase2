//
//  TMNewsPagesViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/15/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMNewsPagesViewController.h"
#import "TMNewsDetailsViewController.h"
#import "SwipeView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TMNewsPagesCellTableViewCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MediaPlayer/MPMediaPlayback.h>
#import "TMNewsPagesTitleTableViewCell.h"
#import "TMNewsPagesWebTableViewCell.h"
#import "TMCollectionTableViewCell.h"
#import "NSDate+Helpers.h"
#import "DataManager.h"
#import "LibraryAPI.h"
#import "TMSwipeViewTableViewCell.h"
#import "TMNewsDetailTitleTableViewCell.h"
#import "Util.h"
#import "Data.h"
#import "OBTableViewCell.h"
#import <OutbrainSDK/OutbrainSDK.h>
#import "UIViewController+ECSlidingViewController.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "TMMenuViewController.h"
#import "TMWebViewController.h"
#import "TMMoviePlayerViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "TMNavigationController.h"
#import "AnalyticManager.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"
#import "TMMoviePlayerViewController.h"

@interface TMNewsPagesViewController ()<UIWebViewDelegate, SwipeViewDelegate, TMNewsPagesViewControllerDelagate, OBWidgetViewDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate, GADBannerViewDelegate, GADAdSizeDelegate>
{
    float webViewHeight;
    bool isCaptionBlank;
    float fontSize;
    bool isTopBookmark;
    //bool isCurrentAfairs;
    bool isthereRecomendations;
}
@property (nonatomic, strong) TMMoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) TMNewsPagesCellTableViewCell *captionCell;
@property (nonatomic, strong) TMNewsDetailTitleTableViewCell *titleCell;
@property (nonatomic, strong) TMNewsPagesWebTableViewCell *webViewCell;
@property (nonatomic, strong) TMSwipeViewTableViewCell *swipeViewCell;
@property (nonatomic, strong) TMSwipeViewTableViewCell *swipeViewCell2;
@property (nonatomic, strong) NSString *captionTitle;
@property (nonatomic, strong) NSString *titleForCell;
@property (nonatomic, strong) NSString *dateForCell;
@property (nonatomic, strong) NSString *categoryForCell;
@property (nonatomic, strong) NSString *updateForCell;
@property (nonatomic, strong) UIButton *invisibleButton;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) SMPageControl *pageControl;
@property (nonatomic, strong) SMPageControl *top5pageControl;
@property (nonatomic, strong) SwipeView *swipeView;
@property (nonatomic, strong) SwipeView *swipeView2;
@property (nonatomic, strong) UICollectionView *topFiveCollectionView;
@property (nonatomic, strong) UIView *fontResizeView;
@property (nonatomic, strong) UIWebView *descriptionWebview;
@property (nonatomic, strong) UIBarButtonItem *bookmarkBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *fontBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *shareBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *separatorBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *menuBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *spaceSeparatorItem;
@property (nonatomic, strong) UIButton *bookmarkButton;
@property (nonatomic, strong) NSString *mostPopularAPI;
@property (nonatomic, strong) NSArray *itemsArray;
@property (nonatomic, strong) NSArray *sliderArray;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSString *mainUrlForWebView;
@property (nonatomic, strong) OBRecommendationResponse *recomendationResponse;

@property (nonatomic) BOOL interceptLinks;
@property (nonatomic) BOOL requestForDescription;
@property (nonatomic) BOOL hasWebViewObserver;
@property (nonatomic) BOOL hasRecomendationResponse;
@property (nonatomic) BOOL hasEmbededVideo;

@property (nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@property (nonatomic) NSInteger add;

@end

@implementation TMNewsPagesViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isBookmark = NO;
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
            [self.swipeView reloadData];
        }
        [[LibraryAPI sharedInstance]getTopFiveNewsWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
            if (array) {
                self.itemsArray = [array mutableCopy];
            }
        }];
    }];
    

    [self.bookmarkButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]];
   
    self.interceptLinks = NO;
    [self requestWebView];
    [self tagView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadSwipeView)
                                                 name:@"willRefreshSwipeView"
                                               object:nil];
    self.sliderArray = self.article.mediagroupList;
    [self.swipeView setCurrentItemIndex:0];

#ifdef TAMIL
    self.add = 10;
#else
    self.add = 5;
#endif
    self.swipeViewCell.swipeView.pagingEnabled = YES;
    isCaptionBlank = YES;
    self.invisibleButton = [[UIButton alloc]initWithFrame:self.view.bounds];
    [self.invisibleButton addTarget:self action:@selector(didTapOutside) forControlEvents:UIControlEventTouchUpInside];
    self.fontResizeView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, -30.0f, self.table.frame.size.width, 30.0f)];
    self.slider = [[UISlider alloc] initWithFrame:self.fontResizeView.bounds];
    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"fontbar_line"];
    UIImage *sliderRightTrackImage = [UIImage imageNamed: @"fontbar_line"];
    sliderLeftTrackImage = [sliderLeftTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    sliderRightTrackImage = [sliderRightTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    [self.slider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
    [self.slider setThumbImage:[UIImage imageNamed: @"thumbButton"] forState:UIControlStateNormal];
    [self.slider addTarget:self action:@selector(sliderAction) forControlEvents:UIControlEventTouchUpInside];
    [self.invisibleButton addTarget:self action:@selector(didTapOutside) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.barTintColor = [Util headerColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.bookmarkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bookmarkButton setFrame:CGRectMake(0, 0, 28, 29)];
    [self.bookmarkButton setImage:[UIImage imageNamed:@"bigStar"] forState:UIControlStateNormal];
    [self.bookmarkButton setImage:[UIImage imageNamed:@"bigStar_selected"] forState:UIControlStateSelected];
    [self.bookmarkButton addTarget:self action:@selector(didTapBookmark) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 28, 29)];
    [menuButton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(didTapMenu) forControlEvents:UIControlEventTouchUpInside];

    UIButton *fontResizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fontResizeButton.frame = CGRectMake(0, 0, 28, 29);
    [fontResizeButton setImage:[UIImage imageNamed:@"fontButton"] forState:UIControlStateNormal];
    [fontResizeButton setImage:[UIImage imageNamed:@"fontButton_selected"] forState:UIControlStateSelected];
    [fontResizeButton addTarget:self action:@selector(displayfontResizeView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *transparentSeparatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 28)];
    [transparentSeparatorImage setImage:[UIImage new]];

    UIImageView *separatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 28)];
    [separatorImage setImage:[UIImage imageNamed:@"separator_icon"]];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setFrame:CGRectMake(0, 0, 28, 29)];
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateSelected];
    [shareButton addTarget:self action:@selector(didTapShare) forControlEvents:UIControlEventTouchUpInside];

  
    _fontBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:fontResizeButton];
    _bookmarkBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.bookmarkButton];
    _shareBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    _separatorBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:separatorImage];
    _menuBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:menuButton];
    _spaceSeparatorItem =[[UIBarButtonItem alloc]initWithCustomView:transparentSeparatorImage];

    
    self.navigationItem.rightBarButtonItems = @[_menuBarButtonItem,_separatorBarButtonItem,_shareBarButtonItem,_spaceSeparatorItem,_bookmarkBarButtonItem,_spaceSeparatorItem,_fontBarButtonItem];
    
    [self setUpads];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.hasWebViewObserver)
        [self.descriptionWebview.scrollView removeObserver:self forKeyPath:@"contentSize"];
    self.hasWebViewObserver = FALSE;
    
    if (self.hasEmbededVideo)
        [self.descriptionWebview loadHTMLString:nil baseURL:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    float newHeight = self.descriptionWebview.scrollView.contentSize.height;
    NSLog(@"newHeight=%f", newHeight);
    if (webViewHeight != newHeight) {
        webViewHeight = newHeight;
        [self.table reloadData];
        self.requestForDescription = NO;
        for (id subview in self.descriptionWebview.subviews){
            if ([[subview class] isSubclassOfClass: [UIScrollView class]]){
                [subview setBounces:NO];
            }
        }
    }
    
    
}


#pragma  mark Swipeview Delegate
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    if(swipeView == self.swipeView){
        return [self.itemsArray count];
    }else if(swipeView == self.swipeView2){
        return [self.sliderArray count];
    }
    return 0;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    float width = [UIScreen mainScreen].bounds.size.width;
    float aheight = 9;
    float awidth = 16;

    if(swipeView == self.swipeView){
  
        
        UIImageView *imageView = nil;
        UIImageView *playOverlay = nil;
        UILabel *categoryLabel = nil;
        UILabel *titleLabel = nil;
        UILabel *dateLabel = nil;
        UIView *backView = nil;
        UIButton *bookmarkButton = nil;
        
        if (view == nil) {
          
      
            view = [[UIView alloc] initWithFrame:swipeView.bounds];
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 16, (width-width/2)+15,(aheight/awidth * ((width-width/2)+15)))];
            [view setBackgroundColor:[UIColor colorWithRed:235/255.0 green:236.0/255.0 blue:237.0/255.0 alpha:1.0]];
            
            
            imageView.tag = 1;

            playOverlay = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_play"]];
            [playOverlay setFrame:CGRectMake(6,(aheight/awidth * ((width-width/2)+15))-26, 20, 20)];

            playOverlay.tag =4;
            [playOverlay setHidden:YES];
            
            backView = [[UIView alloc]initWithFrame:CGRectMake(0, imageView.bounds.size.height +16, (width-width/2)+15, 75)];
            backView.tag = 3;
            [backView setBackgroundColor:[UIColor whiteColor]];
            categoryLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 8, (width-width/2)-10, 13)];
            categoryLabel.tag = 2;
            [categoryLabel setBackgroundColor:[UIColor clearColor]];
            [categoryLabel setFont:[UIFont systemFontOfSize:10]];
            [categoryLabel setTextColor:[UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1]];
            titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 15, (width-width/2)-10, 40)];
            titleLabel.tag = 5;
            [titleLabel setBackgroundColor:[UIColor clearColor]];
            [titleLabel setFont:[UIFont systemFontOfSize:13]];
            [titleLabel setNumberOfLines:2];
            dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 55, (width-width/2)-10, 12)];
            dateLabel.tag = 6;
            [dateLabel setBackgroundColor:[UIColor clearColor]];
            [dateLabel setFont:[UIFont systemFontOfSize:11]];
            [dateLabel setTextColor:[UIColor colorWithRed:153/255 green:153/255 blue:153/255 alpha:1]];
            bookmarkButton = [[UIButton alloc]init];
            [bookmarkButton setFrame: CGRectMake(backView.bounds.size.width-21, backView.bounds.size.height-21, 13, 13)];
            bookmarkButton.tag = 7;
            [bookmarkButton setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
            [bookmarkButton setImage:[UIImage imageNamed:@"star_selected"] forState:UIControlStateSelected];
            [bookmarkButton addTarget:self action:@selector(didTapTopNewsBookmark:) forControlEvents:UIControlEventTouchUpInside];
            
            [backView addSubview:categoryLabel];
            [backView addSubview:titleLabel];
            [backView addSubview:dateLabel];
            [backView addSubview:bookmarkButton];
            [backView bringSubviewToFront:bookmarkButton];
            [view addSubview:imageView];
            [view addSubview:backView];
            [imageView addSubview:playOverlay];
            
        } else {
            imageView = (UIImageView *)[view viewWithTag:1];
            categoryLabel = (UILabel *)[view viewWithTag:2];
            backView = (UIView *)[view viewWithTag:3];
            playOverlay = (UIImageView *)[view viewWithTag:4];
            titleLabel = (UILabel *)[view viewWithTag:5];
            dateLabel = (UILabel *)[view viewWithTag:6];
            bookmarkButton = (UIButton *)[view viewWithTag:7];
        }
         Article *article = (Article *)[self.itemsArray objectAtIndex:index];
        [bookmarkButton setSelected:[[LibraryAPI sharedInstance]isBookmarkedArticle:article]];
        
        NSArray *mediaGroupArray = [article.mediagroupList copy];
        if(mediaGroupArray){
            NSDictionary *mediaDictionary = [mediaGroupArray firstObject];
            NSString *thumbnail =@"";
            NSString *type = [mediaDictionary valueForKey:@"type"];
            
            if ([type isEqualToString:@"video"]){
                playOverlay.hidden = false;
                thumbnail = [mediaDictionary valueForKey:@"thumbnail"];
            }else{
                playOverlay.hidden = true;
                thumbnail = [mediaDictionary valueForKey:@"content"];
            }
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                         placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:SDWebImageRefreshCached];
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
        NSDate *pubDate = [dateFormatter dateFromString:article.pubDate];
        NSString *formatedDate = [pubDate dateString];
        dateLabel.text =formatedDate;
        categoryLabel.text =article.category;
        titleLabel.text =  article.tittle;
    }else{
        UIImageView *imageView = nil;
        UIImageView *playOverlay = nil;
        if (view == nil) {
             view = [[UIView alloc] initWithFrame:swipeView.bounds];
             imageView = [[UIImageView alloc] initWithFrame: swipeView.bounds];
            imageView.tag = 1;
            
            playOverlay = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_play"]];
            [playOverlay setFrame:CGRectMake(8, (aheight/awidth * width)- 28, 20, 20)];
            playOverlay.tag =2;
            [playOverlay setHidden:YES];
            
            [imageView addSubview:playOverlay];
            [view addSubview:imageView];
        }else{
            imageView = (UIImageView *)[view viewWithTag:1];
             playOverlay = (UIImageView *)[view viewWithTag:2];
        }
        NSDictionary *dict = (NSDictionary *)[self.sliderArray objectAtIndex:index];
         NSString *thumbnail = [dict valueForKey:@"thumbnail"];
        NSString *type = [dict valueForKey:@"type"];
        self.captionTitle = [dict valueForKey:@"caption"];
        {
            if([self.captionTitle isEqualToString:@""]){
                isCaptionBlank = YES;
            }else{
                isCaptionBlank = NO;
            }
        }
            [imageView sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                         placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:SDWebImageRefreshCached];
            
            if ([type isEqualToString:@"video"]){
                playOverlay.hidden = false;
                
            }else{
                playOverlay.hidden = true;
            }
    }
    return view;

}

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index{
    if(swipeView == self.swipeView){
        Article *article = (Article *)[self.itemsArray objectAtIndex:index];
        TMNewsPagesViewController *pages = (TMNewsPagesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsPagesID"];
        pages.article = [self.itemsArray objectAtIndex:index];
        pages.delegate = self;
        if (_bookmarks.count) {
            for (Article *articleFromBookmark in _bookmarks) {
                if ([articleFromBookmark.guid isEqualToString:article.guid]) {
                    pages.isBookmark = YES;
                    break;
                }
            }
        }
        pages.article = [self.itemsArray objectAtIndex:index];
        [[AnalyticManager sharedInstance] trackPopularNewsWithArticleTitle:article.tittle andGUID:article.guid];
        [self.navigationController pushViewController:pages animated:YES];
    }else{
        NSArray *mediaGroupArray = self.article.mediagroupList;
        NSDictionary *mediaDictionary = [mediaGroupArray objectAtIndex:index];
        NSString *content = [mediaDictionary valueForKey:@"content"];
        NSString *type = [mediaDictionary valueForKey:@"type"];
        if([type isEqualToString:@"video"]){
            
            TMMoviePlayerViewController *player = [[TMMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:content]];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(MPMoviePlayerDidExitFullscreen:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:nil];

            [self presentMoviePlayerViewControllerAnimated:player];
            [player.moviePlayer play];
            
            
        
        }

    }
  

    
}

#pragma mark MoviePlayerNofication
- (void)MPMoviePlayerDidExitFullscreen:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
  
    [self.moviePlayer.moviePlayer stop];
    self.moviePlayer = nil;
    [self dismissMoviePlayerViewControllerAnimated];
   
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    float width = [UIScreen mainScreen].bounds.size.width;
    float aheight = 9;
    float awidth = 16;
    
    if(swipeView == self.swipeView){
        return CGSizeMake((width-width/2)+23, (aheight/awidth * (width-width/2))+115);
    }else{
        return CGSizeMake(width, (aheight/awidth * width));
    }
    
}


- (void)swipeViewDidScroll:(SwipeView *)swipeView
{
    if(swipeView == self.swipeView){
        self.top5pageControl.currentPage = swipeView.currentPage;
        
            if(swipeView.scrollOffset > 3){
                self.top5pageControl.currentPage = 4;
            }
        if([swipeView numberOfPages] < 5){
            if(swipeView.scrollOffset >=2.25 && swipeView.scrollOffset <= 3){
                self.top5pageControl.currentPage = 3;
            }else if(swipeView.scrollOffset >= 1.25 && swipeView.scrollOffset < 2.25){
                self.top5pageControl.currentPage = 2;
            }else if(swipeView.scrollOffset >= 0.25 && swipeView.scrollOffset < 1.25){
                self.top5pageControl.currentPage = 1;
            }else if(swipeView.scrollOffset >= 0 && swipeView.scrollOffset < 0.25){
                self.top5pageControl.currentPage = 0;
            }
        }
    

    }else{
        self.pageControl.currentPage = swipeView.currentPage;
    }
    
}

-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView{
    if(swipeView == self.swipeView2){
        [self.table beginUpdates];
        [self.table reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation: UITableViewRowAnimationNone];
        [self.table endUpdates];
    }
    
}



#pragma mark tableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 6;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

     if(indexPath.section == 0){
         float heightforTitle = [Util expectedHeightFromText:self.titleForCell withFont:[UIFont boldSystemFontOfSize:fontSize +self.add] andMaximumLabelWidth:self.view.frame.size.width-18];
         float height = heightforTitle + [Util expectedHeightFromText:self.categoryForCell withFont:[UIFont systemFontOfSize:fontSize] andMaximumLabelWidth:self.view.frame.size.width] + [Util expectedHeightFromText:self.dateForCell withFont:[UIFont systemFontOfSize:fontSize-3] andMaximumLabelWidth:self.view.frame.size.width]+16;
         
         return height;
     }else if (indexPath.section == 1) {
        float width = [UIScreen mainScreen].bounds.size.width;
        float aheight = 9;
        float awidth = 16;
        return (aheight/awidth * width);
     }else if (indexPath.section == 2 && !isCaptionBlank) {
        return [self cellHeightWithText:self.captionTitle andFontSize:(fontSize - 2)]+16;
    }else if(indexPath.section == 3){

        return webViewHeight ? webViewHeight : 1.0f;
        
    }else if(indexPath.section == 4){
        if(isthereRecomendations){
            return 300;
        }else{
            return 0;
        }
        
        
    }else if(indexPath.section == 5){
        if([self.itemsArray count]){
            float width = [UIScreen mainScreen].bounds.size.width;
            float aheight = 9;
            float awidth = 16;
            return (aheight/awidth * (width-width/2))+115;
        }else{
            return 0;
        }
        
    }
    return 0;
    
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier1 = @"cell1";
     NSString *cellIdentifier2 = @"svCell2";
     NSString *cellIdentifier3 = @"cell3";
     NSString *cellIdentifier4 = @"cell4";
     NSString *cellIdentifier5 = @"cell5";
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        TMNewsDetailTitleTableViewCell *cell = (TMNewsDetailTitleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier1 forIndexPath:indexPath];
        [cell resignFirstResponder];
        self.titleCell = cell;
    
        NSString *datestring = self.article.pubDate;
        NSString *updateString = self.article.lastUpdateDate;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzz"];
        NSDate *pubDate = [dateFormatter dateFromString:datestring];
        NSDate *updateDate = [dateFormatter dateFromString:updateString];
        NSString *formatedUpdate = [updateDate dateString];
        NSString *formatedDate = [pubDate dateString];
      

        if (self.screenType == SpecialReportPagesScreenType) {
            cell.categoryLabel.text = self.titleForSpecialReport;
        } else {
            cell.categoryLabel.text = self.article.category;
        }

        cell.titleLabel.text = self.article.tittle;
        self.titleForCell =self.article.tittle;
        self.categoryForCell = self.article.category;
        
        NSString *displaydate = formatedDate;
        if ([self.article.lastUpdateDate isEqualToString:@""] || [self.article.type isEqualToString:@"current affair"]) {
        }else{
            displaydate = [NSString stringWithFormat:@"%@  Update: %@",formatedDate, formatedUpdate];
        }
        cell.dateLabel.text = displaydate;
        self.dateForCell = displaydate;
        [cell.updateLabel setHidden:YES];
        
        [self checkFontSizeType];
        [cell.dateLabel setFont:[UIFont systemFontOfSize:fontSize-3]];
        [cell.categoryLabel setFont:[UIFont systemFontOfSize:fontSize]];
        [cell.titleLabel setFont:[UIFont boldSystemFontOfSize:fontSize+self.add]];
        [cell.updateLabel setFont:[UIFont systemFontOfSize:fontSize-2]];
        
        return cell;
    }else if(indexPath.section == 1 && indexPath.row == 0){
        
        TMSwipeViewTableViewCell *cell = (TMSwipeViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
         [cell resignFirstResponder];
        
        cell.swipeView.bounces = NO;
        cell.swipeView.truncateFinalPage = YES;
        self.swipeView2 = cell.swipeView;
        
        cell.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:19.0f/255.0f green:125.0f/255.0f blue:205.0f/255.0f alpha:1];
        cell.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
        cell.pageControl.backgroundColor = [UIColor clearColor];
        cell.pageControl.alpha = 1;
        cell.pageControl.indicatorDiameter = 5.0f;
        cell.pageControl.hidesForSinglePage =YES;
        cell.pageControl.alignment = SMPageControlAlignmentRight;
        cell.pageControl.indicatorMargin = 2.0f;
        self.pageControl = cell.pageControl;
        self.pageControl.numberOfPages = [self.sliderArray count];

        return cell;
     }else if (indexPath.section == 2 && indexPath.row == 0) {
        TMNewsPagesCellTableViewCell *cell = (TMNewsPagesCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier3 forIndexPath:indexPath];
         self.captionCell = cell;
        cell.caption.text = self.captionTitle;
         [self checkFontSizeType];
        [cell.caption setFont:[UIFont systemFontOfSize:fontSize-2]];
                
         
        return cell;
    }else if(indexPath.section == 3 && indexPath.row == 0){
        
        TMNewsPagesWebTableViewCell *cell = (TMNewsPagesWebTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier4 forIndexPath:indexPath];
        [self checkFontSizeType];
        
        [cell.webview setBackgroundColor: [UIColor blackColor]];
        [cell.webview setUserInteractionEnabled:YES];
        if (self.hasWebViewObserver)
            [cell.webview.scrollView removeObserver:self forKeyPath:@"contentSize"];
        [cell.webview.scrollView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
        self.hasWebViewObserver = TRUE;
        

        self.webViewCell = cell;;
        if (!self.descriptionWebview) {
            self.descriptionWebview = cell.webview;
            [self requestWebView];
        }
        [cell setBackgroundColor:[UIColor redColor]];
        
        return cell;
    }else if(indexPath.section == 4 && indexPath.row == 0){
        
        OBTableViewCell *cell = (OBTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"obCell" forIndexPath:indexPath];
        [cell resignFirstResponder];
        cell.recommendationsView.alpha = 0;
        
        // This is the piece that determines if images show or not.
        cell.recommendationsView.showImages = NO;
        cell.recommendationsView.widgetDelegate = self;
        // Create a request
        
        if (!self.hasRecomendationResponse) {
            self.hasRecomendationResponse = YES;
            [MBProgressHUD showHUDAddedTo:cell animated:YES];
            OBRequest * request = [OBRequest requestWithURL:self.article.link widgetID:[Constants OUTBRAIN_WIDGET_ID]];
            [Outbrain fetchRecommendationsForRequest:request withCallback:^(OBRecommendationResponse *response) {
                if(response.error) {
                    cell.recommendationsView.hidden = YES;
                    NSLog(@"Got error from recommendations request %@", response.error);
                } else {
                    self.recomendationResponse = response;
                    cell.recommendationsView.recommendationResponse = response;
                    if ([response.recommendations count]) {
                        isthereRecomendations = YES;
                    } else {
                        isthereRecomendations = NO;
                    }
                    
                    [UIView animateWithDuration:.25f animations:^{
                        // Fade it in
                        cell.recommendationsView.alpha = 1;
                    }];
                }
                [MBProgressHUD hideAllHUDsForView:cell animated:YES];
            }];
        } else {
            cell.recommendationsView.recommendationResponse = self.recomendationResponse;
            [UIView animateWithDuration:.25f animations:^{
                // Fade it in
                cell.recommendationsView.alpha = 1;
            }];
        }
        
        
        return cell;
    }else if (indexPath.section == 5 && indexPath.row == 0){
        TMSwipeViewTableViewCell *cell = (TMSwipeViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier5 forIndexPath:indexPath];
        [cell resignFirstResponder];
        self.top5pageControl = cell.pageControl;
        self.swipeView = cell.swipeView;
        cell.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:19.0f/255.0f green:125.0f/255.0f blue:205.0f/255.0f alpha:1];
        cell.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:178.0f/255.0f green:178.0f/255.0f blue:178.0/255.0f alpha:1];
        cell.topFiveNewsLabel.text = [Constants TOP5_NEWS];
        cell.pageControl.backgroundColor = [UIColor clearColor];
        cell.pageControl.alpha = 1;
        cell.pageControl.hidesForSinglePage =YES;
        cell.pageControl.indicatorDiameter = 5.0f;
        cell.pageControl.alignment = SMPageControlAlignmentRight;
        cell.pageControl.indicatorMargin = 2.0f;
        self.top5pageControl.numberOfPages = [self.itemsArray count];
        self.swipeViewCell2 = cell;
        cell.swipeView.truncateFinalPage = YES;
        return cell;
    }
    return nil;
    
}

-(void)checkFontSizeType{
    switch ([Data sharedInstance].fontSizeType) {
        case FontSizeTypeDefault:
            fontSize = 14;
            break;
        case FontSizeTypeSmall:
            fontSize = 13;
            break ;
        case FontSizeTypeLarge:
            fontSize = 15;
            break ;
        case FontSizeTypeXLarge:
            fontSize = 16;
            break ;
        case FontSizeTypeXSmall:
            fontSize = 12;
            break ;
    }
}
#pragma mark WebView Delegates
-(BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    NSLog(@"navigationType=%ld urlString=%@", navigationType, urlString);
    if ([urlString hasPrefix:@"https://www.youtube.com/"]) {
        self.hasEmbededVideo = YES;
    }
    
    if ([Util checkURLForPush:urlString withNavigationType:navigationType]) {
        if ([urlString hasPrefix:@"https://"] || [urlString hasPrefix:@"http://"] ) {
            TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewControllerStoryboardId"];
            webViewController.urlString = urlString;
            [self.navigationController pushViewController:webViewController animated:YES];
            return NO;
        }
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (self.requestForDescription) {
        self.requestForDescription = NO;
    }
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@" UIwebView Error : %@", error);
}
#pragma mark private Functions
- (void) reloadSwipeView{
    [self.swipeView reloadData];
}
- (void)didTapMenu {
    TMNavigationController *nav = (TMNavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"menuNavigationStoryboardId"];
    TMMenuViewController *dbvc = (TMMenuViewController *)[nav.viewControllers firstObject];
    dbvc.isSubmenu = YES;
    [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
    [nav.navigationBar setBarTintColor:[Util headerColor]];
    [self.navigationController presentViewController:nav animated:NO completion:nil];
}
-(void)didTapShare{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Facebook",
                                  @"Twitter",
                                  @"Email",
                                  nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];

}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self fbShare];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self emailShare];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

- (void)fbShare{

    NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE],self.article.tittle];
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:self.article.link]];
    [self presentViewController:controller animated:YES completion:Nil];
 
}

- (void)emailShare {
    if ([MFMailComposeViewController canSendMail]) {
        
        [UINavigationBar appearance].barTintColor = [Util headerColor];
        NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE], [Constants SHARE_TITTLE]];
        NSString *messageBody = [NSString stringWithFormat:@"%@\n%@", self.article.brief ,self.article.link];
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:title];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        [mc.navigationBar setTranslucent:NO];
        [mc.navigationBar setTintColor:[UIColor whiteColor]];
        
        NSString *thumbnail = @"";
        
            NSArray *mediaGroup = [self.article.mediagroupList copy];
            if (mediaGroup.count){
                NSDictionary *dictionary = [mediaGroup firstObject];
                thumbnail = [dictionary valueForKey:@"thumbnail"];
            }
      
        
        NSURL *url = [NSURL URLWithString:thumbnail];
        NSData *data = [NSData dataWithContentsOfURL:url];
        [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
        
        [self presentViewController:mc animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
}


- (void)TwitterShare {
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    //Article *media = [self.itemsArray objectAtIndex:self.swipeView.currentItemIndex];
    NSString *content = @"";
    
        
        NSArray *mediaGroup = [self.article.mediagroupList copy];
        if(mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            content = [dictionary valueForKey:@"content"];
            
        }
    
    NSString *title = [NSString stringWithFormat:@"%@ %@",[Constants SHARE_PRE_TITTLE],self.article.tittle];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:self.article.link]];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}





-(void)didTapOutside{
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.fontResizeView.frame =  CGRectMake(0.0f, -30.0f, self.view.bounds.size.width, 30.0f);
        UIButton *button = (UIButton *) self.fontBarButtonItem.customView;
        [button setSelected:NO];
        [self.fontResizeView removeFromSuperview];
        [self.invisibleButton removeFromSuperview];
    } completion:^(BOOL finished) {
    }];
}

-(void)checkFontSizeTypeinlabel:(UILabel *)label withCompletionBlock: (void (^)( )) completionBlock
{
    switch ([Data sharedInstance].fontSizeType) {
        case FontSizeTypeDefault:
            fontSize = 14;
            [label setFont:[UIFont systemFontOfSize:14]];
            break;
        case FontSizeTypeSmall:
            fontSize = 13;
            [label setFont:[UIFont systemFontOfSize:13]];
            break ;
        case FontSizeTypeLarge:
            fontSize = 15;
            [label setFont:[UIFont systemFontOfSize:15]];
            
            break ;
        case FontSizeTypeXLarge:
            fontSize = 16;
            [label setFont:[UIFont systemFontOfSize:16]];
            break ;
        case FontSizeTypeXSmall:
            fontSize = 12;
            [label setFont:[UIFont systemFontOfSize:12]];
            break ;
    }
    completionBlock();
    
}

-(void)didTapBookmark
{
    if ([[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]) {
        [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:self.article];
        [self.view makeToast:[Constants REMOVE_FROM_FAV]];
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:self.article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
    }
    
    UIButton *button = (UIButton *) _bookmarkBarButtonItem.customView;
    [button setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]];
    
    if (self.delegate && [self.delegate respondsToSelector: @selector(didTapBookmarkButton)]) {
        [self.delegate performSelector:@selector(didTapBookmarkButton)];
    }

    if ([[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]){
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:self.article.guid andArticleTitle:self.article.guid];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didBookmarkChanged"
                                                        object:nil
                                                      userInfo:nil];
    [self reloadSwipeView];
}

-(void)didTapTopNewsBookmark:(UIButton *)sender {
    Article *article = (Article *) [self.itemsArray objectAtIndex:self.top5pageControl.currentPage];
    
    if ([[LibraryAPI sharedInstance] isBookmarkedArticle:article]) {
        [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:article];
        [self.view makeToast:[Constants REMOVE_FROM_FAV]];
        for (Article *bookmarkArticle in _bookmarks) {
            if ([bookmarkArticle.guid isEqualToString:article.guid]) {
                [_bookmarks removeObject:bookmarkArticle];
                break;
            }
        }
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
        [_bookmarks addObject:article];
    }
    
    if ([[LibraryAPI sharedInstance] isBookmarkedArticle:article]){
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    }
    
    UIButton *button = (UIButton *) sender;
    [button setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:article]];
    [self.bookmarkButton setSelected:[[LibraryAPI sharedInstance] isBookmarkedArticle:self.article]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didBookmarkChanged"
                                                        object:nil
                                                      userInfo:nil];

}

- (float)cellHeightWithText: (NSString *)text andFontSize:(NSInteger)size
{
    float height;
    CGSize maximumLabelSize = CGSizeMake(self.view.bounds.size.width-16, FLT_MAX);
    UIFont *font =[UIFont systemFontOfSize:size];
    CGRect labelRect = [text  boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    height = labelRect.size.height;
    return height;
}

- (void)flushCache
{
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDisk];
}

-(void)sliderAction
{
        switch (lroundf(self.slider.value)) {
            case 0: {
                self.slider.value = 0;
                [Data sharedInstance].fontSizeType = FontSizeTypeXSmall;
                break;
            } case 1: {
                self.slider.value = 1;
                [Data sharedInstance].fontSizeType = FontSizeTypeSmall;
                break;
            } case 2: {
                self.slider.value = 2;
                [Data sharedInstance].fontSizeType = FontSizeTypeDefault;
                break;
            } case 3: {
                self.slider.value = 3;
                [Data sharedInstance].fontSizeType = FontSizeTypeLarge;
                break;
            } case 4: {
                self.slider.value = 4;
                [Data sharedInstance].fontSizeType = FontSizeTypeXLarge;
                break;
            }
        }
   // self.parentViewController
    [self.table reloadData];
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.fontResizeView.frame =  CGRectMake(0.0f, -30.0f, self.view.bounds.size.width, 30.0f);
        UIButton *button = (UIButton *) self.fontBarButtonItem.customView;
        [button setSelected:NO];
        [self.fontResizeView removeFromSuperview];
        [self.invisibleButton removeFromSuperview];
    } completion:^(BOOL finished) {
    }];
    
    
}

- (void) requestWebView
{
    self.descriptionWebview.delegate = self;
    self.requestForDescription = YES;
    NSString *copyright = @"";
    if (self.article.copyright.length) {
        copyright = [NSString stringWithFormat:@"<span style=\"float: left;font-family:%@; font-size:%fpx;\">%@</span>", @"Helvetica Neue",
                     fontSize+2, self.article.copyright];
    }
    
    NSString *pagination = @"";
    if (self.totalPages > 1) {
        
        pagination = [NSString stringWithFormat:@"<span style=\"float: right;font-family:%@; font-size:%fpx;\"><font color=\"#ADADAD\">%ld / %ld</font></span>", @"Helvetica Neue", fontSize+2, (long)self.pageNumber, (long)self.totalPages];
    }
    
    
    NSString *htmlstring = [NSString stringWithFormat:@"<span style=\"font-family:%@; font-size: %f\">%@</span> ",
                            @"Helvetica Neue",
                            fontSize+2,self.article.desc];
    NSString *finalHTML = [NSString stringWithFormat:@"%@ %@ %@<p>&nbsp;</p>", htmlstring, copyright, pagination];
    [self.descriptionWebview loadHTMLString:[Util formatHtmlData:finalHTML] baseURL:[NSURL URLWithString:[Constants URL_BASE]]];
}

-(void) displayfontResizeView:(UIButton *)sender {
    
    UIButton *button = (UIButton *) sender;
    
    if([button isSelected]){
        [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.fontResizeView.frame =  CGRectMake(0.0f, -30.0f, self.view.bounds.size.width, 30.0f);
            UIButton *button = (UIButton *) self.fontBarButtonItem.customView;
            [button setSelected:NO];
            [self.fontResizeView removeFromSuperview];
            [self.invisibleButton removeFromSuperview];
        } completion:^(BOOL finished) {
        }];
    }else{
        [self.fontResizeView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1]];
        
        UILabel *small = [UILabel new];
        [small setText:@"A"];
        [small setFont:[UIFont systemFontOfSize:16]];
        [small setFrame:CGRectMake(16, 0, 25, 30)];
        [small setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
        
        UILabel *big = [UILabel new];
        [big setFrame:CGRectMake(self.view.bounds.size.width-32, 0, 25, 30)];
        [big setText:@"A"];
        [big setFont:[UIFont systemFontOfSize:20]];
        [big setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];

        [self.slider setBackgroundColor:[UIColor clearColor]];
        self.slider.minimumValue = 0.0;
        self.slider.maximumValue = 4.0;
        self.slider.continuous = YES;
         [self.slider setFrame:CGRectMake(40, 0, self.view.bounds.size.width-80, 30)];
        self.slider.value = [Data sharedInstance].fontSizeType ;
        
        switch ([Data sharedInstance].fontSizeType) {
            case FontSizeTypeDefault:
                self.slider.value = 2;
                break;
            case FontSizeTypeSmall:
                self.slider.value = 1;
                break ;
            case FontSizeTypeLarge:
                self.slider.value = 3;
                break ;
            case FontSizeTypeXLarge:
                self.slider.value = 4;
                break ;
            case FontSizeTypeXSmall:
                self.slider.value = 0;
                break ;
        }
        
        
        
        [self.fontResizeView addSubview:self.slider];
        [self.fontResizeView addSubview:small];
        [self.fontResizeView addSubview:big];
        [self.view addSubview:self.fontResizeView];
        [self.view addSubview:self.invisibleButton];
        [self.view bringSubviewToFront:self.fontResizeView];
        
        [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.fontResizeView.frame =  CGRectMake(0.0f, 0.0f, self.table.frame.size.width, 30.0f);
            [button setSelected:YES];
        } completion:^(BOOL finished) {
        }];

    }
}

- (BOOL)validateEmail:(NSString *)candidate
{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
    /*
     NSString *emailRegex = @"[A-Z0-9a-z._%+-:]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
     NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
     return [emailTest evaluateWithObject:emailStr];*/
}

- (void)tagView
{
    if (self.screenType == SpecialReportPagesScreenType) {
        [[AnalyticManager sharedInstance] trackSpecialReportNewsDetailScreenWithGuId:self.article.guid andArticleTitle:self.article.tittle  andSpecialReport:self.article.enCategory andSpecialReportEnTittle:self.specialReportEnTittle];
    } else if (self.screenType == LatestNewsPagesScreenType) {
        [[AnalyticManager sharedInstance] trackContentScreenWithCategoryName:@"latest news" andArticleTitle:self.article.tittle andGUID:self.article.guid];
    } else if (self.screenType == CAPagesScreenType || [self.article.type isEqualToString:[Constants CA_TYPE]]) {
        [[AnalyticManager sharedInstance] trackContentScreenInCAWithCategoryName:self.article.enCategory andArticleTitle:self.article.tittle andGUID:self.article.guid];
    } else {
         [[AnalyticManager sharedInstance] trackContentScreenWithCategoryName:self.article.enCategory andArticleTitle:self.article.tittle andGUID:self.article.guid];
    }

}

#pragma mark OBwidgetDeletegate
- (void)widgetView:(UIView<OBWidgetViewProtocol> *)widgetView tappedRecommendation:(OBRecommendation *)recommendation {

    NSURL * url = [Outbrain getOriginalContentURLAndRegisterClickForRecommendation:recommendation];
    TMWebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewControllerId"];
    webViewController.title = [Constants MORE_FROM_NEWS];
    webViewController.urlString = [url absoluteString];
    [self.navigationController pushViewController:webViewController animated:YES];
}

#pragma ADS

- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    self.bannerView.adSize = kGADAdSizeBanner;
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    /*[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       ]];*/
   // self.bannerView.adSizeDelegate = self;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:request];
    
}

#pragma mark GADbannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view{
    self.bannerHeight.constant = 50;
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
}

@end
