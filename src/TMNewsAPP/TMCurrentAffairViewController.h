//
//  TMCurrentAffairViewController.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
@interface TMCurrentAffairViewController : UIViewController
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@end
