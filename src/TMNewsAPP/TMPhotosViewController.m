//cloned
//
//  TMPhotosViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/8/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMPhotosViewController.h"
#import "TMPhotoCollectionViewCell.h"
#import "TMHeaderSectionCollectionReusableView.h"
#import "TMLongCollectionViewCell.h"
#import "TMPhotoBrowserViewController.h"
#import "AppDelegate.h"
#import "TMMainPageViewController.h"
#import "UIImageView+AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Helpers.h"
#import "MBProgressHUD.h"
#import "DataManager.h"
#import "LibraryAPI.h"
#import "Media.h"
#import "NSString+Helpers.h"
#import "UIImageView+Helpers.h"
#import "Constants.h"
#import "Util.h"
#import "AnalyticManager.h"

@interface TMPhotosViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UIActionSheetDelegate, GADBannerViewDelegate, GADAdSizeDelegate>

@property (nonatomic, strong) NSMutableArray *newsPhotoArray;
@property (nonatomic, strong) NSMutableArray *photoGalleryArray;
@property (nonatomic, assign) NSUInteger *currentPage;

@end
static NSString * const identifier = @"sectionHeader";
static NSString * const cellLeftIdentifier = @"cellLeft";
static NSString * const cellCenterIdentifier = @"cellCenter";
static NSString * const cellRightIdentifier = @"cellRight";

static dispatch_group_t group;

@implementation TMPhotosViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    self.newsPhotoArray = [[NSMutableArray alloc]init];
    self.photoGalleryArray = [[NSMutableArray alloc]init];
    
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [self getPhotoListWithForce:NO];
    if (self.isFromMain){
        [[AnalyticManager sharedInstance] trackPhotoTab];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([Util is_iPad]){
        self.bannerHeight.constant = 0;
    }else {
        self.bannerHeight.constant = 50;
    }
    
    UIRefreshControl *refreshFeed = [[UIRefreshControl alloc] init];
    refreshFeed.backgroundColor = [UIColor whiteColor];
    refreshFeed.tintColor = [UIColor blackColor];
    [refreshFeed addTarget:self action:@selector(reloadTableData:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshFeed];
    [self getPhotoListWithForce:NO];
    [self setUpads];
     self.collectionView.alwaysBounceVertical = YES;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark CollectionView datasource

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([Util is_iPad]) {
        if(self.isFromMain){
            CGRect screenBounds = [[UIScreen mainScreen] bounds];
            float rW = 0.0f;
            float rH = 0.0f;
            float width = 0.0f;
            float width2 = 0.0f;
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
                if (indexPath.section == 0) {
                    width = (768 / 3) ;
                    width2 = (768 / 3);
                } else {
                    width = (768 / 3) - 11;
                    width2 = (768 / 3) - 11;
                }
                
            }else{
                if (indexPath.section == 0) {
                    width = (1024 / 3) + .25;
                    width2 = (1024 / 4) + .25;
                } else {
                    width = (1024 / 3) - 11;
                    width2 = (1024 / 4) - 11;
                }
               
            }
            
            
            float height = 0;
            
            if (indexPath.section == 0) {
                rW = 8.0f;
                rH = 5.0f;
                height = (rH / rW * width) + 50;
                return CGSizeMake(width, height);
            } else {
                rW = 224.0f;
                rH = 137.0f;
                height = (rH / rW * width2) + 115;
                return CGSizeMake(width2, height);
            }
  
        }else{
            
            float width = 0.0f;
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
                width = (768 / 3) - 11;
            }else{
                width = (1024 / 4) - 11;
                
            }
            
            float rW = 0.0f;
            float rH = 0.0f;
            
            
            rW = 224.0f;
            rH = 137.0f;
            float height = 0;
            height = (rH / rW * width) + 115;
            
            return CGSizeMake(width, height);
        }
        
        
    } else {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        float rW = 127.0f;
        float rH = 71.0f;
        if(self.isFromMain){
            if (indexPath.section == 0) {
                float width = (screenBounds.size.width / 2) - 12;
                float height = (rH / rW * width) + 35;
                return CGSizeMake( width, height - 5);
            }else{
                return CGSizeMake(screenBounds.size.width, 85);
            }
        }else{
            return CGSizeMake(screenBounds.size.width, 85);
        }
        
    }
    
    return CGSizeZero;
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if(self.isFromMain){
         return 2;
    }else{
        return 1;
    }
   
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.isFromMain){
        if (section == 0) {
            return self.newsPhotoArray.count;
        } else {
            return self.photoGalleryArray.count;
        }
    }else{
         return self.photoGalleryArray.count;
    }
    
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
   

    if(self.isFromMain){
        if (indexPath.section == 0) {
            NSString *cellId = cellLeftIdentifier;
            
            if ([Util is_iPad]) {
                int remainder = indexPath.row % 3;
                
                if (remainder == 1) {
                    cellId = cellCenterIdentifier;
                } else if (remainder == 2) {
                    cellId = cellRightIdentifier;
                }
            }

            
            Article *article = (Article *) [self.newsPhotoArray objectAtIndex:indexPath.row];
            TMPhotoCollectionViewCell *cell = (TMPhotoCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
            cell.titleLabel.text = article.tittle;
            
            NSString *totalPhotos = nil;
            if ( [article.totalNum integerValue] ==1 )
                totalPhotos = [NSString stringWithFormat:@"%@ %@", article.totalNum, [Constants XPHOTO]];
            else
                totalPhotos = [NSString stringWithFormat:@"%@ %@", article.totalNum, [Constants XPHOTOS]];
                
            cell.keepingCountLabel.text = totalPhotos;
            NSString *thumbnail = @"";
            if (article.mediagroupList) {
                if (article.mediagroupList.count) {
                    NSDictionary *dictionary = [article.mediagroupList firstObject];
                    thumbnail = [dictionary valueForKey:@"thumbnail"];
                }
            }
            
            
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                              placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:SDWebImageRefreshCached];
            
            
           // [UIColor colorWithRed:235.0f/255 green:235.0f/255 blue:235.0f/255 alpha:1];
            //[cell.layer setBorderWidth:1.0];
         //   [cell.layer setBorderColor:[[UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1] CGColor]];
            
            return cell;
            
        }else {
            Article *article = (Article *) [self.photoGalleryArray objectAtIndex:indexPath.row];
            TMLongCollectionViewCell *cell = (TMLongCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell2" forIndexPath:indexPath];
            cell.descLabel.text = article.tittle;
            cell.pubDate.text = [article.pubDate formatDate];

            NSString *totalPhotos = nil;
            if ( [article.totalNum integerValue] ==1 )
                totalPhotos = [NSString stringWithFormat:@"%@ %@", article.totalNum, [Constants XPHOTO]];
            else
                totalPhotos = [NSString stringWithFormat:@"%@ %@", article.totalNum, [Constants XPHOTOS]];
            
            cell.imageCounter.text = totalPhotos;
            
            NSString *thumbnail = @"";
            if (article.mediagroupList) {
                if (article.mediagroupList.count) {
                    NSDictionary *dictionary = [article.mediagroupList firstObject];
                    thumbnail = [dictionary valueForKey:@"thumbnail"];
                }
            }
            [cell.thumbnail setBorder];
            [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                              placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]]
                                       options:SDWebImageRefreshCached];
            return cell;
        }

    }else{
        Article *article = (Article *) [self.photoGalleryArray objectAtIndex:indexPath.row];
        TMLongCollectionViewCell *cell = (TMLongCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell2" forIndexPath:indexPath];
        cell.descLabel.text = article.tittle;
        cell.pubDate.text = [article.pubDate formatDate];

        NSString *totalPhotos = nil;
        if ( [article.totalNum integerValue] ==1 )
            totalPhotos = [NSString stringWithFormat:@"%@ %@", article.totalNum, [Constants XPHOTO]];
        else
            totalPhotos = [NSString stringWithFormat:@"%@ %@", article.totalNum, [Constants XPHOTOS]];
        
        cell.imageCounter.text = totalPhotos;
        
        
        NSString *thumbnail = @"";
        if (article.mediagroupList) {
            if (article.mediagroupList.count) {
                NSDictionary *dictionary = [article.mediagroupList firstObject];
                thumbnail = [dictionary valueForKey:@"thumbnail"];
            }
        }
        [cell.thumbnail setBorder];
        [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:thumbnail]
                          placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]]
                                   options:SDWebImageRefreshCached];
        return cell;
    }

    
   
    
    return nil;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {

    TMHeaderSectionCollectionReusableView *reusableView = (TMHeaderSectionCollectionReusableView *) [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:identifier forIndexPath:indexPath];
    
    if(self.isFromMain){
        if (indexPath.section == 0) {
            reusableView.sectionTitle.text = [Constants LATEST_NEWS_PHOTOS];
            if ([Util is_iPad]) {
                reusableView.backgroundColor = [UIColor whiteColor];
            } else {
                reusableView.backgroundColor = [UIColor colorWithRed:235.0f/255 green:235.0f/255 blue:235.0f/255 alpha:1];
            }
            
            
        }else if (indexPath.section == 1){
            if(self.photoGalleryArray.count){
                reusableView.sectionTitle.text = [Constants LATEST_PHOTO_GALLERIES];
                if ([Util is_iPad]) {
                    reusableView.backgroundColor = [UIColor colorWithRed:235.0f/255 green:235.0f/255 blue:235.0f/255 alpha:1];
                } else {
                    reusableView.backgroundColor = [UIColor whiteColor];
                }
               
            }
            else {
                reusableView.sectionTitle.text = @"";
            }
        }
    }else{
        
        [reusableView setHidden:YES];

    }
    
    return reusableView;
   
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
  
    if ([Util is_iPad]) {
        if(self.isFromMain){
            if (section == 0)
                 return UIEdgeInsetsMake(0, 0, 0, 0);
            
             return UIEdgeInsetsMake(0, 9, 9, 9);
        }else{
            return UIEdgeInsetsMake(9, 9, 9, 9);
        }
       
    } else {
        if (section == 0)
            return UIEdgeInsetsMake(0, 8, 8, 8);
    }
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if ([Util is_iPad]) {
        if (section == 0)
            return 0;
        return 4.5f;
    } else {
        if (section == 0)
            return 5.0f;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if ([Util is_iPad]) {
        if (section == 0)
            return 0;
        return 9.0f;
    } else {
        if (section == 0)
           return 8.0f;
    }

    return 0.0f;
}

#pragma mark CollectionView delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if(!self.isFromMain){
        return CGSizeZero;
    }else{
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), 42);
    }
   
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isFromMain){
        if(indexPath.section == 0){
            Article *article = (Article *)[self.newsPhotoArray objectAtIndex:indexPath.row];
            TMPhotoBrowserViewController *photobrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
            photobrowser.article = article;
            photobrowser.isNewsPhoto = YES;
            photobrowser.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [self presentViewController:photobrowser animated:NO completion:nil];
            //photobrowser.view.alpha = .96;
            
        }else {
            UICollectionViewCell *datasetCell =[collectionView cellForItemAtIndexPath:indexPath];
            datasetCell.backgroundColor = [UIColor lightGrayColor];
            
            Article *article = (Article *)[self.photoGalleryArray objectAtIndex:indexPath.row];
            TMPhotoBrowserViewController *photobrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
            photobrowser.article = article;
            photobrowser.isNewsPhoto = NO;
            photobrowser.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [self presentViewController:photobrowser animated:NO completion:^{
                datasetCell.backgroundColor = [UIColor whiteColor];
            }];
            //photobrowser.view.alpha = .96;
        }
        
    }else{
        Article *article = (Article *)[self.photoGalleryArray objectAtIndex:indexPath.row];
        TMPhotoBrowserViewController *photobrowser = (TMPhotoBrowserViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoBrowserViewController"];
        if(self.screenType == SpecialReportPhotosScreenType){
            photobrowser.isFromExclusive = TRUE;
            photobrowser.photosFromEksklusif = self.photoEklusifTitle;
        }else if (self.screenType == CAPhotosScreenType){
            photobrowser.isFromCA = TRUE;
            photobrowser.photosFromCA = self.photoCATitle;
        }
        
        photobrowser.article = article;
        photobrowser.isNewsPhoto = NO;
        photobrowser.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [self presentViewController:photobrowser animated:NO completion:nil];
        
        //photobrowser.view.alpha = .96;
    }
}

- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor colorWithRed:235/255.0f green:236/255.0f blue:237/255.0f alpha:1];
    }
    
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
        cell.contentView.backgroundColor = nil;
    }
}

#pragma mark Notif Observers
/*- (void)deviceOrientationDidChangeNotification:(NSNotification*)note
{
    
    [self.collectionView reloadData];
}*/

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.collectionView reloadData];
}

#pragma mark private function

-(void)reloadTableData: (UIRefreshControl *)refreshFeed {
    
    if (refreshFeed) {
        
        [self getPhotoListWithForce:YES];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        refreshFeed.attributedTitle = attributedTitle;
        
        [refreshFeed endRefreshing];
    }
}



-(void)getPhotoListWithForce: (BOOL)forced {
    
    if(self.isFromMain){
 
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        [[LibraryAPI sharedInstance]getPhotosWithForce:forced andCompletionBlock:^(NSMutableArray *array) {
            if (array) {
                [self.newsPhotoArray removeAllObjects];
                [self.photoGalleryArray removeAllObjects];
                
                for (Article *article in array) {
                    
                    if ([article.sectionName isEqualToString:@"news photos"]) {
                        [self.newsPhotoArray addObject:article];
                    }
                    
                    if ([article.sectionName isEqualToString:@"gallery photos"]) {
                        [self.photoGalleryArray addObject:article];
                    }
                }
                
                [self.collectionView reloadData];
                
            }
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
        
        
    
    }
    else{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LibraryAPI sharedInstance]getSectionArticlesFromURL:self.sectionURL withFeedUrl:nil isForced:forced withCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            [self.newsPhotoArray removeAllObjects];
            [self.photoGalleryArray removeAllObjects];
            
            for (Article *article in array) {
                
                if ([article.sectionName isEqualToString:@"news photos"]) {
                    [self.newsPhotoArray addObject:article];
                }
                
                if ([article.sectionName isEqualToString:@"gallery photos"]) {
                    [self.photoGalleryArray addObject:article];
                }
            }
            
            [self.collectionView reloadData];
            
        }
        
    }];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    
    
}
#pragma mark admob
- (void)setUpads
{
    DFPRequest *request = [DFPRequest request];
    request.testDevices = @[@"6575c76be4a1036ad13b034d89a678a13cf00474",
                            @"Simulator"
                            ];
    
    self.bannerView.adUnitID = [Constants ADMOB_UNITID_BANNERAD];
    self.bannerView.adSize = kGADAdSizeBanner;
    //[self.bannerView setValidAdSizes:@[NSValueFromGADAdSize(kGADAdSizeBanner),
                                       //NSValueFromGADAdSize(kGADAdSizeLargeBanner)
                                       //]];
    //self.bannerView.adSizeDelegate = self;
    self.bannerView.rootViewController = [AppDelegate sharedInstance].window.rootViewController;;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:request];
    [self.bannerView setHidden:YES];
    
}
#pragma mark GADbannerViewDelegate

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    self.bannerHeight.constant = 0;
    [self.bannerView setHidden:YES];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view {
    if (self.bannerHeight.constant == 0) {
       // if ([Util is_iPad]){
            //self.bannerHeight.constant = 100;
        //}else {
            self.bannerHeight.constant = 50;
        //}
    }
    [self.bannerView setHidden:NO];
}
/*- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)gadSize{
    self.bannerHeight.constant = gadSize.size.height;
}*/

@end

