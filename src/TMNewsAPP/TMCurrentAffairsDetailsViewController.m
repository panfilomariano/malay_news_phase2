//
//  TMCurrentAffairsDetailsViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMCurrentAffairsDetailsViewController.h"

//#import "TMCAPhotoViewController.h"
#import "TMEpisodesViewController.h"
#import "TMWebViewController.h"
#import "LibraryAPI.h"
#import "Article.h"

@interface TMCurrentAffairsDetailsViewController () <ViewPagerDataSource, ViewPagerDelegate>
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation TMCurrentAffairsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    //[self getCurrentAffairsCategory];
}



#pragma mark ViewPager DataSource
-(NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager{
    return [self.newsCategories count];
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index{
    NSDictionary *dictionary = [self.newsCategories objectAtIndex:index];
    NSString *category = [dictionary valueForKey:@"category"];
    UILabel *label = [UILabel new];
    label.text = [NSString stringWithFormat:category, index];
    [label setTextColor:[UIColor whiteColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label sizeToFit];
    return  label;
    
   
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    //Article *article = [self.newsCategories objectAtIndex:index];
    
    NSDictionary *dict = [self.newsCategories objectAtIndex:index];
    NSString *sectionType = [dict valueForKey:@"sectionType"];
    NSString *sectionUrl = [dict valueForKey:@"sectionURL"];
    
    if([sectionType isEqualToString:@"episode"]){
        TMEpisodesViewController *episodeVC = (TMEpisodesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"episodesViewController"];
        episodeVC.sectionUrlApi = sectionUrl;
        return episodeVC;
    
    }else if ([sectionType isEqualToString:@"photos"]){
        TMEpisodesViewController *episodeVC = (TMEpisodesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"episodesViewController"];
        episodeVC.sectionUrlApi = sectionUrl;
        return episodeVC;
    }else if ([sectionType isEqualToString:@"html"]){
        TMWebViewController *webview = (TMWebViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
        webview.urlString = sectionUrl;
        return webview;
    }else {
        TMEpisodesViewController *episodeVC = (TMEpisodesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"episodesViewController"];
        episodeVC.sectionUrlApi = sectionUrl;
        return episodeVC;
    
    }
    
    return nil;
}

#pragma mark ViewPager Delegate
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    return [UIColor colorWithRed:0/255.0f green:112/255.0f blue:202/255.0f alpha:1];
}




















- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
