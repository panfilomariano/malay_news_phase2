//
//  TMPhotoBrowserViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/11/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMPhotoBrowserViewController.h"
#import "SwipeView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Helpers.h"
#import "LibraryAPI.h"
#import "Media.h"
#import "NSString+Helpers.h"
#import "MBProgressHUD.h"
#import "TMNewsCollectionViewLayout.h"
#import "Util.h"
#import "Constants.h"
#import <MessageUI/MessageUI.h>
#import "AnalyticManager.h"
#import <Social/Social.h>

@interface TMPhotoBrowserViewController () <UICollectionViewDataSource, UICollectionViewDelegate, SwipeViewDataSource, SwipeViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    float width;
}
@property (nonatomic, weak) IBOutlet SwipeView *swipeView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *galleryTitle;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UILabel *photoTitle;
@property (nonatomic, weak) IBOutlet UILabel *caption;
@property (nonatomic, weak) IBOutlet UILabel *navTittleLabel;
@property (nonatomic, strong) NSMutableArray *photoList;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *height;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *landscapeHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *swipeViewWidth;



@property (nonatomic) BOOL isPortrait;
- (IBAction)didTapShareButton:(id)sender;
- (IBAction)didTapBackButton:(id)sender;

@end

@implementation TMPhotoBrowserViewController

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (!self) {
        return nil;
    }
    
    _photoList = [[NSMutableArray alloc]init];
    
    return self;
}




- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.currentPage = 0;
    self.swipeView.pagingEnabled = TRUE;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        width = 768.0f;
        self.swipeViewWidth.constant = width;
    }else if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight){
        width = 768;
        self.landscapeHeight.constant = 20.0f;
        self.swipeViewWidth.constant = width;
    }
    
    //[self.navigationController.navigationBar setFrame:CGRectMake(0, 0, width, 44)];
    self.navigationController.navigationBar.translucent = YES;
    if(self.isNewsPhoto){
        self.navTittleLabel.text = self.article.tittle;
        self.galleryTitle.text = @"";
        self.pubDate.text = @"";
    }else{
        self.galleryTitle.text = self.article.tittle;
        self.pubDate.text = [self.article.pubDate formatDate];
        self.navTittleLabel.text = @"";
    }
   
    self.height.constant = 432;
    [MBProgressHUD showHUDAddedTo:self.swipeView animated:YES];
    [[LibraryAPI sharedInstance]getMediaItemsFromURL:self.article.sectionURL andFeedLink:self.article.feedlink withCompletionBlock:^(NSMutableArray *array) {
        self.photoList = [array mutableCopy];
        [self.collectionView reloadData];
        [self.swipeView reloadData];
        if (self.photoList.count){
            
            [self trackTagging];
            
            Media *media = (Media *) [self.photoList objectAtIndex:0];
            self.photoTitle.text = media.tittle;
            NSArray *mediaGroup = [media.mediaGroup copy];
            NSDictionary *dictionary = [mediaGroup firstObject];
            if ([dictionary valueForKey:@"caption"]) {
                self.caption.text = [dictionary valueForKey:@"caption"];
            }

        }

        [MBProgressHUD hideHUDForView:self.swipeView animated:YES];

    }];


}

- (void)trackTagging
{
    
    Media *media = (Media *) [self.photoList objectAtIndex:0];
    if (self.isTopNews) {
        [[AnalyticManager sharedInstance] trackOpenPhotoGalleryWithAlbumName:media.tittle];
    }

    if (self.isFromExclusive){
        [[AnalyticManager sharedInstance] trackPhotosWithGuid:media.guid andPhotoTitle:media.tittle andAlbumName:self.article.tittle andSpecialReport:self.photosFromEksklusif];
    }else if (self.isFromCA){
        [[AnalyticManager sharedInstance] trackCAOpenPhotoGalleryWithCategoryName:self.photosFromCA andAlbumName:self.article.tittle];
    }else{
        [[AnalyticManager sharedInstance] trackOpenPhotoAlbumWithAlbumName:self.article.tittle andGuid:media.guid andArticleTitle:media.tittle andPhotoTitle:media.tittle];
    }
    
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        width = 768.0f;
        self.landscapeHeight.constant = 200.0f;
    } else {
        width = 768.0f;
        self.landscapeHeight.constant = 20.0f;
        
    }
    
    self.height.constant = 432;
    //self.height.constant = (aheight/awidth) * width;
    [self.swipeView reloadData];
}

#pragma mark swipeview data source

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    
    
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.swipeView.bounds];

        //[imageView setContentMode:UIViewContentModeScaleAspectFit];
        if (self.photoList.count) {
            Media *media = (Media *) [self.photoList objectAtIndex:index];
            NSString *thumbnail = @"";
            NSArray *mediaGroup = [media.mediaGroup copy];
            if (mediaGroup.count) {
                NSDictionary *dictionary = [mediaGroup firstObject];
                thumbnail = [dictionary valueForKey:@"content"];
            }
            NSURL *imgurl =  [NSURL URLWithString:thumbnail];
            //imageView.alpha = .96;
            [imageView sd_setImageWithURL:imgurl
                         placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER_BIG]] options:SDWebImageRefreshCached];
            
            

           
            
            UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
            if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
            }
            
        }
        return imageView;
   
}



-(CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
   
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        width = 768.0f;
        self.height.constant = 432;
    }else if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight){
        width = 768;
        self.height.constant = 432;
        
    }
    if ([Util is_iPad]){
        return CGSizeMake(width, 432);
    }else {
        return self.swipeView.bounds.size;
    }

    
    
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return [self.photoList count];
}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:swipeView.currentItemIndex inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    Media *media = (Media *) [self.photoList objectAtIndex:swipeView.currentItemIndex];
    self.photoTitle.text = media.tittle;
     NSArray *mediaGroup = [media.mediaGroup copy];
    NSDictionary *dictionary = [mediaGroup firstObject];
    self.caption.text = @"";
    if ([dictionary valueForKey:@"caption"]) {
        self.caption.text = [dictionary valueForKey:@"caption"];
    }
    
    [[AnalyticManager sharedInstance] trackOpenPhotoAlbumWithAlbumName:self.article.tittle andGuid:media.guid andArticleTitle:media.tittle andPhotoTitle:media.tittle];
}

#pragma mark collectionView delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.swipeView scrollToItemAtIndex:indexPath.row duration:0.5f];
}


#pragma mark collectionview data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  [self.photoList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    NSString *thumbnail = @"";
    if(self.photoList.count){
        Media *media = (Media *) [self.photoList objectAtIndex:indexPath.row];
        NSArray *mediaGroup = [media.mediaGroup copy];
        if(mediaGroup.count){
            NSDictionary *dictionary = [mediaGroup firstObject];
            thumbnail = [dictionary valueForKey:@"thumbnail"];
        }
    }
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"smallCell" forIndexPath:indexPath];
    UIImageView *imgView = (UIImageView *) [cell viewWithTag:111];
    [imgView sd_setImageWithURL:[NSURL URLWithString:thumbnail] placeholderImage:[UIImage imageNamed:[Constants PLACEHOLDER]] options:SDWebImageRefreshCached];
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if ([Util is_iPad]) {
        if([Util isPortrait]){
            return UIEdgeInsetsMake(1, (self.view.frame.size.width /15) - 75/2, 1, 0);
        }else{
            return UIEdgeInsetsMake(1, (self.view.frame.size.width /2 ) - 75/2, 1, 0);
        }
    }
    return UIEdgeInsetsMake(1, (self.view.frame.size.width /2 ) - 75/2, 1, 0);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5.0f;
}

#pragma mark private function

- (IBAction)didTapShareButton:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Facebook",
                                  @"Twitter",
                                  @"Email",
                                  nil];
    
    [actionSheet showInView:self.view];
    actionSheet.tag =1;
}

- (IBAction)didTapBackButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([Util isVersion8Above]) {
        [[AnalyticManager sharedInstance] trackPhotoTab];
    }
}

- (void)FBShare {
 
    if (self.photoList.count) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        Media *media = [self.photoList objectAtIndex:self.swipeView.currentItemIndex];
        
        
        NSString *thumbnail = @"";
        NSString *caption = @"";
        NSString *content = @"";
        if(self.photoList.count){
            
            NSArray *mediaGroup = [media.mediaGroup copy];
            if(mediaGroup.count) {
                NSDictionary *dictionary = [mediaGroup firstObject];
                content = [dictionary valueForKey:@"content"];
                thumbnail = [dictionary valueForKey:@"thumbnail"];
                if ([dictionary valueForKey:@"caption"]){
                    caption = [dictionary valueForKey:@"caption"];
                }
                
            }
            
        }
        
        NSString *title = [NSString stringWithFormat:@"%@ %@\n\n%@", [Constants SHARE_PRE_TITTLE],media.tittle,caption];
        [controller setInitialText:title];
        [controller addURL:[NSURL URLWithString:thumbnail]];
        controller.completionHandler = ^(SLComposeViewControllerResult result) {
            if(result == SLComposeViewControllerResultDone){
                [[AnalyticManager sharedInstance] trackFacebookSharePhotoWithGuid:media.guid andArticleTitle:media.tittle];
            }
        };
        [self presentViewController:controller animated:YES completion:Nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                        message:@"Nothing to share."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }

}

- (void)TwitterShare {
    if (self.photoList.count) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        Media *media = [self.photoList objectAtIndex:self.swipeView.currentItemIndex];
        NSString *content = @"";
        if(self.photoList.count){
            
            NSArray *mediaGroup = [media.mediaGroup copy];
            if(mediaGroup.count) {
                NSDictionary *dictionary = [mediaGroup firstObject];
                content = [dictionary valueForKey:@"content"];
                
            }
        }
        NSString *title = [NSString stringWithFormat:@"%@  %@",[Constants SHARE_PRE_TITTLE],media.tittle];
        [controller setInitialText:title];
        [controller addURL:[NSURL URLWithString:content]];
        controller.completionHandler = ^(SLComposeViewControllerResult result){
            if(result == SLComposeViewControllerResultDone){
                [[AnalyticManager sharedInstance] trackTwitterSharePhotoWithGuid:media.guid andArticleTitle:media.tittle];
            }
        };
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                        message:@"Nothing to share."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

- (void)emailShare {
    if (self.photoList.count) {
        Media *media = [self.photoList objectAtIndex:self.swipeView.currentItemIndex];
        if ([MFMailComposeViewController canSendMail]) {
            
            [UINavigationBar appearance].barTintColor = [Util headerColor];
/*#if TAMIL
            [UINavigationBar appearance].barTintColor = [UIColor blackColor];
#else
            [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:2/255.0f green:25/255.0f blue:39/255.0f alpha:1];
#endif*/
            //[[UINavigationBar appearance] setTranslucent:NO];
            
            NSString *caption = @"";
            NSString *thumbnail = @"";
            if(self.photoList.count){
                
                NSArray *mediaGroup = [media.mediaGroup copy];
                if(mediaGroup.count) {
                    NSDictionary *dictionary = [mediaGroup firstObject];
                    if ([dictionary valueForKey:@"caption"]){
                        caption = [dictionary valueForKey:@"caption"];
                    }
                    thumbnail = [dictionary valueForKey:@"thumbnail"];
                    
                    
                }
            }
            NSString *title = [NSString stringWithFormat:@"%@ %@", [Constants SHARE_PRE_TITTLE], [Constants SHARE_TITTLE]];
            NSString *messageBody = [NSString stringWithFormat:@"%@\n\n%@", media.tittle ,caption];
            
            NSArray *toRecipents = [NSArray arrayWithObject:@""];
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:title];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTranslucent:NO];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];

            NSURL *url = [NSURL URLWithString:thumbnail];
            NSData *data = [NSData dataWithContentsOfURL:url];
            [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
            [self presentViewController:mc animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
            
            //[self presentViewController:mc animated:YES completion:nil];
            
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                          message:@"Nothing to share."
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [alert show];
    }
}



#pragma mark ActonSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
            switch (buttonIndex) {
                case 0:
                    [self FBShare];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self emailShare];
                    break;
                default:
                    break;
            }
}


#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    Media *media = [self.photoList objectAtIndex:self.swipeView.currentItemIndex];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    if(result == MFMailComposeResultSent){
        Media *media = [self.photoList objectAtIndex:self.swipeView.currentItemIndex];
        [[AnalyticManager sharedInstance] trackEmailSharePhotoWithGuid:media.guid andArticleTitle:media.tittle];
    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self dismissViewControllerAnimated:YES completion:NULL];

}



#pragma mark Override
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([Util is_iPad]){
        return UIInterfaceOrientationMaskAll;
    }else {
        return UIInterfaceOrientationMaskPortrait;
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([Util is_iPad]){
        return YES;
    } else {
        return UIInterfaceOrientationPortrait == toInterfaceOrientation;
    }
}



@end
