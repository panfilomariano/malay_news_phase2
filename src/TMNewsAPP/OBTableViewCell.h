//
//  OBTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/6/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OutbrainSDK/OutbrainSDK.h>
#import "OBClassicRecommendationsView.h"
@interface OBTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet OBClassicRecommendationsView * recommendationsView;
@end
