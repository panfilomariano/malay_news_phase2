//
//  TMBeritaListTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/17/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMBeritaListTableViewCellDelagate <NSObject>

- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;

@end


@interface TMBeritaListTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel *date;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UIImageView *playOverlay;
@property (nonatomic, weak) IBOutlet UIButton *starButton;
@property (nonatomic, weak) id <TMBeritaListTableViewCellDelagate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end
