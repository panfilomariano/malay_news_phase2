//
//  TMCollectionTableViewCell.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 1/5/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMtopFiveCollectionViewCell.h"
@interface TMCollectionTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UICollectionView *top5CollectionView;
@property (nonatomic, weak) IBOutlet SMPageControl *pageControl;
@end
