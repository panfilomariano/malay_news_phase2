//
//  TMBookmarksTableViewController.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/23/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMBookmarksTableViewController : UITableViewController
@property (nonatomic, weak) IBOutlet UILabel *noItemsLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *noItemsHeight;
@end
