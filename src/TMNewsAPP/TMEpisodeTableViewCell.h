//
//  TMEpisodeTableViewCell.h
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/18/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LatestEpisodesCellDelegate <NSObject>

- (void) didTapBookMarkButtonAtIndexPath: (NSIndexPath *)indexPath;

@end


@interface TMEpisodeTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *pubDateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UIImageView *playOverlay;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, weak) id <LatestEpisodesCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end
