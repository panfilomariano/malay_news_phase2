//
//  PersitenceManager.m
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/9/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import "PersitenceManager.h"
#import "LibraryAPI.h"
#import "Constants.h"
#import "Weather.h"
#import "Util.h"
@implementation PersitenceManager

- (id) init {
    
    if (self=[super init]) {
        
    }
    
    return self;
}

/*- (void) save
{
   // [[NSUserDefaults standardUserDefaults] setObject:[LibraryAPI sharedInstance].indexItemList forKey:@"INDEX_ITEM_LIST"];
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (Article *article in self.articles) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:article];
        [array addObject:data];
    }
   
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"ARTICLES"];
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"ARTICLES"];
}*/

- (NSArray *) getMediaList
{
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:Constants.MEDIA_KEY];
    
    NSMutableArray *mediaList = [NSMutableArray new];
    for (NSData *data in array) {
        Media *media = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [mediaList addObject:media];
    }
    
    return mediaList;
}

- (NSArray *) getArticles
{
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:Constants.ARTICLES_KEY];
    
    NSMutableArray *articleList = [NSMutableArray new];
    for (NSData *data in array) {
        Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [articleList addObject:article];
    }
    
    return articleList;
}

- (void) deleteArticlesWithFeedURL: (NSString *)feedlink
{
    
    NSArray *articleDataFromDisk = [[NSUserDefaults standardUserDefaults] objectForKey:Constants.ARTICLES_KEY];
    
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleDataFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (NSData *data in articleDataFromDisk) {
        Article *article = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [[LibraryAPI sharedInstance] getIndexListItems:^(NSArray *array) {
            Article *validArticle = nil;
            for (NSDictionary *dictionary in array) {
                NSString *feedSectionUrl = [dictionary valueForKey:@"sectionURL"];
                if ([article.feedlink isEqualToString:feedSectionUrl]) {
                    validArticle = article;
                    break;
                }
            }
            
            if (!validArticle) {
                [tempHolder removeObject:article];
            }
        }];
        
        if ([article.feedlink isEqualToString:feedlink]) {
            [tempHolder removeObject:data];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.ARTICLES_KEY];
    
}

- (void) deleteMediaListWithSectionUrl: (NSString *)sectionUrl
{
    
    NSArray *mediaListDataFromDisk = [[NSUserDefaults standardUserDefaults] objectForKey:Constants.MEDIA_KEY];
    
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in mediaListDataFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (NSData *data in mediaListDataFromDisk) {
        Media *media = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [[LibraryAPI sharedInstance] getIndexListItems:^(NSArray *array) {
            Media *validMedia = nil;
            for (NSDictionary *dictionary in array) {
                NSString *feedSectionUrl = [dictionary valueForKey:@"sectionURL"];
                if ([media.feedlink isEqualToString:feedSectionUrl]) {
                    validMedia = media;
                    break;
                }
            }
            
            if (!validMedia) {
                [tempHolder removeObject:media];
            }
        }];
        
        if ([media.sectionUrl isEqualToString:sectionUrl]) {
            [tempHolder removeObject:media];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.MEDIA_KEY];
}

- (void) deleteArticlesWithKey: (NSString *)key
{
    
    NSArray *articleDataFromDisk = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleDataFromDisk) {
        [tempHolder addObject:data];
    }
    
    [tempHolder removeAllObjects];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.ARTICLES_KEY];
}

- (void) deleteArticleWithKey: (NSString *)key
{
    
    NSArray *articleDataFromDisk = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleDataFromDisk) {
        [tempHolder addObject:data];
    }
    
    [tempHolder removeAllObjects];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:key];
}

- (void) saveArticles: (NSArray *) articles
{
    NSArray *articleListFromDisk = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.ARTICLES_KEY];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleListFromDisk) {
        [tempHolder addObject:data];
    }

    for (Article *article in articles) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:article];
        [tempHolder addObject:data];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.ARTICLES_KEY];
    
    NSArray *articleListFromDiskFinal = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.ARTICLES_KEY];
    
}

- (void) saveMediaList: (NSArray *) mediaList
{
    NSArray *mediaListFromDisk = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.MEDIA_KEY];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in mediaListFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (Media *media in mediaList) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:media];
        [tempHolder addObject:data];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.MEDIA_KEY];
    
    NSArray *mediaListFromDiskFinal = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.MEDIA_KEY];
    
}

- (void) saveMediaList: (NSArray *) mediaList withKey: (NSString *)key
{
    NSArray *mediaListFromDisk = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in mediaListFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (Media *media in mediaList) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:media];
        [tempHolder addObject:data];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:key];
    
    NSArray *mediaListFromDiskFinal = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
}


- (void) saveArticles: (NSArray *) articles withKey: (NSString *)key
{
    NSArray *articleListFromDisk = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleListFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (Article *article in articles) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:article];
        [tempHolder addObject:data];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:key];
     NSArray *articleListFromDiskFinal = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:key];
}
- (void) saveWeather: (NSArray *) weathers withKey: (NSString *)key;
{
    NSArray *weatherListFromDisk = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.WEATHER_KEY];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in weatherListFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (Weather *weather in weathers) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:weather];
        [tempHolder addObject:data];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.WEATHER_KEY];
    NSArray *articleListFromDiskFinal = (NSArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.WEATHER_KEY];
}

- (void) saveWeather:(Weather *)weather
{
    NSMutableArray *articleListFromDisk = (NSMutableArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.WEATHER_KEY];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleListFromDisk) {
        [tempHolder addObject:data];
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:weather];
    [tempHolder addObject:data];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.WEATHER_KEY];

}

- (void) saveArticle: (Article *) article
{
    NSMutableArray *articleListFromDisk = (NSMutableArray *) [[NSUserDefaults standardUserDefaults] objectForKey:Constants.ARTICLES_KEY];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleListFromDisk) {
        [tempHolder addObject:data];
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:article];
    [tempHolder addObject:data];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:Constants.ARTICLES_KEY];

}

- (void) saveArticle: (Article *) article withKey: (NSString *)key
{
    NSMutableArray *articleListFromDisk = (NSMutableArray *) [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleListFromDisk) {
        [tempHolder addObject:data];
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:article];
    [tempHolder addObject:data];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:key];
    
}

- (void) saveObject:(id)object withKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
}

- (void) deleteBookmarkWithArticle: (Article *)article
{
    NSArray *articleDataFromDisk = [[NSUserDefaults standardUserDefaults] objectForKey:[Constants BOOKMARKS_KEY]];
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleDataFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (NSData *data in articleDataFromDisk) {
        Article *articleFromDisk = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if ([articleFromDisk.guid isEqualToString:article.guid]) {
            [tempHolder removeObject:data];
            break;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:[Constants BOOKMARKS_KEY]];
    
}

- (void) deleteWeather: (Weather *)weather
{
    NSArray *articleDataFromDisk = [[NSUserDefaults standardUserDefaults] objectForKey:[Constants WEATHER_KEY]];
    NSMutableArray *tempHolder = [NSMutableArray new];
    
    for (NSData *data in articleDataFromDisk) {
        [tempHolder addObject:data];
    }
    
    for (NSData *data in articleDataFromDisk) {
        Weather *weatherFromDisk = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if ([weatherFromDisk.date isEqualToString:weatherFromDisk.date]) {
            [tempHolder removeObject:data];
            break;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tempHolder forKey:[Constants WEATHER_KEY]];
    
}

@end
