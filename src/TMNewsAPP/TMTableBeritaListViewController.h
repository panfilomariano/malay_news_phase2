//
//  TMTableBeritaListViewController.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/17/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "Article.h"

typedef enum
{
    OthersScreenType,
    NewsScreenType,
    SpecialReportBeritaScreenType,
    CABeritaType
    
} BeritaScreenType;


@protocol TMTableBeritaListViewControllerDelegate <NSObject>
@optional
-(void)loadViewedItems:(NSString *)UrlString;
@end

@interface TMTableBeritaListViewController : UIViewController
@property (nonatomic, weak) id <TMTableBeritaListViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *apiString;
@property (nonatomic, strong) NSMutableDictionary *dictionary;
@property (nonatomic) NSUInteger tabIndex;
@property (nonatomic, strong) NSString *feedLink;
@property (nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bannerHeight;
@property (nonatomic, strong) NSString *categoryForSR;
@property (nonatomic, strong) NSString *specialReportEnTittle;
@property (nonatomic, assign) BeritaScreenType screenType;
@property (nonatomic) BOOL hasLiveStream;

@end
