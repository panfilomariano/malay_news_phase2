//
//  TMMainPageViewController.m
//  TMNewsAPP
//
//  Created by Jehrome Clemente on 12/9/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMMainPageViewController.h"
#import "TMNewsViewController.h"
#import "TMVideosViewController.h"
#import "TMPhotosViewController.h"
#import "TMBeritaViewController.h"
#import "TMPhotoBrowserViewController.h"
#import "TMCurrentAffairViewController.h"
#import "TMMenuViewController.h"
#import "SwipeView.h"
#import "LibraryAPI.h"
#import "Weather.h"
#import "NSDate+Helpers.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DataManager.h"
#import "Util.h"
#import "TMLandingPageViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMNewsIpadViewController.h"
#import "TMIpadfCurrentAffairsViewController.h"
#import "Constants.h"
#import <MediaPlayer/MPMediaPlayback.h>
#import "TMSettingsiPadViewController.h"
#import "QPSplitViewController.h"
#import "TMWebViewController.h"
#import "TMIPADMenuViewController.h"
#import "FPPopoverController.h"
#import "Constants.h"
#import "Article.h"
#import "AnalyticManager.h"
#import "TMLiveStreamingViewController.h"
#import "TMNavigationController.h"

//#import "AnalyticManager.h"



static LibraryAPI *library;

@interface TMMainPageViewController () <ViewPagerDataSource,ViewPagerDelegate, SwipeViewDataSource, SwipeViewDelegate, TMIPADMenuViewControllerDelegate, UIPopoverPresentationControllerDelegate, UIBarPositioningDelegate>


@property (nonatomic, strong) TMPhotosViewController *photosVC;
@property (nonatomic, strong) TMNewsViewController *newsViewController;
@property (nonatomic, strong) NSString *videosAPI;
@property (nonatomic, strong) NSString *photosAPI;
@property (nonatomic, strong) NSArray *arrayView;
@property (nonatomic, strong) UIBarButtonItem *barbuttonItem;
@property (nonatomic, strong) UIBarButtonItem *barbuttonItem2;
@property (nonatomic, strong) UIBarButtonItem *barbuttonItem3;
@property (nonatomic, strong) UIBarButtonItem *menuBarButtonItem;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) NSMutableArray *weatherArray;
@property (nonatomic, strong) UIPopoverController *popOver;
@property (nonatomic, strong) UIView *dimView;
@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic) bool isTherePopOver;
@property (nonatomic, strong) NSMutableArray *menuListArray;
@property (nonatomic, strong) NSMutableArray *newsList;
@property (nonatomic) BOOL firstLoadDone;
@end


@implementation TMMainPageViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self setNeedsReloadOptions];
    if (!self.firstLoadDone) {
        self.firstLoadDone = TRUE;
        [self selectTabAtIndex:self.index];
    }
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didCloseBanner)
                                                 name:@"didCloseBannerNotif"
                                               object:nil];
    
#ifdef TAMIL
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.titleView = [Util is_iPad] ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Tamil_Header_Logo_85x44"]] : [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Tamil_Header_Logo_85x44"]];
#else
    UIImage *navImage = [UIImage imageNamed:@"Malay_Header_bg"];
    if ([Util is_iPad]) {
        [[UINavigationBar appearance] setBarTintColor:[Util headerColor]];
        navImage = UIInterfaceOrientationIsPortrait(self.interfaceOrientation) ? [UIImage imageNamed:@"navigationBackground_malay-iPad"] : [UIImage imageNamed:@"navigationBackground_malay_LS-iPad"];
    }
    
    [self.navigationController.navigationBar setBackgroundImage:navImage forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.titleView = [Util is_iPad] ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"berita_logo"]] : [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"berita_logo"]];;
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"SIGNAL_APPVERSION" object:nil];
    
    self.menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.menuButton setFrame:CGRectMake(0, 0, 28, 29)];
    [self.menuButton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [self.menuButton setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateSelected];
    [self.menuButton addTarget:self action:@selector(didTapMenu:) forControlEvents:UIControlEventTouchUpInside];
    self.menuBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.menuButton];
    self.navigationItem.rightBarButtonItem = self.menuBarButtonItem;
   
    self.dataSource = self;
    self.delegate = self;
    
    [self.slidingViewController setAnchorRightPeekAmount:0];
    [self.slidingViewController setDefaultTransitionDuration:0];
    
    [self weatherFeed];
    
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(refreshWeather) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:[Constants WEATHER_EXP_LENGHT] target:self selector:@selector(weatherFeed) userInfo:nil repeats:YES];
    
    
    

}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)refreshWeather {
    if (![Util is_iPad]) {
        if (self.navigationItem.leftBarButtonItem == self.barbuttonItem) {
            self.navigationItem.leftBarButtonItem = self.barbuttonItem2;
        }else if (self.navigationItem.leftBarButtonItem == self.barbuttonItem2){
            self.navigationItem.leftBarButtonItem = self.barbuttonItem3;
        }else{
            self.navigationItem.leftBarButtonItem = self.barbuttonItem;
        }
    }else {
        if (self.navigationItem.leftBarButtonItem == self.barbuttonItem) {
            self.navigationItem.leftBarButtonItem = self.barbuttonItem2;
        }else {
            self.navigationItem.leftBarButtonItem = self.barbuttonItem;
        }
    }
   
    

}
#pragma mark ViewPagerDatasource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    //return self.array.count;
    if ([Util is_iPad]) {
        return  5;
    }else {
        return 6;
    }
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
        UILabel *label = [UILabel new];
        label.textAlignment = NSTextAlignmentCenter;
        [label setText:[Constants TAB_TITTLES][index]];
        [label setFont:[UIFont boldSystemFontOfSize:13.0f]];
        [label setTextColor:[UIColor colorWithRed:180.0f/255.0f green:200.0f/255.0f blue:241.0f/255.0f alpha:1]];
        [label sizeToFit];
    
    return label;

}

#pragma mark ViewPagerDelegate
- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    if ([Util is_iPad]) {
        
        TMLandingPageViewController *landingPageVc = [self.storyboard instantiateViewControllerWithIdentifier:@"landingPageStroryboardId"];
        if(index == 0){
            return landingPageVc;   
        }else if(index == 1){
            TMNewsIpadViewController *berita = (TMNewsIpadViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"beritaViewControllerID"];
            return berita;
        }else if(index == 2){
            TMIpadfCurrentAffairsViewController *CA = (TMIpadfCurrentAffairsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ipadCuurentAffairsViewControllerID"];
            return CA;
        }else if(index == 3){
            TMPhotosViewController *photos = (TMPhotosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoViewController"];
            photos.isFromMain = YES;
            return photos;
        }else if(index == 4){
            TMVideosViewController *videos = (TMVideosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videosViewController"];
            videos.isFromMain = YES;
            return videos;
        }
        
        return landingPageVc;
    } else {
        if(index == 0){
            TMNewsViewController *vc = (TMNewsViewController *)  [self.storyboard instantiateViewControllerWithIdentifier:@"newsViewController"];
            return vc;
        }else if(index == 1){//ace gwapo test
            TMLiveStreamingViewController *vc = (TMLiveStreamingViewController *)  [self.storyboard instantiateViewControllerWithIdentifier:@"liveStreamingID"];
            return vc;
        }else if(index == 2){
            TMBeritaViewController *berita = (TMBeritaViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"beritaViewControllerID"];
            return berita;
        }else if(index == 3){
            TMCurrentAffairViewController *currentAffairs = (TMCurrentAffairViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"currentAffairViewController"];
            return currentAffairs;
        }else if(index == 4){
            TMPhotosViewController *others = (TMPhotosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"photoViewController"];
            others.isFromMain = YES;
            return others;
        }else if(index == 5){
            TMVideosViewController *videos = (TMVideosViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"videosViewController"];
            videos.isFromMain = YES;
            return videos;
        }
    }
    
    
    return  nil;
}

- (NSNumber *)tabHeight {
    return [NSNumber numberWithFloat:29.0f];
}
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value{
 
        switch (option) {
            case ViewPagerOptionTabOffset: {
                return [Util is_iPad] ? 50.0f : 20.0f;
            }
            default:
                return value;
        }
   
}
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerTabsView:
            return [UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}
#pragma mark TMIPADMenuViewControllerDelegate
-(void)didTapItemAtMenu{
    [self.dimView removeFromSuperview];
    [self.menuButton setSelected:NO];
    [self.popOver dismissPopoverAnimated: YES];
    self.isTherePopOver = NO;

}

#pragma mark privateFunction
- (void)instantiateUIPopoverController{
    TMIPADMenuViewController *dbvc = (TMIPADMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"menuStoryboardId"];
    dbvc.delegate = self;
    dbvc.ecSlidingVC = self.slidingViewController;
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:dbvc];
    self.popOver.delegate = self;

}

- (void)didTapMenu:(UIButton *)sender {

    if ([Util is_iPad]) {
        [[AnalyticManager sharedInstance] trackMenuTab];
        [[LibraryAPI sharedInstance] getCAWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
            self.menuListArray = array;
            [[LibraryAPI sharedInstance] getNewsWithCompletionBlock:^(NSMutableArray *array) {
                self.newsList = [array mutableCopy];;
             
            if(self.popOver == nil){
                [self instantiateUIPopoverController];
            }
            
            UIButton *button =(UIButton *)sender;
            
            self.dimView = [[UIView alloc]initWithFrame:self.parentViewController.view.bounds];
            [self.dimView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
            [self.parentViewController.view addSubview:self.dimView];
        
            if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
                int ctr = [self.menuListArray count]+[self.newsList count];
                [self.popOver setPopoverContentSize:CGSizeMake(320, 275+(40*ctr))];// for ios 7
                self.popOver.contentViewController.preferredContentSize = CGSizeMake(320, 275+(40*ctr));// for ios 8
                [self.popOver presentPopoverFromRect:CGRectMake(453,7, 320, 275+(40*ctr)) inView:self.view permittedArrowDirections:0 animated:YES];
            }else{
              
                int ctr =  (([self.menuListArray count] + [self.newsList count])+2-1)/2;
                  [self.popOver setPopoverContentSize:CGSizeMake(400, 275+(ctr*40))];// for ios 7
                self.popOver.contentViewController.preferredContentSize = CGSizeMake(400, 275+(ctr*40));
                [self.popOver presentPopoverFromRect:CGRectMake(702,7, 400, 275+(ctr*40)) inView:self.view permittedArrowDirections:0 animated:YES];
            }
            
            [button setSelected:YES];
            self.isTherePopOver = YES;
                 }];
        }];
        
    } else {
        
        TMNavigationController *nav = (TMNavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"menuNavigationStoryboardId"];
        TMMenuViewController *dbvc = (TMMenuViewController *)[nav topViewController];
        dbvc.isSubmenu = NO;

        [self.navigationController presentViewController:nav animated:NO completion:nil];
    }
}
#pragma  mark UIPopOverDelegate

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [self.menuButton setSelected:NO];
    [self.dimView removeFromSuperview];
     self.isTherePopOver = NO;
}

#pragma mark Notif Observers



- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self setNeedsReloadOptions];
    if ([Util is_iPad]) {
        if (self.isTherePopOver) {
            if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
                 int ctr = [self.menuListArray count]+[self.newsList count];
                [self.popOver dismissPopoverAnimated: YES];
                self.popOver = nil;
                [self instantiateUIPopoverController];
                [self.popOver setPopoverContentSize:CGSizeMake(320, 275+(40*ctr))];// for ios 7
                self.popOver.contentViewController.preferredContentSize =CGSizeMake(320, 275+(40*ctr));
                [self.popOver presentPopoverFromRect:CGRectMake(453,7, 320, 275+(40*ctr)) inView:self.view permittedArrowDirections:0 animated:YES];
                [self.dimView setFrame:CGRectMake(0, 0, 768, 1024)];
            }else{
                int ctr = (([self.menuListArray count] + [self.newsList count])+2-1)/2;
                [self.popOver dismissPopoverAnimated: YES];
                self.popOver = nil;
                [self instantiateUIPopoverController];
                [self.popOver setPopoverContentSize:CGSizeMake(400, 275+(ctr*40))];// for ios 7
                self.popOver.contentViewController.preferredContentSize =CGSizeMake(400, 275+(ctr*40));
                [self.popOver presentPopoverFromRect:CGRectMake(702,7, 400, 275+(ctr*40)) inView:self.view permittedArrowDirections:0 animated:YES];
                [self.dimView setFrame:CGRectMake(0, 0, 1024, 768)];
            }
        }
    }
}

- (void)receiveNotification:(NSNotification *) notification
{
    if ( [notification.name isEqualToString:@"SIGNAL_APPVERSION"] ){
        
        NSDictionary * userInfo = notification.userInfo;
        NSArray *userInforList = [userInfo valueForKey:@"DATA_APPVERSION"];
        NSDictionary *dictStatus = (NSDictionary *) [userInforList firstObject];
        NSString *status = [dictStatus valueForKey:@"SIGNALDATA_STATUS"];
        if ([status isEqualToString:@"STATUS_OK"]) {
            NSArray *appVersionList = (NSArray *) [userInforList objectAtIndex:1];
            if (appVersionList.count) {
                // self.appVersionObj = [appversionArray objectAtIndex:0];
                
                //TODO should show dialg
                //click ok open app store to download app
                //NOT allow user conitnue
            }
        } else {
            //error
        }
    }
}

- (void) didCloseBanner
{
    [self setNeedsReloadOptions];
}

#pragma mark private functions

-(void)weatherFeed
{
    [[LibraryAPI sharedInstance]getWeatherWithForce:NO andCompletionBlock:^(NSMutableArray *array) {
        if (array.count) {
            Weather *weather = [array firstObject];
            if (![Util is_iPad]){
                UIView *view = [UIView new];
                
                [view setFrame:CGRectMake(0, 0, 52, 44)];
                [view setBackgroundColor:[UIColor clearColor]];
                
                UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 3, 52, 14)];
                [label setTextColor:[UIColor whiteColor]];
                [label setFont:[UIFont systemFontOfSize:10]];
                label.text = weather.time;
                
                UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithTitle:label.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button setEnabled:NO];
                
                UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 14, 52, 14)];
                [label2 setFont:[UIFont systemFontOfSize:12]];
                label2.text = [Constants WEATHER_3HR_PSI];
                [label2 setTextColor:[UIColor whiteColor]];
                UIBarButtonItem *button2 = [[UIBarButtonItem alloc]initWithTitle:label2.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button2 setEnabled:NO];
                
                UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(12, 26, 52, 14)];
                [label1 setFont:[UIFont systemFontOfSize:12]];
                label1.text = weather.psi_3h;
                [label1 setTextColor:[UIColor whiteColor]];
                UIBarButtonItem *button1 = [[UIBarButtonItem alloc]initWithTitle:label1.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button1 setEnabled:NO];
                
                [view addSubview:label];
                [view addSubview:label2];
                [view addSubview:label1];
                
                self.barbuttonItem2 = [[UIBarButtonItem alloc]initWithCustomView:view];
                
                UIView *view2 = [UIView new];
                
                [view2 setFrame:CGRectMake(0, 0, 58, 44)];
                [view2 setBackgroundColor:[UIColor clearColor]];
                
                UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(10, 3, 58, 14)];
                [label3 setTextColor:[UIColor whiteColor]];
                [label3 setFont:[UIFont systemFontOfSize:10]];
                label3.text = weather.time;
                UIBarButtonItem *button3 = [[UIBarButtonItem alloc]initWithTitle:label3.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button3 setEnabled:NO];
                
                UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(0, 14, 58, 14)];
                [label4 setFont:[UIFont systemFontOfSize:12]];
                label4.text = [Constants WEATHER_24HR_PSI];
                [label4 setTextColor:[UIColor whiteColor]];
                UIBarButtonItem *button4 = [[UIBarButtonItem alloc]initWithTitle:label2.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button4 setEnabled:NO];
                
                UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(10, 26, 58, 14)];
                [label5 setFont:[UIFont systemFontOfSize:12]];
                label5.text = weather.psi_24h;
                [label5 setTextColor:[UIColor whiteColor]];
                UIBarButtonItem *button5 = [[UIBarButtonItem alloc]initWithTitle:label1.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button5 setEnabled:NO];
                
                [view2 addSubview:label3];
                [view2 addSubview:label4];
                [view2 addSubview:label5];
                
                self.barbuttonItem3 = [[UIBarButtonItem alloc]initWithCustomView:view2];
               
                UIView *view1 = [UIView new];
                [view1 setFrame:CGRectMake(0, 0, 60, 44)];
                [view1 setBackgroundColor:[UIColor clearColor]];
                
                UILabel *maxLabel = [[UILabel alloc]initWithFrame:CGRectMake(32, 8, 50, 15)];
                maxLabel.text = [NSString stringWithFormat:@"%@°C",weather.min];
                [maxLabel setFont:[UIFont systemFontOfSize:11]];
                [maxLabel setTextColor:[UIColor whiteColor]];
                UILabel *minLabel = [[UILabel alloc]initWithFrame:CGRectMake(32, 20, 50, 15)];
                minLabel.text = [NSString stringWithFormat:@"%@°C",weather.max];
                
                [minLabel setFont:[UIFont systemFontOfSize:11]];
                [minLabel setTextColor:[UIColor whiteColor]];
                
                UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(-8, 3, 36, 36)];
                [imageView setImage:[UIImage imageNamed:weather.thumbnail]];
                [view1 addSubview:maxLabel];
                [view1 addSubview:minLabel];
                [view1 addSubview:imageView];
                
                self.barbuttonItem = [[UIBarButtonItem alloc]initWithCustomView:view1];
                self.navigationItem.leftBarButtonItem = self.barbuttonItem;
            }else {
                UIView *view = [UIView new];
                [view setFrame:CGRectMake(0, 0, 52, 44)];
                [view setBackgroundColor:[UIColor clearColor]];
                
                UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(-15, 0, 40, 45)];
                [imageView setImage:[UIImage imageNamed:weather.thumbnail]];
                
                UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 15, 90, 15)];
                label.text = [NSString stringWithFormat:@"%@°C - %@°C",weather.min,weather.max];
                [label setTextColor:[UIColor whiteColor]];
                [label setFont:[UIFont systemFontOfSize:15]];
                
                [view addSubview:imageView];
                [view addSubview:label];
                
                self.barbuttonItem = [[UIBarButtonItem alloc]initWithCustomView:view];
                self.navigationItem.leftBarButtonItem = self.barbuttonItem;
                
                UIView *view2 = [UIView new];
                [view2 setFrame:CGRectMake(0, 0, 52, 44)];
                [view2 setBackgroundColor:[UIColor clearColor]];
                
                UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(-8, 7, 52, 14)];
                timeLabel.text = weather.time;
                [timeLabel setTextColor:[UIColor whiteColor]];
                [timeLabel setFont:[UIFont systemFontOfSize:13]];
                
                UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithTitle:timeLabel.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button setEnabled:NO];
                
                UILabel *psiLabel = [[UILabel alloc] initWithFrame:CGRectMake(-8, 20, 300, 15)];
                psiLabel.text = [NSString stringWithFormat:@"24-hour PSI:%@°C, 3-hour PSI:%@°C",weather.min, weather.max];
                [psiLabel setTextColor:[UIColor whiteColor]];
                [psiLabel setFont:[UIFont systemFontOfSize:13]];
                UIBarButtonItem *button1 = [[UIBarButtonItem alloc]initWithTitle:psiLabel.text style:UIBarButtonItemStylePlain target:self action:nil];
                [button1 setEnabled:NO];
                
                [view2 addSubview:timeLabel];
                [view2 addSubview:psiLabel];
                
                self.barbuttonItem2 = [[UIBarButtonItem alloc] initWithCustomView:view2];
    
            }
            
        }
    }];

}


@end
