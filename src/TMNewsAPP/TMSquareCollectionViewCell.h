//
//  TMSquareCollectionViewCell.h
//  TMNewsAPP
//
//  Created by Sil Piplay on 1/29/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
@protocol TMSquareCollectionViewCellDelagate <NSObject>
- (void) didTapBookmarkButtonAtIndexPath: (NSIndexPath *)indexPath;
@end

@interface TMSquareCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *tittle;
@property (nonatomic, weak) IBOutlet UILabel *category;
@property (nonatomic, weak) IBOutlet UILabel *pubDate;
@property (nonatomic, weak) IBOutlet UIImageView *playImage;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *pubdateSpace;
@property (nonatomic, weak) id <TMSquareCollectionViewCellDelagate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@end
