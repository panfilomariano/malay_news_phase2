//
//  TMIPADMenuViewController.h
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 2/24/15.
//  Copyright (c) 2015 pace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+ECSlidingViewController.h"
#import "TMSlidingViewController.h"
@protocol TMIPADMenuViewControllerDelegate <NSObject>

- (void) didTapItemAtMenu;
@end;
@interface TMIPADMenuViewController : UIViewController
@property (nonatomic, weak) id <TMIPADMenuViewControllerDelegate> delegate;
@property (nonatomic, strong) TMSlidingViewController *ecSlidingVC;
@end
