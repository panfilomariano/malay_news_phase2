//
//  TMNewsDetailsViewController.m
//  TMNewsAPP
//
//  Created by Ace Jerald Agtang on 12/15/14.
//  Copyright (c) 2014 pace. All rights reserved.
//

#import "TMNewsDetailsViewController.h"
#import "TMNewsPagesViewController.h"
#import "SwipeView.h"
#import "Data.h"
#import "LibraryAPI.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TMMenuViewController.h"
#import "Util.h"
#import "UIView+Toast.h"
#import "Constants.h"
#import "Media.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "TMNewsPagesTitleTableViewCell.h"
#import "TMNavigationController.h"
#import "AnalyticManager.h"

@interface TMNewsDetailsViewController ()<ViewPagerDataSource,ViewPagerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) UIView *fontResizeView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) NSMutableArray *itemsArray;
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) UIBarButtonItem *bookmarkBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *fontBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *shareBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *separatorBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *menuBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *spaceSeparatorItem;
@property (nonatomic, strong) TMNewsPagesViewController *currentPageViewController;
@property (nonatomic, strong) NSMutableArray *bookmarks;
@property (nonatomic, strong) NSMutableDictionary *pagerViewControllers;
@property (nonatomic, strong) UIButton *invisibleButton;
@property (nonatomic, strong) UIButton *bookmarkButton;

@end

@implementation TMNewsDetailsViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [self.bookmarkButton setSelected:[self checkBookmarkForIndex:self.activeTabIndex]];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.itemsArray = self.passedArray;
    self.dataSource = self;
    self.delegate = self;
    self.activeTabIndex = self.index;
    self.fontResizeView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, -34.0f, self.view.bounds.size.width, 34.0f)];
    self.invisibleButton = [[UIButton alloc]initWithFrame:self.view.bounds];
    self.slider = [[UISlider alloc] initWithFrame:self.fontResizeView.bounds];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadViews)
                                                 name:@"didBookmarkChanged"
                                               object:nil];
    
    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"fontbar_line"];
    UIImage *sliderRightTrackImage = [UIImage imageNamed: @"fontbar_line"];
    sliderLeftTrackImage = [sliderLeftTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    sliderRightTrackImage = [sliderRightTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    
    [self.slider setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:sliderRightTrackImage forState:UIControlStateNormal];
    [self.slider setThumbImage:[UIImage imageNamed: @"thumbButton"] forState:UIControlStateNormal];
    [self.slider addTarget:self action:@selector(sliderAction) forControlEvents:UIControlEventTouchUpInside];
    self.pagerViewControllers = [[NSMutableDictionary alloc]init];

   [self.invisibleButton addTarget:self action:@selector(didTapOutside) forControlEvents:UIControlEventTouchUpInside];
    self.bookmarkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bookmarkButton setFrame:CGRectMake(0, 0, 28, 29)];
    [self.bookmarkButton setImage:[UIImage imageNamed:@"bigStar"] forState:UIControlStateNormal];
    [self.bookmarkButton setImage:[UIImage imageNamed:@"bigStar_selected"] forState:UIControlStateSelected];
    [self.bookmarkButton addTarget:self action:@selector(didTapBookmark) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *separatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 29)];
    [separatorImage setImage:[UIImage imageNamed:@"separator_icon"]];
 
    UIImageView *transparentSeparatorImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 1, 28)];
    [transparentSeparatorImage setImage:[UIImage new]];
    
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(0, 0, 28, 29)];
    [menuButton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(didTapMenu) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *fontSizeSelectorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fontSizeSelectorButton setFrame:CGRectMake(0, 0, 28, 29)];
    [fontSizeSelectorButton setImage:[UIImage imageNamed:@"fontButton"] forState:UIControlStateNormal];
    [fontSizeSelectorButton setImage:[UIImage imageNamed:@"fontButton_selected"] forState:UIControlStateSelected];
    [fontSizeSelectorButton addTarget:self action:@selector(displayfontResizeView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setFrame:CGRectMake(0, 0, 28, 29)];
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"shareButton"] forState:UIControlStateSelected];
    [shareButton addTarget:self action:@selector(didTapShare) forControlEvents:UIControlEventTouchUpInside];
  
    _fontBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:fontSizeSelectorButton] ;
    _bookmarkBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.bookmarkButton];
    _shareBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    _menuBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:menuButton];
    _separatorBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:separatorImage];
    _spaceSeparatorItem =[[UIBarButtonItem alloc] initWithCustomView:transparentSeparatorImage];
    self.navigationItem.rightBarButtonItems = @[_menuBarButtonItem,_separatorBarButtonItem,_shareBarButtonItem,_spaceSeparatorItem,_bookmarkBarButtonItem,_spaceSeparatorItem,_fontBarButtonItem];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    //return self.array.count;
    if(self.isTopNews){
        return 1;
    }else{
        return [self.itemsArray count];
    }
    
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    return  label;
    
}
#pragma mark ViewPagerDelegate
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerTabsView:
            return [UIColor redColor];
        default:
            return color;
    }
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index{
    
    TMNewsPagesViewController *pages = (TMNewsPagesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"newsPagesID"];
    pages.article = [self.itemsArray objectAtIndex:index];
    pages.pageNumber = index+1;
    pages.totalPages = [self.itemsArray count];
    pages.titleForSpecialReport = self.categoryForSR;

    switch (self.screenType) {
        case LatestNewsScreen:
            pages.screenType = LatestNewsScreen;
            break;
        case CAScreen:
            pages.screenType = CAPagesScreenType;
            break;
        case SpecialReportScreen:
            pages.screenType = SpecialReportPagesScreenType;
            pages.specialReportEnTittle = self.specialReportEnTittle;
            break;
        case NewsScreen:
            break;

    }
    
    NSString *key = [NSString stringWithFormat:@"%lu", (unsigned long)index];
    [self.pagerViewControllers setObject:pages forKey:key];

    
    return pages; 
    
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    [[LibraryAPI sharedInstance] getBookmarksWithCompletionBlock:^(NSMutableArray *array) {
        if (array) {
            _bookmarks = [array mutableCopy];
        }
    }];

    Article *article = (Article *) [self.itemsArray objectAtIndex:index];
    self.isBookmark = [[LibraryAPI sharedInstance]isBookmarkedArticle:article];
    
    UIButton *button = (UIButton *) _bookmarkBarButtonItem.customView;
    [button setSelected:[[LibraryAPI sharedInstance]isBookmarkedArticle:article]];

    
}
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    switch (option) {
        case ViewPagerOptionTabHeight:
            return 0.0;
        case ViewPagerOptionTabOffset:
            return 0.0;
        case ViewPagerOptionTabLocation:
            return 0.0;
        default:
            
            return value;
    }
}

#pragma mark private Functions
- (BOOL)checkBookmarkForIndex: (NSUInteger)index
{
    if (index < self.itemsArray.count) {
        Article *article = (Article *) [self.itemsArray objectAtIndex:index];
        return [[LibraryAPI sharedInstance] isBookmarkedArticle:article];
    }
    return NO;
}
-(void)didTapOutside{
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.fontResizeView.frame =  CGRectMake(0.0f, -34.0f, self.view.bounds.size.width, 34.0f);
        UIButton *button = (UIButton *) self.fontBarButtonItem.customView;
        [button setSelected:NO];
        [self.fontResizeView removeFromSuperview];
        [self.invisibleButton removeFromSuperview];
    } completion:^(BOOL finished) {
    }];
}
-(void)sliderAction
{
    
    switch (lroundf(self.slider.value)) {
        case 0: {
            self.slider.value = 0;
            [Data sharedInstance].fontSizeType = FontSizeTypeXSmall;
            break;
        } case 1: {
             self.slider.value = 1;
            [Data sharedInstance].fontSizeType = FontSizeTypeSmall;
        
            break;
        } case 2: {
             self.slider.value = 2;
            [Data sharedInstance].fontSizeType = FontSizeTypeDefault;
        
            break;
        } case 3: {
             self.slider.value = 3;
            [Data sharedInstance].fontSizeType = FontSizeTypeLarge;
         ;
            break;
        } case 4: {
             self.slider.value = 4;
            [Data sharedInstance].fontSizeType = FontSizeTypeXLarge;
          
            break;
        }
           
    }

    [self reloadData];
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.fontResizeView.frame =  CGRectMake(0.0f, -34.0f, self.view.bounds.size.width, 34.0f);
        UIButton *button = (UIButton *) self.fontBarButtonItem.customView;
        [button setSelected:NO];
        [self.fontResizeView removeFromSuperview];
        [self.invisibleButton removeFromSuperview];
    } completion:^(BOOL finished) {
    }];
    
}
-(void) displayfontResizeView:(UIButton *)sender {
    UIButton *button = (UIButton *) sender;
    if([button isSelected]){
        [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.fontResizeView.frame =  CGRectMake(0.0f, -34.0f, self.view.bounds.size.width, 34.0f);
            [button setSelected:NO];
            [self.fontResizeView removeFromSuperview];
            [self.invisibleButton removeFromSuperview];
        } completion:^(BOOL finished) {
        }];
    }else{
        [self.fontResizeView setBackgroundColor:[UIColor colorWithRed:0/255.0f green:123/255.0f blue:208/255.0f alpha:1]];
        [self.slider setBackgroundColor:[UIColor clearColor]];
        UILabel *small = [UILabel new];
        [small setText:@"A"];
        [small setFont:[UIFont systemFontOfSize:16]];
        [small setFrame:CGRectMake(16, 0, 25, 30)];
        [small setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
        
        UILabel *big = [UILabel new];
        [big setFrame:CGRectMake(self.view.bounds.size.width-32, 0, 25, 30)];
        [big setText:@"A"];
        [big setFont:[UIFont systemFontOfSize:20]];
        [big setTextColor:[UIColor colorWithRed:205.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
        
        [self.slider setFrame:CGRectMake(40, 0, self.view.bounds.size.width-80, 30)];
        self.slider.minimumValue = 0.0;
        self.slider.maximumValue = 4.0;
        self.slider.continuous = YES;
        self.slider.value = [Data sharedInstance].fontSizeType;
        
        switch ([Data sharedInstance].fontSizeType) {
            case FontSizeTypeDefault:
                self.slider.value = 2;
                break;
            case FontSizeTypeSmall:
                self.slider.value = 1;
                break ;
            case FontSizeTypeLarge:
                self.slider.value = 3;
                break ;
            case FontSizeTypeXLarge:
                self.slider.value = 4;
                break ;
            case FontSizeTypeXSmall:
                self.slider.value = 0;
                break ;
        }
        
        [self.fontResizeView addSubview:self.slider];
        [self.fontResizeView addSubview:small];
        [self.fontResizeView addSubview:big];
        [self.view addSubview:self.fontResizeView];
        [self.view addSubview:self.invisibleButton];
        [self.view bringSubviewToFront:self.fontResizeView];
        
        [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.fontResizeView.frame =  CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 34.0f);
            [button setSelected:YES];
        } completion:^(BOOL finished) {
        }];
        
    }
    
}

-(void)didTapBookmark {
    Article *article = (Article *) [self.passedArray objectAtIndex:self.activeTabIndex];
   
    if ([[LibraryAPI sharedInstance]isBookmarkedArticle:article]) {
        [[LibraryAPI sharedInstance] deleteBookmarkWithArticle:article];
        [self.view makeToast:[Constants REMOVE_FROM_FAV]];
    } else {
        [[LibraryAPI sharedInstance] addBookmarkWithArticle:article];
        [self.view makeToast:[Constants ADD_TO_FAV]];
    }
    
    UIButton *button = (UIButton *) _bookmarkBarButtonItem.customView;
    [button setSelected:[[LibraryAPI sharedInstance]isBookmarkedArticle:article]];
   
    
    if (self.didTapBookmarkButtonDelegate && [self.didTapBookmarkButtonDelegate respondsToSelector: @selector(didTapBookmarkButton)]) {
        [self.didTapBookmarkButtonDelegate performSelector:@selector(didTapBookmarkButton)];
    }
    
    if ([[LibraryAPI sharedInstance]isBookmarkedArticle:article]) {
        [[AnalyticManager sharedInstance] trackBookmarkWithGuid:article.guid andArticleTitle:article.tittle];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"willRefreshSwipeView"
                                                        object:nil
                                                      userInfo:nil];
    
    [self.bookmarkButton setSelected:[self checkBookmarkForIndex:self.activeTabIndex]];
 
}

-(void)didTapShare {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Facebook",
                                  @"Twitter",
                                  @"Email",
                                  nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];

}
- (void)didTapMenu {
    if (![Util is_iPad]) {
        TMNavigationController *nav = (TMNavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"menuNavigationStoryboardId"];
        [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
        [nav.navigationBar setBarTintColor:[Util headerColor]];
        [self.navigationController presentViewController:nav animated:NO completion:nil];
    }
}

#pragma mark ActonSheet Delegate
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self fbShare];
                    break;
                case 1:
                    [self TwitterShare];
                    break;
                case 2:
                    [self emailShare];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

- (void)fbShare{
    Article *article = (Article *) [self.itemsArray objectAtIndex:self.activeTabIndex];
    NSDictionary *dictionary = [article.mediagroupList firstObject];
    NSString *title = [NSString stringWithFormat:@"%@ %@\n", [Constants SHARE_PRE_TITTLE],article.tittle];
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:article.link]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultDone){
            [[AnalyticManager sharedInstance] trackFacebookShareNewsWithCategory:article.enCategory andArticleTitle:article.tittle andGuid:article.guid];
        }
    };
    
    [self presentViewController:controller animated:YES completion:Nil];
    
    
}
- (void)emailShare {
    
    
    if (self.itemsArray.count) {
        if ([MFMailComposeViewController canSendMail]) {
            [UINavigationBar appearance].barTintColor = [Util headerColor];
            
            Article *media = [self.itemsArray objectAtIndex:self.activeTabIndex];
            NSString *title = [NSString stringWithFormat:@"%@ %@",[Constants SHARE_PRE_TITTLE], [Constants SHARE_TITTLE]];
            NSString *messageBody = [NSString stringWithFormat:@"%@\n%@", media.tittle ,media.link];
            NSArray *toRecipents = [NSArray arrayWithObject:@""];
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:title];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            [mc.navigationBar setTranslucent:NO];
            [mc.navigationBar setTintColor:[UIColor whiteColor]];
            
            NSString *thumbnail = @"";
            if (self.itemsArray.count){
                NSArray *mediaGroup = [media.mediagroupList copy];
                if (mediaGroup.count){
                    NSDictionary *dictionary = [mediaGroup firstObject];
                    thumbnail = [dictionary valueForKey:@"thumbnail"];
                }
            }
            
            NSURL *url = [NSURL URLWithString:thumbnail];
            NSData *data = [NSData dataWithContentsOfURL:url];
            [mc addAttachmentData:data mimeType:@"image/jpeg" fileName:@"photo"];
            
            [self presentViewController:mc animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            }];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information"
                                                        message:@"Nothing to share."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }

}

- (void)TwitterShare {
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    Article *media = [self.itemsArray objectAtIndex:self.activeTabIndex];
    NSString *content = @"";
    if(self.itemsArray.count){
        
        NSArray *mediaGroup = [media.mediagroupList copy];
        if(mediaGroup.count) {
            NSDictionary *dictionary = [mediaGroup firstObject];
            content = [dictionary valueForKey:@"content"];
            
        }
    }
    NSString *title = [NSString stringWithFormat:@"%@ %@",[Constants SHARE_PRE_TITTLE],media.tittle];
    
    [controller setInitialText:title];
    [controller addURL:[NSURL URLWithString:media.link]];
    controller.completionHandler = ^(SLComposeViewControllerResult result){
    if (result == SLComposeViewControllerResultDone){
        [[AnalyticManager sharedInstance] trackTwitterShareNewsWithCategoryName:media.enCategory andGuId:media.guid andArticleTitle:media.tittle];
    }
    };
    
    [self presentViewController:controller animated:YES completion:nil];
}




#pragma mark MFMailComposeViewControllerDelegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    Article *media = [self.itemsArray objectAtIndex:self.activeTabIndex];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    if(result == MFMailComposeResultSent){
       
        [[AnalyticManager sharedInstance] trackEmailShareNewsWithCategoryName:media.enCategory andGuid:media.guid andArticleTitle:media.tittle];
    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self dismissViewControllerAnimated:YES completion:NULL];
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)reloadViews{
    [self.bookmarkButton setSelected:[self checkBookmarkForIndex:self.activeTabIndex]];
}
@end
